import re

from cl.bci.falcon.constantreader.AbstractConstantProcessor import AbstractConstantProcessor
from cl.bci.falcon.utils.fileutils import get_file_content


class DependenciesConstantsProcessor(AbstractConstantProcessor):
    __resource_pattern = r'object\s((?:[A-Z][a-z0-9]+)+).*?{(.*?)}$'

    def process(self, path):
        content = get_file_content(path)
        constant = {}
        iter_matches = re.findall(self.__resource_pattern, content, re.DOTALL | re.MULTILINE)
        for match in iter_matches:
            clazz = match[0]
            body = match[1]
            findall = re.findall(r'(\w+)\s*=(.*?)$', body, re.DOTALL | re.MULTILINE)
            for find in findall:
                key = '%s.%s' % (clazz, find[0])
                value = str(find[1]).replace('"', '').replace(' ', '')
                constant[key] = value
        for key, value in constant.items():
            version = r'\$\{(.*?)\}'
            match = re.findall(version, value, re.DOTALL)
            if match:
                version_app_value = match[0]
                constant[key] = str(value).replace('${%s}' % version_app_value, constant[version_app_value])
        return constant


if __name__ == '__main__':
    file_path = '/Users/nisum/Work/Bci/CoE/FixError/app-personas-android-fixerror/' \
                'buildSrc/src/main/kotlin/dependencies/dependencies.gradle.kt'
    DependenciesConstantsProcessor().process(file_path)
