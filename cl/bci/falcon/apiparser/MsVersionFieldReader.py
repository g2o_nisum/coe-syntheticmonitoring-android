import os
import re

from cl.bci.falcon.utils.StaticImportResolver import StaticImportResolver
from cl.bci.falcon.utils.fileutils import get_file_content


class MsVersionFieldReader:
    __exclude_directories = ['build', 'demo', 'test', 'di']
    __included_file_pattern = r'datasource\w*\.(java|kt)$'
    __named_field_pattern = r'@Inject[\s|\n]*@(?:field:){0,1}Named\(((?:\w+|_|\.)*)\)\n\s+(?:(.*?)(' \
                            r'\w+):\sString\n|.*?String\s(\w+);)'

    def __init__(self, workspace, constants):
        self.__workspace = workspace
        self.dagger_constant = constants

    def read(self):
        api_ms_version = self.visit_files_directory(self.process_file)
        return api_ms_version

    def process_file(self, root, file):
        file_path = '{}/{}'.format(root, file)
        content = get_file_content(file_path)

        import_resolver = StaticImportResolver(content)

        file_dic_list = []
        matches = re.findall(self.__named_field_pattern, content, re.DOTALL)

        for match in matches:
            constant = import_resolver.resolve(match[0])
            if constant in self.dagger_constant:
                constant = self.dagger_constant[constant]
            else:
                print(f'Constante "{constant}" no resuelta correctamente en {file_path}')

            field = match[2] if match[2] else match[3]
            field_name_getter = self.find_getter_method(content, field)

            ms_field_dict = {
                'filename': file, 'constant': constant, 'field': field,
                'field_name_getter': field_name_getter
            }
            file_dic_list.append(ms_field_dict)
        return file_dic_list

    def find_getter_method(self, content, field):
        search = re.search(self.build_getter_pattern(field), content, re.DOTALL)
        if search:
            return search.group()
        return ''

    def build_getter_pattern(self, field):
        return r'(get\w+)(?=\(\)\s*\{\s+return\s' + field + r'(?:.*?)\})'

    def visit_files_directory(self, func):
        api_files = []
        for root, dirs, files in os.walk(self.__workspace):
            dirs[:] = [d for d in dirs if d not in self.__exclude_directories]
            for file in files:
                if re.search(self.__included_file_pattern, file.lower()):
                    result = func(root, file)
                    if result:
                        api_files.extend(result)
        return api_files
