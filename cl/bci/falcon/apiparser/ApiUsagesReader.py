import os
import re

from cl.bci.falcon.apiparser.RestResourceParser import ApiDTO, RestResourceDTO
from cl.bci.falcon.utils.fileutils import get_file_content


class ApiUsagesReader:
    __exclude_directories = ['build', 'demo', 'test', 'di']
    __included_file_pattern = r'datasource\w*\.(java|kt)$'
    __named_field_pattern = r'@Inject[\s|\n]*@(?:field:){0,1}Named\(((?:\w+|_|\.)*)\)\n\s+(?:(.*?)(' \
                            r'\w+):\sString\n|.*?String\s(\w+);)'

    def __init__(self, workspace, rest_resource, ms_version_field):
        self.__workspace = workspace
        self.api_dto_list: list[ApiDTO] = rest_resource
        self.ms_version_field = ms_version_field

    def read(self):
        self.visit_files_directory(self.process_file)
        return self.api_dto_list

    def process_file(self, root, file):
        file_path = '%s/%s' % (root, file)
        content = get_file_content(file_path)

        if not self.ms_version_field:
            pass
        elif len(self.ms_version_field) == 1:
            self.get_api_dto_list_for_single_ms_version_field_provider()
        else:
            self.get_api_dto_list_for_multiple_ms_version_field_provider(content, file)

    def get_api_dto_list_for_multiple_ms_version_field_provider(self, content, file):
        api_name_list = [item.api for item in self.api_dto_list]
        api_pattern = '|'.join(api_name_list)
        pattern = r'(%s)' % api_pattern
        matches = re.compile(pattern).findall(content)
        if not matches:
            pass

        for api_name in set(matches):
            api_index = next(
                index
                for index, item in enumerate(self.api_dto_list)
                if item.api == api_name)
            resource_list: list[RestResourceDTO] = self.api_dto_list[api_index].resource_list

            if not resource_list:
                continue

            resource_method_list = ['\.(%s)\(\s*(\w+[\.\w+])' % item.method_name for item in resource_list]
            method_name_pattern = r'(%s)' % '|'.join(resource_method_list)
            method_usages = re.compile(method_name_pattern).findall(content)

            if method_usages:
                usages = method_usages[0]
                api_method_name = usages[1]
                ms_field_caller = usages[2]
                local_constant = self.find_version_from_same_file(file, ms_field_caller)
                if local_constant:
                    self.api_dto_list[api_index].set_version_to_resource(api_method_name, local_constant)
                else:
                    pass

    def find_version_from_same_file(self, file, ms_field_caller):
        for provider in self.ms_version_field:
            if file == provider['filename'] and (
                    ms_field_caller == provider['field_name_getter'] or ms_field_caller == provider['field']):
                return provider['constant']

        # file_dic_list = []
        # matches = re.findall(self.__named_field_pattern, content, re.DOTALL)

        # for matches in matches:
        #     constant = import_resolver.resolve(matches[0])
        #     if constant in self.dagger_constant:
        #         constant = self.dagger_constant[constant]
        #     else:
        #         print('Constante "{}" no resuelta correctamente en {}'.format(constant, file_path))
        #     field = matches[2] if matches[2] else matches[3]
        #     method_which_use_field = self.get_method_which_use_field(content, field)
        #
        #     if method_which_use_field:
        #         for method_dic in method_which_use_field:
        #             ms_field_dict = {
        #                 'filename': file, 'constant': constant, 'field': field,
        #                 'method_name': method_dic['method_name']
        #             }
        #             file_dic_list.append(ms_field_dict)
        #     else:
        #         field_name_getter = self.find_getter_method(content, field)
        #         ms_field_dict = {
        #             'filename': file, 'constant': constant, 'field': field,
        #             'field_name_getter': field_name_getter
        #         }
        #         file_dic_list.append(ms_field_dict)
        #
        # return file_dic_list

    def get_api_dto_list_for_single_ms_version_field_provider(self):
        constant = self.ms_version_field[0]['constant']
        for idx, api_dto in enumerate(self.api_dto_list):
            self.api_dto_list[idx].set_version_to_all_resource(constant)
        return self.api_dto_list

    def find_getter_method(self, content, field):
        search = re.search(self.build_getter_pattern(field), content, re.DOTALL)
        if search:
            return search.group()
        return ''

    def get_method_which_use_field(self, content, field):
        method_list = []
        pattern = self.build_field_use_pattern(field)
        matches = re.findall(pattern, content, re.DOTALL | re.IGNORECASE) or ''
        for match in matches:
            api_field_name = match[0]
            api_method_name = match[1]
            result = {'method_name': api_method_name, 'api': api_field_name}
            method_list.append(result)
        return method_list

    def build_getter_pattern(self, field):
        return r'(get\w+)(?=\(\)\s*\{\s+return\s' + field + r'(?:.*?)\})'

    def build_field_use_pattern(self, field):
        return r'(\w+)\.(\w+)[\s|\n]*\([\s|\n]*[\w+\.]*[w+]*' + field + r'(?:\s*,|\(\))'

    def visit_files_directory(self, func):
        api_files = []
        for root, dirs, files in os.walk(self.__workspace):
            dirs[:] = [d for d in dirs if d not in self.__exclude_directories]
            for file in files:
                if re.search(self.__included_file_pattern, file.lower()):
                    result = func(root, file)
                    if result:
                        api_files.extend(result)
        return api_files
