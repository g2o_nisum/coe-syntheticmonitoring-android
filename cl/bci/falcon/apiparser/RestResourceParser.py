import dataclasses as dc
import os
import re

from cl.bci.falcon.utils.StaticImportResolver import StaticImportResolver
from cl.bci.falcon.utils.fileutils import get_file_content


@dc.dataclass(unsafe_hash=True)
class RestResourceDTO:
    tipo: str
    url: str
    line: str = ''
    method_name: str = ''


@dc.dataclass(unsafe_hash=True)
class ApiDTO:
    api: int
    file_path: float
    resource_list = []

    def set_version_to_resource(self, method_name, version):
        if not self.resource_list:
            pass
        for i, rest_resource in enumerate(self.resource_list):
            if rest_resource.method_name == method_name:
                self.replace_version(i, rest_resource, version)

    def set_version_to_all_resource(self, version):
        if not self.resource_list:
            pass
        for i, rest_resource in enumerate(self.resource_list):
            self.replace_version(i, rest_resource, version)

    def replace_version(self, index, rest_resource, version):
        url = rest_resource.url.replace('{version}', version)
        self.resource_list[index].url = url


class RestResourceParser:
    __exclude_directories = ['build', 'demo', 'test', 'di']
    __api_file_pattern = r'api\.(java|kt)$'
    __resource_ms_version_pattern = r"@(?:GET|POST|PUT|DELETE|PATCH)\((?:.*?)@Path(?:.*?)(?:(?:String " \
                                    r"msVersion|msVersion: String))"

    __resource_pattern = r"@(?:GET|POST|PUT|DELETE|PATCH)\(.*?\)"
    __method_name_pattern = r'(\w+)(?:\((?:\n|\s)*@Path)'

    def __init__(self, workspace, application_constants):
        self.__workspace = workspace
        self.application_constants = application_constants

    def read(self):
        files = self.find_api_files(self.process_file)
        return files

    def process_file(self, root, file):
        file_path = '{}/{}'.format(root, file)
        content = get_file_content(file_path)

        resource_ms_version_list = re.findall(self.__resource_ms_version_pattern, content, re.DOTALL)
        resource_all_list = re.findall(self.__resource_pattern, content, re.DOTALL)

        mapper = StringToEndpointMapper(content, self.application_constants)
        rest_resource_dict_list = []

        if not resource_all_list:
            return None

        api_dto = ApiDTO(api=file.split('.')[0], file_path=file_path)
        for resource in resource_all_list:
            method_name = self.find_method_name(resource, resource_ms_version_list)
            if self.resource_is_mock(resource, method_name):
                pass

            rest_dto = mapper.map(resource)
            rest_dto.line = resource
            rest_dto.method_name = method_name

            api_dto.resource_list.append(rest_dto)

        return api_dto

    def find_method_name(self, resource, resource_ms_version_list):
        require_ms_version = next((elem for elem in resource_ms_version_list if resource in elem), '')
        if require_ms_version:
            method_name = re.search(self.__method_name_pattern, require_ms_version, re.DOTALL)
            if method_name:
                return method_name.groups()[0]
        return ''

    def find_api_files(self, func):
        api_files = []
        for root, dirs, files in os.walk(self.__workspace):
            dirs[:] = [d for d in dirs if d not in self.__exclude_directories]
            for file in files:
                if re.search(self.__api_file_pattern, file.lower()):
                    result = func(root, file)
                    if result:
                        api_files.append(result)
        return api_files

    def resource_is_mock(self, line, method_name):
        return "mock" in line.lower() or "mock" in method_name.lower()


class StringToEndpointMapper:

    def __init__(self, content, application_constants):
        self.import_resolver = StaticImportResolver(content)
        self.application_constants = application_constants

    def map(self, line):
        match_endpoint_line = re.search(r'\((.*?)\)', line, re.DOTALL)
        tipo = re.search(r'@(\w+)\(', line).groups()[0]
        endpoint_line = match_endpoint_line.groups()[0]
        url = ''
        match_endpoint_constants = re.split(r'[\s\+]+', endpoint_line)
        # match_endpoint_constants = re.findall(r'[A-Z]+(?:_[A-Z]+)*', endpoint_line)
        for endpoint_constant in match_endpoint_constants:
            full_constant_path = self.import_resolver.resolve(endpoint_constant)
            if full_constant_path in self.application_constants:
                constant_value = self.application_constants[full_constant_path]
            else:
                literal = re.match(r'"(.*?)"', full_constant_path, re.DOTALL)
                if literal:
                    constant_value = literal.groups()[0]
                else:
                    constant_value = '{%s}' % full_constant_path
            url = url + constant_value

        return RestResourceDTO(tipo=tipo, url=url)
