from cl.bci.falcon.InfoData import InfoData
from cl.bci.falcon.RepoStruct import RepoStruct


class TagInfo(InfoData):
    repository = {"release": True}

    repository_info = {}

    def __init__(self, folder_route, tag_version):
        super().__init__(self.repository, self.repository_info, folder_route)
        self.folder_route = folder_route
        self.tag_version = tag_version

        self.repository_info = {
            "release": RepoStruct("app-personas-android", folder_name="tag", branch=tag_version)
        }
