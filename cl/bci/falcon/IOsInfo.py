from cl import InfoData
from cl import RepoStruct


class IOsInfo(InfoData):
    repository = {
        "AlcanciaDigital": True,
        "Autenticacion": True,
        "Commons": True,
        "Consumo": True,
        "Cuentas": True,
        "Descuentos": True,
        "Entrust": True,
        "Fidelizacion": True,
        "Inversiones": True,
        "Insights": True,
        "OnOff": True,
        "Pagos": True,
        "PFM": True,
        "Recargas": True,
        "TarjetaCreditoAvisoViaje": True,
        "Tarjetas": True,
        "TarjetaVirtual": True,
        "Transferencias": True,
        "Transversal": True,
        "UIElements": True,
        "VirtualAssistant": True,
        "Testing": True
    }

    repository_info = {
    
        "AlcanciaDigital": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-alcanciadigital.git", "app-ios-alcanciadigital", "develop"),
        "Autenticacion": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-autenticacion.git", "app-ios-autenticacion", "develop"),
        "Commons": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-commons.git", "app-ios-commons", "develop"),
        "Consumo": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-consumo.git", "app-ios-consumo", "develop"),
        "Cuentas": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-cuentas.git", "app-ios-cuentas", "develop"),
        "Descuentos": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-descuentos.git", "app-ios-descuentos", "develop"),
        "Entrust": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-entrust.git", "app-ios-entrust", "develop"),
        "Fidelizacion": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-fidelizacion.git", "app-ios-fidelizacion", "develop"),
        "Inversiones": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-inversiones.git", "app-ios-inversiones", "develop"),
        "Insights": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-insights.git", "app-ios-insights", "develop"),
        "OnOff": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-onoff.git", "app-ios-onoff", "develop"),
        "Pagos": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-pagos.git", "app-ios-pagos", "develop"),
        "PFM": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-pfm.git", "app-ios-pfm", "develop"),
        "Recargas": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-recargas.git", "app-ios-recargas", "develop"),
        "TarjetaCreditoAvisoViaje": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-tarjeta-credito-aviso-viaje.git", "app-ios-tarjeta-credito-aviso-viaje", "develop"),
        "Tarjetas": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-tarjetas.git", "app-ios-tarjetas", "develop"),
        "TarjetaVirtual": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-tarjetavirtual.git", "app-ios-tarjetavirtual", "develop"),
        "Transferencias": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-transferencias.git", "app-ios-transferencias", "develop"),
        "Transversal": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-transversal.git", "app-ios-transversal", "develop"),
        "UIElements": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-ui-elements.git", "app-ios-ui-elements", "develop"),
        "VirtualAssistant": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-virtual-assistant.git", "app-ios-virtual-assistant", "develop"),
        "Testing": RepoStruct("git@bitbucket.org:bancocreditoeinversiones/app-ios-abtest.git", "app-ios-abtest", "develop"),

    }

    def __init__(self, folder_route):
        super().__init__(self.repository, self.repository_info, folder_route)
        self.folder_route = folder_route

