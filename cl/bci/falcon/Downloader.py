import os

import git
from git import Repo


class Downloader:

    def __init__(self, info_data):
        self.info_data = info_data

    def download_folders(self):
        repo_info = self.info_data.repository_info
        repo_route = self.info_data.folder_route
        for item in self.info_data.repositories.items():
            (repo, should_download) = item
            if should_download:
                self.download_repo(repo, repo_info[repo], repo_route)

    def download_repo(self, repo, repo_info, repo_route):
        repo_path = "{}/{}".format(repo_route, repo_info.folder_name)
        if not os.path.exists(repo_path):
            print("Descargando {}".format(repo))
            name_file_array = repo_info.git_route.split("/")
            origin_route = "{}/{}".format(repo_route, name_file_array[-1].split(".")[0])
            git.Git(repo_route).clone(repo_info.git_route)
            os.rename(origin_route, repo_path)
        self.check_branch(repo_path, repo_info.branch)

    def check_branch(self, repo_path, branch):
        repo = Repo(repo_path)
        current_branch = "{}".format(repo.head.ref)
        if current_branch != branch:
            print("Cambiando Branch a {}".format(branch))
            repo.head.reset(index=True, working_tree=True)
            git.Git(repo_path).checkout(branch)
