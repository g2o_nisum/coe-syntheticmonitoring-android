import re

from cl.bci.falcon.AndroidInfo import AndroidInfo
from cl.bci.falcon.apiparser.ApiUsagesReader import ApiUsagesReader
from cl.bci.falcon.apiparser.MsVersionFieldReader import MsVersionFieldReader
from cl.bci.falcon.apiparser.RestResourceParser import RestResourceParser
from cl.bci.falcon.constantreader.ApplicationConstantReader import ApplicationConstantReader


class ApplicationRestEndpointFinder:

    def __init__(self, android_info: AndroidInfo):
        self.set_url = set()
        self.android_info = android_info
        self.constants = ApplicationConstantReader(android_info).read()

    def find(self):
        repositories = self.android_info.repositories
        repository_info_dict = self.android_info.repository_info

        for key, repository in repository_info_dict.items():
            if repositories[key]:
                self.process_module(key, repository)
        ms_exp_set = set()
        for url in self.set_url:
            result = re.search(r'ms-.*?-personasmobile-exp', url)
            if result:
                ms_exp_set.add(result.group())

        print("Endpoints de la app")

        sorted_set_url = sorted(self.set_url)
        [print(url) for url in sorted_set_url]

        # print("ms de experiencia")
        # for ms_exp in ms_exp_set:
        #    print(ms_exp)

    def process_module(self, key, repository):
        print(f'Analizando modulo {key}:')
        path = f"{self.android_info.folder_route}/{repository.folder_name}"

        parser = RestResourceParser(path, self.constants)
        result = parser.read()

        reader = MsVersionFieldReader(path, self.constants)
        datasource_info_list = reader.read()

        api_usages = ApiUsagesReader(path, result, datasource_info_list)
        api_dto_list = api_usages.read()

        for api_dto in api_dto_list:
            for resource in api_dto.resource_list:
                self.set_url.add(resource.url)
                print(f'Api: {api_dto.api} - endpoint: {resource.url}')
                print(f'Debug: {resource} ')


if __name__ == '__main__':
    test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
    info = AndroidInfo(test_route)
    ApplicationRestEndpointFinder(info).find()
