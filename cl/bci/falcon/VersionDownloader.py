import os

import git
from git import Repo

from cl.bci.falcon.Downloader import Downloader


class VersionDownloader(Downloader):

    def __init__(self, info_data):
        Downloader.__init__(self, info_data)

    def check_branch(self, repo_path, version_number):
        repo = Repo(repo_path)
        branch_name = self.search_branch_by_version_number(version_number, repo)
        if branch_name is not None:
            current_branch = "{}".format(repo.head.ref)
            if current_branch != branch_name:
                print("Cambiando Branch a {}".format(branch_name))
                self.execute_branch_checkout(branch_name, repo, repo_path)
        else:
            remote_refs = reversed(repo.remotes.origin.refs)
            branches = [ref for ref in remote_refs if '/re' in ref.name]

            for branch in branches:
                branch_name = branch.name
                self.execute_branch_checkout(branch_name, repo, repo_path)

                content = self.get_repo_version_content(repo, repo_path, branch_name)
                if version_number in content:
                    print('versionName = {} encontrado en la rama {} del repo {}'.format(version_number, branch_name, repo_path))
                    return
            print('No branch found with versionName = {} in repo {}'.format(version_number, repo_path))

    def execute_branch_checkout(self, branch_name, repo, repo_path):
        repo.head.reset(index=True, working_tree=True)
        git.Git(repo_path).checkout(branch_name)

    def get_repo_version_content(self, repo, repo_path, branch_name):
        configuration_path = 'buildSrc/src/main/kotlin/Configuration.kt'
        build_gradle_kts = '{}/build.gradle.kts'.format(repo_path)
        build_gradle = 'build.gradle'
        if os.path.exists(build_gradle_kts):
            content = repo.git.show("{}:{}".format(branch_name, configuration_path))
        else:
            content = repo.git.show("{}:{}".format(branch_name, build_gradle))
        return content

    def search_branch_by_version_number(self, version_number, repo):
        references = repo.tags
        remote_refs = reversed(repo.remotes.origin.refs)
        branches = [ref for ref in remote_refs if '/re' in ref.name]
        references.extend(branches)

        hyphen_version_number = version_number.replace('.', '-')
        v_version = "v{}".format(hyphen_version_number)

        for reference in references:
            reference_name = reference.name
            if reference_name.endswith(version_number) or reference_name.endswith(hyphen_version_number) \
                    or (v_version in reference_name):
                return reference_name
        return None

    def search_version_in_release_branch(self, version_number):
        pass
