import os.path
import re

import git

from cl.bci.falcon.dependencies.DependenciesConstantsProcessor import DependenciesConstantsProcessor
from cl.bci.falcon.utils.fileutils import get_file_content


class VersionExtractor:

    def __init__(self, tag_info):
        self.tag_info = tag_info
        repository_info = self.tag_info.repository_info
        repository_tag = repository_info.get('release')
        self.tag_info_folder_name = "{}/{}".format(self.tag_info.folder_name, repository_tag.folder_name)

    def extract(self):
        bci_dependencies_file = '%s/buildSrc/src/main/kotlin/BciDependencies.kt' % self.tag_info_folder_name
        content = get_file_content(bci_dependencies_file)
        module_list = []
        module_list.extend(self.find_bci_dependencies_artifacts(content))
        module_list.extend(self.find_dependencies_kts_artifacts(content))
        return module_list

    def find_bci_dependencies_artifacts(self, content):
        module_list = []
        result = re.findall(r"([\w\.]+):([\w\-]+):([\w\.]+)", content)
        for dependecy in result:
            depency_dict = self.map_dependency_to_dictionary(dependecy)
            module_list.append(depency_dict)
        return module_list

    def map_dependency_to_dictionary(self, dependecy):
        depency_dict = {'group': dependecy[0], 'artifact': dependecy[1], 'version': dependecy[2]}
        return depency_dict

    def find_dependencies_kts_artifacts(self, content):
        submodule_path = 'buildSrc/src/main/kotlin/dependencies/dependencies.gradle.kt'
        dependencies_kts_path = '{}/{}'.format(self.tag_info_folder_name, submodule_path)

        if not os.path.isfile(dependencies_kts_path):
            repo = git.Repo(self.tag_info_folder_name)
            repo.git.submodule('update', '--init')

        if os.path.isfile(dependencies_kts_path):
            processor = DependenciesConstantsProcessor()
            dependencies_kts_constant = processor.process(dependencies_kts_path)
            result = [self.map_dependency_to_dictionary(value.split(':')) for key, value in dependencies_kts_constant.items() if value.startswith('cl.bci')]
            return result
