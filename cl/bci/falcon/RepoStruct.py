class RepoStruct:

    def __init__(self, git_repo_name, git_host="git@bitbucket.org:bancocreditoeinversiones/", folder_name=None,
                 branch="develop"):
        self.git_host = git_host
        self.git_route = self.git_host + git_repo_name + ".git"
        self.folder_name = folder_name
        if folder_name is None:
            self.folder_name = git_repo_name
        self.branch = branch
