def get_file_content(file_name):
    f = open(file_name, "r")
    content = f.read()
    f.close()
    return content


def write_content_to_file(file_name, content):
    f = open(file_name, "w")
    f.write(content)
    f.close()
