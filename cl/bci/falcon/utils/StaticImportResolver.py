import re


class StaticImportResolver:
    __import_static_pattern = r'import.*?(cl\.bci\.app\.(?:\w+\.)+\w+)'
    __inclusive_import_static_pattern = r'import.*?(cl\.bci\.app\.(?:\w+\.)+\w+)[\.][\*]'
    __bci_prefix = 'cl.bci.app'

    def __init__(self, file_content):
        self.content = file_content

        self.static_imports = re.findall(self.__import_static_pattern, self.content, re.DOTALL)
        self.inclusive_static_imports = re.findall(self.__inclusive_import_static_pattern, self.content, re.DOTALL)

    def resolve(self, constant):
        if constant.startswith(self.__bci_prefix):
            return self.clean_kotlin_companion(constant)

        if '.' in constant:
            constant_segment = constant.split('.')
            end_static_identifier = constant_segment[0]
            for imports in self.static_imports:
                if imports.endswith('.%s' % end_static_identifier):
                    companion = self.clean_kotlin_companion(imports)
                    return '%s.%s' % (companion, '.'.join(constant_segment[1:]))

        for imports in self.static_imports:
            if imports.endswith('.%s' % constant):
                return self.clean_kotlin_companion(imports)

        if self.inclusive_static_imports:
            for imports in self.inclusive_static_imports:
                companion = self.clean_kotlin_companion(imports)
                return '%s.%s' % (companion, constant)

        return constant

    def clean_kotlin_companion(self, imports):
        return imports.replace('.Companion', '')
