import re


class KtConstantReplacer:

    def __init__(self, constante):
        self.constante = constante

    def replace(self, composite):
        composite = self.resolver_inline_constant(composite)
        composite = self.resolve_concat_constant(composite)
        return composite

    def resolver_inline_constant(self, composite):
        version = r'\$(\w+)'
        matches = re.findall(version, composite, re.DOTALL)
        for match in matches:
            posible_matches = [self.constante[key] for key in self.constante if str(key).endswith(f'.{match}')]
            if posible_matches:
                composite = str(composite).replace(f'${match}', posible_matches[0])
        return composite

    def resolve_concat_constant(self, composite):
        pattern = r'[A-Z]+(?:_[A-Z]+)*'
        matches = re.findall(pattern, composite)
        for match in matches:
            posible_matches = [self.constante[key] for key in self.constante if str(key).endswith('.%s' % match)]
            if posible_matches:
                composite = composite.replace(match, posible_matches[0])
        composite = re.sub(r'"|\+', '', composite)
        return composite
