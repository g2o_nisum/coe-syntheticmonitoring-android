from cl.bci.falcon.InfoData import InfoData
from cl.bci.falcon.RepoStruct import RepoStruct


class AndroidInfo(InfoData):
    repository = {"abTest": True,
                  "alcanciadigital": True,
                  "autenticacion": True,
                  "avisoviaje": True,
                  "bcm": True,
                  "bciwidgets": True,
                  "casoscliente": True,
                  "chat": True,
                  "certificadosdigitales": True,
                  "core": True,
                  "commons": True,
                  "consumo": True,
                  "crm": True,
                  "crm-feed": True,
                  "cuentas": True,
                  "descuentos": True,
                  "entrust": True,
                  "fidelizacion": True,
                  "insights": True,
                  "inversiones": True,
                  "notificaciones": True,
                  "onoff": True,
                  "pagos": True,
                  "pfm": True,
                  "recargas": True,
                  "tarjetavirtual": True,
                  "transferencia": True,
                  "tarjetacredito": True,
                  "virtualassistant": True
                  }

    repository_info = {
        "abTest": RepoStruct("app-android-abtest", branch="feature-cf-abtesting"),
        "alcanciadigital": RepoStruct("app-android-alcanciadigital"),
        "autenticacion": RepoStruct("app-android-autenticacion"),
        "avisoviaje": RepoStruct("app-android-tarjeta-credito-aviso-viaje"),
        "bcm": RepoStruct("app-android-push"),
        "bciwidgets": RepoStruct("app-android-bciwidgets"),
        "casoscliente": RepoStruct("app-android-casoscliente"),
        "chat": RepoStruct("app-android-chat"),
        "certificadosdigitales": RepoStruct("app-android-certificadosdigitales"),
        "core": RepoStruct("app-android-core"),
        "commons": RepoStruct("app-android-commons"),
        "consumo": RepoStruct("app-android-consumo"),
        "crm": RepoStruct("app-android-crm"),
        "crm-feed": RepoStruct("app-android-crm-feed"),
        "cuentas": RepoStruct("app-android-cuentas"),
        "descuentos": RepoStruct("app-android-descuentos"),
        "entrust": RepoStruct("app-android-entrust"),
        "fidelizacion": RepoStruct("app-android-fidelizacion"),
        "insights": RepoStruct("app-android-insights"),
        "inversiones": RepoStruct("app-android-inversiones"),
        "notificaciones": RepoStruct("app-android-notificaciones"),
        "onoff": RepoStruct("app-android-onoff"),
        "pagos": RepoStruct("app-android-pagos"),
        "pfm": RepoStruct("app-android-pfm"),
        "recargas": RepoStruct("app-android-recargas"),
        "tarjetavirtual": RepoStruct("app-android-tarjetavirtual"),
        "transferencia": RepoStruct("app-android-transferencias"),
        "tarjetacredito": RepoStruct("app-android-tarjetacredito"),
        "virtualassistant": RepoStruct("app-android-virtual-assistant")
    }

    def __init__(self, folder_route):
        super().__init__(self.repository, self.repository_info, folder_route)
        self.folder_route = folder_route
