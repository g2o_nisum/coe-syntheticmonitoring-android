import re

from cl.bci.falcon.constantreader.BuildConfigConstantsReader import BuildConfigConstantsReader
from cl.bci.falcon.utils.fileutils import get_file_content

CONSTANT_PATTERN = r"(?<=\()(.*?)(?=\))"
VERSION_MS_PATTERN = r"(?<=return BuildConfig\.)(.*?)(?=;)"
NAMED_ANNOTATION_PATTERN = r"@Named(.*?)}"
IMPORT_STATIC_PATTERN = r"^import\sstatic\s((\w+.)+);$"


class NetworkModuleConstantsReader:

    def __init__(self, route):
        self.route = route
        self.build_config_reader = BuildConfigConstantsReader(route)

    @property
    def read(self):
        network_file = 'tag/app/src/main/java/cl/bci/app/personas/di/module/NetworkModule.java'
        network_module_path = f'{self.route}/{network_file}'
        file_content = get_file_content(network_module_path)

        provided_constants = self.get_provided_constant(file_content)
        build_config_constants = self.build_config_reader.read()

        for key, value in provided_constants.items():
            if value not in build_config_constants:
                print(f'{value} not found in BuildConfig')
            else:
                provided_constants[key] = build_config_constants[value]

        return provided_constants

    def get_static_imports(self, file_content):
        static_imports = re.findall(IMPORT_STATIC_PATTERN, file_content, re.MULTILINE)
        return [item[0] for item in static_imports]

    def get_provided_constant(self, file_content):
        static_imports = self.get_static_imports(file_content)
        provided_constant_dict = {}

        provider_methods_body = re.findall(NAMED_ANNOTATION_PATTERN, file_content, re.DOTALL)
        for method_body in provider_methods_body:
            if "BuildConfig.VERSION" in method_body:
                dagger_constant = re.search(CONSTANT_PATTERN, method_body).group()
                dagger_constant = self.check_uncomplete_constant(dagger_constant, static_imports)

                build_config_constant = re.search(VERSION_MS_PATTERN, method_body, re.DOTALL).group()
                provided_constant_dict[dagger_constant] = build_config_constant

        return provided_constant_dict

    def check_uncomplete_constant(self, constant, static_imports):
        bci_prefix = 'cl.bci.app'
        if constant.startswith(bci_prefix):
            return constant
        for imports in static_imports:
            if imports.endswith(constant):
                return imports
        return constant
