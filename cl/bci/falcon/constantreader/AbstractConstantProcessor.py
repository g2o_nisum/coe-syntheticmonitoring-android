import abc  # Python's built-in abstract class library


class AbstractConstantProcessor(object):
    """You do not need to know about metaclasses.
    Just know that this is how you define abstract
    classes in Python."""
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def process(self, path):
        """Required Method"""
