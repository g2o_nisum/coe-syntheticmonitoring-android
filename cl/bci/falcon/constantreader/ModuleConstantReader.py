import os
import re

from cl.bci.falcon.constantreader.AbstractConstantProcessor import AbstractConstantProcessor
from cl.bci.falcon.constantreader.java.JavaConstantsProcessor import JavaConstantsProcessor
from cl.bci.falcon.constantreader.kt.KotlinConstantsProcessor import KotlinConstantsProcessor


class ModuleConstantReader:
    __exclude_directories = ['build', 'demo', 'test', 'di', 'Analytics', 'Tracking', 'CRM']
    __constants_file_pattern = r'^((?!analytics|track)\w)*constants\w*\.(?:java|kt)$'

    def __init__(self, workspace, network_module_constant):
        self.__workspace = workspace
        self.__network_module_constant = network_module_constant

    def read(self):
        print(f'Leyendo constantes de los modulos')
        modules_constants = self.visit_files_directory(self.process_file)
        return modules_constants

    def process_file(self, root, module, file):
        print(f'Obteniendo constantes de {root}/{file} modulo {module}:')
        file_path = '{}/{}'.format(root, file)
        processor = self.get_processor(file_path)
        constants = processor.process(file_path)
        print(f'Carga exitosa de {file} modulo {module}')
        return constants

    def get_processor(self, file_path):
        processor: AbstractConstantProcessor
        if file_path.endswith('.kt'):
            processor = KotlinConstantsProcessor(self.__network_module_constant)
        else:
            processor = JavaConstantsProcessor(self.__network_module_constant)
        return processor

    def find_module_name(self, root):
        root = root.replace(self.__workspace, '')
        try:
            return root.split(r'/')[1]
        except:
            pass

    def visit_files_directory(self, func):
        api_files = {}
        for root, dirs, files in os.walk(self.__workspace):
            dirs[:] = [d for d in dirs if d not in self.__exclude_directories]
            module = self.find_module_name(root)
            for file in files:
                if re.search(self.__constants_file_pattern, file.lower()):
                    # print('entre a modulo %s, file %s/%s' % (module, root, file))
                    result = func(root, module, file)
                    if result:
                        for k, v in result.items():
                            api_files[k] = v
        return api_files
