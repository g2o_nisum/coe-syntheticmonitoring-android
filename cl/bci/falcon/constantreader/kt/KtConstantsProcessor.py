import re

from cl.bci.falcon.constantreader.AbstractConstantProcessor import AbstractConstantProcessor
from cl.bci.falcon.utils.fileutils import get_file_content


class KtConstantsProcessor(AbstractConstantProcessor):
    __constant_field_pattern = r'([0-9A-Z_y=]+)\s*=\s*(.*?)(?:;|\n)'

    def process(self, path):
        content = get_file_content(path)

        module_constants_list = self.find_constant_list(content)
        file_dic_list = {}
        for module_constant in module_constants_list:
            file_dic_list[module_constant[0]] = module_constant[1]

        return file_dic_list

    def find_constant_list(self, content):
        module_constants_list = re.findall(self.__constant_field_pattern, content, re.DOTALL)
        return module_constants_list


if __name__ == '__main__':
    file_path = "/Users/nisum/Work/Bci/CoE/Script/repos/app-android-pagos/pagos/src/main/java/cl/bci/app/pagocuentas/commons/utils/Constants.kt"
    KtConstantsProcessor().process(file_path)
