from cl.bci.falcon.constantreader.kt.KotlinParser import KotlinParser
from cl.bci.falcon.constantreader.kt.KotlinParserListener import KotlinParserListener
from cl.bci.falcon.utils.KtConstantReplacer import KtConstantReplacer


class KotlinConstantsVisitor(KotlinParserListener):

    def __init__(self, package_name):
        self.package_name = package_name
        self.__constante = {}

    def enterPropertyDeclaration(self, ctx):  # ctx:KotlinParser.PropertyDeclarationContext
        parent_classes = self.get_parent_classes(ctx)
        const_token = 'const'
        try:
            modifiers = ctx.modifiers().getText()
            if const_token in modifiers:
                contante = ctx.variableDeclaration().getText()
                value = ctx.expression().getText()
                full_path = self.package_name + '.' + parent_classes + contante
                self.__constante[full_path] = value
        except Exception as e:
            print(f'Error al parsear la clase: {str(e)}')

    def get_parent_classes(self, ctx):
        constante = ''
        parentCtx = ctx.parentCtx
        while parentCtx:
            parentCtx = parentCtx.parentCtx
            if isinstance(parentCtx, KotlinParser.ClassDeclarationContext) or isinstance(parentCtx, KotlinParser.ObjectDeclarationContext):
                constante = '%s.%s' % (parentCtx.simpleIdentifier().getText(), constante)
        return constante

    def get_constants(self, network_module_constant):
        replacer = KtConstantReplacer(self.__constante)
        self.__constante.update(network_module_constant)
        for constant, value in self.__constante.items():
            self.__constante[constant] = replacer.replace(value)
        return self.__constante
