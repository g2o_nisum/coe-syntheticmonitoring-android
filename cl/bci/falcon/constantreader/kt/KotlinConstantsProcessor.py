from antlr4 import *

from cl.bci.falcon.constantreader.AbstractConstantProcessor import AbstractConstantProcessor
from cl.bci.falcon.constantreader.kt.KotlinConstantsVisitor import KotlinConstantsVisitor
from cl.bci.falcon.constantreader.kt.KotlinLexer import KotlinLexer
from cl.bci.falcon.constantreader.kt.KotlinParser import KotlinParser


class KotlinConstantsProcessor(AbstractConstantProcessor):

    def __init__(self, network_module_constant):
        self.__network_module_constant = network_module_constant

    def process(self, path):
        input_stream = FileStream(path, 'utf-8')
        lexer = KotlinLexer(input_stream)
        stream = CommonTokenStream(lexer)
        parser = KotlinParser(stream)
        tree = parser.kotlinFile()
        package_name = tree.packageHeader().identifier().getText()

        visitor = KotlinConstantsVisitor(package_name)
        walker = ParseTreeWalker()
        walker.walk(visitor, tree)

        constants = visitor.get_constants(self.__network_module_constant)

        return constants


if __name__ == '__main__':
    file_path = '/Users/nisum/Work/Bci/CoE/Script/repos/app-android-pagos/pagos/src/main/java/cl/bci/app/pagocuentas/commons/utils/Constants.kt'
    KotlinConstantsProcessor().process(file_path)
