# Generated from KotlinParser.g4 by ANTLR 4.8
# encoding: utf-8
import sys
from io import StringIO

from antlr4 import *

if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u00ac")
        buf.write("\u0dfc\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4")
        buf.write("/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t\64")
        buf.write("\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t")
        buf.write(";\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\t")
        buf.write("D\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\tL\4M\t")
        buf.write("M\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\tU\4V\t")
        buf.write("V\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4")
        buf.write("_\t_\4`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4")
        buf.write("h\th\4i\ti\4j\tj\4k\tk\4l\tl\4m\tm\4n\tn\4o\to\4p\tp\4")
        buf.write("q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4w\tw\4x\tx\4y\ty\4")
        buf.write("z\tz\4{\t{\4|\t|\4}\t}\4~\t~\4\177\t\177\4\u0080\t\u0080")
        buf.write("\4\u0081\t\u0081\4\u0082\t\u0082\4\u0083\t\u0083\4\u0084")
        buf.write("\t\u0084\4\u0085\t\u0085\4\u0086\t\u0086\4\u0087\t\u0087")
        buf.write("\4\u0088\t\u0088\4\u0089\t\u0089\4\u008a\t\u008a\4\u008b")
        buf.write("\t\u008b\4\u008c\t\u008c\4\u008d\t\u008d\4\u008e\t\u008e")
        buf.write("\4\u008f\t\u008f\4\u0090\t\u0090\4\u0091\t\u0091\4\u0092")
        buf.write("\t\u0092\4\u0093\t\u0093\4\u0094\t\u0094\4\u0095\t\u0095")
        buf.write("\4\u0096\t\u0096\4\u0097\t\u0097\4\u0098\t\u0098\4\u0099")
        buf.write("\t\u0099\4\u009a\t\u009a\4\u009b\t\u009b\4\u009c\t\u009c")
        buf.write("\4\u009d\t\u009d\4\u009e\t\u009e\4\u009f\t\u009f\4\u00a0")
        buf.write("\t\u00a0\4\u00a1\t\u00a1\4\u00a2\t\u00a2\4\u00a3\t\u00a3")
        buf.write("\4\u00a4\t\u00a4\4\u00a5\t\u00a5\4\u00a6\t\u00a6\4\u00a7")
        buf.write("\t\u00a7\4\u00a8\t\u00a8\4\u00a9\t\u00a9\4\u00aa\t\u00aa")
        buf.write("\4\u00ab\t\u00ab\4\u00ac\t\u00ac\4\u00ad\t\u00ad\3\2\5")
        buf.write("\2\u015c\n\2\3\2\7\2\u015f\n\2\f\2\16\2\u0162\13\2\3\2")
        buf.write("\7\2\u0165\n\2\f\2\16\2\u0168\13\2\3\2\3\2\3\2\7\2\u016d")
        buf.write("\n\2\f\2\16\2\u0170\13\2\3\2\3\2\3\3\5\3\u0175\n\3\3\3")
        buf.write("\7\3\u0178\n\3\f\3\16\3\u017b\13\3\3\3\7\3\u017e\n\3\f")
        buf.write("\3\16\3\u0181\13\3\3\3\3\3\3\3\3\3\3\3\7\3\u0188\n\3\f")
        buf.write("\3\16\3\u018b\13\3\3\3\3\3\3\4\3\4\6\4\u0191\n\4\r\4\16")
        buf.write("\4\u0192\3\5\3\5\3\5\7\5\u0198\n\5\f\5\16\5\u019b\13\5")
        buf.write("\3\5\3\5\7\5\u019f\n\5\f\5\16\5\u01a2\13\5\3\5\3\5\6\5")
        buf.write("\u01a6\n\5\r\5\16\5\u01a7\3\5\3\5\3\5\5\5\u01ad\n\5\3")
        buf.write("\5\7\5\u01b0\n\5\f\5\16\5\u01b3\13\5\3\6\3\6\3\6\5\6\u01b8")
        buf.write("\n\6\5\6\u01ba\n\6\3\7\7\7\u01bd\n\7\f\7\16\7\u01c0\13")
        buf.write("\7\3\b\3\b\3\b\3\b\3\b\5\b\u01c7\n\b\3\b\5\b\u01ca\n\b")
        buf.write("\3\t\3\t\3\t\3\n\3\n\5\n\u01d1\n\n\3\13\5\13\u01d4\n\13")
        buf.write("\3\13\3\13\7\13\u01d8\n\13\f\13\16\13\u01db\13\13\3\13")
        buf.write("\3\13\7\13\u01df\n\13\f\13\16\13\u01e2\13\13\3\13\5\13")
        buf.write("\u01e5\n\13\3\13\7\13\u01e8\n\13\f\13\16\13\u01eb\13\13")
        buf.write("\3\13\3\13\7\13\u01ef\n\13\f\13\16\13\u01f2\13\13\3\13")
        buf.write("\3\13\3\f\3\f\3\f\3\f\3\f\5\f\u01fb\n\f\3\r\5\r\u01fe")
        buf.write("\n\r\3\r\3\r\3\r\7\r\u0203\n\r\f\r\16\r\u0206\13\r\5\r")
        buf.write("\u0208\n\r\3\r\5\r\u020b\n\r\3\r\7\r\u020e\n\r\f\r\16")
        buf.write("\r\u0211\13\r\3\r\3\r\7\r\u0215\n\r\f\r\16\r\u0218\13")
        buf.write("\r\3\r\5\r\u021b\n\r\3\r\7\r\u021e\n\r\f\r\16\r\u0221")
        buf.write("\13\r\3\r\5\r\u0224\n\r\3\r\7\r\u0227\n\r\f\r\16\r\u022a")
        buf.write("\13\r\3\r\3\r\7\r\u022e\n\r\f\r\16\r\u0231\13\r\3\r\5")
        buf.write("\r\u0234\n\r\3\r\7\r\u0237\n\r\f\r\16\r\u023a\13\r\3\r")
        buf.write("\5\r\u023d\n\r\3\r\7\r\u0240\n\r\f\r\16\r\u0243\13\r\3")
        buf.write("\r\3\r\7\r\u0247\n\r\f\r\16\r\u024a\13\r\3\r\5\r\u024d")
        buf.write("\n\r\3\16\5\16\u0250\n\16\3\16\3\16\7\16\u0254\n\16\f")
        buf.write("\16\16\16\u0257\13\16\5\16\u0259\n\16\3\16\3\16\3\17\3")
        buf.write("\17\7\17\u025f\n\17\f\17\16\17\u0262\13\17\3\17\3\17\7")
        buf.write("\17\u0266\n\17\f\17\16\17\u0269\13\17\3\17\3\17\3\20\3")
        buf.write("\20\7\20\u026f\n\20\f\20\16\20\u0272\13\20\3\20\3\20\7")
        buf.write("\20\u0276\n\20\f\20\16\20\u0279\13\20\3\20\3\20\7\20\u027d")
        buf.write("\n\20\f\20\16\20\u0280\13\20\3\20\7\20\u0283\n\20\f\20")
        buf.write("\16\20\u0286\13\20\3\20\7\20\u0289\n\20\f\20\16\20\u028c")
        buf.write("\13\20\3\20\5\20\u028f\n\20\5\20\u0291\n\20\3\20\7\20")
        buf.write("\u0294\n\20\f\20\16\20\u0297\13\20\3\20\3\20\3\21\5\21")
        buf.write("\u029c\n\21\3\21\5\21\u029f\n\21\3\21\7\21\u02a2\n\21")
        buf.write("\f\21\16\21\u02a5\13\21\3\21\3\21\3\21\7\21\u02aa\n\21")
        buf.write("\f\21\16\21\u02ad\13\21\3\21\3\21\7\21\u02b1\n\21\f\21")
        buf.write("\16\21\u02b4\13\21\3\21\3\21\7\21\u02b8\n\21\f\21\16\21")
        buf.write("\u02bb\13\21\3\21\5\21\u02be\n\21\3\22\3\22\7\22\u02c2")
        buf.write("\n\22\f\22\16\22\u02c5\13\22\3\22\3\22\7\22\u02c9\n\22")
        buf.write("\f\22\16\22\u02cc\13\22\3\22\7\22\u02cf\n\22\f\22\16\22")
        buf.write("\u02d2\13\22\3\23\3\23\3\23\3\23\5\23\u02d8\n\23\3\24")
        buf.write("\3\24\3\24\3\25\7\25\u02de\n\25\f\25\16\25\u02e1\13\25")
        buf.write("\3\25\7\25\u02e4\n\25\f\25\16\25\u02e7\13\25\3\25\3\25")
        buf.write("\3\26\3\26\5\26\u02ed\n\26\3\26\7\26\u02f0\n\26\f\26\16")
        buf.write("\26\u02f3\13\26\3\26\3\26\7\26\u02f7\n\26\f\26\16\26\u02fa")
        buf.write("\13\26\3\26\3\26\3\27\3\27\7\27\u0300\n\27\f\27\16\27")
        buf.write("\u0303\13\27\3\27\3\27\7\27\u0307\n\27\f\27\16\27\u030a")
        buf.write("\13\27\3\27\3\27\7\27\u030e\n\27\f\27\16\27\u0311\13\27")
        buf.write("\3\27\7\27\u0314\n\27\f\27\16\27\u0317\13\27\3\27\7\27")
        buf.write("\u031a\n\27\f\27\16\27\u031d\13\27\3\27\5\27\u0320\n\27")
        buf.write("\3\27\7\27\u0323\n\27\f\27\16\27\u0326\13\27\3\27\3\27")
        buf.write("\3\30\5\30\u032b\n\30\3\30\7\30\u032e\n\30\f\30\16\30")
        buf.write("\u0331\13\30\3\30\3\30\7\30\u0335\n\30\f\30\16\30\u0338")
        buf.write("\13\30\3\30\3\30\7\30\u033c\n\30\f\30\16\30\u033f\13\30")
        buf.write("\3\30\5\30\u0342\n\30\3\31\3\31\7\31\u0346\n\31\f\31\16")
        buf.write("\31\u0349\13\31\3\31\3\31\7\31\u034d\n\31\f\31\16\31\u0350")
        buf.write("\13\31\3\31\3\31\7\31\u0354\n\31\f\31\16\31\u0357\13\31")
        buf.write("\3\31\7\31\u035a\n\31\f\31\16\31\u035d\13\31\3\32\7\32")
        buf.write("\u0360\n\32\f\32\16\32\u0363\13\32\3\32\3\32\7\32\u0367")
        buf.write("\n\32\f\32\16\32\u036a\13\32\3\32\3\32\7\32\u036e\n\32")
        buf.write("\f\32\16\32\u0371\13\32\3\32\3\32\3\33\3\33\5\33\u0377")
        buf.write("\n\33\7\33\u0379\n\33\f\33\16\33\u037c\13\33\3\34\3\34")
        buf.write("\3\34\3\34\5\34\u0382\n\34\3\35\3\35\7\35\u0386\n\35\f")
        buf.write("\35\16\35\u0389\13\35\3\35\3\35\3\36\5\36\u038e\n\36\3")
        buf.write("\36\3\36\7\36\u0392\n\36\f\36\16\36\u0395\13\36\3\36\3")
        buf.write("\36\7\36\u0399\n\36\f\36\16\36\u039c\13\36\3\36\5\36\u039f")
        buf.write("\n\36\3\36\7\36\u03a2\n\36\f\36\16\36\u03a5\13\36\3\36")
        buf.write("\3\36\7\36\u03a9\n\36\f\36\16\36\u03ac\13\36\3\36\5\36")
        buf.write("\u03af\n\36\3\36\7\36\u03b2\n\36\f\36\16\36\u03b5\13\36")
        buf.write("\3\36\5\36\u03b8\n\36\3\37\3\37\7\37\u03bc\n\37\f\37\16")
        buf.write("\37\u03bf\13\37\3\37\3\37\7\37\u03c3\n\37\f\37\16\37\u03c6")
        buf.write("\13\37\3\37\3\37\7\37\u03ca\n\37\f\37\16\37\u03cd\13\37")
        buf.write("\3\37\7\37\u03d0\n\37\f\37\16\37\u03d3\13\37\3\37\7\37")
        buf.write("\u03d6\n\37\f\37\16\37\u03d9\13\37\3\37\5\37\u03dc\n\37")
        buf.write("\5\37\u03de\n\37\3\37\7\37\u03e1\n\37\f\37\16\37\u03e4")
        buf.write("\13\37\3\37\3\37\3 \5 \u03e9\n \3 \3 \7 \u03ed\n \f \16")
        buf.write(" \u03f0\13 \3 \3 \7 \u03f4\n \f \16 \u03f7\13 \3 \5 \u03fa")
        buf.write("\n \3!\5!\u03fd\n!\3!\3!\7!\u0401\n!\f!\16!\u0404\13!")
        buf.write("\3!\5!\u0407\n!\3!\7!\u040a\n!\f!\16!\u040d\13!\3!\3!")
        buf.write("\7!\u0411\n!\f!\16!\u0414\13!\3!\3!\5!\u0418\n!\3!\7!")
        buf.write("\u041b\n!\f!\16!\u041e\13!\3!\3!\7!\u0422\n!\f!\16!\u0425")
        buf.write("\13!\3!\3!\7!\u0429\n!\f!\16!\u042c\13!\3!\3!\7!\u0430")
        buf.write("\n!\f!\16!\u0433\13!\3!\5!\u0436\n!\3!\7!\u0439\n!\f!")
        buf.write("\16!\u043c\13!\3!\5!\u043f\n!\3!\7!\u0442\n!\f!\16!\u0445")
        buf.write("\13!\3!\5!\u0448\n!\3\"\3\"\3\"\7\"\u044d\n\"\f\"\16\"")
        buf.write("\u0450\13\"\3\"\5\"\u0453\n\"\3#\7#\u0456\n#\f#\16#\u0459")
        buf.write("\13#\3#\7#\u045c\n#\f#\16#\u045f\13#\3#\3#\7#\u0463\n")
        buf.write("#\f#\16#\u0466\13#\3#\3#\7#\u046a\n#\f#\16#\u046d\13#")
        buf.write("\3#\5#\u0470\n#\3$\3$\7$\u0474\n$\f$\16$\u0477\13$\3$")
        buf.write("\3$\7$\u047b\n$\f$\16$\u047e\13$\3$\3$\7$\u0482\n$\f$")
        buf.write("\16$\u0485\13$\3$\7$\u0488\n$\f$\16$\u048b\13$\3$\7$\u048e")
        buf.write("\n$\f$\16$\u0491\13$\3$\5$\u0494\n$\3$\7$\u0497\n$\f$")
        buf.write("\16$\u049a\13$\3$\3$\3%\5%\u049f\n%\3%\3%\7%\u04a3\n%")
        buf.write("\f%\16%\u04a6\13%\3%\5%\u04a9\n%\3%\7%\u04ac\n%\f%\16")
        buf.write("%\u04af\13%\3%\3%\7%\u04b3\n%\f%\16%\u04b6\13%\3%\3%\5")
        buf.write("%\u04ba\n%\3%\7%\u04bd\n%\f%\16%\u04c0\13%\3%\3%\5%\u04c4")
        buf.write("\n%\3%\7%\u04c7\n%\f%\16%\u04ca\13%\3%\5%\u04cd\n%\3%")
        buf.write("\7%\u04d0\n%\f%\16%\u04d3\13%\3%\3%\7%\u04d7\n%\f%\16")
        buf.write("%\u04da\13%\3%\3%\5%\u04de\n%\5%\u04e0\n%\3%\6%\u04e3")
        buf.write("\n%\r%\16%\u04e4\3%\5%\u04e8\n%\3%\7%\u04eb\n%\f%\16%")
        buf.write("\u04ee\13%\3%\5%\u04f1\n%\3%\7%\u04f4\n%\f%\16%\u04f7")
        buf.write("\13%\3%\5%\u04fa\n%\3%\5%\u04fd\n%\3%\5%\u0500\n%\3%\7")
        buf.write("%\u0503\n%\f%\16%\u0506\13%\3%\5%\u0509\n%\3%\5%\u050c")
        buf.write("\n%\5%\u050e\n%\3&\3&\7&\u0512\n&\f&\16&\u0515\13&\3&")
        buf.write("\3&\3\'\5\'\u051a\n\'\3\'\3\'\5\'\u051e\n\'\3\'\3\'\7")
        buf.write("\'\u0522\n\'\f\'\16\'\u0525\13\'\3\'\3\'\7\'\u0529\n\'")
        buf.write("\f\'\16\'\u052c\13\'\3\'\3\'\7\'\u0530\n\'\f\'\16\'\u0533")
        buf.write("\13\'\3\'\3\'\7\'\u0537\n\'\f\'\16\'\u053a\13\'\3\'\5")
        buf.write("\'\u053d\n\'\3\'\7\'\u0540\n\'\f\'\16\'\u0543\13\'\3\'")
        buf.write("\5\'\u0546\n\'\3(\5(\u0549\n(\3(\3(\5(\u054d\n(\3(\3(")
        buf.write("\7(\u0551\n(\f(\16(\u0554\13(\3(\3(\7(\u0558\n(\f(\16")
        buf.write("(\u055b\13(\3(\3(\7(\u055f\n(\f(\16(\u0562\13(\3(\5(\u0565")
        buf.write("\n(\3(\7(\u0568\n(\f(\16(\u056b\13(\3(\3(\7(\u056f\n(")
        buf.write("\f(\16(\u0572\13(\3(\3(\7(\u0576\n(\f(\16(\u0579\13(\3")
        buf.write("(\5(\u057c\n(\3(\7(\u057f\n(\f(\16(\u0582\13(\3(\3(\5")
        buf.write("(\u0586\n(\3)\3)\7)\u058a\n)\f)\16)\u058d\13)\3)\3)\7")
        buf.write(")\u0591\n)\f)\16)\u0594\13)\3)\3)\7)\u0598\n)\f)\16)\u059b")
        buf.write("\13)\3)\7)\u059e\n)\f)\16)\u05a1\13)\3)\7)\u05a4\n)\f")
        buf.write(")\16)\u05a7\13)\3)\5)\u05aa\n)\5)\u05ac\n)\3)\7)\u05af")
        buf.write("\n)\f)\16)\u05b2\13)\3)\3)\3*\5*\u05b7\n*\3*\3*\7*\u05bb")
        buf.write("\n*\f*\16*\u05be\13*\3*\3*\7*\u05c2\n*\f*\16*\u05c5\13")
        buf.write("*\3*\5*\u05c8\n*\3+\3+\7+\u05cc\n+\f+\16+\u05cf\13+\3")
        buf.write("+\3+\7+\u05d3\n+\f+\16+\u05d6\13+\3+\3+\3,\5,\u05db\n")
        buf.write(",\3,\3,\7,\u05df\n,\f,\16,\u05e2\13,\3,\3,\7,\u05e6\n")
        buf.write(",\f,\16,\u05e9\13,\3,\3,\7,\u05ed\n,\f,\16,\u05f0\13,")
        buf.write("\3,\5,\u05f3\n,\3,\7,\u05f6\n,\f,\16,\u05f9\13,\3,\5,")
        buf.write("\u05fc\n,\3-\5-\u05ff\n-\3-\3-\7-\u0603\n-\f-\16-\u0606")
        buf.write("\13-\3-\3-\7-\u060a\n-\f-\16-\u060d\13-\3-\3-\7-\u0611")
        buf.write("\n-\f-\16-\u0614\13-\3-\5-\u0617\n-\3-\7-\u061a\n-\f-")
        buf.write("\16-\u061d\13-\3-\5-\u0620\n-\3.\3.\7.\u0624\n.\f.\16")
        buf.write(".\u0627\13.\3.\3.\3.\7.\u062c\n.\f.\16.\u062f\13.\3.\5")
        buf.write(".\u0632\n.\3/\3/\7/\u0636\n/\f/\16/\u0639\13/\3/\5/\u063c")
        buf.write("\n/\3/\7/\u063f\n/\f/\16/\u0642\13/\3/\3/\7/\u0646\n/")
        buf.write("\f/\16/\u0649\13/\3/\5/\u064c\n/\3/\7/\u064f\n/\f/\16")
        buf.write("/\u0652\13/\3/\3/\3\60\3\60\7\60\u0658\n\60\f\60\16\60")
        buf.write("\u065b\13\60\3\60\3\60\7\60\u065f\n\60\f\60\16\60\u0662")
        buf.write("\13\60\3\60\7\60\u0665\n\60\f\60\16\60\u0668\13\60\3\60")
        buf.write("\7\60\u066b\n\60\f\60\16\60\u066e\13\60\3\60\5\60\u0671")
        buf.write("\n\60\3\61\3\61\7\61\u0675\n\61\f\61\16\61\u0678\13\61")
        buf.write("\5\61\u067a\n\61\3\61\3\61\7\61\u067e\n\61\f\61\16\61")
        buf.write("\u0681\13\61\3\61\5\61\u0684\n\61\3\61\7\61\u0687\n\61")
        buf.write("\f\61\16\61\u068a\13\61\3\61\5\61\u068d\n\61\3\62\5\62")
        buf.write("\u0690\n\62\3\62\3\62\3\62\3\62\5\62\u0696\n\62\3\63\3")
        buf.write("\63\5\63\u069a\n\63\3\64\3\64\5\64\u069e\n\64\3\64\7\64")
        buf.write("\u06a1\n\64\f\64\16\64\u06a4\13\64\3\64\6\64\u06a7\n\64")
        buf.write("\r\64\16\64\u06a8\3\65\3\65\3\66\3\66\7\66\u06af\n\66")
        buf.write("\f\66\16\66\u06b2\13\66\3\66\3\66\7\66\u06b6\n\66\f\66")
        buf.write("\16\66\u06b9\13\66\3\66\7\66\u06bc\n\66\f\66\16\66\u06bf")
        buf.write("\13\66\3\67\3\67\7\67\u06c3\n\67\f\67\16\67\u06c6\13\67")
        buf.write("\3\67\5\67\u06c9\n\67\38\58\u06cc\n8\38\38\58\u06d0\n")
        buf.write("8\39\69\u06d3\n9\r9\169\u06d4\3:\3:\7:\u06d9\n:\f:\16")
        buf.write(":\u06dc\13:\3:\5:\u06df\n:\3;\3;\7;\u06e3\n;\f;\16;\u06e6")
        buf.write("\13;\3;\3;\7;\u06ea\n;\f;\16;\u06ed\13;\5;\u06ef\n;\3")
        buf.write(";\3;\7;\u06f3\n;\f;\16;\u06f6\13;\3;\3;\7;\u06fa\n;\f")
        buf.write(";\16;\u06fd\13;\3;\3;\3<\3<\7<\u0703\n<\f<\16<\u0706\13")
        buf.write("<\3<\3<\5<\u070a\n<\3<\7<\u070d\n<\f<\16<\u0710\13<\3")
        buf.write("<\3<\7<\u0714\n<\f<\16<\u0717\13<\3<\3<\5<\u071b\n<\7")
        buf.write("<\u071d\n<\f<\16<\u0720\13<\3<\7<\u0723\n<\f<\16<\u0726")
        buf.write("\13<\3<\5<\u0729\n<\3<\7<\u072c\n<\f<\16<\u072f\13<\3")
        buf.write("<\3<\3=\3=\7=\u0735\n=\f=\16=\u0738\13=\3=\3=\7=\u073c")
        buf.write("\n=\f=\16=\u073f\13=\3=\3=\3>\5>\u0744\n>\3>\3>\3>\5>")
        buf.write("\u0749\n>\3?\3?\7?\u074d\n?\f?\16?\u0750\13?\3?\3?\7?")
        buf.write("\u0754\n?\f?\16?\u0757\13?\3?\3?\3?\3?\7?\u075d\n?\f?")
        buf.write("\16?\u0760\13?\3?\3?\7?\u0764\n?\f?\16?\u0767\13?\3?\3")
        buf.write("?\5?\u076b\n?\3@\3@\3@\3@\7@\u0771\n@\f@\16@\u0774\13")
        buf.write("@\5@\u0776\n@\3@\5@\u0779\n@\3A\3A\7A\u077d\nA\fA\16A")
        buf.write("\u0780\13A\3A\3A\3A\3A\5A\u0786\nA\3B\3B\3B\7B\u078b\n")
        buf.write("B\fB\16B\u078e\13B\3C\3C\5C\u0792\nC\3D\3D\7D\u0796\n")
        buf.write("D\fD\16D\u0799\13D\3D\3D\7D\u079d\nD\fD\16D\u07a0\13D")
        buf.write("\3D\3D\3E\3E\3E\5E\u07a7\nE\3F\3F\7F\u07ab\nF\fF\16F\u07ae")
        buf.write("\13F\3F\3F\7F\u07b2\nF\fF\16F\u07b5\13F\3F\3F\5F\u07b9")
        buf.write("\nF\3F\3F\3F\3F\7F\u07bf\nF\fF\16F\u07c2\13F\3F\5F\u07c5")
        buf.write("\nF\3G\3G\7G\u07c9\nG\fG\16G\u07cc\13G\3G\3G\3G\3G\7G")
        buf.write("\u07d2\nG\fG\16G\u07d5\13G\3G\3G\3G\3G\7G\u07db\nG\fG")
        buf.write("\16G\u07de\13G\3G\3G\3G\3G\7G\u07e4\nG\fG\16G\u07e7\13")
        buf.write("G\3G\3G\5G\u07eb\nG\3H\3H\7H\u07ef\nH\fH\16H\u07f2\13")
        buf.write("H\3H\5H\u07f5\nH\3H\7H\u07f8\nH\fH\16H\u07fb\13H\3H\3")
        buf.write("H\7H\u07ff\nH\fH\16H\u0802\13H\3H\3H\3H\3H\3I\3I\3I\7")
        buf.write("I\u080b\nI\fI\16I\u080e\13I\3I\3I\3I\3I\3I\7I\u0815\n")
        buf.write("I\fI\16I\u0818\13I\3I\3I\5I\u081c\nI\3J\3J\7J\u0820\n")
        buf.write("J\fJ\16J\u0823\13J\3J\5J\u0826\nJ\3K\6K\u0829\nK\rK\16")
        buf.write("K\u082a\3K\5K\u082e\nK\3L\3L\3M\3M\7M\u0834\nM\fM\16M")
        buf.write("\u0837\13M\3M\3M\7M\u083b\nM\fM\16M\u083e\13M\3M\7M\u0841")
        buf.write("\nM\fM\16M\u0844\13M\3N\3N\7N\u0848\nN\fN\16N\u084b\13")
        buf.write("N\3N\3N\7N\u084f\nN\fN\16N\u0852\13N\3N\7N\u0855\nN\f")
        buf.write("N\16N\u0858\13N\3O\3O\3O\7O\u085d\nO\fO\16O\u0860\13O")
        buf.write("\3O\3O\7O\u0864\nO\fO\16O\u0867\13O\3P\3P\3P\7P\u086c")
        buf.write("\nP\fP\16P\u086f\13P\3P\3P\5P\u0873\nP\3Q\3Q\3Q\7Q\u0878")
        buf.write("\nQ\fQ\16Q\u087b\13Q\3Q\3Q\3Q\3Q\7Q\u0881\nQ\fQ\16Q\u0884")
        buf.write("\13Q\3Q\3Q\7Q\u0888\nQ\fQ\16Q\u088b\13Q\3R\3R\7R\u088f")
        buf.write("\nR\fR\16R\u0892\13R\3R\3R\7R\u0896\nR\fR\16R\u0899\13")
        buf.write("R\3R\3R\7R\u089d\nR\fR\16R\u08a0\13R\3S\3S\3S\3T\3T\3")
        buf.write("T\7T\u08a8\nT\fT\16T\u08ab\13T\3T\3T\7T\u08af\nT\fT\16")
        buf.write("T\u08b2\13T\3U\3U\3U\7U\u08b7\nU\fU\16U\u08ba\13U\3U\7")
        buf.write("U\u08bd\nU\fU\16U\u08c0\13U\3V\3V\3V\7V\u08c5\nV\fV\16")
        buf.write("V\u08c8\13V\3V\3V\7V\u08cc\nV\fV\16V\u08cf\13V\3W\3W\3")
        buf.write("W\7W\u08d4\nW\fW\16W\u08d7\13W\3W\3W\7W\u08db\nW\fW\16")
        buf.write("W\u08de\13W\3X\3X\7X\u08e2\nX\fX\16X\u08e5\13X\3X\3X\7")
        buf.write("X\u08e9\nX\fX\16X\u08ec\13X\3X\3X\5X\u08f0\nX\3Y\3Y\7")
        buf.write("Y\u08f4\nY\fY\16Y\u08f7\13Y\3Y\3Y\7Y\u08fb\nY\fY\16Y\u08fe")
        buf.write("\13Y\3Y\3Y\7Y\u0902\nY\fY\16Y\u0905\13Y\3Y\3Y\7Y\u0909")
        buf.write("\nY\fY\16Y\u090c\13Y\3Y\3Y\5Y\u0910\nY\7Y\u0912\nY\fY")
        buf.write("\16Y\u0915\13Y\3Z\7Z\u0918\nZ\fZ\16Z\u091b\13Z\3Z\3Z\3")
        buf.write("[\3[\3[\3[\7[\u0923\n[\f[\16[\u0926\13[\5[\u0928\n[\3")
        buf.write("\\\3\\\3\\\6\\\u092d\n\\\r\\\16\\\u092e\5\\\u0931\n\\")
        buf.write("\3]\3]\3]\3]\3]\5]\u0938\n]\3^\3^\3^\3^\3^\5^\u093f\n")
        buf.write("^\3_\3_\7_\u0943\n_\f_\16_\u0946\13_\3_\3_\7_\u094a\n")
        buf.write("_\f_\16_\u094d\13_\3_\3_\3`\3`\5`\u0953\n`\3a\3a\7a\u0957")
        buf.write("\na\fa\16a\u095a\13a\3a\3a\7a\u095e\na\fa\16a\u0961\13")
        buf.write("a\3a\3a\3b\3b\3b\5b\u0968\nb\3c\3c\7c\u096c\nc\fc\16c")
        buf.write("\u096f\13c\3c\3c\7c\u0973\nc\fc\16c\u0976\13c\3c\3c\7")
        buf.write("c\u097a\nc\fc\16c\u097d\13c\3c\7c\u0980\nc\fc\16c\u0983")
        buf.write("\13c\3c\7c\u0986\nc\fc\16c\u0989\13c\3c\5c\u098c\nc\3")
        buf.write("c\7c\u098f\nc\fc\16c\u0992\13c\3c\3c\3d\7d\u0997\nd\f")
        buf.write("d\16d\u099a\13d\3d\3d\7d\u099e\nd\fd\16d\u09a1\13d\3d")
        buf.write("\3d\3d\5d\u09a6\nd\3e\5e\u09a9\ne\3e\5e\u09ac\ne\3e\3")
        buf.write("e\5e\u09b0\ne\3e\5e\u09b3\ne\3f\7f\u09b6\nf\ff\16f\u09b9")
        buf.write("\13f\3f\5f\u09bc\nf\3f\7f\u09bf\nf\ff\16f\u09c2\13f\3")
        buf.write("f\3f\3g\3g\7g\u09c8\ng\fg\16g\u09cb\13g\3g\3g\7g\u09cf")
        buf.write("\ng\fg\16g\u09d2\13g\3g\3g\7g\u09d6\ng\fg\16g\u09d9\13")
        buf.write("g\3g\7g\u09dc\ng\fg\16g\u09df\13g\3g\7g\u09e2\ng\fg\16")
        buf.write("g\u09e5\13g\3g\5g\u09e8\ng\3g\7g\u09eb\ng\fg\16g\u09ee")
        buf.write("\13g\3g\3g\3h\3h\7h\u09f4\nh\fh\16h\u09f7\13h\3h\3h\3")
        buf.write("h\7h\u09fc\nh\fh\16h\u09ff\13h\3h\3h\7h\u0a03\nh\fh\16")
        buf.write("h\u0a06\13h\3h\3h\7h\u0a0a\nh\fh\16h\u0a0d\13h\3h\7h\u0a10")
        buf.write("\nh\fh\16h\u0a13\13h\3h\7h\u0a16\nh\fh\16h\u0a19\13h\3")
        buf.write("h\5h\u0a1c\nh\3h\7h\u0a1f\nh\fh\16h\u0a22\13h\3h\3h\5")
        buf.write("h\u0a26\nh\3i\5i\u0a29\ni\3i\7i\u0a2c\ni\fi\16i\u0a2f")
        buf.write("\13i\3i\3i\7i\u0a33\ni\fi\16i\u0a36\13i\3i\3i\7i\u0a3a")
        buf.write("\ni\fi\16i\u0a3d\13i\5i\u0a3f\ni\3i\5i\u0a42\ni\3i\7i")
        buf.write("\u0a45\ni\fi\16i\u0a48\13i\3i\3i\3j\3j\3j\3j\3j\3j\3j")
        buf.write("\3j\3j\3j\3j\3j\3j\3j\5j\u0a5a\nj\3k\3k\7k\u0a5e\nk\f")
        buf.write("k\16k\u0a61\13k\3k\3k\7k\u0a65\nk\fk\16k\u0a68\13k\3k")
        buf.write("\3k\3l\3l\7l\u0a6e\nl\fl\16l\u0a71\13l\3l\3l\7l\u0a75")
        buf.write("\nl\fl\16l\u0a78\13l\3l\3l\7l\u0a7c\nl\fl\16l\u0a7f\13")
        buf.write("l\3l\7l\u0a82\nl\fl\16l\u0a85\13l\3l\7l\u0a88\nl\fl\16")
        buf.write("l\u0a8b\13l\3l\5l\u0a8e\nl\3l\7l\u0a91\nl\fl\16l\u0a94")
        buf.write("\13l\3l\3l\3l\3l\7l\u0a9a\nl\fl\16l\u0a9d\13l\3l\5l\u0aa0")
        buf.write("\nl\3m\3m\3n\3n\5n\u0aa6\nn\3o\3o\3o\7o\u0aab\no\fo\16")
        buf.write("o\u0aae\13o\3o\3o\3p\3p\3p\3p\7p\u0ab6\np\fp\16p\u0ab9")
        buf.write("\13p\3p\3p\3q\3q\3r\3r\3r\3r\3s\3s\3t\3t\7t\u0ac7\nt\f")
        buf.write("t\16t\u0aca\13t\3t\3t\7t\u0ace\nt\ft\16t\u0ad1\13t\3t")
        buf.write("\3t\3u\3u\7u\u0ad7\nu\fu\16u\u0ada\13u\3u\3u\7u\u0ade")
        buf.write("\nu\fu\16u\u0ae1\13u\3u\3u\3u\3u\7u\u0ae7\nu\fu\16u\u0aea")
        buf.write("\13u\3u\5u\u0aed\nu\3u\7u\u0af0\nu\fu\16u\u0af3\13u\3")
        buf.write("u\3u\7u\u0af7\nu\fu\16u\u0afa\13u\3u\3u\7u\u0afe\nu\f")
        buf.write("u\16u\u0b01\13u\3u\3u\5u\u0b05\nu\3v\3v\7v\u0b09\nv\f")
        buf.write("v\16v\u0b0c\13v\3v\3v\7v\u0b10\nv\fv\16v\u0b13\13v\3v")
        buf.write("\7v\u0b16\nv\fv\16v\u0b19\13v\3v\7v\u0b1c\nv\fv\16v\u0b1f")
        buf.write("\13v\3v\5v\u0b22\nv\3w\3w\3w\7w\u0b27\nw\fw\16w\u0b2a")
        buf.write("\13w\3w\3w\7w\u0b2e\nw\fw\16w\u0b31\13w\3w\5w\u0b34\n")
        buf.write("w\5w\u0b36\nw\3x\3x\7x\u0b3a\nx\fx\16x\u0b3d\13x\3x\3")
        buf.write("x\7x\u0b41\nx\fx\16x\u0b44\13x\3x\3x\5x\u0b48\nx\3x\7")
        buf.write("x\u0b4b\nx\fx\16x\u0b4e\13x\3x\3x\7x\u0b52\nx\fx\16x\u0b55")
        buf.write("\13x\3x\3x\7x\u0b59\nx\fx\16x\u0b5c\13x\3x\5x\u0b5f\n")
        buf.write("x\3x\7x\u0b62\nx\fx\16x\u0b65\13x\3x\5x\u0b68\nx\3x\7")
        buf.write("x\u0b6b\nx\fx\16x\u0b6e\13x\3x\5x\u0b71\nx\3y\3y\5y\u0b75")
        buf.write("\ny\3z\3z\7z\u0b79\nz\fz\16z\u0b7c\13z\3z\3z\7z\u0b80")
        buf.write("\nz\fz\16z\u0b83\13z\3z\3z\7z\u0b87\nz\fz\16z\u0b8a\13")
        buf.write("z\3z\3z\3z\3z\7z\u0b90\nz\fz\16z\u0b93\13z\3z\5z\u0b96")
        buf.write("\nz\3{\3{\3|\3|\3|\7|\u0b9d\n|\f|\16|\u0ba0\13|\3|\3|")
        buf.write("\7|\u0ba4\n|\f|\16|\u0ba7\13|\3|\3|\5|\u0bab\n|\3|\3|")
        buf.write("\5|\u0baf\n|\3|\5|\u0bb2\n|\3}\3}\7}\u0bb6\n}\f}\16}\u0bb9")
        buf.write("\13}\3}\3}\7}\u0bbd\n}\f}\16}\u0bc0\13}\3}\3}\7}\u0bc4")
        buf.write("\n}\f}\16}\u0bc7\13}\3}\3}\7}\u0bcb\n}\f}\16}\u0bce\13")
        buf.write("}\3}\3}\5}\u0bd2\n}\3}\3}\7}\u0bd6\n}\f}\16}\u0bd9\13")
        buf.write("}\3}\3}\7}\u0bdd\n}\f}\16}\u0be0\13}\3}\3}\7}\u0be4\n")
        buf.write("}\f}\16}\u0be7\13}\3}\3}\7}\u0beb\n}\f}\16}\u0bee\13}")
        buf.write("\3}\5}\u0bf1\n}\3}\7}\u0bf4\n}\f}\16}\u0bf7\13}\3}\5}")
        buf.write("\u0bfa\n}\3}\7}\u0bfd\n}\f}\16}\u0c00\13}\3}\3}\7}\u0c04")
        buf.write("\n}\f}\16}\u0c07\13}\3}\3}\5}\u0c0b\n}\5}\u0c0d\n}\3~")
        buf.write("\3~\7~\u0c11\n~\f~\16~\u0c14\13~\3~\7~\u0c17\n~\f~\16")
        buf.write("~\u0c1a\13~\3~\3~\7~\u0c1e\n~\f~\16~\u0c21\13~\3~\3~\7")
        buf.write("~\u0c25\n~\f~\16~\u0c28\13~\3~\3~\7~\u0c2c\n~\f~\16~\u0c2f")
        buf.write("\13~\5~\u0c31\n~\3~\3~\3~\3\177\3\177\7\177\u0c38\n\177")
        buf.write("\f\177\16\177\u0c3b\13\177\3\177\5\177\u0c3e\n\177\3\177")
        buf.write("\7\177\u0c41\n\177\f\177\16\177\u0c44\13\177\3\177\3\177")
        buf.write("\7\177\u0c48\n\177\f\177\16\177\u0c4b\13\177\3\177\3\177")
        buf.write("\7\177\u0c4f\n\177\f\177\16\177\u0c52\13\177\7\177\u0c54")
        buf.write("\n\177\f\177\16\177\u0c57\13\177\3\177\7\177\u0c5a\n\177")
        buf.write("\f\177\16\177\u0c5d\13\177\3\177\3\177\3\u0080\3\u0080")
        buf.write("\7\u0080\u0c63\n\u0080\f\u0080\16\u0080\u0c66\13\u0080")
        buf.write("\3\u0080\3\u0080\7\u0080\u0c6a\n\u0080\f\u0080\16\u0080")
        buf.write("\u0c6d\13\u0080\3\u0080\7\u0080\u0c70\n\u0080\f\u0080")
        buf.write("\16\u0080\u0c73\13\u0080\3\u0080\7\u0080\u0c76\n\u0080")
        buf.write("\f\u0080\16\u0080\u0c79\13\u0080\3\u0080\5\u0080\u0c7c")
        buf.write("\n\u0080\3\u0080\7\u0080\u0c7f\n\u0080\f\u0080\16\u0080")
        buf.write("\u0c82\13\u0080\3\u0080\3\u0080\7\u0080\u0c86\n\u0080")
        buf.write("\f\u0080\16\u0080\u0c89\13\u0080\3\u0080\3\u0080\5\u0080")
        buf.write("\u0c8d\n\u0080\3\u0080\3\u0080\7\u0080\u0c91\n\u0080\f")
        buf.write("\u0080\16\u0080\u0c94\13\u0080\3\u0080\3\u0080\7\u0080")
        buf.write("\u0c98\n\u0080\f\u0080\16\u0080\u0c9b\13\u0080\3\u0080")
        buf.write("\3\u0080\5\u0080\u0c9f\n\u0080\5\u0080\u0ca1\n\u0080\3")
        buf.write("\u0081\3\u0081\3\u0081\5\u0081\u0ca6\n\u0081\3\u0082\3")
        buf.write("\u0082\7\u0082\u0caa\n\u0082\f\u0082\16\u0082\u0cad\13")
        buf.write("\u0082\3\u0082\3\u0082\3\u0083\3\u0083\7\u0083\u0cb3\n")
        buf.write("\u0083\f\u0083\16\u0083\u0cb6\13\u0083\3\u0083\3\u0083")
        buf.write("\3\u0084\3\u0084\7\u0084\u0cbc\n\u0084\f\u0084\16\u0084")
        buf.write("\u0cbf\13\u0084\3\u0084\3\u0084\7\u0084\u0cc3\n\u0084")
        buf.write("\f\u0084\16\u0084\u0cc6\13\u0084\3\u0084\6\u0084\u0cc9")
        buf.write("\n\u0084\r\u0084\16\u0084\u0cca\3\u0084\7\u0084\u0cce")
        buf.write("\n\u0084\f\u0084\16\u0084\u0cd1\13\u0084\3\u0084\5\u0084")
        buf.write("\u0cd4\n\u0084\3\u0084\7\u0084\u0cd7\n\u0084\f\u0084\16")
        buf.write("\u0084\u0cda\13\u0084\3\u0084\5\u0084\u0cdd\n\u0084\3")
        buf.write("\u0085\3\u0085\7\u0085\u0ce1\n\u0085\f\u0085\16\u0085")
        buf.write("\u0ce4\13\u0085\3\u0085\3\u0085\7\u0085\u0ce8\n\u0085")
        buf.write("\f\u0085\16\u0085\u0ceb\13\u0085\3\u0085\3\u0085\3\u0085")
        buf.write("\3\u0085\7\u0085\u0cf1\n\u0085\f\u0085\16\u0085\u0cf4")
        buf.write("\13\u0085\3\u0085\5\u0085\u0cf7\n\u0085\3\u0085\3\u0085")
        buf.write("\7\u0085\u0cfb\n\u0085\f\u0085\16\u0085\u0cfe\13\u0085")
        buf.write("\3\u0085\3\u0085\3\u0086\3\u0086\7\u0086\u0d04\n\u0086")
        buf.write("\f\u0086\16\u0086\u0d07\13\u0086\3\u0086\3\u0086\3\u0087")
        buf.write("\3\u0087\7\u0087\u0d0d\n\u0087\f\u0087\16\u0087\u0d10")
        buf.write("\13\u0087\3\u0087\3\u0087\3\u0087\5\u0087\u0d15\n\u0087")
        buf.write("\3\u0087\3\u0087\3\u0087\3\u0087\5\u0087\u0d1b\n\u0087")
        buf.write("\3\u0088\5\u0088\u0d1e\n\u0088\3\u0088\7\u0088\u0d21\n")
        buf.write("\u0088\f\u0088\16\u0088\u0d24\13\u0088\3\u0088\3\u0088")
        buf.write("\7\u0088\u0d28\n\u0088\f\u0088\16\u0088\u0d2b\13\u0088")
        buf.write("\3\u0088\3\u0088\5\u0088\u0d2f\n\u0088\3\u0089\3\u0089")
        buf.write("\3\u008a\3\u008a\3\u008b\3\u008b\3\u008c\3\u008c\3\u008d")
        buf.write("\3\u008d\3\u008e\3\u008e\3\u008f\3\u008f\3\u0090\3\u0090")
        buf.write("\3\u0091\3\u0091\3\u0091\3\u0091\3\u0091\5\u0091\u0d46")
        buf.write("\n\u0091\3\u0092\3\u0092\3\u0092\3\u0092\5\u0092\u0d4c")
        buf.write("\n\u0092\3\u0093\3\u0093\3\u0094\3\u0094\3\u0094\5\u0094")
        buf.write("\u0d53\n\u0094\3\u0095\3\u0095\3\u0095\3\u0096\3\u0096")
        buf.write("\6\u0096\u0d5a\n\u0096\r\u0096\16\u0096\u0d5b\3\u0097")
        buf.write("\3\u0097\6\u0097\u0d60\n\u0097\r\u0097\16\u0097\u0d61")
        buf.write("\3\u0098\3\u0098\3\u0098\3\u0098\3\u0098\3\u0098\3\u0098")
        buf.write("\3\u0098\5\u0098\u0d6c\n\u0098\3\u0098\7\u0098\u0d6f\n")
        buf.write("\u0098\f\u0098\16\u0098\u0d72\13\u0098\3\u0099\6\u0099")
        buf.write("\u0d75\n\u0099\r\u0099\16\u0099\u0d76\3\u009a\3\u009a")
        buf.write("\3\u009a\7\u009a\u0d7c\n\u009a\f\u009a\16\u009a\u0d7f")
        buf.write("\13\u009a\5\u009a\u0d81\n\u009a\3\u009b\3\u009b\3\u009c")
        buf.write("\3\u009c\3\u009d\3\u009d\3\u009e\3\u009e\3\u009f\6\u009f")
        buf.write("\u0d8c\n\u009f\r\u009f\16\u009f\u0d8d\3\u00a0\3\u00a0")
        buf.write("\7\u00a0\u0d92\n\u00a0\f\u00a0\16\u00a0\u0d95\13\u00a0")
        buf.write("\3\u00a0\3\u00a0\7\u00a0\u0d99\n\u00a0\f\u00a0\16\u00a0")
        buf.write("\u0d9c\13\u00a0\3\u00a0\5\u00a0\u0d9f\n\u00a0\3\u00a1")
        buf.write("\3\u00a1\3\u00a2\3\u00a2\3\u00a3\3\u00a3\3\u00a4\3\u00a4")
        buf.write("\3\u00a5\3\u00a5\3\u00a6\3\u00a6\3\u00a7\3\u00a7\5\u00a7")
        buf.write("\u0daf\n\u00a7\3\u00a7\7\u00a7\u0db2\n\u00a7\f\u00a7\16")
        buf.write("\u00a7\u0db5\13\u00a7\3\u00a8\3\u00a8\7\u00a8\u0db9\n")
        buf.write("\u00a8\f\u00a8\16\u00a8\u0dbc\13\u00a8\3\u00a8\3\u00a8")
        buf.write("\3\u00a8\3\u00a8\5\u00a8\u0dc2\n\u00a8\3\u00a9\3\u00a9")
        buf.write("\7\u00a9\u0dc6\n\u00a9\f\u00a9\16\u00a9\u0dc9\13\u00a9")
        buf.write("\3\u00a9\3\u00a9\6\u00a9\u0dcd\n\u00a9\r\u00a9\16\u00a9")
        buf.write("\u0dce\3\u00a9\3\u00a9\3\u00a9\3\u00a9\3\u00a9\6\u00a9")
        buf.write("\u0dd6\n\u00a9\r\u00a9\16\u00a9\u0dd7\3\u00a9\3\u00a9")
        buf.write("\5\u00a9\u0ddc\n\u00a9\3\u00aa\3\u00aa\3\u00aa\7\u00aa")
        buf.write("\u0de1\n\u00aa\f\u00aa\16\u00aa\u0de4\13\u00aa\3\u00aa")
        buf.write("\3\u00aa\3\u00ab\3\u00ab\5\u00ab\u0dea\n\u00ab\3\u00ac")
        buf.write("\3\u00ac\3\u00ad\3\u00ad\7\u00ad\u0df0\n\u00ad\f\u00ad")
        buf.write("\16\u00ad\u0df3\13\u00ad\3\u00ad\3\u00ad\7\u00ad\u0df7")
        buf.write("\n\u00ad\f\u00ad\16\u00ad\u0dfa\13\u00ad\3\u00ad\2\2\u00ae")
        buf.write("\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62")
        buf.write("\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082")
        buf.write("\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094")
        buf.write("\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6")
        buf.write("\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6\u00b8")
        buf.write("\u00ba\u00bc\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca")
        buf.write("\u00cc\u00ce\u00d0\u00d2\u00d4\u00d6\u00d8\u00da\u00dc")
        buf.write("\u00de\u00e0\u00e2\u00e4\u00e6\u00e8\u00ea\u00ec\u00ee")
        buf.write("\u00f0\u00f2\u00f4\u00f6\u00f8\u00fa\u00fc\u00fe\u0100")
        buf.write("\u0102\u0104\u0106\u0108\u010a\u010c\u010e\u0110\u0112")
        buf.write("\u0114\u0116\u0118\u011a\u011c\u011e\u0120\u0122\u0124")
        buf.write("\u0126\u0128\u012a\u012c\u012e\u0130\u0132\u0134\u0136")
        buf.write("\u0138\u013a\u013c\u013e\u0140\u0142\u0144\u0146\u0148")
        buf.write("\u014a\u014c\u014e\u0150\u0152\u0154\u0156\u0158\2\37")
        buf.write("\4\2**,,\3\2NO\3\2./\3\2*+\4\2\7\7\35\35\4\2\u0088\u0088")
        buf.write("\u008b\u0092\3\2\u00a0\u00a2\3\2\u00a5\u00a7\4\2==UU\4")
        buf.write("\2::cc\3\2\37#\4\2\64\65\678\3\2\60\63\4\2hhjj\4\2ggi")
        buf.write("i\3\2\24\25\3\2\21\23\4\2\66\66ff\3\2\32\33\3\2qu\4\2")
        buf.write("||\u0081\u0081\3\2mp\4\2hhkk\3\2v{\3\2}\177\3\2\u0082")
        buf.write("\u0084\3\2\u0086\u0087\3\2@G\t\2?GIIQTXX]^k\u0087\u0093")
        buf.write("\u0093\2\u0faa\2\u015b\3\2\2\2\4\u0174\3\2\2\2\6\u018e")
        buf.write("\3\2\2\2\b\u0194\3\2\2\2\n\u01b9\3\2\2\2\f\u01be\3\2\2")
        buf.write("\2\16\u01c1\3\2\2\2\20\u01cb\3\2\2\2\22\u01ce\3\2\2\2")
        buf.write("\24\u01d3\3\2\2\2\26\u01fa\3\2\2\2\30\u01fd\3\2\2\2\32")
        buf.write("\u0258\3\2\2\2\34\u025c\3\2\2\2\36\u026c\3\2\2\2 \u029b")
        buf.write("\3\2\2\2\"\u02bf\3\2\2\2$\u02d7\3\2\2\2&\u02d9\3\2\2\2")
        buf.write("(\u02df\3\2\2\2*\u02ec\3\2\2\2,\u02fd\3\2\2\2.\u032a\3")
        buf.write("\2\2\2\60\u0343\3\2\2\2\62\u0361\3\2\2\2\64\u037a\3\2")
        buf.write("\2\2\66\u0381\3\2\2\28\u0383\3\2\2\2:\u038d\3\2\2\2<\u03b9")
        buf.write("\3\2\2\2>\u03e8\3\2\2\2@\u03fc\3\2\2\2B\u0452\3\2\2\2")
        buf.write("D\u0457\3\2\2\2F\u0471\3\2\2\2H\u049e\3\2\2\2J\u050f\3")
        buf.write("\2\2\2L\u0545\3\2\2\2N\u0585\3\2\2\2P\u0587\3\2\2\2R\u05b6")
        buf.write("\3\2\2\2T\u05c9\3\2\2\2V\u05da\3\2\2\2X\u05fe\3\2\2\2")
        buf.write("Z\u0631\3\2\2\2\\\u0633\3\2\2\2^\u0655\3\2\2\2`\u0679")
        buf.write("\3\2\2\2b\u068f\3\2\2\2d\u0699\3\2\2\2f\u069d\3\2\2\2")
        buf.write("h\u06aa\3\2\2\2j\u06ac\3\2\2\2l\u06c0\3\2\2\2n\u06cf\3")
        buf.write("\2\2\2p\u06d2\3\2\2\2r\u06de\3\2\2\2t\u06ee\3\2\2\2v\u0700")
        buf.write("\3\2\2\2x\u0732\3\2\2\2z\u0743\3\2\2\2|\u076a\3\2\2\2")
        buf.write("~\u0775\3\2\2\2\u0080\u077e\3\2\2\2\u0082\u0787\3\2\2")
        buf.write("\2\u0084\u0791\3\2\2\2\u0086\u0793\3\2\2\2\u0088\u07a6")
        buf.write("\3\2\2\2\u008a\u07a8\3\2\2\2\u008c\u07ea\3\2\2\2\u008e")
        buf.write("\u07ec\3\2\2\2\u0090\u081b\3\2\2\2\u0092\u0825\3\2\2\2")
        buf.write("\u0094\u082d\3\2\2\2\u0096\u082f\3\2\2\2\u0098\u0831\3")
        buf.write("\2\2\2\u009a\u0845\3\2\2\2\u009c\u0859\3\2\2\2\u009e\u0868")
        buf.write("\3\2\2\2\u00a0\u0874\3\2\2\2\u00a2\u088c\3\2\2\2\u00a4")
        buf.write("\u08a1\3\2\2\2\u00a6\u08a4\3\2\2\2\u00a8\u08b3\3\2\2\2")
        buf.write("\u00aa\u08c1\3\2\2\2\u00ac\u08d0\3\2\2\2\u00ae\u08df\3")
        buf.write("\2\2\2\u00b0\u08f1\3\2\2\2\u00b2\u0919\3\2\2\2\u00b4\u0927")
        buf.write("\3\2\2\2\u00b6\u0930\3\2\2\2\u00b8\u0937\3\2\2\2\u00ba")
        buf.write("\u093e\3\2\2\2\u00bc\u0940\3\2\2\2\u00be\u0952\3\2\2\2")
        buf.write("\u00c0\u0954\3\2\2\2\u00c2\u0967\3\2\2\2\u00c4\u0969\3")
        buf.write("\2\2\2\u00c6\u0998\3\2\2\2\u00c8\u09b2\3\2\2\2\u00ca\u09b7")
        buf.write("\3\2\2\2\u00cc\u09c5\3\2\2\2\u00ce\u0a25\3\2\2\2\u00d0")
        buf.write("\u0a28\3\2\2\2\u00d2\u0a59\3\2\2\2\u00d4\u0a5b\3\2\2\2")
        buf.write("\u00d6\u0a9f\3\2\2\2\u00d8\u0aa1\3\2\2\2\u00da\u0aa5\3")
        buf.write("\2\2\2\u00dc\u0aa7\3\2\2\2\u00de\u0ab1\3\2\2\2\u00e0\u0abc")
        buf.write("\3\2\2\2\u00e2\u0abe\3\2\2\2\u00e4\u0ac2\3\2\2\2\u00e6")
        buf.write("\u0ac4\3\2\2\2\u00e8\u0b04\3\2\2\2\u00ea\u0b06\3\2\2\2")
        buf.write("\u00ec\u0b35\3\2\2\2\u00ee\u0b37\3\2\2\2\u00f0\u0b74\3")
        buf.write("\2\2\2\u00f2\u0b95\3\2\2\2\u00f4\u0b97\3\2\2\2\u00f6\u0bb1")
        buf.write("\3\2\2\2\u00f8\u0c0c\3\2\2\2\u00fa\u0c0e\3\2\2\2\u00fc")
        buf.write("\u0c35\3\2\2\2\u00fe\u0ca0\3\2\2\2\u0100\u0ca5\3\2\2\2")
        buf.write("\u0102\u0ca7\3\2\2\2\u0104\u0cb0\3\2\2\2\u0106\u0cb9\3")
        buf.write("\2\2\2\u0108\u0cde\3\2\2\2\u010a\u0d01\3\2\2\2\u010c\u0d1a")
        buf.write("\3\2\2\2\u010e\u0d1d\3\2\2\2\u0110\u0d30\3\2\2\2\u0112")
        buf.write("\u0d32\3\2\2\2\u0114\u0d34\3\2\2\2\u0116\u0d36\3\2\2\2")
        buf.write("\u0118\u0d38\3\2\2\2\u011a\u0d3a\3\2\2\2\u011c\u0d3c\3")
        buf.write("\2\2\2\u011e\u0d3e\3\2\2\2\u0120\u0d45\3\2\2\2\u0122\u0d4b")
        buf.write("\3\2\2\2\u0124\u0d4d\3\2\2\2\u0126\u0d52\3\2\2\2\u0128")
        buf.write("\u0d54\3\2\2\2\u012a\u0d59\3\2\2\2\u012c\u0d5f\3\2\2\2")
        buf.write("\u012e\u0d6b\3\2\2\2\u0130\u0d74\3\2\2\2\u0132\u0d80\3")
        buf.write("\2\2\2\u0134\u0d82\3\2\2\2\u0136\u0d84\3\2\2\2\u0138\u0d86")
        buf.write("\3\2\2\2\u013a\u0d88\3\2\2\2\u013c\u0d8b\3\2\2\2\u013e")
        buf.write("\u0d9e\3\2\2\2\u0140\u0da0\3\2\2\2\u0142\u0da2\3\2\2\2")
        buf.write("\u0144\u0da4\3\2\2\2\u0146\u0da6\3\2\2\2\u0148\u0da8\3")
        buf.write("\2\2\2\u014a\u0daa\3\2\2\2\u014c\u0dae\3\2\2\2\u014e\u0dc1")
        buf.write("\3\2\2\2\u0150\u0ddb\3\2\2\2\u0152\u0ddd\3\2\2\2\u0154")
        buf.write("\u0de9\3\2\2\2\u0156\u0deb\3\2\2\2\u0158\u0ded\3\2\2\2")
        buf.write("\u015a\u015c\5\6\4\2\u015b\u015a\3\2\2\2\u015b\u015c\3")
        buf.write("\2\2\2\u015c\u0160\3\2\2\2\u015d\u015f\7\7\2\2\u015e\u015d")
        buf.write("\3\2\2\2\u015f\u0162\3\2\2\2\u0160\u015e\3\2\2\2\u0160")
        buf.write("\u0161\3\2\2\2\u0161\u0166\3\2\2\2\u0162\u0160\3\2\2\2")
        buf.write("\u0163\u0165\5\b\5\2\u0164\u0163\3\2\2\2\u0165\u0168\3")
        buf.write("\2\2\2\u0166\u0164\3\2\2\2\u0166\u0167\3\2\2\2\u0167\u0169")
        buf.write("\3\2\2\2\u0168\u0166\3\2\2\2\u0169\u016a\5\n\6\2\u016a")
        buf.write("\u016e\5\f\7\2\u016b\u016d\5\22\n\2\u016c\u016b\3\2\2")
        buf.write("\2\u016d\u0170\3\2\2\2\u016e\u016c\3\2\2\2\u016e\u016f")
        buf.write("\3\2\2\2\u016f\u0171\3\2\2\2\u0170\u016e\3\2\2\2\u0171")
        buf.write("\u0172\7\2\2\3\u0172\3\3\2\2\2\u0173\u0175\5\6\4\2\u0174")
        buf.write("\u0173\3\2\2\2\u0174\u0175\3\2\2\2\u0175\u0179\3\2\2\2")
        buf.write("\u0176\u0178\7\7\2\2\u0177\u0176\3\2\2\2\u0178\u017b\3")
        buf.write("\2\2\2\u0179\u0177\3\2\2\2\u0179\u017a\3\2\2\2\u017a\u017f")
        buf.write("\3\2\2\2\u017b\u0179\3\2\2\2\u017c\u017e\5\b\5\2\u017d")
        buf.write("\u017c\3\2\2\2\u017e\u0181\3\2\2\2\u017f\u017d\3\2\2\2")
        buf.write("\u017f\u0180\3\2\2\2\u0180\u0182\3\2\2\2\u0181\u017f\3")
        buf.write("\2\2\2\u0182\u0183\5\n\6\2\u0183\u0189\5\f\7\2\u0184\u0185")
        buf.write("\5\u0080A\2\u0185\u0186\5\u0092J\2\u0186\u0188\3\2\2\2")
        buf.write("\u0187\u0184\3\2\2\2\u0188\u018b\3\2\2\2\u0189\u0187\3")
        buf.write("\2\2\2\u0189\u018a\3\2\2\2\u018a\u018c\3\2\2\2\u018b\u0189")
        buf.write("\3\2\2\2\u018c\u018d\7\2\2\3\u018d\5\3\2\2\2\u018e\u0190")
        buf.write("\7\3\2\2\u018f\u0191\7\7\2\2\u0190\u018f\3\2\2\2\u0191")
        buf.write("\u0192\3\2\2\2\u0192\u0190\3\2\2\2\u0192\u0193\3\2\2\2")
        buf.write("\u0193\7\3\2\2\2\u0194\u0195\t\2\2\2\u0195\u0199\7?\2")
        buf.write("\2\u0196\u0198\7\7\2\2\u0197\u0196\3\2\2\2\u0198\u019b")
        buf.write("\3\2\2\2\u0199\u0197\3\2\2\2\u0199\u019a\3\2\2\2\u019a")
        buf.write("\u019c\3\2\2\2\u019b\u0199\3\2\2\2\u019c\u01a0\7\34\2")
        buf.write("\2\u019d\u019f\7\7\2\2\u019e\u019d\3\2\2\2\u019f\u01a2")
        buf.write("\3\2\2\2\u01a0\u019e\3\2\2\2\u01a0\u01a1\3\2\2\2\u01a1")
        buf.write("\u01ac\3\2\2\2\u01a2\u01a0\3\2\2\2\u01a3\u01a5\7\r\2\2")
        buf.write("\u01a4\u01a6\5\u0154\u00ab\2\u01a5\u01a4\3\2\2\2\u01a6")
        buf.write("\u01a7\3\2\2\2\u01a7\u01a5\3\2\2\2\u01a7\u01a8\3\2\2\2")
        buf.write("\u01a8\u01a9\3\2\2\2\u01a9\u01aa\7\16\2\2\u01aa\u01ad")
        buf.write("\3\2\2\2\u01ab\u01ad\5\u0154\u00ab\2\u01ac\u01a3\3\2\2")
        buf.write("\2\u01ac\u01ab\3\2\2\2\u01ad\u01b1\3\2\2\2\u01ae\u01b0")
        buf.write("\7\7\2\2\u01af\u01ae\3\2\2\2\u01b0\u01b3\3\2\2\2\u01b1")
        buf.write("\u01af\3\2\2\2\u01b1\u01b2\3\2\2\2\u01b2\t\3\2\2\2\u01b3")
        buf.write("\u01b1\3\2\2\2\u01b4\u01b5\7H\2\2\u01b5\u01b7\5\u0158")
        buf.write("\u00ad\2\u01b6\u01b8\5\u0092J\2\u01b7\u01b6\3\2\2\2\u01b7")
        buf.write("\u01b8\3\2\2\2\u01b8\u01ba\3\2\2\2\u01b9\u01b4\3\2\2\2")
        buf.write("\u01b9\u01ba\3\2\2\2\u01ba\13\3\2\2\2\u01bb\u01bd\5\16")
        buf.write("\b\2\u01bc\u01bb\3\2\2\2\u01bd\u01c0\3\2\2\2\u01be\u01bc")
        buf.write("\3\2\2\2\u01be\u01bf\3\2\2\2\u01bf\r\3\2\2\2\u01c0\u01be")
        buf.write("\3\2\2\2\u01c1\u01c2\7I\2\2\u01c2\u01c6\5\u0158\u00ad")
        buf.write("\2\u01c3\u01c4\7\t\2\2\u01c4\u01c7\7\21\2\2\u01c5\u01c7")
        buf.write("\5\20\t\2\u01c6\u01c3\3\2\2\2\u01c6\u01c5\3\2\2\2\u01c6")
        buf.write("\u01c7\3\2\2\2\u01c7\u01c9\3\2\2\2\u01c8\u01ca\5\u0092")
        buf.write("J\2\u01c9\u01c8\3\2\2\2\u01c9\u01ca\3\2\2\2\u01ca\17\3")
        buf.write("\2\2\2\u01cb\u01cc\7f\2\2\u01cc\u01cd\5\u0156\u00ac\2")
        buf.write("\u01cd\21\3\2\2\2\u01ce\u01d0\5\26\f\2\u01cf\u01d1\5\u0094")
        buf.write("K\2\u01d0\u01cf\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\23\3")
        buf.write("\2\2\2\u01d2\u01d4\5\u012a\u0096\2\u01d3\u01d2\3\2\2\2")
        buf.write("\u01d3\u01d4\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5\u01d9\7")
        buf.write("P\2\2\u01d6\u01d8\7\7\2\2\u01d7\u01d6\3\2\2\2\u01d8\u01db")
        buf.write("\3\2\2\2\u01d9\u01d7\3\2\2\2\u01d9\u01da\3\2\2\2\u01da")
        buf.write("\u01dc\3\2\2\2\u01db\u01d9\3\2\2\2\u01dc\u01e4\5\u0156")
        buf.write("\u00ac\2\u01dd\u01df\7\7\2\2\u01de\u01dd\3\2\2\2\u01df")
        buf.write("\u01e2\3\2\2\2\u01e0\u01de\3\2\2\2\u01e0\u01e1\3\2\2\2")
        buf.write("\u01e1\u01e3\3\2\2\2\u01e2\u01e0\3\2\2\2\u01e3\u01e5\5")
        buf.write(",\27\2\u01e4\u01e0\3\2\2\2\u01e4\u01e5\3\2\2\2\u01e5\u01e9")
        buf.write("\3\2\2\2\u01e6\u01e8\7\7\2\2\u01e7\u01e6\3\2\2\2\u01e8")
        buf.write("\u01eb\3\2\2\2\u01e9\u01e7\3\2\2\2\u01e9\u01ea\3\2\2\2")
        buf.write("\u01ea\u01ec\3\2\2\2\u01eb\u01e9\3\2\2\2\u01ec\u01f0\7")
        buf.write("\36\2\2\u01ed\u01ef\7\7\2\2\u01ee\u01ed\3\2\2\2\u01ef")
        buf.write("\u01f2\3\2\2\2\u01f0\u01ee\3\2\2\2\u01f0\u01f1\3\2\2\2")
        buf.write("\u01f1\u01f3\3\2\2\2\u01f2\u01f0\3\2\2\2\u01f3\u01f4\5")
        buf.write("b\62\2\u01f4\25\3\2\2\2\u01f5\u01fb\5\30\r\2\u01f6\u01fb")
        buf.write("\5V,\2\u01f7\u01fb\5@!\2\u01f8\u01fb\5H%\2\u01f9\u01fb")
        buf.write("\5\24\13\2\u01fa\u01f5\3\2\2\2\u01fa\u01f6\3\2\2\2\u01fa")
        buf.write("\u01f7\3\2\2\2\u01fa\u01f8\3\2\2\2\u01fa\u01f9\3\2\2\2")
        buf.write("\u01fb\27\3\2\2\2\u01fc\u01fe\5\u012a\u0096\2\u01fd\u01fc")
        buf.write("\3\2\2\2\u01fd\u01fe\3\2\2\2\u01fe\u020a\3\2\2\2\u01ff")
        buf.write("\u020b\7J\2\2\u0200\u0204\7L\2\2\u0201\u0203\7\7\2\2\u0202")
        buf.write("\u0201\3\2\2\2\u0203\u0206\3\2\2\2\u0204\u0202\3\2\2\2")
        buf.write("\u0204\u0205\3\2\2\2\u0205\u0208\3\2\2\2\u0206\u0204\3")
        buf.write("\2\2\2\u0207\u0200\3\2\2\2\u0207\u0208\3\2\2\2\u0208\u0209")
        buf.write("\3\2\2\2\u0209\u020b\7K\2\2\u020a\u01ff\3\2\2\2\u020a")
        buf.write("\u0207\3\2\2\2\u020b\u020f\3\2\2\2\u020c\u020e\7\7\2\2")
        buf.write("\u020d\u020c\3\2\2\2\u020e\u0211\3\2\2\2\u020f\u020d\3")
        buf.write("\2\2\2\u020f\u0210\3\2\2\2\u0210\u0212\3\2\2\2\u0211\u020f")
        buf.write("\3\2\2\2\u0212\u021a\5\u0156\u00ac\2\u0213\u0215\7\7\2")
        buf.write("\2\u0214\u0213\3\2\2\2\u0215\u0218\3\2\2\2\u0216\u0214")
        buf.write("\3\2\2\2\u0216\u0217\3\2\2\2\u0217\u0219\3\2\2\2\u0218")
        buf.write("\u0216\3\2\2\2\u0219\u021b\5,\27\2\u021a\u0216\3\2\2\2")
        buf.write("\u021a\u021b\3\2\2\2\u021b\u0223\3\2\2\2\u021c\u021e\7")
        buf.write("\7\2\2\u021d\u021c\3\2\2\2\u021e\u0221\3\2\2\2\u021f\u021d")
        buf.write("\3\2\2\2\u021f\u0220\3\2\2\2\u0220\u0222\3\2\2\2\u0221")
        buf.write("\u021f\3\2\2\2\u0222\u0224\5\32\16\2\u0223\u021f\3\2\2")
        buf.write("\2\u0223\u0224\3\2\2\2\u0224\u0233\3\2\2\2\u0225\u0227")
        buf.write("\7\7\2\2\u0226\u0225\3\2\2\2\u0227\u022a\3\2\2\2\u0228")
        buf.write("\u0226\3\2\2\2\u0228\u0229\3\2\2\2\u0229\u022b\3\2\2\2")
        buf.write("\u022a\u0228\3\2\2\2\u022b\u022f\7\34\2\2\u022c\u022e")
        buf.write("\7\7\2\2\u022d\u022c\3\2\2\2\u022e\u0231\3\2\2\2\u022f")
        buf.write("\u022d\3\2\2\2\u022f\u0230\3\2\2\2\u0230\u0232\3\2\2\2")
        buf.write("\u0231\u022f\3\2\2\2\u0232\u0234\5\"\22\2\u0233\u0228")
        buf.write("\3\2\2\2\u0233\u0234\3\2\2\2\u0234\u023c\3\2\2\2\u0235")
        buf.write("\u0237\7\7\2\2\u0236\u0235\3\2\2\2\u0237\u023a\3\2\2\2")
        buf.write("\u0238\u0236\3\2\2\2\u0238\u0239\3\2\2\2\u0239\u023b\3")
        buf.write("\2\2\2\u023a\u0238\3\2\2\2\u023b\u023d\5\60\31\2\u023c")
        buf.write("\u0238\3\2\2\2\u023c\u023d\3\2\2\2\u023d\u024c\3\2\2\2")
        buf.write("\u023e\u0240\7\7\2\2\u023f\u023e\3\2\2\2\u0240\u0243\3")
        buf.write("\2\2\2\u0241\u023f\3\2\2\2\u0241\u0242\3\2\2\2\u0242\u0244")
        buf.write("\3\2\2\2\u0243\u0241\3\2\2\2\u0244\u024d\5\34\17\2\u0245")
        buf.write("\u0247\7\7\2\2\u0246\u0245\3\2\2\2\u0247\u024a\3\2\2\2")
        buf.write("\u0248\u0246\3\2\2\2\u0248\u0249\3\2\2\2\u0249\u024b\3")
        buf.write("\2\2\2\u024a\u0248\3\2\2\2\u024b\u024d\5\\/\2\u024c\u0241")
        buf.write("\3\2\2\2\u024c\u0248\3\2\2\2\u024c\u024d\3\2\2\2\u024d")
        buf.write("\31\3\2\2\2\u024e\u0250\5\u012a\u0096\2\u024f\u024e\3")
        buf.write("\2\2\2\u024f\u0250\3\2\2\2\u0250\u0251\3\2\2\2\u0251\u0255")
        buf.write("\7Q\2\2\u0252\u0254\7\7\2\2\u0253\u0252\3\2\2\2\u0254")
        buf.write("\u0257\3\2\2\2\u0255\u0253\3\2\2\2\u0255\u0256\3\2\2\2")
        buf.write("\u0256\u0259\3\2\2\2\u0257\u0255\3\2\2\2\u0258\u024f\3")
        buf.write("\2\2\2\u0258\u0259\3\2\2\2\u0259\u025a\3\2\2\2\u025a\u025b")
        buf.write("\5\36\20\2\u025b\33\3\2\2\2\u025c\u0260\7\17\2\2\u025d")
        buf.write("\u025f\7\7\2\2\u025e\u025d\3\2\2\2\u025f\u0262\3\2\2\2")
        buf.write("\u0260\u025e\3\2\2\2\u0260\u0261\3\2\2\2\u0261\u0263\3")
        buf.write("\2\2\2\u0262\u0260\3\2\2\2\u0263\u0267\5\64\33\2\u0264")
        buf.write("\u0266\7\7\2\2\u0265\u0264\3\2\2\2\u0266\u0269\3\2\2\2")
        buf.write("\u0267\u0265\3\2\2\2\u0267\u0268\3\2\2\2\u0268\u026a\3")
        buf.write("\2\2\2\u0269\u0267\3\2\2\2\u026a\u026b\7\20\2\2\u026b")
        buf.write("\35\3\2\2\2\u026c\u0270\7\13\2\2\u026d\u026f\7\7\2\2\u026e")
        buf.write("\u026d\3\2\2\2\u026f\u0272\3\2\2\2\u0270\u026e\3\2\2\2")
        buf.write("\u0270\u0271\3\2\2\2\u0271\u0290\3\2\2\2\u0272\u0270\3")
        buf.write("\2\2\2\u0273\u0284\5 \21\2\u0274\u0276\7\7\2\2\u0275\u0274")
        buf.write("\3\2\2\2\u0276\u0279\3\2\2\2\u0277\u0275\3\2\2\2\u0277")
        buf.write("\u0278\3\2\2\2\u0278\u027a\3\2\2\2\u0279\u0277\3\2\2\2")
        buf.write("\u027a\u027e\7\n\2\2\u027b\u027d\7\7\2\2\u027c\u027b\3")
        buf.write("\2\2\2\u027d\u0280\3\2\2\2\u027e\u027c\3\2\2\2\u027e\u027f")
        buf.write("\3\2\2\2\u027f\u0281\3\2\2\2\u0280\u027e\3\2\2\2\u0281")
        buf.write("\u0283\5 \21\2\u0282\u0277\3\2\2\2\u0283\u0286\3\2\2\2")
        buf.write("\u0284\u0282\3\2\2\2\u0284\u0285\3\2\2\2\u0285\u028e\3")
        buf.write("\2\2\2\u0286\u0284\3\2\2\2\u0287\u0289\7\7\2\2\u0288\u0287")
        buf.write("\3\2\2\2\u0289\u028c\3\2\2\2\u028a\u0288\3\2\2\2\u028a")
        buf.write("\u028b\3\2\2\2\u028b\u028d\3\2\2\2\u028c\u028a\3\2\2\2")
        buf.write("\u028d\u028f\7\n\2\2\u028e\u028a\3\2\2\2\u028e\u028f\3")
        buf.write("\2\2\2\u028f\u0291\3\2\2\2\u0290\u0273\3\2\2\2\u0290\u0291")
        buf.write("\3\2\2\2\u0291\u0295\3\2\2\2\u0292\u0294\7\7\2\2\u0293")
        buf.write("\u0292\3\2\2\2\u0294\u0297\3\2\2\2\u0295\u0293\3\2\2\2")
        buf.write("\u0295\u0296\3\2\2\2\u0296\u0298\3\2\2\2\u0297\u0295\3")
        buf.write("\2\2\2\u0298\u0299\7\f\2\2\u0299\37\3\2\2\2\u029a\u029c")
        buf.write("\5\u012a\u0096\2\u029b\u029a\3\2\2\2\u029b\u029c\3\2\2")
        buf.write("\2\u029c\u029e\3\2\2\2\u029d\u029f\t\3\2\2\u029e\u029d")
        buf.write("\3\2\2\2\u029e\u029f\3\2\2\2\u029f\u02a3\3\2\2\2\u02a0")
        buf.write("\u02a2\7\7\2\2\u02a1\u02a0\3\2\2\2\u02a2\u02a5\3\2\2\2")
        buf.write("\u02a3\u02a1\3\2\2\2\u02a3\u02a4\3\2\2\2\u02a4\u02a6\3")
        buf.write("\2\2\2\u02a5\u02a3\3\2\2\2\u02a6\u02a7\5\u0156\u00ac\2")
        buf.write("\u02a7\u02ab\7\34\2\2\u02a8\u02aa\7\7\2\2\u02a9\u02a8")
        buf.write("\3\2\2\2\u02aa\u02ad\3\2\2\2\u02ab\u02a9\3\2\2\2\u02ab")
        buf.write("\u02ac\3\2\2\2\u02ac\u02ae\3\2\2\2\u02ad\u02ab\3\2\2\2")
        buf.write("\u02ae\u02bd\5b\62\2\u02af\u02b1\7\7\2\2\u02b0\u02af\3")
        buf.write("\2\2\2\u02b1\u02b4\3\2\2\2\u02b2\u02b0\3\2\2\2\u02b2\u02b3")
        buf.write("\3\2\2\2\u02b3\u02b5\3\2\2\2\u02b4\u02b2\3\2\2\2\u02b5")
        buf.write("\u02b9\7\36\2\2\u02b6\u02b8\7\7\2\2\u02b7\u02b6\3\2\2")
        buf.write("\2\u02b8\u02bb\3\2\2\2\u02b9\u02b7\3\2\2\2\u02b9\u02ba")
        buf.write("\3\2\2\2\u02ba\u02bc\3\2\2\2\u02bb\u02b9\3\2\2\2\u02bc")
        buf.write("\u02be\5\u0096L\2\u02bd\u02b2\3\2\2\2\u02bd\u02be\3\2")
        buf.write("\2\2\u02be!\3\2\2\2\u02bf\u02d0\5(\25\2\u02c0\u02c2\7")
        buf.write("\7\2\2\u02c1\u02c0\3\2\2\2\u02c2\u02c5\3\2\2\2\u02c3\u02c1")
        buf.write("\3\2\2\2\u02c3\u02c4\3\2\2\2\u02c4\u02c6\3\2\2\2\u02c5")
        buf.write("\u02c3\3\2\2\2\u02c6\u02ca\7\n\2\2\u02c7\u02c9\7\7\2\2")
        buf.write("\u02c8\u02c7\3\2\2\2\u02c9\u02cc\3\2\2\2\u02ca\u02c8\3")
        buf.write("\2\2\2\u02ca\u02cb\3\2\2\2\u02cb\u02cd\3\2\2\2\u02cc\u02ca")
        buf.write("\3\2\2\2\u02cd\u02cf\5(\25\2\u02ce\u02c3\3\2\2\2\u02cf")
        buf.write("\u02d2\3\2\2\2\u02d0\u02ce\3\2\2\2\u02d0\u02d1\3\2\2\2")
        buf.write("\u02d1#\3\2\2\2\u02d2\u02d0\3\2\2\2\u02d3\u02d8\5&\24")
        buf.write("\2\u02d4\u02d8\5*\26\2\u02d5\u02d8\5j\66\2\u02d6\u02d8")
        buf.write("\5t;\2\u02d7\u02d3\3\2\2\2\u02d7\u02d4\3\2\2\2\u02d7\u02d5")
        buf.write("\3\2\2\2\u02d7\u02d6\3\2\2\2\u02d8%\3\2\2\2\u02d9\u02da")
        buf.write("\5j\66\2\u02da\u02db\5\u00ceh\2\u02db\'\3\2\2\2\u02dc")
        buf.write("\u02de\5\u014c\u00a7\2\u02dd\u02dc\3\2\2\2\u02de\u02e1")
        buf.write("\3\2\2\2\u02df\u02dd\3\2\2\2\u02df\u02e0\3\2\2\2\u02e0")
        buf.write("\u02e5\3\2\2\2\u02e1\u02df\3\2\2\2\u02e2\u02e4\7\7\2\2")
        buf.write("\u02e3\u02e2\3\2\2\2\u02e4\u02e7\3\2\2\2\u02e5\u02e3\3")
        buf.write("\2\2\2\u02e5\u02e6\3\2\2\2\u02e6\u02e8\3\2\2\2\u02e7\u02e5")
        buf.write("\3\2\2\2\u02e8\u02e9\5$\23\2\u02e9)\3\2\2\2\u02ea\u02ed")
        buf.write("\5j\66\2\u02eb\u02ed\5t;\2\u02ec\u02ea\3\2\2\2\u02ec\u02eb")
        buf.write("\3\2\2\2\u02ed\u02f1\3\2\2\2\u02ee\u02f0\7\7\2\2\u02ef")
        buf.write("\u02ee\3\2\2\2\u02f0\u02f3\3\2\2\2\u02f1\u02ef\3\2\2\2")
        buf.write("\u02f1\u02f2\3\2\2\2\u02f2\u02f4\3\2\2\2\u02f3\u02f1\3")
        buf.write("\2\2\2\u02f4\u02f8\7R\2\2\u02f5\u02f7\7\7\2\2\u02f6\u02f5")
        buf.write("\3\2\2\2\u02f7\u02fa\3\2\2\2\u02f8\u02f6\3\2\2\2\u02f8")
        buf.write("\u02f9\3\2\2\2\u02f9\u02fb\3\2\2\2\u02fa\u02f8\3\2\2\2")
        buf.write("\u02fb\u02fc\5\u0096L\2\u02fc+\3\2\2\2\u02fd\u0301\7\60")
        buf.write("\2\2\u02fe\u0300\7\7\2\2\u02ff\u02fe\3\2\2\2\u0300\u0303")
        buf.write("\3\2\2\2\u0301\u02ff\3\2\2\2\u0301\u0302\3\2\2\2\u0302")
        buf.write("\u0304\3\2\2\2\u0303\u0301\3\2\2\2\u0304\u0315\5.\30\2")
        buf.write("\u0305\u0307\7\7\2\2\u0306\u0305\3\2\2\2\u0307\u030a\3")
        buf.write("\2\2\2\u0308\u0306\3\2\2\2\u0308\u0309\3\2\2\2\u0309\u030b")
        buf.write("\3\2\2\2\u030a\u0308\3\2\2\2\u030b\u030f\7\n\2\2\u030c")
        buf.write("\u030e\7\7\2\2\u030d\u030c\3\2\2\2\u030e\u0311\3\2\2\2")
        buf.write("\u030f\u030d\3\2\2\2\u030f\u0310\3\2\2\2\u0310\u0312\3")
        buf.write("\2\2\2\u0311\u030f\3\2\2\2\u0312\u0314\5.\30\2\u0313\u0308")
        buf.write("\3\2\2\2\u0314\u0317\3\2\2\2\u0315\u0313\3\2\2\2\u0315")
        buf.write("\u0316\3\2\2\2\u0316\u031f\3\2\2\2\u0317\u0315\3\2\2\2")
        buf.write("\u0318\u031a\7\7\2\2\u0319\u0318\3\2\2\2\u031a\u031d\3")
        buf.write("\2\2\2\u031b\u0319\3\2\2\2\u031b\u031c\3\2\2\2\u031c\u031e")
        buf.write("\3\2\2\2\u031d\u031b\3\2\2\2\u031e\u0320\7\n\2\2\u031f")
        buf.write("\u031b\3\2\2\2\u031f\u0320\3\2\2\2\u0320\u0324\3\2\2\2")
        buf.write("\u0321\u0323\7\7\2\2\u0322\u0321\3\2\2\2\u0323\u0326\3")
        buf.write("\2\2\2\u0324\u0322\3\2\2\2\u0324\u0325\3\2\2\2\u0325\u0327")
        buf.write("\3\2\2\2\u0326\u0324\3\2\2\2\u0327\u0328\7\61\2\2\u0328")
        buf.write("-\3\2\2\2\u0329\u032b\5\u013c\u009f\2\u032a\u0329\3\2")
        buf.write("\2\2\u032a\u032b\3\2\2\2\u032b\u032f\3\2\2\2\u032c\u032e")
        buf.write("\7\7\2\2\u032d\u032c\3\2\2\2\u032e\u0331\3\2\2\2\u032f")
        buf.write("\u032d\3\2\2\2\u032f\u0330\3\2\2\2\u0330\u0332\3\2\2\2")
        buf.write("\u0331\u032f\3\2\2\2\u0332\u0341\5\u0156\u00ac\2\u0333")
        buf.write("\u0335\7\7\2\2\u0334\u0333\3\2\2\2\u0335\u0338\3\2\2\2")
        buf.write("\u0336\u0334\3\2\2\2\u0336\u0337\3\2\2\2\u0337\u0339\3")
        buf.write("\2\2\2\u0338\u0336\3\2\2\2\u0339\u033d\7\34\2\2\u033a")
        buf.write("\u033c\7\7\2\2\u033b\u033a\3\2\2\2\u033c\u033f\3\2\2\2")
        buf.write("\u033d\u033b\3\2\2\2\u033d\u033e\3\2\2\2\u033e\u0340\3")
        buf.write("\2\2\2\u033f\u033d\3\2\2\2\u0340\u0342\5b\62\2\u0341\u0336")
        buf.write("\3\2\2\2\u0341\u0342\3\2\2\2\u0342/\3\2\2\2\u0343\u0347")
        buf.write("\7X\2\2\u0344\u0346\7\7\2\2\u0345\u0344\3\2\2\2\u0346")
        buf.write("\u0349\3\2\2\2\u0347\u0345\3\2\2\2\u0347\u0348\3\2\2\2")
        buf.write("\u0348\u034a\3\2\2\2\u0349\u0347\3\2\2\2\u034a\u035b\5")
        buf.write("\62\32\2\u034b\u034d\7\7\2\2\u034c\u034b\3\2\2\2\u034d")
        buf.write("\u0350\3\2\2\2\u034e\u034c\3\2\2\2\u034e\u034f\3\2\2\2")
        buf.write("\u034f\u0351\3\2\2\2\u0350\u034e\3\2\2\2\u0351\u0355\7")
        buf.write("\n\2\2\u0352\u0354\7\7\2\2\u0353\u0352\3\2\2\2\u0354\u0357")
        buf.write("\3\2\2\2\u0355\u0353\3\2\2\2\u0355\u0356\3\2\2\2\u0356")
        buf.write("\u0358\3\2\2\2\u0357\u0355\3\2\2\2\u0358\u035a\5\62\32")
        buf.write("\2\u0359\u034e\3\2\2\2\u035a\u035d\3\2\2\2\u035b\u0359")
        buf.write("\3\2\2\2\u035b\u035c\3\2\2\2\u035c\61\3\2\2\2\u035d\u035b")
        buf.write("\3\2\2\2\u035e\u0360\5\u014c\u00a7\2\u035f\u035e\3\2\2")
        buf.write("\2\u0360\u0363\3\2\2\2\u0361\u035f\3\2\2\2\u0361\u0362")
        buf.write("\3\2\2\2\u0362\u0364\3\2\2\2\u0363\u0361\3\2\2\2\u0364")
        buf.write("\u0368\5\u0156\u00ac\2\u0365\u0367\7\7\2\2\u0366\u0365")
        buf.write("\3\2\2\2\u0367\u036a\3\2\2\2\u0368\u0366\3\2\2\2\u0368")
        buf.write("\u0369\3\2\2\2\u0369\u036b\3\2\2\2\u036a\u0368\3\2\2\2")
        buf.write("\u036b\u036f\7\34\2\2\u036c\u036e\7\7\2\2\u036d\u036c")
        buf.write("\3\2\2\2\u036e\u0371\3\2\2\2\u036f\u036d\3\2\2\2\u036f")
        buf.write("\u0370\3\2\2\2\u0370\u0372\3\2\2\2\u0371\u036f\3\2\2\2")
        buf.write("\u0372\u0373\5b\62\2\u0373\63\3\2\2\2\u0374\u0376\5\66")
        buf.write("\34\2\u0375\u0377\5\u0094K\2\u0376\u0375\3\2\2\2\u0376")
        buf.write("\u0377\3\2\2\2\u0377\u0379\3\2\2\2\u0378\u0374\3\2\2\2")
        buf.write("\u0379\u037c\3\2\2\2\u037a\u0378\3\2\2\2\u037a\u037b\3")
        buf.write("\2\2\2\u037b\65\3\2\2\2\u037c\u037a\3\2\2\2\u037d\u0382")
        buf.write("\5\26\f\2\u037e\u0382\5:\36\2\u037f\u0382\58\35\2\u0380")
        buf.write("\u0382\5X-\2\u0381\u037d\3\2\2\2\u0381\u037e\3\2\2\2\u0381")
        buf.write("\u037f\3\2\2\2\u0381\u0380\3\2\2\2\u0382\67\3\2\2\2\u0383")
        buf.write("\u0387\7T\2\2\u0384\u0386\7\7\2\2\u0385\u0384\3\2\2\2")
        buf.write("\u0386\u0389\3\2\2\2\u0387\u0385\3\2\2\2\u0387\u0388\3")
        buf.write("\2\2\2\u0388\u038a\3\2\2\2\u0389\u0387\3\2\2\2\u038a\u038b")
        buf.write("\5\u0086D\2\u038b9\3\2\2\2\u038c\u038e\5\u012a\u0096\2")
        buf.write("\u038d\u038c\3\2\2\2\u038d\u038e\3\2\2\2\u038e\u038f\3")
        buf.write("\2\2\2\u038f\u0393\7S\2\2\u0390\u0392\7\7\2\2\u0391\u0390")
        buf.write("\3\2\2\2\u0392\u0395\3\2\2\2\u0393\u0391\3\2\2\2\u0393")
        buf.write("\u0394\3\2\2\2\u0394\u0396\3\2\2\2\u0395\u0393\3\2\2\2")
        buf.write("\u0396\u039e\7M\2\2\u0397\u0399\7\7\2\2\u0398\u0397\3")
        buf.write("\2\2\2\u0399\u039c\3\2\2\2\u039a\u0398\3\2\2\2\u039a\u039b")
        buf.write("\3\2\2\2\u039b\u039d\3\2\2\2\u039c\u039a\3\2\2\2\u039d")
        buf.write("\u039f\5\u0156\u00ac\2\u039e\u039a\3\2\2\2\u039e\u039f")
        buf.write("\3\2\2\2\u039f\u03ae\3\2\2\2\u03a0\u03a2\7\7\2\2\u03a1")
        buf.write("\u03a0\3\2\2\2\u03a2\u03a5\3\2\2\2\u03a3\u03a1\3\2\2\2")
        buf.write("\u03a3\u03a4\3\2\2\2\u03a4\u03a6\3\2\2\2\u03a5\u03a3\3")
        buf.write("\2\2\2\u03a6\u03aa\7\34\2\2\u03a7\u03a9\7\7\2\2\u03a8")
        buf.write("\u03a7\3\2\2\2\u03a9\u03ac\3\2\2\2\u03aa\u03a8\3\2\2\2")
        buf.write("\u03aa\u03ab\3\2\2\2\u03ab\u03ad\3\2\2\2\u03ac\u03aa\3")
        buf.write("\2\2\2\u03ad\u03af\5\"\22\2\u03ae\u03a3\3\2\2\2\u03ae")
        buf.write("\u03af\3\2\2\2\u03af\u03b7\3\2\2\2\u03b0\u03b2\7\7\2\2")
        buf.write("\u03b1\u03b0\3\2\2\2\u03b2\u03b5\3\2\2\2\u03b3\u03b1\3")
        buf.write("\2\2\2\u03b3\u03b4\3\2\2\2\u03b4\u03b6\3\2\2\2\u03b5\u03b3")
        buf.write("\3\2\2\2\u03b6\u03b8\5\34\17\2\u03b7\u03b3\3\2\2\2\u03b7")
        buf.write("\u03b8\3\2\2\2\u03b8;\3\2\2\2\u03b9\u03bd\7\13\2\2\u03ba")
        buf.write("\u03bc\7\7\2\2\u03bb\u03ba\3\2\2\2\u03bc\u03bf\3\2\2\2")
        buf.write("\u03bd\u03bb\3\2\2\2\u03bd\u03be\3\2\2\2\u03be\u03dd\3")
        buf.write("\2\2\2\u03bf\u03bd\3\2\2\2\u03c0\u03d1\5> \2\u03c1\u03c3")
        buf.write("\7\7\2\2\u03c2\u03c1\3\2\2\2\u03c3\u03c6\3\2\2\2\u03c4")
        buf.write("\u03c2\3\2\2\2\u03c4\u03c5\3\2\2\2\u03c5\u03c7\3\2\2\2")
        buf.write("\u03c6\u03c4\3\2\2\2\u03c7\u03cb\7\n\2\2\u03c8\u03ca\7")
        buf.write("\7\2\2\u03c9\u03c8\3\2\2\2\u03ca\u03cd\3\2\2\2\u03cb\u03c9")
        buf.write("\3\2\2\2\u03cb\u03cc\3\2\2\2\u03cc\u03ce\3\2\2\2\u03cd")
        buf.write("\u03cb\3\2\2\2\u03ce\u03d0\5> \2\u03cf\u03c4\3\2\2\2\u03d0")
        buf.write("\u03d3\3\2\2\2\u03d1\u03cf\3\2\2\2\u03d1\u03d2\3\2\2\2")
        buf.write("\u03d2\u03db\3\2\2\2\u03d3\u03d1\3\2\2\2\u03d4\u03d6\7")
        buf.write("\7\2\2\u03d5\u03d4\3\2\2\2\u03d6\u03d9\3\2\2\2\u03d7\u03d5")
        buf.write("\3\2\2\2\u03d7\u03d8\3\2\2\2\u03d8\u03da\3\2\2\2\u03d9")
        buf.write("\u03d7\3\2\2\2\u03da\u03dc\7\n\2\2\u03db\u03d7\3\2\2\2")
        buf.write("\u03db\u03dc\3\2\2\2\u03dc\u03de\3\2\2\2\u03dd\u03c0\3")
        buf.write("\2\2\2\u03dd\u03de\3\2\2\2\u03de\u03e2\3\2\2\2\u03df\u03e1")
        buf.write("\7\7\2\2\u03e0\u03df\3\2\2\2\u03e1\u03e4\3\2\2\2\u03e2")
        buf.write("\u03e0\3\2\2\2\u03e2\u03e3\3\2\2\2\u03e3\u03e5\3\2\2\2")
        buf.write("\u03e4\u03e2\3\2\2\2\u03e5\u03e6\7\f\2\2\u03e6=\3\2\2")
        buf.write("\2\u03e7\u03e9\5\u012c\u0097\2\u03e8\u03e7\3\2\2\2\u03e8")
        buf.write("\u03e9\3\2\2\2\u03e9\u03ea\3\2\2\2\u03ea\u03f9\5T+\2\u03eb")
        buf.write("\u03ed\7\7\2\2\u03ec\u03eb\3\2\2\2\u03ed\u03f0\3\2\2\2")
        buf.write("\u03ee\u03ec\3\2\2\2\u03ee\u03ef\3\2\2\2\u03ef\u03f1\3")
        buf.write("\2\2\2\u03f0\u03ee\3\2\2\2\u03f1\u03f5\7\36\2\2\u03f2")
        buf.write("\u03f4\7\7\2\2\u03f3\u03f2\3\2\2\2\u03f4\u03f7\3\2\2\2")
        buf.write("\u03f5\u03f3\3\2\2\2\u03f5\u03f6\3\2\2\2\u03f6\u03f8\3")
        buf.write("\2\2\2\u03f7\u03f5\3\2\2\2\u03f8\u03fa\5\u0096L\2\u03f9")
        buf.write("\u03ee\3\2\2\2\u03f9\u03fa\3\2\2\2\u03fa?\3\2\2\2\u03fb")
        buf.write("\u03fd\5\u012a\u0096\2\u03fc\u03fb\3\2\2\2\u03fc\u03fd")
        buf.write("\3\2\2\2\u03fd\u03fe\3\2\2\2\u03fe\u0406\7L\2\2\u03ff")
        buf.write("\u0401\7\7\2\2\u0400\u03ff\3\2\2\2\u0401\u0404\3\2\2\2")
        buf.write("\u0402\u0400\3\2\2\2\u0402\u0403\3\2\2\2\u0403\u0405\3")
        buf.write("\2\2\2\u0404\u0402\3\2\2\2\u0405\u0407\5,\27\2\u0406\u0402")
        buf.write("\3\2\2\2\u0406\u0407\3\2\2\2\u0407\u0417\3\2\2\2\u0408")
        buf.write("\u040a\7\7\2\2\u0409\u0408\3\2\2\2\u040a\u040d\3\2\2\2")
        buf.write("\u040b\u0409\3\2\2\2\u040b\u040c\3\2\2\2\u040c\u040e\3")
        buf.write("\2\2\2\u040d\u040b\3\2\2\2\u040e\u0412\5z>\2\u040f\u0411")
        buf.write("\7\7\2\2\u0410\u040f\3\2\2\2\u0411\u0414\3\2\2\2\u0412")
        buf.write("\u0410\3\2\2\2\u0412\u0413\3\2\2\2\u0413\u0415\3\2\2\2")
        buf.write("\u0414\u0412\3\2\2\2\u0415\u0416\7\t\2\2\u0416\u0418\3")
        buf.write("\2\2\2\u0417\u040b\3\2\2\2\u0417\u0418\3\2\2\2\u0418\u041c")
        buf.write("\3\2\2\2\u0419\u041b\7\7\2\2\u041a\u0419\3\2\2\2\u041b")
        buf.write("\u041e\3\2\2\2\u041c\u041a\3\2\2\2\u041c\u041d\3\2\2\2")
        buf.write("\u041d\u041f\3\2\2\2\u041e\u041c\3\2\2\2\u041f\u0423\5")
        buf.write("\u0156\u00ac\2\u0420\u0422\7\7\2\2\u0421\u0420\3\2\2\2")
        buf.write("\u0422\u0425\3\2\2\2\u0423\u0421\3\2\2\2\u0423\u0424\3")
        buf.write("\2\2\2\u0424\u0426\3\2\2\2\u0425\u0423\3\2\2\2\u0426\u0435")
        buf.write("\5<\37\2\u0427\u0429\7\7\2\2\u0428\u0427\3\2\2\2\u0429")
        buf.write("\u042c\3\2\2\2\u042a\u0428\3\2\2\2\u042a\u042b\3\2\2\2")
        buf.write("\u042b\u042d\3\2\2\2\u042c\u042a\3\2\2\2\u042d\u0431\7")
        buf.write("\34\2\2\u042e\u0430\7\7\2\2\u042f\u042e\3\2\2\2\u0430")
        buf.write("\u0433\3\2\2\2\u0431\u042f\3\2\2\2\u0431\u0432\3\2\2\2")
        buf.write("\u0432\u0434\3\2\2\2\u0433\u0431\3\2\2\2\u0434\u0436\5")
        buf.write("b\62\2\u0435\u042a\3\2\2\2\u0435\u0436\3\2\2\2\u0436\u043e")
        buf.write("\3\2\2\2\u0437\u0439\7\7\2\2\u0438\u0437\3\2\2\2\u0439")
        buf.write("\u043c\3\2\2\2\u043a\u0438\3\2\2\2\u043a\u043b\3\2\2\2")
        buf.write("\u043b\u043d\3\2\2\2\u043c\u043a\3\2\2\2\u043d\u043f\5")
        buf.write("\60\31\2\u043e\u043a\3\2\2\2\u043e\u043f\3\2\2\2\u043f")
        buf.write("\u0447\3\2\2\2\u0440\u0442\7\7\2\2\u0441\u0440\3\2\2\2")
        buf.write("\u0442\u0445\3\2\2\2\u0443\u0441\3\2\2\2\u0443\u0444\3")
        buf.write("\2\2\2\u0444\u0446\3\2\2\2\u0445\u0443\3\2\2\2\u0446\u0448")
        buf.write("\5B\"\2\u0447\u0443\3\2\2\2\u0447\u0448\3\2\2\2\u0448")
        buf.write("A\3\2\2\2\u0449\u0453\5\u0086D\2\u044a\u044e\7\36\2\2")
        buf.write("\u044b\u044d\7\7\2\2\u044c\u044b\3\2\2\2\u044d\u0450\3")
        buf.write("\2\2\2\u044e\u044c\3\2\2\2\u044e\u044f\3\2\2\2\u044f\u0451")
        buf.write("\3\2\2\2\u0450\u044e\3\2\2\2\u0451\u0453\5\u0096L\2\u0452")
        buf.write("\u0449\3\2\2\2\u0452\u044a\3\2\2\2\u0453C\3\2\2\2\u0454")
        buf.write("\u0456\5\u014c\u00a7\2\u0455\u0454\3\2\2\2\u0456\u0459")
        buf.write("\3\2\2\2\u0457\u0455\3\2\2\2\u0457\u0458\3\2\2\2\u0458")
        buf.write("\u045d\3\2\2\2\u0459\u0457\3\2\2\2\u045a\u045c\7\7\2\2")
        buf.write("\u045b\u045a\3\2\2\2\u045c\u045f\3\2\2\2\u045d\u045b\3")
        buf.write("\2\2\2\u045d\u045e\3\2\2\2\u045e\u0460\3\2\2\2\u045f\u045d")
        buf.write("\3\2\2\2\u0460\u046f\5\u0156\u00ac\2\u0461\u0463\7\7\2")
        buf.write("\2\u0462\u0461\3\2\2\2\u0463\u0466\3\2\2\2\u0464\u0462")
        buf.write("\3\2\2\2\u0464\u0465\3\2\2\2\u0465\u0467\3\2\2\2\u0466")
        buf.write("\u0464\3\2\2\2\u0467\u046b\7\34\2\2\u0468\u046a\7\7\2")
        buf.write("\2\u0469\u0468\3\2\2\2\u046a\u046d\3\2\2\2\u046b\u0469")
        buf.write("\3\2\2\2\u046b\u046c\3\2\2\2\u046c\u046e\3\2\2\2\u046d")
        buf.write("\u046b\3\2\2\2\u046e\u0470\5b\62\2\u046f\u0464\3\2\2\2")
        buf.write("\u046f\u0470\3\2\2\2\u0470E\3\2\2\2\u0471\u0475\7\13\2")
        buf.write("\2\u0472\u0474\7\7\2\2\u0473\u0472\3\2\2\2\u0474\u0477")
        buf.write("\3\2\2\2\u0475\u0473\3\2\2\2\u0475\u0476\3\2\2\2\u0476")
        buf.write("\u0478\3\2\2\2\u0477\u0475\3\2\2\2\u0478\u0489\5D#\2\u0479")
        buf.write("\u047b\7\7\2\2\u047a\u0479\3\2\2\2\u047b\u047e\3\2\2\2")
        buf.write("\u047c\u047a\3\2\2\2\u047c\u047d\3\2\2\2\u047d\u047f\3")
        buf.write("\2\2\2\u047e\u047c\3\2\2\2\u047f\u0483\7\n\2\2\u0480\u0482")
        buf.write("\7\7\2\2\u0481\u0480\3\2\2\2\u0482\u0485\3\2\2\2\u0483")
        buf.write("\u0481\3\2\2\2\u0483\u0484\3\2\2\2\u0484\u0486\3\2\2\2")
        buf.write("\u0485\u0483\3\2\2\2\u0486\u0488\5D#\2\u0487\u047c\3\2")
        buf.write("\2\2\u0488\u048b\3\2\2\2\u0489\u0487\3\2\2\2\u0489\u048a")
        buf.write("\3\2\2\2\u048a\u0493\3\2\2\2\u048b\u0489\3\2\2\2\u048c")
        buf.write("\u048e\7\7\2\2\u048d\u048c\3\2\2\2\u048e\u0491\3\2\2\2")
        buf.write("\u048f\u048d\3\2\2\2\u048f\u0490\3\2\2\2\u0490\u0492\3")
        buf.write("\2\2\2\u0491\u048f\3\2\2\2\u0492\u0494\7\n\2\2\u0493\u048f")
        buf.write("\3\2\2\2\u0493\u0494\3\2\2\2\u0494\u0498\3\2\2\2\u0495")
        buf.write("\u0497\7\7\2\2\u0496\u0495\3\2\2\2\u0497\u049a\3\2\2\2")
        buf.write("\u0498\u0496\3\2\2\2\u0498\u0499\3\2\2\2\u0499\u049b\3")
        buf.write("\2\2\2\u049a\u0498\3\2\2\2\u049b\u049c\7\f\2\2\u049cG")
        buf.write("\3\2\2\2\u049d\u049f\5\u012a\u0096\2\u049e\u049d\3\2\2")
        buf.write("\2\u049e\u049f\3\2\2\2\u049f\u04a0\3\2\2\2\u04a0\u04a8")
        buf.write("\t\3\2\2\u04a1\u04a3\7\7\2\2\u04a2\u04a1\3\2\2\2\u04a3")
        buf.write("\u04a6\3\2\2\2\u04a4\u04a2\3\2\2\2\u04a4\u04a5\3\2\2\2")
        buf.write("\u04a5\u04a7\3\2\2\2\u04a6\u04a4\3\2\2\2\u04a7\u04a9\5")
        buf.write(",\27\2\u04a8\u04a4\3\2\2\2\u04a8\u04a9\3\2\2\2\u04a9\u04b9")
        buf.write("\3\2\2\2\u04aa\u04ac\7\7\2\2\u04ab\u04aa\3\2\2\2\u04ac")
        buf.write("\u04af\3\2\2\2\u04ad\u04ab\3\2\2\2\u04ad\u04ae\3\2\2\2")
        buf.write("\u04ae\u04b0\3\2\2\2\u04af\u04ad\3\2\2\2\u04b0\u04b4\5")
        buf.write("z>\2\u04b1\u04b3\7\7\2\2\u04b2\u04b1\3\2\2\2\u04b3\u04b6")
        buf.write("\3\2\2\2\u04b4\u04b2\3\2\2\2\u04b4\u04b5\3\2\2\2\u04b5")
        buf.write("\u04b7\3\2\2\2\u04b6\u04b4\3\2\2\2\u04b7\u04b8\7\t\2\2")
        buf.write("\u04b8\u04ba\3\2\2\2\u04b9\u04ad\3\2\2\2\u04b9\u04ba\3")
        buf.write("\2\2\2\u04ba\u04be\3\2\2\2\u04bb\u04bd\7\7\2\2\u04bc\u04bb")
        buf.write("\3\2\2\2\u04bd\u04c0\3\2\2\2\u04be\u04bc\3\2\2\2\u04be")
        buf.write("\u04bf\3\2\2\2\u04bf\u04c3\3\2\2\2\u04c0\u04be\3\2\2\2")
        buf.write("\u04c1\u04c4\5F$\2\u04c2\u04c4\5D#\2\u04c3\u04c1\3\2\2")
        buf.write("\2\u04c3\u04c2\3\2\2\2\u04c4\u04cc\3\2\2\2\u04c5\u04c7")
        buf.write("\7\7\2\2\u04c6\u04c5\3\2\2\2\u04c7\u04ca\3\2\2\2\u04c8")
        buf.write("\u04c6\3\2\2\2\u04c8\u04c9\3\2\2\2\u04c9\u04cb\3\2\2\2")
        buf.write("\u04ca\u04c8\3\2\2\2\u04cb\u04cd\5\60\31\2\u04cc\u04c8")
        buf.write("\3\2\2\2\u04cc\u04cd\3\2\2\2\u04cd\u04df\3\2\2\2\u04ce")
        buf.write("\u04d0\7\7\2\2\u04cf\u04ce\3\2\2\2\u04d0\u04d3\3\2\2\2")
        buf.write("\u04d1\u04cf\3\2\2\2\u04d1\u04d2\3\2\2\2\u04d2\u04dd\3")
        buf.write("\2\2\2\u04d3\u04d1\3\2\2\2\u04d4\u04d8\7\36\2\2\u04d5")
        buf.write("\u04d7\7\7\2\2\u04d6\u04d5\3\2\2\2\u04d7\u04da\3\2\2\2")
        buf.write("\u04d8\u04d6\3\2\2\2\u04d8\u04d9\3\2\2\2\u04d9\u04db\3")
        buf.write("\2\2\2\u04da\u04d8\3\2\2\2\u04db\u04de\5\u0096L\2\u04dc")
        buf.write("\u04de\5J&\2\u04dd\u04d4\3\2\2\2\u04dd\u04dc\3\2\2\2\u04de")
        buf.write("\u04e0\3\2\2\2\u04df\u04d1\3\2\2\2\u04df\u04e0\3\2\2\2")
        buf.write("\u04e0\u04e7\3\2\2\2\u04e1\u04e3\7\7\2\2\u04e2\u04e1\3")
        buf.write("\2\2\2\u04e3\u04e4\3\2\2\2\u04e4\u04e2\3\2\2\2\u04e4\u04e5")
        buf.write("\3\2\2\2\u04e5\u04e6\3\2\2\2\u04e6\u04e8\7\35\2\2\u04e7")
        buf.write("\u04e2\3\2\2\2\u04e7\u04e8\3\2\2\2\u04e8\u04ec\3\2\2\2")
        buf.write("\u04e9\u04eb\7\7\2\2\u04ea\u04e9\3\2\2\2\u04eb\u04ee\3")
        buf.write("\2\2\2\u04ec\u04ea\3\2\2\2\u04ec\u04ed\3\2\2\2\u04ed\u050d")
        buf.write("\3\2\2\2\u04ee\u04ec\3\2\2\2\u04ef\u04f1\5L\'\2\u04f0")
        buf.write("\u04ef\3\2\2\2\u04f0\u04f1\3\2\2\2\u04f1\u04fc\3\2\2\2")
        buf.write("\u04f2\u04f4\7\7\2\2\u04f3\u04f2\3\2\2\2\u04f4\u04f7\3")
        buf.write("\2\2\2\u04f5\u04f3\3\2\2\2\u04f5\u04f6\3\2\2\2\u04f6\u04f9")
        buf.write("\3\2\2\2\u04f7\u04f5\3\2\2\2\u04f8\u04fa\5\u0092J\2\u04f9")
        buf.write("\u04f8\3\2\2\2\u04f9\u04fa\3\2\2\2\u04fa\u04fb\3\2\2\2")
        buf.write("\u04fb\u04fd\5N(\2\u04fc\u04f5\3\2\2\2\u04fc\u04fd\3\2")
        buf.write("\2\2\u04fd\u050e\3\2\2\2\u04fe\u0500\5N(\2\u04ff\u04fe")
        buf.write("\3\2\2\2\u04ff\u0500\3\2\2\2\u0500\u050b\3\2\2\2\u0501")
        buf.write("\u0503\7\7\2\2\u0502\u0501\3\2\2\2\u0503\u0506\3\2\2\2")
        buf.write("\u0504\u0502\3\2\2\2\u0504\u0505\3\2\2\2\u0505\u0508\3")
        buf.write("\2\2\2\u0506\u0504\3\2\2\2\u0507\u0509\5\u0092J\2\u0508")
        buf.write("\u0507\3\2\2\2\u0508\u0509\3\2\2\2\u0509\u050a\3\2\2\2")
        buf.write("\u050a\u050c\5L\'\2\u050b\u0504\3\2\2\2\u050b\u050c\3")
        buf.write("\2\2\2\u050c\u050e\3\2\2\2\u050d\u04f0\3\2\2\2\u050d\u04ff")
        buf.write("\3\2\2\2\u050eI\3\2\2\2\u050f\u0513\7R\2\2\u0510\u0512")
        buf.write("\7\7\2\2\u0511\u0510\3\2\2\2\u0512\u0515\3\2\2\2\u0513")
        buf.write("\u0511\3\2\2\2\u0513\u0514\3\2\2\2\u0514\u0516\3\2\2\2")
        buf.write("\u0515\u0513\3\2\2\2\u0516\u0517\5\u0096L\2\u0517K\3\2")
        buf.write("\2\2\u0518\u051a\5\u012a\u0096\2\u0519\u0518\3\2\2\2\u0519")
        buf.write("\u051a\3\2\2\2\u051a\u051b\3\2\2\2\u051b\u0546\7B\2\2")
        buf.write("\u051c\u051e\5\u012a\u0096\2\u051d\u051c\3\2\2\2\u051d")
        buf.write("\u051e\3\2\2\2\u051e\u051f\3\2\2\2\u051f\u0523\7B\2\2")
        buf.write("\u0520\u0522\7\7\2\2\u0521\u0520\3\2\2\2\u0522\u0525\3")
        buf.write("\2\2\2\u0523\u0521\3\2\2\2\u0523\u0524\3\2\2\2\u0524\u0526")
        buf.write("\3\2\2\2\u0525\u0523\3\2\2\2\u0526\u052a\7\13\2\2\u0527")
        buf.write("\u0529\7\7\2\2\u0528\u0527\3\2\2\2\u0529\u052c\3\2\2\2")
        buf.write("\u052a\u0528\3\2\2\2\u052a\u052b\3\2\2\2\u052b\u052d\3")
        buf.write("\2\2\2\u052c\u052a\3\2\2\2\u052d\u053c\7\f\2\2\u052e\u0530")
        buf.write("\7\7\2\2\u052f\u052e\3\2\2\2\u0530\u0533\3\2\2\2\u0531")
        buf.write("\u052f\3\2\2\2\u0531\u0532\3\2\2\2\u0532\u0534\3\2\2\2")
        buf.write("\u0533\u0531\3\2\2\2\u0534\u0538\7\34\2\2\u0535\u0537")
        buf.write("\7\7\2\2\u0536\u0535\3\2\2\2\u0537\u053a\3\2\2\2\u0538")
        buf.write("\u0536\3\2\2\2\u0538\u0539\3\2\2\2\u0539\u053b\3\2\2\2")
        buf.write("\u053a\u0538\3\2\2\2\u053b\u053d\5b\62\2\u053c\u0531\3")
        buf.write("\2\2\2\u053c\u053d\3\2\2\2\u053d\u0541\3\2\2\2\u053e\u0540")
        buf.write("\7\7\2\2\u053f\u053e\3\2\2\2\u0540\u0543\3\2\2\2\u0541")
        buf.write("\u053f\3\2\2\2\u0541\u0542\3\2\2\2\u0542\u0544\3\2\2\2")
        buf.write("\u0543\u0541\3\2\2\2\u0544\u0546\5B\"\2\u0545\u0519\3")
        buf.write("\2\2\2\u0545\u051d\3\2\2\2\u0546M\3\2\2\2\u0547\u0549")
        buf.write("\5\u012a\u0096\2\u0548\u0547\3\2\2\2\u0548\u0549\3\2\2")
        buf.write("\2\u0549\u054a\3\2\2\2\u054a\u0586\7C\2\2\u054b\u054d")
        buf.write("\5\u012a\u0096\2\u054c\u054b\3\2\2\2\u054c\u054d\3\2\2")
        buf.write("\2\u054d\u054e\3\2\2\2\u054e\u0552\7C\2\2\u054f\u0551")
        buf.write("\7\7\2\2\u0550\u054f\3\2\2\2\u0551\u0554\3\2\2\2\u0552")
        buf.write("\u0550\3\2\2\2\u0552\u0553\3\2\2\2\u0553\u0555\3\2\2\2")
        buf.write("\u0554\u0552\3\2\2\2\u0555\u0559\7\13\2\2\u0556\u0558")
        buf.write("\7\7\2\2\u0557\u0556\3\2\2\2\u0558\u055b\3\2\2\2\u0559")
        buf.write("\u0557\3\2\2\2\u0559\u055a\3\2\2\2\u055a\u055c\3\2\2\2")
        buf.write("\u055b\u0559\3\2\2\2\u055c\u0564\5R*\2\u055d\u055f\7\7")
        buf.write("\2\2\u055e\u055d\3\2\2\2\u055f\u0562\3\2\2\2\u0560\u055e")
        buf.write("\3\2\2\2\u0560\u0561\3\2\2\2\u0561\u0563\3\2\2\2\u0562")
        buf.write("\u0560\3\2\2\2\u0563\u0565\7\n\2\2\u0564\u0560\3\2\2\2")
        buf.write("\u0564\u0565\3\2\2\2\u0565\u0569\3\2\2\2\u0566\u0568\7")
        buf.write("\7\2\2\u0567\u0566\3\2\2\2\u0568\u056b\3\2\2\2\u0569\u0567")
        buf.write("\3\2\2\2\u0569\u056a\3\2\2\2\u056a\u056c\3\2\2\2\u056b")
        buf.write("\u0569\3\2\2\2\u056c\u057b\7\f\2\2\u056d\u056f\7\7\2\2")
        buf.write("\u056e\u056d\3\2\2\2\u056f\u0572\3\2\2\2\u0570\u056e\3")
        buf.write("\2\2\2\u0570\u0571\3\2\2\2\u0571\u0573\3\2\2\2\u0572\u0570")
        buf.write("\3\2\2\2\u0573\u0577\7\34\2\2\u0574\u0576\7\7\2\2\u0575")
        buf.write("\u0574\3\2\2\2\u0576\u0579\3\2\2\2\u0577\u0575\3\2\2\2")
        buf.write("\u0577\u0578\3\2\2\2\u0578\u057a\3\2\2\2\u0579\u0577\3")
        buf.write("\2\2\2\u057a\u057c\5b\62\2\u057b\u0570\3\2\2\2\u057b\u057c")
        buf.write("\3\2\2\2\u057c\u0580\3\2\2\2\u057d\u057f\7\7\2\2\u057e")
        buf.write("\u057d\3\2\2\2\u057f\u0582\3\2\2\2\u0580\u057e\3\2\2\2")
        buf.write("\u0580\u0581\3\2\2\2\u0581\u0583\3\2\2\2\u0582\u0580\3")
        buf.write("\2\2\2\u0583\u0584\5B\"\2\u0584\u0586\3\2\2\2\u0585\u0548")
        buf.write("\3\2\2\2\u0585\u054c\3\2\2\2\u0586O\3\2\2\2\u0587\u058b")
        buf.write("\7\13\2\2\u0588\u058a\7\7\2\2\u0589\u0588\3\2\2\2\u058a")
        buf.write("\u058d\3\2\2\2\u058b\u0589\3\2\2\2\u058b\u058c\3\2\2\2")
        buf.write("\u058c\u05ab\3\2\2\2\u058d\u058b\3\2\2\2\u058e\u059f\5")
        buf.write("R*\2\u058f\u0591\7\7\2\2\u0590\u058f\3\2\2\2\u0591\u0594")
        buf.write("\3\2\2\2\u0592\u0590\3\2\2\2\u0592\u0593\3\2\2\2\u0593")
        buf.write("\u0595\3\2\2\2\u0594\u0592\3\2\2\2\u0595\u0599\7\n\2\2")
        buf.write("\u0596\u0598\7\7\2\2\u0597\u0596\3\2\2\2\u0598\u059b\3")
        buf.write("\2\2\2\u0599\u0597\3\2\2\2\u0599\u059a\3\2\2\2\u059a\u059c")
        buf.write("\3\2\2\2\u059b\u0599\3\2\2\2\u059c\u059e\5R*\2\u059d\u0592")
        buf.write("\3\2\2\2\u059e\u05a1\3\2\2\2\u059f\u059d\3\2\2\2\u059f")
        buf.write("\u05a0\3\2\2\2\u05a0\u05a9\3\2\2\2\u05a1\u059f\3\2\2\2")
        buf.write("\u05a2\u05a4\7\7\2\2\u05a3\u05a2\3\2\2\2\u05a4\u05a7\3")
        buf.write("\2\2\2\u05a5\u05a3\3\2\2\2\u05a5\u05a6\3\2\2\2\u05a6\u05a8")
        buf.write("\3\2\2\2\u05a7\u05a5\3\2\2\2\u05a8\u05aa\7\n\2\2\u05a9")
        buf.write("\u05a5\3\2\2\2\u05a9\u05aa\3\2\2\2\u05aa\u05ac\3\2\2\2")
        buf.write("\u05ab\u058e\3\2\2\2\u05ab\u05ac\3\2\2\2\u05ac\u05b0\3")
        buf.write("\2\2\2\u05ad\u05af\7\7\2\2\u05ae\u05ad\3\2\2\2\u05af\u05b2")
        buf.write("\3\2\2\2\u05b0\u05ae\3\2\2\2\u05b0\u05b1\3\2\2\2\u05b1")
        buf.write("\u05b3\3\2\2\2\u05b2\u05b0\3\2\2\2\u05b3\u05b4\7\f\2\2")
        buf.write("\u05b4Q\3\2\2\2\u05b5\u05b7\5\u012c\u0097\2\u05b6\u05b5")
        buf.write("\3\2\2\2\u05b6\u05b7\3\2\2\2\u05b7\u05b8\3\2\2\2\u05b8")
        buf.write("\u05bc\5\u0156\u00ac\2\u05b9\u05bb\7\7\2\2\u05ba\u05b9")
        buf.write("\3\2\2\2\u05bb\u05be\3\2\2\2\u05bc\u05ba\3\2\2\2\u05bc")
        buf.write("\u05bd\3\2\2\2\u05bd\u05c7\3\2\2\2\u05be\u05bc\3\2\2\2")
        buf.write("\u05bf\u05c3\7\34\2\2\u05c0\u05c2\7\7\2\2\u05c1\u05c0")
        buf.write("\3\2\2\2\u05c2\u05c5\3\2\2\2\u05c3\u05c1\3\2\2\2\u05c3")
        buf.write("\u05c4\3\2\2\2\u05c4\u05c6\3\2\2\2\u05c5\u05c3\3\2\2\2")
        buf.write("\u05c6\u05c8\5b\62\2\u05c7\u05bf\3\2\2\2\u05c7\u05c8\3")
        buf.write("\2\2\2\u05c8S\3\2\2\2\u05c9\u05cd\5\u0156\u00ac\2\u05ca")
        buf.write("\u05cc\7\7\2\2\u05cb\u05ca\3\2\2\2\u05cc\u05cf\3\2\2\2")
        buf.write("\u05cd\u05cb\3\2\2\2\u05cd\u05ce\3\2\2\2\u05ce\u05d0\3")
        buf.write("\2\2\2\u05cf\u05cd\3\2\2\2\u05d0\u05d4\7\34\2\2\u05d1")
        buf.write("\u05d3\7\7\2\2\u05d2\u05d1\3\2\2\2\u05d3\u05d6\3\2\2\2")
        buf.write("\u05d4\u05d2\3\2\2\2\u05d4\u05d5\3\2\2\2\u05d5\u05d7\3")
        buf.write("\2\2\2\u05d6\u05d4\3\2\2\2\u05d7\u05d8\5b\62\2\u05d8U")
        buf.write("\3\2\2\2\u05d9\u05db\5\u012a\u0096\2\u05da\u05d9\3\2\2")
        buf.write("\2\u05da\u05db\3\2\2\2\u05db\u05dc\3\2\2\2\u05dc\u05e0")
        buf.write("\7M\2\2\u05dd\u05df\7\7\2\2\u05de\u05dd\3\2\2\2\u05df")
        buf.write("\u05e2\3\2\2\2\u05e0\u05de\3\2\2\2\u05e0\u05e1\3\2\2\2")
        buf.write("\u05e1\u05e3\3\2\2\2\u05e2\u05e0\3\2\2\2\u05e3\u05f2\5")
        buf.write("\u0156\u00ac\2\u05e4\u05e6\7\7\2\2\u05e5\u05e4\3\2\2\2")
        buf.write("\u05e6\u05e9\3\2\2\2\u05e7\u05e5\3\2\2\2\u05e7\u05e8\3")
        buf.write("\2\2\2\u05e8\u05ea\3\2\2\2\u05e9\u05e7\3\2\2\2\u05ea\u05ee")
        buf.write("\7\34\2\2\u05eb\u05ed\7\7\2\2\u05ec\u05eb\3\2\2\2\u05ed")
        buf.write("\u05f0\3\2\2\2\u05ee\u05ec\3\2\2\2\u05ee\u05ef\3\2\2\2")
        buf.write("\u05ef\u05f1\3\2\2\2\u05f0\u05ee\3\2\2\2\u05f1\u05f3\5")
        buf.write("\"\22\2\u05f2\u05e7\3\2\2\2\u05f2\u05f3\3\2\2\2\u05f3")
        buf.write("\u05fb\3\2\2\2\u05f4\u05f6\7\7\2\2\u05f5\u05f4\3\2\2\2")
        buf.write("\u05f6\u05f9\3\2\2\2\u05f7\u05f5\3\2\2\2\u05f7\u05f8\3")
        buf.write("\2\2\2\u05f8\u05fa\3\2\2\2\u05f9\u05f7\3\2\2\2\u05fa\u05fc")
        buf.write("\5\34\17\2\u05fb\u05f7\3\2\2\2\u05fb\u05fc\3\2\2\2\u05fc")
        buf.write("W\3\2\2\2\u05fd\u05ff\5\u012a\u0096\2\u05fe\u05fd\3\2")
        buf.write("\2\2\u05fe\u05ff\3\2\2\2\u05ff\u0600\3\2\2\2\u0600\u0604")
        buf.write("\7Q\2\2\u0601\u0603\7\7\2\2\u0602\u0601\3\2\2\2\u0603")
        buf.write("\u0606\3\2\2\2\u0604\u0602\3\2\2\2\u0604\u0605\3\2\2\2")
        buf.write("\u0605\u0607\3\2\2\2\u0606\u0604\3\2\2\2\u0607\u0616\5")
        buf.write("<\37\2\u0608\u060a\7\7\2\2\u0609\u0608\3\2\2\2\u060a\u060d")
        buf.write("\3\2\2\2\u060b\u0609\3\2\2\2\u060b\u060c\3\2\2\2\u060c")
        buf.write("\u060e\3\2\2\2\u060d\u060b\3\2\2\2\u060e\u0612\7\34\2")
        buf.write("\2\u060f\u0611\7\7\2\2\u0610\u060f\3\2\2\2\u0611\u0614")
        buf.write("\3\2\2\2\u0612\u0610\3\2\2\2\u0612\u0613\3\2\2\2\u0613")
        buf.write("\u0615\3\2\2\2\u0614\u0612\3\2\2\2\u0615\u0617\5Z.\2\u0616")
        buf.write("\u060b\3\2\2\2\u0616\u0617\3\2\2\2\u0617\u061b\3\2\2\2")
        buf.write("\u0618\u061a\7\7\2\2\u0619\u0618\3\2\2\2\u061a\u061d\3")
        buf.write("\2\2\2\u061b\u0619\3\2\2\2\u061b\u061c\3\2\2\2\u061c\u061f")
        buf.write("\3\2\2\2\u061d\u061b\3\2\2\2\u061e\u0620\5\u0086D\2\u061f")
        buf.write("\u061e\3\2\2\2\u061f\u0620\3\2\2\2\u0620Y\3\2\2\2\u0621")
        buf.write("\u0625\7U\2\2\u0622\u0624\7\7\2\2\u0623\u0622\3\2\2\2")
        buf.write("\u0624\u0627\3\2\2\2\u0625\u0623\3\2\2\2\u0625\u0626\3")
        buf.write("\2\2\2\u0626\u0628\3\2\2\2\u0627\u0625\3\2\2\2\u0628\u0632")
        buf.write("\5\u00ceh\2\u0629\u062d\7V\2\2\u062a\u062c\7\7\2\2\u062b")
        buf.write("\u062a\3\2\2\2\u062c\u062f\3\2\2\2\u062d\u062b\3\2\2\2")
        buf.write("\u062d\u062e\3\2\2\2\u062e\u0630\3\2\2\2\u062f\u062d\3")
        buf.write("\2\2\2\u0630\u0632\5\u00ceh\2\u0631\u0621\3\2\2\2\u0631")
        buf.write("\u0629\3\2\2\2\u0632[\3\2\2\2\u0633\u0637\7\17\2\2\u0634")
        buf.write("\u0636\7\7\2\2\u0635\u0634\3\2\2\2\u0636\u0639\3\2\2\2")
        buf.write("\u0637\u0635\3\2\2\2\u0637\u0638\3\2\2\2\u0638\u063b\3")
        buf.write("\2\2\2\u0639\u0637\3\2\2\2\u063a\u063c\5^\60\2\u063b\u063a")
        buf.write("\3\2\2\2\u063b\u063c\3\2\2\2\u063c\u064b\3\2\2\2\u063d")
        buf.write("\u063f\7\7\2\2\u063e\u063d\3\2\2\2\u063f\u0642\3\2\2\2")
        buf.write("\u0640\u063e\3\2\2\2\u0640\u0641\3\2\2\2\u0641\u0643\3")
        buf.write("\2\2\2\u0642\u0640\3\2\2\2\u0643\u0647\7\35\2\2\u0644")
        buf.write("\u0646\7\7\2\2\u0645\u0644\3\2\2\2\u0646\u0649\3\2\2\2")
        buf.write("\u0647\u0645\3\2\2\2\u0647\u0648\3\2\2\2\u0648\u064a\3")
        buf.write("\2\2\2\u0649\u0647\3\2\2\2\u064a\u064c\5\64\33\2\u064b")
        buf.write("\u0640\3\2\2\2\u064b\u064c\3\2\2\2\u064c\u0650\3\2\2\2")
        buf.write("\u064d\u064f\7\7\2\2\u064e\u064d\3\2\2\2\u064f\u0652\3")
        buf.write("\2\2\2\u0650\u064e\3\2\2\2\u0650\u0651\3\2\2\2\u0651\u0653")
        buf.write("\3\2\2\2\u0652\u0650\3\2\2\2\u0653\u0654\7\20\2\2\u0654")
        buf.write("]\3\2\2\2\u0655\u0666\5`\61\2\u0656\u0658\7\7\2\2\u0657")
        buf.write("\u0656\3\2\2\2\u0658\u065b\3\2\2\2\u0659\u0657\3\2\2\2")
        buf.write("\u0659\u065a\3\2\2\2\u065a\u065c\3\2\2\2\u065b\u0659\3")
        buf.write("\2\2\2\u065c\u0660\7\n\2\2\u065d\u065f\7\7\2\2\u065e\u065d")
        buf.write("\3\2\2\2\u065f\u0662\3\2\2\2\u0660\u065e\3\2\2\2\u0660")
        buf.write("\u0661\3\2\2\2\u0661\u0663\3\2\2\2\u0662\u0660\3\2\2\2")
        buf.write("\u0663\u0665\5`\61\2\u0664\u0659\3\2\2\2\u0665\u0668\3")
        buf.write("\2\2\2\u0666\u0664\3\2\2\2\u0666\u0667\3\2\2\2\u0667\u066c")
        buf.write("\3\2\2\2\u0668\u0666\3\2\2\2\u0669\u066b\7\7\2\2\u066a")
        buf.write("\u0669\3\2\2\2\u066b\u066e\3\2\2\2\u066c\u066a\3\2\2\2")
        buf.write("\u066c\u066d\3\2\2\2\u066d\u0670\3\2\2\2\u066e\u066c\3")
        buf.write("\2\2\2\u066f\u0671\7\n\2\2\u0670\u066f\3\2\2\2\u0670\u0671")
        buf.write("\3\2\2\2\u0671_\3\2\2\2\u0672\u0676\5\u012a\u0096\2\u0673")
        buf.write("\u0675\7\7\2\2\u0674\u0673\3\2\2\2\u0675\u0678\3\2\2\2")
        buf.write("\u0676\u0674\3\2\2\2\u0676\u0677\3\2\2\2\u0677\u067a\3")
        buf.write("\2\2\2\u0678\u0676\3\2\2\2\u0679\u0672\3\2\2\2\u0679\u067a")
        buf.write("\3\2\2\2\u067a\u067b\3\2\2\2\u067b\u0683\5\u0156\u00ac")
        buf.write("\2\u067c\u067e\7\7\2\2\u067d\u067c\3\2\2\2\u067e\u0681")
        buf.write("\3\2\2\2\u067f\u067d\3\2\2\2\u067f\u0680\3\2\2\2\u0680")
        buf.write("\u0682\3\2\2\2\u0681\u067f\3\2\2\2\u0682\u0684\5\u00ce")
        buf.write("h\2\u0683\u067f\3\2\2\2\u0683\u0684\3\2\2\2\u0684\u068c")
        buf.write("\3\2\2\2\u0685\u0687\7\7\2\2\u0686\u0685\3\2\2\2\u0687")
        buf.write("\u068a\3\2\2\2\u0688\u0686\3\2\2\2\u0688\u0689\3\2\2\2")
        buf.write("\u0689\u068b\3\2\2\2\u068a\u0688\3\2\2\2\u068b\u068d\5")
        buf.write("\34\17\2\u068c\u0688\3\2\2\2\u068c\u068d\3\2\2\2\u068d")
        buf.write("a\3\2\2\2\u068e\u0690\5\u0130\u0099\2\u068f\u068e\3\2")
        buf.write("\2\2\u068f\u0690\3\2\2\2\u0690\u0695\3\2\2\2\u0691\u0696")
        buf.write("\5x=\2\u0692\u0696\5f\64\2\u0693\u0696\5d\63\2\u0694\u0696")
        buf.write("\5t;\2\u0695\u0691\3\2\2\2\u0695\u0692\3\2\2\2\u0695\u0693")
        buf.write("\3\2\2\2\u0695\u0694\3\2\2\2\u0696c\3\2\2\2\u0697\u069a")
        buf.write("\5j\66\2\u0698\u069a\7l\2\2\u0699\u0697\3\2\2\2\u0699")
        buf.write("\u0698\3\2\2\2\u069ae\3\2\2\2\u069b\u069e\5d\63\2\u069c")
        buf.write("\u069e\5x=\2\u069d\u069b\3\2\2\2\u069d\u069c\3\2\2\2\u069e")
        buf.write("\u06a2\3\2\2\2\u069f\u06a1\7\7\2\2\u06a0\u069f\3\2\2\2")
        buf.write("\u06a1\u06a4\3\2\2\2\u06a2\u06a0\3\2\2\2\u06a2\u06a3\3")
        buf.write("\2\2\2\u06a3\u06a6\3\2\2\2\u06a4\u06a2\3\2\2\2\u06a5\u06a7")
        buf.write("\5h\65\2\u06a6\u06a5\3\2\2\2\u06a7\u06a8\3\2\2\2\u06a8")
        buf.write("\u06a6\3\2\2\2\u06a8\u06a9\3\2\2\2\u06a9g\3\2\2\2\u06aa")
        buf.write("\u06ab\t\4\2\2\u06abi\3\2\2\2\u06ac\u06bd\5l\67\2\u06ad")
        buf.write("\u06af\7\7\2\2\u06ae\u06ad\3\2\2\2\u06af\u06b2\3\2\2\2")
        buf.write("\u06b0\u06ae\3\2\2\2\u06b0\u06b1\3\2\2\2\u06b1\u06b3\3")
        buf.write("\2\2\2\u06b2\u06b0\3\2\2\2\u06b3\u06b7\7\t\2\2\u06b4\u06b6")
        buf.write("\7\7\2\2\u06b5\u06b4\3\2\2\2\u06b6\u06b9\3\2\2\2\u06b7")
        buf.write("\u06b5\3\2\2\2\u06b7\u06b8\3\2\2\2\u06b8\u06ba\3\2\2\2")
        buf.write("\u06b9\u06b7\3\2\2\2\u06ba\u06bc\5l\67\2\u06bb\u06b0\3")
        buf.write("\2\2\2\u06bc\u06bf\3\2\2\2\u06bd\u06bb\3\2\2\2\u06bd\u06be")
        buf.write("\3\2\2\2\u06bek\3\2\2\2\u06bf\u06bd\3\2\2\2\u06c0\u06c8")
        buf.write("\5\u0156\u00ac\2\u06c1\u06c3\7\7\2\2\u06c2\u06c1\3\2\2")
        buf.write("\2\u06c3\u06c6\3\2\2\2\u06c4\u06c2\3\2\2\2\u06c4\u06c5")
        buf.write("\3\2\2\2\u06c5\u06c7\3\2\2\2\u06c6\u06c4\3\2\2\2\u06c7")
        buf.write("\u06c9\5\u00ccg\2\u06c8\u06c4\3\2\2\2\u06c8\u06c9\3\2")
        buf.write("\2\2\u06c9m\3\2\2\2\u06ca\u06cc\5p9\2\u06cb\u06ca\3\2")
        buf.write("\2\2\u06cb\u06cc\3\2\2\2\u06cc\u06cd\3\2\2\2\u06cd\u06d0")
        buf.write("\5b\62\2\u06ce\u06d0\7\21\2\2\u06cf\u06cb\3\2\2\2\u06cf")
        buf.write("\u06ce\3\2\2\2\u06d0o\3\2\2\2\u06d1\u06d3\5r:\2\u06d2")
        buf.write("\u06d1\3\2\2\2\u06d3\u06d4\3\2\2\2\u06d4\u06d2\3\2\2\2")
        buf.write("\u06d4\u06d5\3\2\2\2\u06d5q\3\2\2\2\u06d6\u06da\5\u013a")
        buf.write("\u009e\2\u06d7\u06d9\7\7\2\2\u06d8\u06d7\3\2\2\2\u06d9")
        buf.write("\u06dc\3\2\2\2\u06da\u06d8\3\2\2\2\u06da\u06db\3\2\2\2")
        buf.write("\u06db\u06df\3\2\2\2\u06dc\u06da\3\2\2\2\u06dd\u06df\5")
        buf.write("\u014c\u00a7\2\u06de\u06d6\3\2\2\2\u06de\u06dd\3\2\2\2")
        buf.write("\u06dfs\3\2\2\2\u06e0\u06e4\5z>\2\u06e1\u06e3\7\7\2\2")
        buf.write("\u06e2\u06e1\3\2\2\2\u06e3\u06e6\3\2\2\2\u06e4\u06e2\3")
        buf.write("\2\2\2\u06e4\u06e5\3\2\2\2\u06e5\u06e7\3\2\2\2\u06e6\u06e4")
        buf.write("\3\2\2\2\u06e7\u06eb\7\t\2\2\u06e8\u06ea\7\7\2\2\u06e9")
        buf.write("\u06e8\3\2\2\2\u06ea\u06ed\3\2\2\2\u06eb\u06e9\3\2\2\2")
        buf.write("\u06eb\u06ec\3\2\2\2\u06ec\u06ef\3\2\2\2\u06ed\u06eb\3")
        buf.write("\2\2\2\u06ee\u06e0\3\2\2\2\u06ee\u06ef\3\2\2\2\u06ef\u06f0")
        buf.write("\3\2\2\2\u06f0\u06f4\5v<\2\u06f1\u06f3\7\7\2\2\u06f2\u06f1")
        buf.write("\3\2\2\2\u06f3\u06f6\3\2\2\2\u06f4\u06f2\3\2\2\2\u06f4")
        buf.write("\u06f5\3\2\2\2\u06f5\u06f7\3\2\2\2\u06f6\u06f4\3\2\2\2")
        buf.write("\u06f7\u06fb\7$\2\2\u06f8\u06fa\7\7\2\2\u06f9\u06f8\3")
        buf.write("\2\2\2\u06fa\u06fd\3\2\2\2\u06fb\u06f9\3\2\2\2\u06fb\u06fc")
        buf.write("\3\2\2\2\u06fc\u06fe\3\2\2\2\u06fd\u06fb\3\2\2\2\u06fe")
        buf.write("\u06ff\5b\62\2\u06ffu\3\2\2\2\u0700\u0704\7\13\2\2\u0701")
        buf.write("\u0703\7\7\2\2\u0702\u0701\3\2\2\2\u0703\u0706\3\2\2\2")
        buf.write("\u0704\u0702\3\2\2\2\u0704\u0705\3\2\2\2\u0705\u0709\3")
        buf.write("\2\2\2\u0706\u0704\3\2\2\2\u0707\u070a\5T+\2\u0708\u070a")
        buf.write("\5b\62\2\u0709\u0707\3\2\2\2\u0709\u0708\3\2\2\2\u0709")
        buf.write("\u070a\3\2\2\2\u070a\u071e\3\2\2\2\u070b\u070d\7\7\2\2")
        buf.write("\u070c\u070b\3\2\2\2\u070d\u0710\3\2\2\2\u070e\u070c\3")
        buf.write("\2\2\2\u070e\u070f\3\2\2\2\u070f\u0711\3\2\2\2\u0710\u070e")
        buf.write("\3\2\2\2\u0711\u0715\7\n\2\2\u0712\u0714\7\7\2\2\u0713")
        buf.write("\u0712\3\2\2\2\u0714\u0717\3\2\2\2\u0715\u0713\3\2\2\2")
        buf.write("\u0715\u0716\3\2\2\2\u0716\u071a\3\2\2\2\u0717\u0715\3")
        buf.write("\2\2\2\u0718\u071b\5T+\2\u0719\u071b\5b\62\2\u071a\u0718")
        buf.write("\3\2\2\2\u071a\u0719\3\2\2\2\u071b\u071d\3\2\2\2\u071c")
        buf.write("\u070e\3\2\2\2\u071d\u0720\3\2\2\2\u071e\u071c\3\2\2\2")
        buf.write("\u071e\u071f\3\2\2\2\u071f\u0728\3\2\2\2\u0720\u071e\3")
        buf.write("\2\2\2\u0721\u0723\7\7\2\2\u0722\u0721\3\2\2\2\u0723\u0726")
        buf.write("\3\2\2\2\u0724\u0722\3\2\2\2\u0724\u0725\3\2\2\2\u0725")
        buf.write("\u0727\3\2\2\2\u0726\u0724\3\2\2\2\u0727\u0729\7\n\2\2")
        buf.write("\u0728\u0724\3\2\2\2\u0728\u0729\3\2\2\2\u0729\u072d\3")
        buf.write("\2\2\2\u072a\u072c\7\7\2\2\u072b\u072a\3\2\2\2\u072c\u072f")
        buf.write("\3\2\2\2\u072d\u072b\3\2\2\2\u072d\u072e\3\2\2\2\u072e")
        buf.write("\u0730\3\2\2\2\u072f\u072d\3\2\2\2\u0730\u0731\7\f\2\2")
        buf.write("\u0731w\3\2\2\2\u0732\u0736\7\13\2\2\u0733\u0735\7\7\2")
        buf.write("\2\u0734\u0733\3\2\2\2\u0735\u0738\3\2\2\2\u0736\u0734")
        buf.write("\3\2\2\2\u0736\u0737\3\2\2\2\u0737\u0739\3\2\2\2\u0738")
        buf.write("\u0736\3\2\2\2\u0739\u073d\5b\62\2\u073a\u073c\7\7\2\2")
        buf.write("\u073b\u073a\3\2\2\2\u073c\u073f\3\2\2\2\u073d\u073b\3")
        buf.write("\2\2\2\u073d\u073e\3\2\2\2\u073e\u0740\3\2\2\2\u073f\u073d")
        buf.write("\3\2\2\2\u0740\u0741\7\f\2\2\u0741y\3\2\2\2\u0742\u0744")
        buf.write("\5\u0130\u0099\2\u0743\u0742\3\2\2\2\u0743\u0744\3\2\2")
        buf.write("\2\u0744\u0748\3\2\2\2\u0745\u0749\5x=\2\u0746\u0749\5")
        buf.write("f\64\2\u0747\u0749\5d\63\2\u0748\u0745\3\2\2\2\u0748\u0746")
        buf.write("\3\2\2\2\u0748\u0747\3\2\2\2\u0749{\3\2\2\2\u074a\u074e")
        buf.write("\7\13\2\2\u074b\u074d\7\7\2\2\u074c\u074b\3\2\2\2\u074d")
        buf.write("\u0750\3\2\2\2\u074e\u074c\3\2\2\2\u074e\u074f\3\2\2\2")
        buf.write("\u074f\u0751\3\2\2\2\u0750\u074e\3\2\2\2\u0751\u0755\5")
        buf.write("j\66\2\u0752\u0754\7\7\2\2\u0753\u0752\3\2\2\2\u0754\u0757")
        buf.write("\3\2\2\2\u0755\u0753\3\2\2\2\u0755\u0756\3\2\2\2\u0756")
        buf.write("\u0758\3\2\2\2\u0757\u0755\3\2\2\2\u0758\u0759\7\f\2\2")
        buf.write("\u0759\u076b\3\2\2\2\u075a\u075e\7\13\2\2\u075b\u075d")
        buf.write("\7\7\2\2\u075c\u075b\3\2\2\2\u075d\u0760\3\2\2\2\u075e")
        buf.write("\u075c\3\2\2\2\u075e\u075f\3\2\2\2\u075f\u0761\3\2\2\2")
        buf.write("\u0760\u075e\3\2\2\2\u0761\u0765\5|?\2\u0762\u0764\7\7")
        buf.write("\2\2\u0763\u0762\3\2\2\2\u0764\u0767\3\2\2\2\u0765\u0763")
        buf.write("\3\2\2\2\u0765\u0766\3\2\2\2\u0766\u0768\3\2\2\2\u0767")
        buf.write("\u0765\3\2\2\2\u0768\u0769\7\f\2\2\u0769\u076b\3\2\2\2")
        buf.write("\u076a\u074a\3\2\2\2\u076a\u075a\3\2\2\2\u076b}\3\2\2")
        buf.write("\2\u076c\u0772\5\u0080A\2\u076d\u076e\5\u0094K\2\u076e")
        buf.write("\u076f\5\u0080A\2\u076f\u0771\3\2\2\2\u0770\u076d\3\2")
        buf.write("\2\2\u0771\u0774\3\2\2\2\u0772\u0770\3\2\2\2\u0772\u0773")
        buf.write("\3\2\2\2\u0773\u0776\3\2\2\2\u0774\u0772\3\2\2\2\u0775")
        buf.write("\u076c\3\2\2\2\u0775\u0776\3\2\2\2\u0776\u0778\3\2\2\2")
        buf.write("\u0777\u0779\5\u0094K\2\u0778\u0777\3\2\2\2\u0778\u0779")
        buf.write("\3\2\2\2\u0779\177\3\2\2\2\u077a\u077d\5\u0082B\2\u077b")
        buf.write("\u077d\5\u014c\u00a7\2\u077c\u077a\3\2\2\2\u077c\u077b")
        buf.write("\3\2\2\2\u077d\u0780\3\2\2\2\u077e\u077c\3\2\2\2\u077e")
        buf.write("\u077f\3\2\2\2\u077f\u0785\3\2\2\2\u0780\u077e\3\2\2\2")
        buf.write("\u0781\u0786\5\26\f\2\u0782\u0786\5\u0090I\2\u0783\u0786")
        buf.write("\5\u0088E\2\u0784\u0786\5\u0096L\2\u0785\u0781\3\2\2\2")
        buf.write("\u0785\u0782\3\2\2\2\u0785\u0783\3\2\2\2\u0785\u0784\3")
        buf.write("\2\2\2\u0786\u0081\3\2\2\2\u0787\u0788\5\u0156\u00ac\2")
        buf.write("\u0788\u078c\t\5\2\2\u0789\u078b\7\7\2\2\u078a\u0789\3")
        buf.write("\2\2\2\u078b\u078e\3\2\2\2\u078c\u078a\3\2\2\2\u078c\u078d")
        buf.write("\3\2\2\2\u078d\u0083\3\2\2\2\u078e\u078c\3\2\2\2\u078f")
        buf.write("\u0792\5\u0086D\2\u0790\u0792\5\u0080A\2\u0791\u078f\3")
        buf.write("\2\2\2\u0791\u0790\3\2\2\2\u0792\u0085\3\2\2\2\u0793\u0797")
        buf.write("\7\17\2\2\u0794\u0796\7\7\2\2\u0795\u0794\3\2\2\2\u0796")
        buf.write("\u0799\3\2\2\2\u0797\u0795\3\2\2\2\u0797\u0798\3\2\2\2")
        buf.write("\u0798\u079a\3\2\2\2\u0799\u0797\3\2\2\2\u079a\u079e\5")
        buf.write("~@\2\u079b\u079d\7\7\2\2\u079c\u079b\3\2\2\2\u079d\u07a0")
        buf.write("\3\2\2\2\u079e\u079c\3\2\2\2\u079e\u079f\3\2\2\2\u079f")
        buf.write("\u07a1\3\2\2\2\u07a0\u079e\3\2\2\2\u07a1\u07a2\7\20\2")
        buf.write("\2\u07a2\u0087\3\2\2\2\u07a3\u07a7\5\u008aF\2\u07a4\u07a7")
        buf.write("\5\u008cG\2\u07a5\u07a7\5\u008eH\2\u07a6\u07a3\3\2\2\2")
        buf.write("\u07a6\u07a4\3\2\2\2\u07a6\u07a5\3\2\2\2\u07a7\u0089\3")
        buf.write("\2\2\2\u07a8\u07ac\7_\2\2\u07a9\u07ab\7\7\2\2\u07aa\u07a9")
        buf.write("\3\2\2\2\u07ab\u07ae\3\2\2\2\u07ac\u07aa\3\2\2\2\u07ac")
        buf.write("\u07ad\3\2\2\2\u07ad\u07af\3\2\2\2\u07ae\u07ac\3\2\2\2")
        buf.write("\u07af\u07b3\7\13\2\2\u07b0\u07b2\5\u014c\u00a7\2\u07b1")
        buf.write("\u07b0\3\2\2\2\u07b2\u07b5\3\2\2\2\u07b3\u07b1\3\2\2\2")
        buf.write("\u07b3\u07b4\3\2\2\2\u07b4\u07b8\3\2\2\2\u07b5\u07b3\3")
        buf.write("\2\2\2\u07b6\u07b9\5D#\2\u07b7\u07b9\5F$\2\u07b8\u07b6")
        buf.write("\3\2\2\2\u07b8\u07b7\3\2\2\2\u07b9\u07ba\3\2\2\2\u07ba")
        buf.write("\u07bb\7h\2\2\u07bb\u07bc\5\u0096L\2\u07bc\u07c0\7\f\2")
        buf.write("\2\u07bd\u07bf\7\7\2\2\u07be\u07bd\3\2\2\2\u07bf\u07c2")
        buf.write("\3\2\2\2\u07c0\u07be\3\2\2\2\u07c0\u07c1\3\2\2\2\u07c1")
        buf.write("\u07c4\3\2\2\2\u07c2\u07c0\3\2\2\2\u07c3\u07c5\5\u0084")
        buf.write("C\2\u07c4\u07c3\3\2\2\2\u07c4\u07c5\3\2\2\2\u07c5\u008b")
        buf.write("\3\2\2\2\u07c6\u07ca\7a\2\2\u07c7\u07c9\7\7\2\2\u07c8")
        buf.write("\u07c7\3\2\2\2\u07c9\u07cc\3\2\2\2\u07ca\u07c8\3\2\2\2")
        buf.write("\u07ca\u07cb\3\2\2\2\u07cb\u07cd\3\2\2\2\u07cc\u07ca\3")
        buf.write("\2\2\2\u07cd\u07ce\7\13\2\2\u07ce\u07cf\5\u0096L\2\u07cf")
        buf.write("\u07d3\7\f\2\2\u07d0\u07d2\7\7\2\2\u07d1\u07d0\3\2\2\2")
        buf.write("\u07d2\u07d5\3\2\2\2\u07d3\u07d1\3\2\2\2\u07d3\u07d4\3")
        buf.write("\2\2\2\u07d4\u07d6\3\2\2\2\u07d5\u07d3\3\2\2\2\u07d6\u07d7")
        buf.write("\5\u0084C\2\u07d7\u07eb\3\2\2\2\u07d8\u07dc\7a\2\2\u07d9")
        buf.write("\u07db\7\7\2\2\u07da\u07d9\3\2\2\2\u07db\u07de\3\2\2\2")
        buf.write("\u07dc\u07da\3\2\2\2\u07dc\u07dd\3\2\2\2\u07dd\u07df\3")
        buf.write("\2\2\2\u07de\u07dc\3\2\2\2\u07df\u07e0\7\13\2\2\u07e0")
        buf.write("\u07e1\5\u0096L\2\u07e1\u07e5\7\f\2\2\u07e2\u07e4\7\7")
        buf.write("\2\2\u07e3\u07e2\3\2\2\2\u07e4\u07e7\3\2\2\2\u07e5\u07e3")
        buf.write("\3\2\2\2\u07e5\u07e6\3\2\2\2\u07e6\u07e8\3\2\2\2\u07e7")
        buf.write("\u07e5\3\2\2\2\u07e8\u07e9\7\35\2\2\u07e9\u07eb\3\2\2")
        buf.write("\2\u07ea\u07c6\3\2\2\2\u07ea\u07d8\3\2\2\2\u07eb\u008d")
        buf.write("\3\2\2\2\u07ec\u07f0\7`\2\2\u07ed\u07ef\7\7\2\2\u07ee")
        buf.write("\u07ed\3\2\2\2\u07ef\u07f2\3\2\2\2\u07f0\u07ee\3\2\2\2")
        buf.write("\u07f0\u07f1\3\2\2\2\u07f1\u07f4\3\2\2\2\u07f2\u07f0\3")
        buf.write("\2\2\2\u07f3\u07f5\5\u0084C\2\u07f4\u07f3\3\2\2\2\u07f4")
        buf.write("\u07f5\3\2\2\2\u07f5\u07f9\3\2\2\2\u07f6\u07f8\7\7\2\2")
        buf.write("\u07f7\u07f6\3\2\2\2\u07f8\u07fb\3\2\2\2\u07f9\u07f7\3")
        buf.write("\2\2\2\u07f9\u07fa\3\2\2\2\u07fa\u07fc\3\2\2\2\u07fb\u07f9")
        buf.write("\3\2\2\2\u07fc\u0800\7a\2\2\u07fd\u07ff\7\7\2\2\u07fe")
        buf.write("\u07fd\3\2\2\2\u07ff\u0802\3\2\2\2\u0800\u07fe\3\2\2\2")
        buf.write("\u0800\u0801\3\2\2\2\u0801\u0803\3\2\2\2\u0802\u0800\3")
        buf.write("\2\2\2\u0803\u0804\7\13\2\2\u0804\u0805\5\u0096L\2\u0805")
        buf.write("\u0806\7\f\2\2\u0806\u008f\3\2\2\2\u0807\u0808\5\u00ba")
        buf.write("^\2\u0808\u080c\7\36\2\2\u0809\u080b\7\7\2\2\u080a\u0809")
        buf.write("\3\2\2\2\u080b\u080e\3\2\2\2\u080c\u080a\3\2\2\2\u080c")
        buf.write("\u080d\3\2\2\2\u080d\u080f\3\2\2\2\u080e\u080c\3\2\2\2")
        buf.write("\u080f\u0810\5\u0096L\2\u0810\u081c\3\2\2\2\u0811\u0812")
        buf.write("\5\u00be`\2\u0812\u0816\5\u0110\u0089\2\u0813\u0815\7")
        buf.write("\7\2\2\u0814\u0813\3\2\2\2\u0815\u0818\3\2\2\2\u0816\u0814")
        buf.write("\3\2\2\2\u0816\u0817\3\2\2\2\u0817\u0819\3\2\2\2\u0818")
        buf.write("\u0816\3\2\2\2\u0819\u081a\5\u0096L\2\u081a\u081c\3\2")
        buf.write("\2\2\u081b\u0807\3\2\2\2\u081b\u0811\3\2\2\2\u081c\u0091")
        buf.write("\3\2\2\2\u081d\u0821\t\6\2\2\u081e\u0820\7\7\2\2\u081f")
        buf.write("\u081e\3\2\2\2\u0820\u0823\3\2\2\2\u0821\u081f\3\2\2\2")
        buf.write("\u0821\u0822\3\2\2\2\u0822\u0826\3\2\2\2\u0823\u0821\3")
        buf.write("\2\2\2\u0824\u0826\7\2\2\3\u0825\u081d\3\2\2\2\u0825\u0824")
        buf.write("\3\2\2\2\u0826\u0093\3\2\2\2\u0827\u0829\t\6\2\2\u0828")
        buf.write("\u0827\3\2\2\2\u0829\u082a\3\2\2\2\u082a\u0828\3\2\2\2")
        buf.write("\u082a\u082b\3\2\2\2\u082b\u082e\3\2\2\2\u082c\u082e\7")
        buf.write("\2\2\3\u082d\u0828\3\2\2\2\u082d\u082c\3\2\2\2\u082e\u0095")
        buf.write("\3\2\2\2\u082f\u0830\5\u0098M\2\u0830\u0097\3\2\2\2\u0831")
        buf.write("\u0842\5\u009aN\2\u0832\u0834\7\7\2\2\u0833\u0832\3\2")
        buf.write("\2\2\u0834\u0837\3\2\2\2\u0835\u0833\3\2\2\2\u0835\u0836")
        buf.write("\3\2\2\2\u0836\u0838\3\2\2\2\u0837\u0835\3\2\2\2\u0838")
        buf.write("\u083c\7\31\2\2\u0839\u083b\7\7\2\2\u083a\u0839\3\2\2")
        buf.write("\2\u083b\u083e\3\2\2\2\u083c\u083a\3\2\2\2\u083c\u083d")
        buf.write("\3\2\2\2\u083d\u083f\3\2\2\2\u083e\u083c\3\2\2\2\u083f")
        buf.write("\u0841\5\u009aN\2\u0840\u0835\3\2\2\2\u0841\u0844\3\2")
        buf.write("\2\2\u0842\u0840\3\2\2\2\u0842\u0843\3\2\2\2\u0843\u0099")
        buf.write("\3\2\2\2\u0844\u0842\3\2\2\2\u0845\u0856\5\u009cO\2\u0846")
        buf.write("\u0848\7\7\2\2\u0847\u0846\3\2\2\2\u0848\u084b\3\2\2\2")
        buf.write("\u0849\u0847\3\2\2\2\u0849\u084a\3\2\2\2\u084a\u084c\3")
        buf.write("\2\2\2\u084b\u0849\3\2\2\2\u084c\u0850\7\30\2\2\u084d")
        buf.write("\u084f\7\7\2\2\u084e\u084d\3\2\2\2\u084f\u0852\3\2\2\2")
        buf.write("\u0850\u084e\3\2\2\2\u0850\u0851\3\2\2\2\u0851\u0853\3")
        buf.write("\2\2\2\u0852\u0850\3\2\2\2\u0853\u0855\5\u009cO\2\u0854")
        buf.write("\u0849\3\2\2\2\u0855\u0858\3\2\2\2\u0856\u0854\3\2\2\2")
        buf.write("\u0856\u0857\3\2\2\2\u0857\u009b\3\2\2\2\u0858\u0856\3")
        buf.write("\2\2\2\u0859\u0865\5\u009eP\2\u085a\u085e\5\u0112\u008a")
        buf.write("\2\u085b\u085d\7\7\2\2\u085c\u085b\3\2\2\2\u085d\u0860")
        buf.write("\3\2\2\2\u085e\u085c\3\2\2\2\u085e\u085f\3\2\2\2\u085f")
        buf.write("\u0861\3\2\2\2\u0860\u085e\3\2\2\2\u0861\u0862\5\u009e")
        buf.write("P\2\u0862\u0864\3\2\2\2\u0863\u085a\3\2\2\2\u0864\u0867")
        buf.write("\3\2\2\2\u0865\u0863\3\2\2\2\u0865\u0866\3\2\2\2\u0866")
        buf.write("\u009d\3\2\2\2\u0867\u0865\3\2\2\2\u0868\u0872\5\u00a0")
        buf.write("Q\2\u0869\u086d\5\u0114\u008b\2\u086a\u086c\7\7\2\2\u086b")
        buf.write("\u086a\3\2\2\2\u086c\u086f\3\2\2\2\u086d\u086b\3\2\2\2")
        buf.write("\u086d\u086e\3\2\2\2\u086e\u0870\3\2\2\2\u086f\u086d\3")
        buf.write("\2\2\2\u0870\u0871\5\u00a0Q\2\u0871\u0873\3\2\2\2\u0872")
        buf.write("\u0869\3\2\2\2\u0872\u0873\3\2\2\2\u0873\u009f\3\2\2\2")
        buf.write("\u0874\u0889\5\u00a2R\2\u0875\u0879\5\u0116\u008c\2\u0876")
        buf.write("\u0878\7\7\2\2\u0877\u0876\3\2\2\2\u0878\u087b\3\2\2\2")
        buf.write("\u0879\u0877\3\2\2\2\u0879\u087a\3\2\2\2\u087a\u087c\3")
        buf.write("\2\2\2\u087b\u0879\3\2\2\2\u087c\u087d\5\u00a2R\2\u087d")
        buf.write("\u0888\3\2\2\2\u087e\u0882\5\u0118\u008d\2\u087f\u0881")
        buf.write("\7\7\2\2\u0880\u087f\3\2\2\2\u0881\u0884\3\2\2\2\u0882")
        buf.write("\u0880\3\2\2\2\u0882\u0883\3\2\2\2\u0883\u0885\3\2\2\2")
        buf.write("\u0884\u0882\3\2\2\2\u0885\u0886\5b\62\2\u0886\u0888\3")
        buf.write("\2\2\2\u0887\u0875\3\2\2\2\u0887\u087e\3\2\2\2\u0888\u088b")
        buf.write("\3\2\2\2\u0889\u0887\3\2\2\2\u0889\u088a\3\2\2\2\u088a")
        buf.write("\u00a1\3\2\2\2\u088b\u0889\3\2\2\2\u088c\u089e\5\u00a6")
        buf.write("T\2\u088d\u088f\7\7\2\2\u088e\u088d\3\2\2\2\u088f\u0892")
        buf.write("\3\2\2\2\u0890\u088e\3\2\2\2\u0890\u0891\3\2\2\2\u0891")
        buf.write("\u0893\3\2\2\2\u0892\u0890\3\2\2\2\u0893\u0897\5\u00a4")
        buf.write("S\2\u0894\u0896\7\7\2\2\u0895\u0894\3\2\2\2\u0896\u0899")
        buf.write("\3\2\2\2\u0897\u0895\3\2\2\2\u0897\u0898\3\2\2\2\u0898")
        buf.write("\u089a\3\2\2\2\u0899\u0897\3\2\2\2\u089a\u089b\5\u00a6")
        buf.write("T\2\u089b\u089d\3\2\2\2\u089c\u0890\3\2\2\2\u089d\u08a0")
        buf.write("\3\2\2\2\u089e\u089c\3\2\2\2\u089e\u089f\3\2\2\2\u089f")
        buf.write("\u00a3\3\2\2\2\u08a0\u089e\3\2\2\2\u08a1\u08a2\7/\2\2")
        buf.write("\u08a2\u08a3\7\34\2\2\u08a3\u00a5\3\2\2\2\u08a4\u08b0")
        buf.write("\5\u00a8U\2\u08a5\u08a9\5\u0156\u00ac\2\u08a6\u08a8\7")
        buf.write("\7\2\2\u08a7\u08a6\3\2\2\2\u08a8\u08ab\3\2\2\2\u08a9\u08a7")
        buf.write("\3\2\2\2\u08a9\u08aa\3\2\2\2\u08aa\u08ac\3\2\2\2\u08ab")
        buf.write("\u08a9\3\2\2\2\u08ac\u08ad\5\u00a8U\2\u08ad\u08af\3\2")
        buf.write("\2\2\u08ae\u08a5\3\2\2\2\u08af\u08b2\3\2\2\2\u08b0\u08ae")
        buf.write("\3\2\2\2\u08b0\u08b1\3\2\2\2\u08b1\u00a7\3\2\2\2\u08b2")
        buf.write("\u08b0\3\2\2\2\u08b3\u08be\5\u00aaV\2\u08b4\u08b8\7&\2")
        buf.write("\2\u08b5\u08b7\7\7\2\2\u08b6\u08b5\3\2\2\2\u08b7\u08ba")
        buf.write("\3\2\2\2\u08b8\u08b6\3\2\2\2\u08b8\u08b9\3\2\2\2\u08b9")
        buf.write("\u08bb\3\2\2\2\u08ba\u08b8\3\2\2\2\u08bb\u08bd\5\u00aa")
        buf.write("V\2\u08bc\u08b4\3\2\2\2\u08bd\u08c0\3\2\2\2\u08be\u08bc")
        buf.write("\3\2\2\2\u08be\u08bf\3\2\2\2\u08bf\u00a9\3\2\2\2\u08c0")
        buf.write("\u08be\3\2\2\2\u08c1\u08cd\5\u00acW\2\u08c2\u08c6\5\u011a")
        buf.write("\u008e\2\u08c3\u08c5\7\7\2\2\u08c4\u08c3\3\2\2\2\u08c5")
        buf.write("\u08c8\3\2\2\2\u08c6\u08c4\3\2\2\2\u08c6\u08c7\3\2\2\2")
        buf.write("\u08c7\u08c9\3\2\2\2\u08c8\u08c6\3\2\2\2\u08c9\u08ca\5")
        buf.write("\u00acW\2\u08ca\u08cc\3\2\2\2\u08cb\u08c2\3\2\2\2\u08cc")
        buf.write("\u08cf\3\2\2\2\u08cd\u08cb\3\2\2\2\u08cd\u08ce\3\2\2\2")
        buf.write("\u08ce\u00ab\3\2\2\2\u08cf\u08cd\3\2\2\2\u08d0\u08dc\5")
        buf.write("\u00aeX\2\u08d1\u08d5\5\u011c\u008f\2\u08d2\u08d4\7\7")
        buf.write("\2\2\u08d3\u08d2\3\2\2\2\u08d4\u08d7\3\2\2\2\u08d5\u08d3")
        buf.write("\3\2\2\2\u08d5\u08d6\3\2\2\2\u08d6\u08d8\3\2\2\2\u08d7")
        buf.write("\u08d5\3\2\2\2\u08d8\u08d9\5\u00aeX\2\u08d9\u08db\3\2")
        buf.write("\2\2\u08da\u08d1\3\2\2\2\u08db\u08de\3\2\2\2\u08dc\u08da")
        buf.write("\3\2\2\2\u08dc\u08dd\3\2\2\2\u08dd\u00ad\3\2\2\2\u08de")
        buf.write("\u08dc\3\2\2\2\u08df\u08ef\5\u00b0Y\2\u08e0\u08e2\7\7")
        buf.write("\2\2\u08e1\u08e0\3\2\2\2\u08e2\u08e5\3\2\2\2\u08e3\u08e1")
        buf.write("\3\2\2\2\u08e3\u08e4\3\2\2\2\u08e4\u08e6\3\2\2\2\u08e5")
        buf.write("\u08e3\3\2\2\2\u08e6\u08ea\5\u011e\u0090\2\u08e7\u08e9")
        buf.write("\7\7\2\2\u08e8\u08e7\3\2\2\2\u08e9\u08ec\3\2\2\2\u08ea")
        buf.write("\u08e8\3\2\2\2\u08ea\u08eb\3\2\2\2\u08eb\u08ed\3\2\2\2")
        buf.write("\u08ec\u08ea\3\2\2\2\u08ed\u08ee\5b\62\2\u08ee\u08f0\3")
        buf.write("\2\2\2\u08ef\u08e3\3\2\2\2\u08ef\u08f0\3\2\2\2\u08f0\u00af")
        buf.write("\3\2\2\2\u08f1\u0913\5\u00b2Z\2\u08f2\u08f4\7\7\2\2\u08f3")
        buf.write("\u08f2\3\2\2\2\u08f4\u08f7\3\2\2\2\u08f5\u08f3\3\2\2\2")
        buf.write("\u08f5\u08f6\3\2\2\2\u08f6\u08f8\3\2\2\2\u08f7\u08f5\3")
        buf.write("\2\2\2\u08f8\u08fc\7\60\2\2\u08f9\u08fb\7\7\2\2\u08fa")
        buf.write("\u08f9\3\2\2\2\u08fb\u08fe\3\2\2\2\u08fc\u08fa\3\2\2\2")
        buf.write("\u08fc\u08fd\3\2\2\2\u08fd\u08ff\3\2\2\2\u08fe\u08fc\3")
        buf.write("\2\2\2\u08ff\u0903\5\u00d8m\2\u0900\u0902\7\7\2\2\u0901")
        buf.write("\u0900\3\2\2\2\u0902\u0905\3\2\2\2\u0903\u0901\3\2\2\2")
        buf.write("\u0903\u0904\3\2\2\2\u0904\u0906\3\2\2\2\u0905\u0903\3")
        buf.write("\2\2\2\u0906\u090a\7\61\2\2\u0907\u0909\7\7\2\2\u0908")
        buf.write("\u0907\3\2\2\2\u0909\u090c\3\2\2\2\u090a\u0908\3\2\2\2")
        buf.write("\u090a\u090b\3\2\2\2\u090b\u090f\3\2\2\2\u090c\u090a\3")
        buf.write("\2\2\2\u090d\u0910\5\u0096L\2\u090e\u0910\5\u00d4k\2\u090f")
        buf.write("\u090d\3\2\2\2\u090f\u090e\3\2\2\2\u0910\u0912\3\2\2\2")
        buf.write("\u0911\u08f5\3\2\2\2\u0912\u0915\3\2\2\2\u0913\u0911\3")
        buf.write("\2\2\2\u0913\u0914\3\2\2\2\u0914\u00b1\3\2\2\2\u0915\u0913")
        buf.write("\3\2\2\2\u0916\u0918\5\u00b4[\2\u0917\u0916\3\2\2\2\u0918")
        buf.write("\u091b\3\2\2\2\u0919\u0917\3\2\2\2\u0919\u091a\3\2\2\2")
        buf.write("\u091a\u091c\3\2\2\2\u091b\u0919\3\2\2\2\u091c\u091d\5")
        buf.write("\u00b6\\\2\u091d\u00b3\3\2\2\2\u091e\u0928\5\u014c\u00a7")
        buf.write("\2\u091f\u0928\5\u0082B\2\u0920\u0924\5\u0120\u0091\2")
        buf.write("\u0921\u0923\7\7\2\2\u0922\u0921\3\2\2\2\u0923\u0926\3")
        buf.write("\2\2\2\u0924\u0922\3\2\2\2\u0924\u0925\3\2\2\2\u0925\u0928")
        buf.write("\3\2\2\2\u0926\u0924\3\2\2\2\u0927\u091e\3\2\2\2\u0927")
        buf.write("\u091f\3\2\2\2\u0927\u0920\3\2\2\2\u0928\u00b5\3\2\2\2")
        buf.write("\u0929\u0931\5\u00d2j\2\u092a\u092c\5\u00d2j\2\u092b\u092d")
        buf.write("\5\u00b8]\2\u092c\u092b\3\2\2\2\u092d\u092e\3\2\2\2\u092e")
        buf.write("\u092c\3\2\2\2\u092e\u092f\3\2\2\2\u092f\u0931\3\2\2\2")
        buf.write("\u0930\u0929\3\2\2\2\u0930\u092a\3\2\2\2\u0931\u00b7\3")
        buf.write("\2\2\2\u0932\u0938\5\u0122\u0092\2\u0933\u0938\5\u00cc")
        buf.write("g\2\u0934\u0938\5\u00c8e\2\u0935\u0938\5\u00c4c\2\u0936")
        buf.write("\u0938\5\u00c6d\2\u0937\u0932\3\2\2\2\u0937\u0933\3\2")
        buf.write("\2\2\u0937\u0934\3\2\2\2\u0937\u0935\3\2\2\2\u0937\u0936")
        buf.write("\3\2\2\2\u0938\u00b9\3\2\2\2\u0939\u093a\5\u00b6\\\2\u093a")
        buf.write("\u093b\5\u00c2b\2\u093b\u093f\3\2\2\2\u093c\u093f\5\u0156")
        buf.write("\u00ac\2\u093d\u093f\5\u00bc_\2\u093e\u0939\3\2\2\2\u093e")
        buf.write("\u093c\3\2\2\2\u093e\u093d\3\2\2\2\u093f\u00bb\3\2\2\2")
        buf.write("\u0940\u0944\7\13\2\2\u0941\u0943\7\7\2\2\u0942\u0941")
        buf.write("\3\2\2\2\u0943\u0946\3\2\2\2\u0944\u0942\3\2\2\2\u0944")
        buf.write("\u0945\3\2\2\2\u0945\u0947\3\2\2\2\u0946\u0944\3\2\2\2")
        buf.write("\u0947\u094b\5\u00ba^\2\u0948\u094a\7\7\2\2\u0949\u0948")
        buf.write("\3\2\2\2\u094a\u094d\3\2\2\2\u094b\u0949\3\2\2\2\u094b")
        buf.write("\u094c\3\2\2\2\u094c\u094e\3\2\2\2\u094d\u094b\3\2\2\2")
        buf.write("\u094e\u094f\7\f\2\2\u094f\u00bd\3\2\2\2\u0950\u0953\5")
        buf.write("\u00b2Z\2\u0951\u0953\5\u00c0a\2\u0952\u0950\3\2\2\2\u0952")
        buf.write("\u0951\3\2\2\2\u0953\u00bf\3\2\2\2\u0954\u0958\7\13\2")
        buf.write("\2\u0955\u0957\7\7\2\2\u0956\u0955\3\2\2\2\u0957\u095a")
        buf.write("\3\2\2\2\u0958\u0956\3\2\2\2\u0958\u0959\3\2\2\2\u0959")
        buf.write("\u095b\3\2\2\2\u095a\u0958\3\2\2\2\u095b\u095f\5\u00be")
        buf.write("`\2\u095c\u095e\7\7\2\2\u095d\u095c\3\2\2\2\u095e\u0961")
        buf.write("\3\2\2\2\u095f\u095d\3\2\2\2\u095f\u0960\3\2\2\2\u0960")
        buf.write("\u0962\3\2\2\2\u0961\u095f\3\2\2\2\u0962\u0963\7\f\2\2")
        buf.write("\u0963\u00c1\3\2\2\2\u0964\u0968\5\u00ccg\2\u0965\u0968")
        buf.write("\5\u00c4c\2\u0966\u0968\5\u00c6d\2\u0967\u0964\3\2\2\2")
        buf.write("\u0967\u0965\3\2\2\2\u0967\u0966\3\2\2\2\u0968\u00c3\3")
        buf.write("\2\2\2\u0969\u096d\7\r\2\2\u096a\u096c\7\7\2\2\u096b\u096a")
        buf.write("\3\2\2\2\u096c\u096f\3\2\2\2\u096d\u096b\3\2\2\2\u096d")
        buf.write("\u096e\3\2\2\2\u096e\u0970\3\2\2\2\u096f\u096d\3\2\2\2")
        buf.write("\u0970\u0981\5\u0096L\2\u0971\u0973\7\7\2\2\u0972\u0971")
        buf.write("\3\2\2\2\u0973\u0976\3\2\2\2\u0974\u0972\3\2\2\2\u0974")
        buf.write("\u0975\3\2\2\2\u0975\u0977\3\2\2\2\u0976\u0974\3\2\2\2")
        buf.write("\u0977\u097b\7\n\2\2\u0978\u097a\7\7\2\2\u0979\u0978\3")
        buf.write("\2\2\2\u097a\u097d\3\2\2\2\u097b\u0979\3\2\2\2\u097b\u097c")
        buf.write("\3\2\2\2\u097c\u097e\3\2\2\2\u097d\u097b\3\2\2\2\u097e")
        buf.write("\u0980\5\u0096L\2\u097f\u0974\3\2\2\2\u0980\u0983\3\2")
        buf.write("\2\2\u0981\u097f\3\2\2\2\u0981\u0982\3\2\2\2\u0982\u098b")
        buf.write("\3\2\2\2\u0983\u0981\3\2\2\2\u0984\u0986\7\7\2\2\u0985")
        buf.write("\u0984\3\2\2\2\u0986\u0989\3\2\2\2\u0987\u0985\3\2\2\2")
        buf.write("\u0987\u0988\3\2\2\2\u0988\u098a\3\2\2\2\u0989\u0987\3")
        buf.write("\2\2\2\u098a\u098c\7\n\2\2\u098b\u0987\3\2\2\2\u098b\u098c")
        buf.write("\3\2\2\2\u098c\u0990\3\2\2\2\u098d\u098f\7\7\2\2\u098e")
        buf.write("\u098d\3\2\2\2\u098f\u0992\3\2\2\2\u0990\u098e\3\2\2\2")
        buf.write("\u0990\u0991\3\2\2\2\u0991\u0993\3\2\2\2\u0992\u0990\3")
        buf.write("\2\2\2\u0993\u0994\7\16\2\2\u0994\u00c5\3\2\2\2\u0995")
        buf.write("\u0997\7\7\2\2\u0996\u0995\3\2\2\2\u0997\u099a\3\2\2\2")
        buf.write("\u0998\u0996\3\2\2\2\u0998\u0999\3\2\2\2\u0999\u099b\3")
        buf.write("\2\2\2\u099a\u0998\3\2\2\2\u099b\u099f\5\u0126\u0094\2")
        buf.write("\u099c\u099e\7\7\2\2\u099d\u099c\3\2\2\2\u099e\u09a1\3")
        buf.write("\2\2\2\u099f\u099d\3\2\2\2\u099f\u09a0\3\2\2\2\u09a0\u09a5")
        buf.write("\3\2\2\2\u09a1\u099f\3\2\2\2\u09a2\u09a6\5\u0156\u00ac")
        buf.write("\2\u09a3\u09a6\5\u00d4k\2\u09a4\u09a6\7J\2\2\u09a5\u09a2")
        buf.write("\3\2\2\2\u09a5\u09a3\3\2\2\2\u09a5\u09a4\3\2\2\2\u09a6")
        buf.write("\u00c7\3\2\2\2\u09a7\u09a9\5\u00ccg\2\u09a8\u09a7\3\2")
        buf.write("\2\2\u09a8\u09a9\3\2\2\2\u09a9\u09ab\3\2\2\2\u09aa\u09ac")
        buf.write("\5\u00ceh\2\u09ab\u09aa\3\2\2\2\u09ab\u09ac\3\2\2\2\u09ac")
        buf.write("\u09ad\3\2\2\2\u09ad\u09b3\5\u00caf\2\u09ae\u09b0\5\u00cc")
        buf.write("g\2\u09af\u09ae\3\2\2\2\u09af\u09b0\3\2\2\2\u09b0\u09b1")
        buf.write("\3\2\2\2\u09b1\u09b3\5\u00ceh\2\u09b2\u09a8\3\2\2\2\u09b2")
        buf.write("\u09af\3\2\2\2\u09b3\u00c9\3\2\2\2\u09b4\u09b6\5\u014c")
        buf.write("\u00a7\2\u09b5\u09b4\3\2\2\2\u09b6\u09b9\3\2\2\2\u09b7")
        buf.write("\u09b5\3\2\2\2\u09b7\u09b8\3\2\2\2\u09b8\u09bb\3\2\2\2")
        buf.write("\u09b9\u09b7\3\2\2\2\u09ba\u09bc\5\u0082B\2\u09bb\u09ba")
        buf.write("\3\2\2\2\u09bb\u09bc\3\2\2\2\u09bc\u09c0\3\2\2\2\u09bd")
        buf.write("\u09bf\7\7\2\2\u09be\u09bd\3\2\2\2\u09bf\u09c2\3\2\2\2")
        buf.write("\u09c0\u09be\3\2\2\2\u09c0\u09c1\3\2\2\2\u09c1\u09c3\3")
        buf.write("\2\2\2\u09c2\u09c0\3\2\2\2\u09c3\u09c4\5\u00e8u\2\u09c4")
        buf.write("\u00cb\3\2\2\2\u09c5\u09c9\7\60\2\2\u09c6\u09c8\7\7\2")
        buf.write("\2\u09c7\u09c6\3\2\2\2\u09c8\u09cb\3\2\2\2\u09c9\u09c7")
        buf.write("\3\2\2\2\u09c9\u09ca\3\2\2\2\u09ca\u09cc\3\2\2\2\u09cb")
        buf.write("\u09c9\3\2\2\2\u09cc\u09dd\5n8\2\u09cd\u09cf\7\7\2\2\u09ce")
        buf.write("\u09cd\3\2\2\2\u09cf\u09d2\3\2\2\2\u09d0\u09ce\3\2\2\2")
        buf.write("\u09d0\u09d1\3\2\2\2\u09d1\u09d3\3\2\2\2\u09d2\u09d0\3")
        buf.write("\2\2\2\u09d3\u09d7\7\n\2\2\u09d4\u09d6\7\7\2\2\u09d5\u09d4")
        buf.write("\3\2\2\2\u09d6\u09d9\3\2\2\2\u09d7\u09d5\3\2\2\2\u09d7")
        buf.write("\u09d8\3\2\2\2\u09d8\u09da\3\2\2\2\u09d9\u09d7\3\2\2\2")
        buf.write("\u09da\u09dc\5n8\2\u09db\u09d0\3\2\2\2\u09dc\u09df\3\2")
        buf.write("\2\2\u09dd\u09db\3\2\2\2\u09dd\u09de\3\2\2\2\u09de\u09e7")
        buf.write("\3\2\2\2\u09df\u09dd\3\2\2\2\u09e0\u09e2\7\7\2\2\u09e1")
        buf.write("\u09e0\3\2\2\2\u09e2\u09e5\3\2\2\2\u09e3\u09e1\3\2\2\2")
        buf.write("\u09e3\u09e4\3\2\2\2\u09e4\u09e6\3\2\2\2\u09e5\u09e3\3")
        buf.write("\2\2\2\u09e6\u09e8\7\n\2\2\u09e7\u09e3\3\2\2\2\u09e7\u09e8")
        buf.write("\3\2\2\2\u09e8\u09ec\3\2\2\2\u09e9\u09eb\7\7\2\2\u09ea")
        buf.write("\u09e9\3\2\2\2\u09eb\u09ee\3\2\2\2\u09ec\u09ea\3\2\2\2")
        buf.write("\u09ec\u09ed\3\2\2\2\u09ed\u09ef\3\2\2\2\u09ee\u09ec\3")
        buf.write("\2\2\2\u09ef\u09f0\7\61\2\2\u09f0\u00cd\3\2\2\2\u09f1")
        buf.write("\u09f5\7\13\2\2\u09f2\u09f4\7\7\2\2\u09f3\u09f2\3\2\2")
        buf.write("\2\u09f4\u09f7\3\2\2\2\u09f5\u09f3\3\2\2\2\u09f5\u09f6")
        buf.write("\3\2\2\2\u09f6\u09f8\3\2\2\2\u09f7\u09f5\3\2\2\2\u09f8")
        buf.write("\u0a26\7\f\2\2\u09f9\u09fd\7\13\2\2\u09fa\u09fc\7\7\2")
        buf.write("\2\u09fb\u09fa\3\2\2\2\u09fc\u09ff\3\2\2\2\u09fd\u09fb")
        buf.write("\3\2\2\2\u09fd\u09fe\3\2\2\2\u09fe\u0a00\3\2\2\2\u09ff")
        buf.write("\u09fd\3\2\2\2\u0a00\u0a11\5\u00d0i\2\u0a01\u0a03\7\7")
        buf.write("\2\2\u0a02\u0a01\3\2\2\2\u0a03\u0a06\3\2\2\2\u0a04\u0a02")
        buf.write("\3\2\2\2\u0a04\u0a05\3\2\2\2\u0a05\u0a07\3\2\2\2\u0a06")
        buf.write("\u0a04\3\2\2\2\u0a07\u0a0b\7\n\2\2\u0a08\u0a0a\7\7\2\2")
        buf.write("\u0a09\u0a08\3\2\2\2\u0a0a\u0a0d\3\2\2\2\u0a0b\u0a09\3")
        buf.write("\2\2\2\u0a0b\u0a0c\3\2\2\2\u0a0c\u0a0e\3\2\2\2\u0a0d\u0a0b")
        buf.write("\3\2\2\2\u0a0e\u0a10\5\u00d0i\2\u0a0f\u0a04\3\2\2\2\u0a10")
        buf.write("\u0a13\3\2\2\2\u0a11\u0a0f\3\2\2\2\u0a11\u0a12\3\2\2\2")
        buf.write("\u0a12\u0a1b\3\2\2\2\u0a13\u0a11\3\2\2\2\u0a14\u0a16\7")
        buf.write("\7\2\2\u0a15\u0a14\3\2\2\2\u0a16\u0a19\3\2\2\2\u0a17\u0a15")
        buf.write("\3\2\2\2\u0a17\u0a18\3\2\2\2\u0a18\u0a1a\3\2\2\2\u0a19")
        buf.write("\u0a17\3\2\2\2\u0a1a\u0a1c\7\n\2\2\u0a1b\u0a17\3\2\2\2")
        buf.write("\u0a1b\u0a1c\3\2\2\2\u0a1c\u0a20\3\2\2\2\u0a1d\u0a1f\7")
        buf.write("\7\2\2\u0a1e\u0a1d\3\2\2\2\u0a1f\u0a22\3\2\2\2\u0a20\u0a1e")
        buf.write("\3\2\2\2\u0a20\u0a21\3\2\2\2\u0a21\u0a23\3\2\2\2\u0a22")
        buf.write("\u0a20\3\2\2\2\u0a23\u0a24\7\f\2\2\u0a24\u0a26\3\2\2\2")
        buf.write("\u0a25\u09f1\3\2\2\2\u0a25\u09f9\3\2\2\2\u0a26\u00cf\3")
        buf.write("\2\2\2\u0a27\u0a29\5\u014c\u00a7\2\u0a28\u0a27\3\2\2\2")
        buf.write("\u0a28\u0a29\3\2\2\2\u0a29\u0a2d\3\2\2\2\u0a2a\u0a2c\7")
        buf.write("\7\2\2\u0a2b\u0a2a\3\2\2\2\u0a2c\u0a2f\3\2\2\2\u0a2d\u0a2b")
        buf.write("\3\2\2\2\u0a2d\u0a2e\3\2\2\2\u0a2e\u0a3e\3\2\2\2\u0a2f")
        buf.write("\u0a2d\3\2\2\2\u0a30\u0a34\5\u0156\u00ac\2\u0a31\u0a33")
        buf.write("\7\7\2\2\u0a32\u0a31\3\2\2\2\u0a33\u0a36\3\2\2\2\u0a34")
        buf.write("\u0a32\3\2\2\2\u0a34\u0a35\3\2\2\2\u0a35\u0a37\3\2\2\2")
        buf.write("\u0a36\u0a34\3\2\2\2\u0a37\u0a3b\7\36\2\2\u0a38\u0a3a")
        buf.write("\7\7\2\2\u0a39\u0a38\3\2\2\2\u0a3a\u0a3d\3\2\2\2\u0a3b")
        buf.write("\u0a39\3\2\2\2\u0a3b\u0a3c\3\2\2\2\u0a3c\u0a3f\3\2\2\2")
        buf.write("\u0a3d\u0a3b\3\2\2\2\u0a3e\u0a30\3\2\2\2\u0a3e\u0a3f\3")
        buf.write("\2\2\2\u0a3f\u0a41\3\2\2\2\u0a40\u0a42\7\21\2\2\u0a41")
        buf.write("\u0a40\3\2\2\2\u0a41\u0a42\3\2\2\2\u0a42\u0a46\3\2\2\2")
        buf.write("\u0a43\u0a45\7\7\2\2\u0a44\u0a43\3\2\2\2\u0a45\u0a48\3")
        buf.write("\2\2\2\u0a46\u0a44\3\2\2\2\u0a46\u0a47\3\2\2\2\u0a47\u0a49")
        buf.write("\3\2\2\2\u0a48\u0a46\3\2\2\2\u0a49\u0a4a\5\u0096L\2\u0a4a")
        buf.write("\u00d1\3\2\2\2\u0a4b\u0a5a\5\u00d4k\2\u0a4c\u0a5a\5\u0156")
        buf.write("\u00ac\2\u0a4d\u0a5a\5\u00d8m\2\u0a4e\u0a5a\5\u00dan\2")
        buf.write("\u0a4f\u0a5a\5\u010e\u0088\2\u0a50\u0a5a\5\u00f0y\2\u0a51")
        buf.write("\u0a5a\5\u00f2z\2\u0a52\u0a5a\5\u00d6l\2\u0a53\u0a5a\5")
        buf.write("\u00f4{\2\u0a54\u0a5a\5\u00f6|\2\u0a55\u0a5a\5\u00f8}")
        buf.write("\2\u0a56\u0a5a\5\u00fc\177\2\u0a57\u0a5a\5\u0106\u0084")
        buf.write("\2\u0a58\u0a5a\5\u010c\u0087\2\u0a59\u0a4b\3\2\2\2\u0a59")
        buf.write("\u0a4c\3\2\2\2\u0a59\u0a4d\3\2\2\2\u0a59\u0a4e\3\2\2\2")
        buf.write("\u0a59\u0a4f\3\2\2\2\u0a59\u0a50\3\2\2\2\u0a59\u0a51\3")
        buf.write("\2\2\2\u0a59\u0a52\3\2\2\2\u0a59\u0a53\3\2\2\2\u0a59\u0a54")
        buf.write("\3\2\2\2\u0a59\u0a55\3\2\2\2\u0a59\u0a56\3\2\2\2\u0a59")
        buf.write("\u0a57\3\2\2\2\u0a59\u0a58\3\2\2\2\u0a5a\u00d3\3\2\2\2")
        buf.write("\u0a5b\u0a5f\7\13\2\2\u0a5c\u0a5e\7\7\2\2\u0a5d\u0a5c")
        buf.write("\3\2\2\2\u0a5e\u0a61\3\2\2\2\u0a5f\u0a5d\3\2\2\2\u0a5f")
        buf.write("\u0a60\3\2\2\2\u0a60\u0a62\3\2\2\2\u0a61\u0a5f\3\2\2\2")
        buf.write("\u0a62\u0a66\5\u0096L\2\u0a63\u0a65\7\7\2\2\u0a64\u0a63")
        buf.write("\3\2\2\2\u0a65\u0a68\3\2\2\2\u0a66\u0a64\3\2\2\2\u0a66")
        buf.write("\u0a67\3\2\2\2\u0a67\u0a69\3\2\2\2\u0a68\u0a66\3\2\2\2")
        buf.write("\u0a69\u0a6a\7\f\2\2\u0a6a\u00d5\3\2\2\2\u0a6b\u0a6f\7")
        buf.write("\r\2\2\u0a6c\u0a6e\7\7\2\2\u0a6d\u0a6c\3\2\2\2\u0a6e\u0a71")
        buf.write("\3\2\2\2\u0a6f\u0a6d\3\2\2\2\u0a6f\u0a70\3\2\2\2\u0a70")
        buf.write("\u0a72\3\2\2\2\u0a71\u0a6f\3\2\2\2\u0a72\u0a83\5\u0096")
        buf.write("L\2\u0a73\u0a75\7\7\2\2\u0a74\u0a73\3\2\2\2\u0a75\u0a78")
        buf.write("\3\2\2\2\u0a76\u0a74\3\2\2\2\u0a76\u0a77\3\2\2\2\u0a77")
        buf.write("\u0a79\3\2\2\2\u0a78\u0a76\3\2\2\2\u0a79\u0a7d\7\n\2\2")
        buf.write("\u0a7a\u0a7c\7\7\2\2\u0a7b\u0a7a\3\2\2\2\u0a7c\u0a7f\3")
        buf.write("\2\2\2\u0a7d\u0a7b\3\2\2\2\u0a7d\u0a7e\3\2\2\2\u0a7e\u0a80")
        buf.write("\3\2\2\2\u0a7f\u0a7d\3\2\2\2\u0a80\u0a82\5\u0096L\2\u0a81")
        buf.write("\u0a76\3\2\2\2\u0a82\u0a85\3\2\2\2\u0a83\u0a81\3\2\2\2")
        buf.write("\u0a83\u0a84\3\2\2\2\u0a84\u0a8d\3\2\2\2\u0a85\u0a83\3")
        buf.write("\2\2\2\u0a86\u0a88\7\7\2\2\u0a87\u0a86\3\2\2\2\u0a88\u0a8b")
        buf.write("\3\2\2\2\u0a89\u0a87\3\2\2\2\u0a89\u0a8a\3\2\2\2\u0a8a")
        buf.write("\u0a8c\3\2\2\2\u0a8b\u0a89\3\2\2\2\u0a8c\u0a8e\7\n\2\2")
        buf.write("\u0a8d\u0a89\3\2\2\2\u0a8d\u0a8e\3\2\2\2\u0a8e\u0a92\3")
        buf.write("\2\2\2\u0a8f\u0a91\7\7\2\2\u0a90\u0a8f\3\2\2\2\u0a91\u0a94")
        buf.write("\3\2\2\2\u0a92\u0a90\3\2\2\2\u0a92\u0a93\3\2\2\2\u0a93")
        buf.write("\u0a95\3\2\2\2\u0a94\u0a92\3\2\2\2\u0a95\u0a96\7\16\2")
        buf.write("\2\u0a96\u0aa0\3\2\2\2\u0a97\u0a9b\7\r\2\2\u0a98\u0a9a")
        buf.write("\7\7\2\2\u0a99\u0a98\3\2\2\2\u0a9a\u0a9d\3\2\2\2\u0a9b")
        buf.write("\u0a99\3\2\2\2\u0a9b\u0a9c\3\2\2\2\u0a9c\u0a9e\3\2\2\2")
        buf.write("\u0a9d\u0a9b\3\2\2\2\u0a9e\u0aa0\7\16\2\2\u0a9f\u0a6b")
        buf.write("\3\2\2\2\u0a9f\u0a97\3\2\2\2\u0aa0\u00d7\3\2\2\2\u0aa1")
        buf.write("\u0aa2\t\7\2\2\u0aa2\u00d9\3\2\2\2\u0aa3\u0aa6\5\u00dc")
        buf.write("o\2\u0aa4\u0aa6\5\u00dep\2\u0aa5\u0aa3\3\2\2\2\u0aa5\u0aa4")
        buf.write("\3\2\2\2\u0aa6\u00db\3\2\2\2\u0aa7\u0aac\7\u0096\2\2\u0aa8")
        buf.write("\u0aab\5\u00e0q\2\u0aa9\u0aab\5\u00e2r\2\u0aaa\u0aa8\3")
        buf.write("\2\2\2\u0aaa\u0aa9\3\2\2\2\u0aab\u0aae\3\2\2\2\u0aac\u0aaa")
        buf.write("\3\2\2\2\u0aac\u0aad\3\2\2\2\u0aad\u0aaf\3\2\2\2\u0aae")
        buf.write("\u0aac\3\2\2\2\u0aaf\u0ab0\7\u009f\2\2\u0ab0\u00dd\3\2")
        buf.write("\2\2\u0ab1\u0ab7\7\u0097\2\2\u0ab2\u0ab6\5\u00e4s\2\u0ab3")
        buf.write("\u0ab6\5\u00e6t\2\u0ab4\u0ab6\7\u00a5\2\2\u0ab5\u0ab2")
        buf.write("\3\2\2\2\u0ab5\u0ab3\3\2\2\2\u0ab5\u0ab4\3\2\2\2\u0ab6")
        buf.write("\u0ab9\3\2\2\2\u0ab7\u0ab5\3\2\2\2\u0ab7\u0ab8\3\2\2\2")
        buf.write("\u0ab8\u0aba\3\2\2\2\u0ab9\u0ab7\3\2\2\2\u0aba\u0abb\7")
        buf.write("\u00a4\2\2\u0abb\u00df\3\2\2\2\u0abc\u0abd\t\b\2\2\u0abd")
        buf.write("\u00e1\3\2\2\2\u0abe\u0abf\7\u00a3\2\2\u0abf\u0ac0\5\u0096")
        buf.write("L\2\u0ac0\u0ac1\7\20\2\2\u0ac1\u00e3\3\2\2\2\u0ac2\u0ac3")
        buf.write("\t\t\2\2\u0ac3\u00e5\3\2\2\2\u0ac4\u0ac8\7\u00a8\2\2\u0ac5")
        buf.write("\u0ac7\7\7\2\2\u0ac6\u0ac5\3\2\2\2\u0ac7\u0aca\3\2\2\2")
        buf.write("\u0ac8\u0ac6\3\2\2\2\u0ac8\u0ac9\3\2\2\2\u0ac9\u0acb\3")
        buf.write("\2\2\2\u0aca\u0ac8\3\2\2\2\u0acb\u0acf\5\u0096L\2\u0acc")
        buf.write("\u0ace\7\7\2\2\u0acd\u0acc\3\2\2\2\u0ace\u0ad1\3\2\2\2")
        buf.write("\u0acf\u0acd\3\2\2\2\u0acf\u0ad0\3\2\2\2\u0ad0\u0ad2\3")
        buf.write("\2\2\2\u0ad1\u0acf\3\2\2\2\u0ad2\u0ad3\7\20\2\2\u0ad3")
        buf.write("\u00e7\3\2\2\2\u0ad4\u0ad8\7\17\2\2\u0ad5\u0ad7\7\7\2")
        buf.write("\2\u0ad6\u0ad5\3\2\2\2\u0ad7\u0ada\3\2\2\2\u0ad8\u0ad6")
        buf.write("\3\2\2\2\u0ad8\u0ad9\3\2\2\2\u0ad9\u0adb\3\2\2\2\u0ada")
        buf.write("\u0ad8\3\2\2\2\u0adb\u0adf\5~@\2\u0adc\u0ade\7\7\2\2\u0add")
        buf.write("\u0adc\3\2\2\2\u0ade\u0ae1\3\2\2\2\u0adf\u0add\3\2\2\2")
        buf.write("\u0adf\u0ae0\3\2\2\2\u0ae0\u0ae2\3\2\2\2\u0ae1\u0adf\3")
        buf.write("\2\2\2\u0ae2\u0ae3\7\20\2\2\u0ae3\u0b05\3\2\2\2\u0ae4")
        buf.write("\u0ae8\7\17\2\2\u0ae5\u0ae7\7\7\2\2\u0ae6\u0ae5\3\2\2")
        buf.write("\2\u0ae7\u0aea\3\2\2\2\u0ae8\u0ae6\3\2\2\2\u0ae8\u0ae9")
        buf.write("\3\2\2\2\u0ae9\u0aec\3\2\2\2\u0aea\u0ae8\3\2\2\2\u0aeb")
        buf.write("\u0aed\5\u00eav\2\u0aec\u0aeb\3\2\2\2\u0aec\u0aed\3\2")
        buf.write("\2\2\u0aed\u0af1\3\2\2\2\u0aee\u0af0\7\7\2\2\u0aef\u0aee")
        buf.write("\3\2\2\2\u0af0\u0af3\3\2\2\2\u0af1\u0aef\3\2\2\2\u0af1")
        buf.write("\u0af2\3\2\2\2\u0af2\u0af4\3\2\2\2\u0af3\u0af1\3\2\2\2")
        buf.write("\u0af4\u0af8\7$\2\2\u0af5\u0af7\7\7\2\2\u0af6\u0af5\3")
        buf.write("\2\2\2\u0af7\u0afa\3\2\2\2\u0af8\u0af6\3\2\2\2\u0af8\u0af9")
        buf.write("\3\2\2\2\u0af9\u0afb\3\2\2\2\u0afa\u0af8\3\2\2\2\u0afb")
        buf.write("\u0aff\5~@\2\u0afc\u0afe\7\7\2\2\u0afd\u0afc\3\2\2\2\u0afe")
        buf.write("\u0b01\3\2\2\2\u0aff\u0afd\3\2\2\2\u0aff\u0b00\3\2\2\2")
        buf.write("\u0b00\u0b02\3\2\2\2\u0b01\u0aff\3\2\2\2\u0b02\u0b03\7")
        buf.write("\20\2\2\u0b03\u0b05\3\2\2\2\u0b04\u0ad4\3\2\2\2\u0b04")
        buf.write("\u0ae4\3\2\2\2\u0b05\u00e9\3\2\2\2\u0b06\u0b17\5\u00ec")
        buf.write("w\2\u0b07\u0b09\7\7\2\2\u0b08\u0b07\3\2\2\2\u0b09\u0b0c")
        buf.write("\3\2\2\2\u0b0a\u0b08\3\2\2\2\u0b0a\u0b0b\3\2\2\2\u0b0b")
        buf.write("\u0b0d\3\2\2\2\u0b0c\u0b0a\3\2\2\2\u0b0d\u0b11\7\n\2\2")
        buf.write("\u0b0e\u0b10\7\7\2\2\u0b0f\u0b0e\3\2\2\2\u0b10\u0b13\3")
        buf.write("\2\2\2\u0b11\u0b0f\3\2\2\2\u0b11\u0b12\3\2\2\2\u0b12\u0b14")
        buf.write("\3\2\2\2\u0b13\u0b11\3\2\2\2\u0b14\u0b16\5\u00ecw\2\u0b15")
        buf.write("\u0b0a\3\2\2\2\u0b16\u0b19\3\2\2\2\u0b17\u0b15\3\2\2\2")
        buf.write("\u0b17\u0b18\3\2\2\2\u0b18\u0b21\3\2\2\2\u0b19\u0b17\3")
        buf.write("\2\2\2\u0b1a\u0b1c\7\7\2\2\u0b1b\u0b1a\3\2\2\2\u0b1c\u0b1f")
        buf.write("\3\2\2\2\u0b1d\u0b1b\3\2\2\2\u0b1d\u0b1e\3\2\2\2\u0b1e")
        buf.write("\u0b20\3\2\2\2\u0b1f\u0b1d\3\2\2\2\u0b20\u0b22\7\n\2\2")
        buf.write("\u0b21\u0b1d\3\2\2\2\u0b21\u0b22\3\2\2\2\u0b22\u00eb\3")
        buf.write("\2\2\2\u0b23\u0b36\5D#\2\u0b24\u0b33\5F$\2\u0b25\u0b27")
        buf.write("\7\7\2\2\u0b26\u0b25\3\2\2\2\u0b27\u0b2a\3\2\2\2\u0b28")
        buf.write("\u0b26\3\2\2\2\u0b28\u0b29\3\2\2\2\u0b29\u0b2b\3\2\2\2")
        buf.write("\u0b2a\u0b28\3\2\2\2\u0b2b\u0b2f\7\34\2\2\u0b2c\u0b2e")
        buf.write("\7\7\2\2\u0b2d\u0b2c\3\2\2\2\u0b2e\u0b31\3\2\2\2\u0b2f")
        buf.write("\u0b2d\3\2\2\2\u0b2f\u0b30\3\2\2\2\u0b30\u0b32\3\2\2\2")
        buf.write("\u0b31\u0b2f\3\2\2\2\u0b32\u0b34\5b\62\2\u0b33\u0b28\3")
        buf.write("\2\2\2\u0b33\u0b34\3\2\2\2\u0b34\u0b36\3\2\2\2\u0b35\u0b23")
        buf.write("\3\2\2\2\u0b35\u0b24\3\2\2\2\u0b36\u00ed\3\2\2\2\u0b37")
        buf.write("\u0b47\7L\2\2\u0b38\u0b3a\7\7\2\2\u0b39\u0b38\3\2\2\2")
        buf.write("\u0b3a\u0b3d\3\2\2\2\u0b3b\u0b39\3\2\2\2\u0b3b\u0b3c\3")
        buf.write("\2\2\2\u0b3c\u0b3e\3\2\2\2\u0b3d\u0b3b\3\2\2\2\u0b3e\u0b42")
        buf.write("\5b\62\2\u0b3f\u0b41\7\7\2\2\u0b40\u0b3f\3\2\2\2\u0b41")
        buf.write("\u0b44\3\2\2\2\u0b42\u0b40\3\2\2\2\u0b42\u0b43\3\2\2\2")
        buf.write("\u0b43\u0b45\3\2\2\2\u0b44\u0b42\3\2\2\2\u0b45\u0b46\7")
        buf.write("\t\2\2\u0b46\u0b48\3\2\2\2\u0b47\u0b3b\3\2\2\2\u0b47\u0b48")
        buf.write("\3\2\2\2\u0b48\u0b4c\3\2\2\2\u0b49\u0b4b\7\7\2\2\u0b4a")
        buf.write("\u0b49\3\2\2\2\u0b4b\u0b4e\3\2\2\2\u0b4c\u0b4a\3\2\2\2")
        buf.write("\u0b4c\u0b4d\3\2\2\2\u0b4d\u0b4f\3\2\2\2\u0b4e\u0b4c\3")
        buf.write("\2\2\2\u0b4f\u0b5e\5P)\2\u0b50\u0b52\7\7\2\2\u0b51\u0b50")
        buf.write("\3\2\2\2\u0b52\u0b55\3\2\2\2\u0b53\u0b51\3\2\2\2\u0b53")
        buf.write("\u0b54\3\2\2\2\u0b54\u0b56\3\2\2\2\u0b55\u0b53\3\2\2\2")
        buf.write("\u0b56\u0b5a\7\34\2\2\u0b57\u0b59\7\7\2\2\u0b58\u0b57")
        buf.write("\3\2\2\2\u0b59\u0b5c\3\2\2\2\u0b5a\u0b58\3\2\2\2\u0b5a")
        buf.write("\u0b5b\3\2\2\2\u0b5b\u0b5d\3\2\2\2\u0b5c\u0b5a\3\2\2\2")
        buf.write("\u0b5d\u0b5f\5b\62\2\u0b5e\u0b53\3\2\2\2\u0b5e\u0b5f\3")
        buf.write("\2\2\2\u0b5f\u0b67\3\2\2\2\u0b60\u0b62\7\7\2\2\u0b61\u0b60")
        buf.write("\3\2\2\2\u0b62\u0b65\3\2\2\2\u0b63\u0b61\3\2\2\2\u0b63")
        buf.write("\u0b64\3\2\2\2\u0b64\u0b66\3\2\2\2\u0b65\u0b63\3\2\2\2")
        buf.write("\u0b66\u0b68\5\60\31\2\u0b67\u0b63\3\2\2\2\u0b67\u0b68")
        buf.write("\3\2\2\2\u0b68\u0b70\3\2\2\2\u0b69\u0b6b\7\7\2\2\u0b6a")
        buf.write("\u0b69\3\2\2\2\u0b6b\u0b6e\3\2\2\2\u0b6c\u0b6a\3\2\2\2")
        buf.write("\u0b6c\u0b6d\3\2\2\2\u0b6d\u0b6f\3\2\2\2\u0b6e\u0b6c\3")
        buf.write("\2\2\2\u0b6f\u0b71\5B\"\2\u0b70\u0b6c\3\2\2\2\u0b70\u0b71")
        buf.write("\3\2\2\2\u0b71\u00ef\3\2\2\2\u0b72\u0b75\5\u00e8u\2\u0b73")
        buf.write("\u0b75\5\u00eex\2\u0b74\u0b72\3\2\2\2\u0b74\u0b73\3\2")
        buf.write("\2\2\u0b75\u00f1\3\2\2\2\u0b76\u0b7a\7M\2\2\u0b77\u0b79")
        buf.write("\7\7\2\2\u0b78\u0b77\3\2\2\2\u0b79\u0b7c\3\2\2\2\u0b7a")
        buf.write("\u0b78\3\2\2\2\u0b7a\u0b7b\3\2\2\2\u0b7b\u0b7d\3\2\2\2")
        buf.write("\u0b7c\u0b7a\3\2\2\2\u0b7d\u0b81\7\34\2\2\u0b7e\u0b80")
        buf.write("\7\7\2\2\u0b7f\u0b7e\3\2\2\2\u0b80\u0b83\3\2\2\2\u0b81")
        buf.write("\u0b7f\3\2\2\2\u0b81\u0b82\3\2\2\2\u0b82\u0b84\3\2\2\2")
        buf.write("\u0b83\u0b81\3\2\2\2\u0b84\u0b88\5\"\22\2\u0b85\u0b87")
        buf.write("\7\7\2\2\u0b86\u0b85\3\2\2\2\u0b87\u0b8a\3\2\2\2\u0b88")
        buf.write("\u0b86\3\2\2\2\u0b88\u0b89\3\2\2\2\u0b89\u0b8b\3\2\2\2")
        buf.write("\u0b8a\u0b88\3\2\2\2\u0b8b\u0b8c\5\34\17\2\u0b8c\u0b96")
        buf.write("\3\2\2\2\u0b8d\u0b91\7M\2\2\u0b8e\u0b90\7\7\2\2\u0b8f")
        buf.write("\u0b8e\3\2\2\2\u0b90\u0b93\3\2\2\2\u0b91\u0b8f\3\2\2\2")
        buf.write("\u0b91\u0b92\3\2\2\2\u0b92\u0b94\3\2\2\2\u0b93\u0b91\3")
        buf.write("\2\2\2\u0b94\u0b96\5\34\17\2\u0b95\u0b76\3\2\2\2\u0b95")
        buf.write("\u0b8d\3\2\2\2\u0b96\u00f3\3\2\2\2\u0b97\u0b98\t\n\2\2")
        buf.write("\u0b98\u00f5\3\2\2\2\u0b99\u0baa\7V\2\2\u0b9a\u0b9e\7")
        buf.write("\60\2\2\u0b9b\u0b9d\7\7\2\2\u0b9c\u0b9b\3\2\2\2\u0b9d")
        buf.write("\u0ba0\3\2\2\2\u0b9e\u0b9c\3\2\2\2\u0b9e\u0b9f\3\2\2\2")
        buf.write("\u0b9f\u0ba1\3\2\2\2\u0ba0\u0b9e\3\2\2\2\u0ba1\u0ba5\5")
        buf.write("b\62\2\u0ba2\u0ba4\7\7\2\2\u0ba3\u0ba2\3\2\2\2\u0ba4\u0ba7")
        buf.write("\3\2\2\2\u0ba5\u0ba3\3\2\2\2\u0ba5\u0ba6\3\2\2\2\u0ba6")
        buf.write("\u0ba8\3\2\2\2\u0ba7\u0ba5\3\2\2\2\u0ba8\u0ba9\7\61\2")
        buf.write("\2\u0ba9\u0bab\3\2\2\2\u0baa\u0b9a\3\2\2\2\u0baa\u0bab")
        buf.write("\3\2\2\2\u0bab\u0bae\3\2\2\2\u0bac\u0bad\7*\2\2\u0bad")
        buf.write("\u0baf\5\u0156\u00ac\2\u0bae\u0bac\3\2\2\2\u0bae\u0baf")
        buf.write("\3\2\2\2\u0baf\u0bb2\3\2\2\2\u0bb0\u0bb2\7>\2\2\u0bb1")
        buf.write("\u0b99\3\2\2\2\u0bb1\u0bb0\3\2\2\2\u0bb2\u00f7\3\2\2\2")
        buf.write("\u0bb3\u0bb7\7Y\2\2\u0bb4\u0bb6\7\7\2\2\u0bb5\u0bb4\3")
        buf.write("\2\2\2\u0bb6\u0bb9\3\2\2\2\u0bb7\u0bb5\3\2\2\2\u0bb7\u0bb8")
        buf.write("\3\2\2\2\u0bb8\u0bba\3\2\2\2\u0bb9\u0bb7\3\2\2\2\u0bba")
        buf.write("\u0bbe\7\13\2\2\u0bbb\u0bbd\7\7\2\2\u0bbc\u0bbb\3\2\2")
        buf.write("\2\u0bbd\u0bc0\3\2\2\2\u0bbe\u0bbc\3\2\2\2\u0bbe\u0bbf")
        buf.write("\3\2\2\2\u0bbf\u0bc1\3\2\2\2\u0bc0\u0bbe\3\2\2\2\u0bc1")
        buf.write("\u0bc5\5\u0096L\2\u0bc2\u0bc4\7\7\2\2\u0bc3\u0bc2\3\2")
        buf.write("\2\2\u0bc4\u0bc7\3\2\2\2\u0bc5\u0bc3\3\2\2\2\u0bc5\u0bc6")
        buf.write("\3\2\2\2\u0bc6\u0bc8\3\2\2\2\u0bc7\u0bc5\3\2\2\2\u0bc8")
        buf.write("\u0bcc\7\f\2\2\u0bc9\u0bcb\7\7\2\2\u0bca\u0bc9\3\2\2\2")
        buf.write("\u0bcb\u0bce\3\2\2\2\u0bcc\u0bca\3\2\2\2\u0bcc\u0bcd\3")
        buf.write("\2\2\2\u0bcd\u0bd1\3\2\2\2\u0bce\u0bcc\3\2\2\2\u0bcf\u0bd2")
        buf.write("\5\u0084C\2\u0bd0\u0bd2\7\35\2\2\u0bd1\u0bcf\3\2\2\2\u0bd1")
        buf.write("\u0bd0\3\2\2\2\u0bd2\u0c0d\3\2\2\2\u0bd3\u0bd7\7Y\2\2")
        buf.write("\u0bd4\u0bd6\7\7\2\2\u0bd5\u0bd4\3\2\2\2\u0bd6\u0bd9\3")
        buf.write("\2\2\2\u0bd7\u0bd5\3\2\2\2\u0bd7\u0bd8\3\2\2\2\u0bd8\u0bda")
        buf.write("\3\2\2\2\u0bd9\u0bd7\3\2\2\2\u0bda\u0bde\7\13\2\2\u0bdb")
        buf.write("\u0bdd\7\7\2\2\u0bdc\u0bdb\3\2\2\2\u0bdd\u0be0\3\2\2\2")
        buf.write("\u0bde\u0bdc\3\2\2\2\u0bde\u0bdf\3\2\2\2\u0bdf\u0be1\3")
        buf.write("\2\2\2\u0be0\u0bde\3\2\2\2\u0be1\u0be5\5\u0096L\2\u0be2")
        buf.write("\u0be4\7\7\2\2\u0be3\u0be2\3\2\2\2\u0be4\u0be7\3\2\2\2")
        buf.write("\u0be5\u0be3\3\2\2\2\u0be5\u0be6\3\2\2\2\u0be6\u0be8\3")
        buf.write("\2\2\2\u0be7\u0be5\3\2\2\2\u0be8\u0bec\7\f\2\2\u0be9\u0beb")
        buf.write("\7\7\2\2\u0bea\u0be9\3\2\2\2\u0beb\u0bee\3\2\2\2\u0bec")
        buf.write("\u0bea\3\2\2\2\u0bec\u0bed\3\2\2\2\u0bed\u0bf0\3\2\2\2")
        buf.write("\u0bee\u0bec\3\2\2\2\u0bef\u0bf1\5\u0084C\2\u0bf0\u0bef")
        buf.write("\3\2\2\2\u0bf0\u0bf1\3\2\2\2\u0bf1\u0bf5\3\2\2\2\u0bf2")
        buf.write("\u0bf4\7\7\2\2\u0bf3\u0bf2\3\2\2\2\u0bf4\u0bf7\3\2\2\2")
        buf.write("\u0bf5\u0bf3\3\2\2\2\u0bf5\u0bf6\3\2\2\2\u0bf6\u0bf9\3")
        buf.write("\2\2\2\u0bf7\u0bf5\3\2\2\2\u0bf8\u0bfa\7\35\2\2\u0bf9")
        buf.write("\u0bf8\3\2\2\2\u0bf9\u0bfa\3\2\2\2\u0bfa\u0bfe\3\2\2\2")
        buf.write("\u0bfb\u0bfd\7\7\2\2\u0bfc\u0bfb\3\2\2\2\u0bfd\u0c00\3")
        buf.write("\2\2\2\u0bfe\u0bfc\3\2\2\2\u0bfe\u0bff\3\2\2\2\u0bff\u0c01")
        buf.write("\3\2\2\2\u0c00\u0bfe\3\2\2\2\u0c01\u0c05\7Z\2\2\u0c02")
        buf.write("\u0c04\7\7\2\2\u0c03\u0c02\3\2\2\2\u0c04\u0c07\3\2\2\2")
        buf.write("\u0c05\u0c03\3\2\2\2\u0c05\u0c06\3\2\2\2\u0c06\u0c0a\3")
        buf.write("\2\2\2\u0c07\u0c05\3\2\2\2\u0c08\u0c0b\5\u0084C\2\u0c09")
        buf.write("\u0c0b\7\35\2\2\u0c0a\u0c08\3\2\2\2\u0c0a\u0c09\3\2\2")
        buf.write("\2\u0c0b\u0c0d\3\2\2\2\u0c0c\u0bb3\3\2\2\2\u0c0c\u0bd3")
        buf.write("\3\2\2\2\u0c0d\u00f9\3\2\2\2\u0c0e\u0c30\7\13\2\2\u0c0f")
        buf.write("\u0c11\5\u014c\u00a7\2\u0c10\u0c0f\3\2\2\2\u0c11\u0c14")
        buf.write("\3\2\2\2\u0c12\u0c10\3\2\2\2\u0c12\u0c13\3\2\2\2\u0c13")
        buf.write("\u0c18\3\2\2\2\u0c14\u0c12\3\2\2\2\u0c15\u0c17\7\7\2\2")
        buf.write("\u0c16\u0c15\3\2\2\2\u0c17\u0c1a\3\2\2\2\u0c18\u0c16\3")
        buf.write("\2\2\2\u0c18\u0c19\3\2\2\2\u0c19\u0c1b\3\2\2\2\u0c1a\u0c18")
        buf.write("\3\2\2\2\u0c1b\u0c1f\7N\2\2\u0c1c\u0c1e\7\7\2\2\u0c1d")
        buf.write("\u0c1c\3\2\2\2\u0c1e\u0c21\3\2\2\2\u0c1f\u0c1d\3\2\2\2")
        buf.write("\u0c1f\u0c20\3\2\2\2\u0c20\u0c22\3\2\2\2\u0c21\u0c1f\3")
        buf.write("\2\2\2\u0c22\u0c26\5D#\2\u0c23\u0c25\7\7\2\2\u0c24\u0c23")
        buf.write("\3\2\2\2\u0c25\u0c28\3\2\2\2\u0c26\u0c24\3\2\2\2\u0c26")
        buf.write("\u0c27\3\2\2\2\u0c27\u0c29\3\2\2\2\u0c28\u0c26\3\2\2\2")
        buf.write("\u0c29\u0c2d\7\36\2\2\u0c2a\u0c2c\7\7\2\2\u0c2b\u0c2a")
        buf.write("\3\2\2\2\u0c2c\u0c2f\3\2\2\2\u0c2d\u0c2b\3\2\2\2\u0c2d")
        buf.write("\u0c2e\3\2\2\2\u0c2e\u0c31\3\2\2\2\u0c2f\u0c2d\3\2\2\2")
        buf.write("\u0c30\u0c12\3\2\2\2\u0c30\u0c31\3\2\2\2\u0c31\u0c32\3")
        buf.write("\2\2\2\u0c32\u0c33\5\u0096L\2\u0c33\u0c34\7\f\2\2\u0c34")
        buf.write("\u00fb\3\2\2\2\u0c35\u0c39\7[\2\2\u0c36\u0c38\7\7\2\2")
        buf.write("\u0c37\u0c36\3\2\2\2\u0c38\u0c3b\3\2\2\2\u0c39\u0c37\3")
        buf.write("\2\2\2\u0c39\u0c3a\3\2\2\2\u0c3a\u0c3d\3\2\2\2\u0c3b\u0c39")
        buf.write("\3\2\2\2\u0c3c\u0c3e\5\u00fa~\2\u0c3d\u0c3c\3\2\2\2\u0c3d")
        buf.write("\u0c3e\3\2\2\2\u0c3e\u0c42\3\2\2\2\u0c3f\u0c41\7\7\2\2")
        buf.write("\u0c40\u0c3f\3\2\2\2\u0c41\u0c44\3\2\2\2\u0c42\u0c40\3")
        buf.write("\2\2\2\u0c42\u0c43\3\2\2\2\u0c43\u0c45\3\2\2\2\u0c44\u0c42")
        buf.write("\3\2\2\2\u0c45\u0c49\7\17\2\2\u0c46\u0c48\7\7\2\2\u0c47")
        buf.write("\u0c46\3\2\2\2\u0c48\u0c4b\3\2\2\2\u0c49\u0c47\3\2\2\2")
        buf.write("\u0c49\u0c4a\3\2\2\2\u0c4a\u0c55\3\2\2\2\u0c4b\u0c49\3")
        buf.write("\2\2\2\u0c4c\u0c50\5\u00fe\u0080\2\u0c4d\u0c4f\7\7\2\2")
        buf.write("\u0c4e\u0c4d\3\2\2\2\u0c4f\u0c52\3\2\2\2\u0c50\u0c4e\3")
        buf.write("\2\2\2\u0c50\u0c51\3\2\2\2\u0c51\u0c54\3\2\2\2\u0c52\u0c50")
        buf.write("\3\2\2\2\u0c53\u0c4c\3\2\2\2\u0c54\u0c57\3\2\2\2\u0c55")
        buf.write("\u0c53\3\2\2\2\u0c55\u0c56\3\2\2\2\u0c56\u0c5b\3\2\2\2")
        buf.write("\u0c57\u0c55\3\2\2\2\u0c58\u0c5a\7\7\2\2\u0c59\u0c58\3")
        buf.write("\2\2\2\u0c5a\u0c5d\3\2\2\2\u0c5b\u0c59\3\2\2\2\u0c5b\u0c5c")
        buf.write("\3\2\2\2\u0c5c\u0c5e\3\2\2\2\u0c5d\u0c5b\3\2\2\2\u0c5e")
        buf.write("\u0c5f\7\20\2\2\u0c5f\u00fd\3\2\2\2\u0c60\u0c71\5\u0100")
        buf.write("\u0081\2\u0c61\u0c63\7\7\2\2\u0c62\u0c61\3\2\2\2\u0c63")
        buf.write("\u0c66\3\2\2\2\u0c64\u0c62\3\2\2\2\u0c64\u0c65\3\2\2\2")
        buf.write("\u0c65\u0c67\3\2\2\2\u0c66\u0c64\3\2\2\2\u0c67\u0c6b\7")
        buf.write("\n\2\2\u0c68\u0c6a\7\7\2\2\u0c69\u0c68\3\2\2\2\u0c6a\u0c6d")
        buf.write("\3\2\2\2\u0c6b\u0c69\3\2\2\2\u0c6b\u0c6c\3\2\2\2\u0c6c")
        buf.write("\u0c6e\3\2\2\2\u0c6d\u0c6b\3\2\2\2\u0c6e\u0c70\5\u0100")
        buf.write("\u0081\2\u0c6f\u0c64\3\2\2\2\u0c70\u0c73\3\2\2\2\u0c71")
        buf.write("\u0c6f\3\2\2\2\u0c71\u0c72\3\2\2\2\u0c72\u0c7b\3\2\2\2")
        buf.write("\u0c73\u0c71\3\2\2\2\u0c74\u0c76\7\7\2\2\u0c75\u0c74\3")
        buf.write("\2\2\2\u0c76\u0c79\3\2\2\2\u0c77\u0c75\3\2\2\2\u0c77\u0c78")
        buf.write("\3\2\2\2\u0c78\u0c7a\3\2\2\2\u0c79\u0c77\3\2\2\2\u0c7a")
        buf.write("\u0c7c\7\n\2\2\u0c7b\u0c77\3\2\2\2\u0c7b\u0c7c\3\2\2\2")
        buf.write("\u0c7c\u0c80\3\2\2\2\u0c7d\u0c7f\7\7\2\2\u0c7e\u0c7d\3")
        buf.write("\2\2\2\u0c7f\u0c82\3\2\2\2\u0c80\u0c7e\3\2\2\2\u0c80\u0c81")
        buf.write("\3\2\2\2\u0c81\u0c83\3\2\2\2\u0c82\u0c80\3\2\2\2\u0c83")
        buf.write("\u0c87\7$\2\2\u0c84\u0c86\7\7\2\2\u0c85\u0c84\3\2\2\2")
        buf.write("\u0c86\u0c89\3\2\2\2\u0c87\u0c85\3\2\2\2\u0c87\u0c88\3")
        buf.write("\2\2\2\u0c88\u0c8a\3\2\2\2\u0c89\u0c87\3\2\2\2\u0c8a\u0c8c")
        buf.write("\5\u0084C\2\u0c8b\u0c8d\5\u0092J\2\u0c8c\u0c8b\3\2\2\2")
        buf.write("\u0c8c\u0c8d\3\2\2\2\u0c8d\u0ca1\3\2\2\2\u0c8e\u0c92\7")
        buf.write("Z\2\2\u0c8f\u0c91\7\7\2\2\u0c90\u0c8f\3\2\2\2\u0c91\u0c94")
        buf.write("\3\2\2\2\u0c92\u0c90\3\2\2\2\u0c92\u0c93\3\2\2\2\u0c93")
        buf.write("\u0c95\3\2\2\2\u0c94\u0c92\3\2\2\2\u0c95\u0c99\7$\2\2")
        buf.write("\u0c96\u0c98\7\7\2\2\u0c97\u0c96\3\2\2\2\u0c98\u0c9b\3")
        buf.write("\2\2\2\u0c99\u0c97\3\2\2\2\u0c99\u0c9a\3\2\2\2\u0c9a\u0c9c")
        buf.write("\3\2\2\2\u0c9b\u0c99\3\2\2\2\u0c9c\u0c9e\5\u0084C\2\u0c9d")
        buf.write("\u0c9f\5\u0092J\2\u0c9e\u0c9d\3\2\2\2\u0c9e\u0c9f\3\2")
        buf.write("\2\2\u0c9f\u0ca1\3\2\2\2\u0ca0\u0c60\3\2\2\2\u0ca0\u0c8e")
        buf.write("\3\2\2\2\u0ca1\u00ff\3\2\2\2\u0ca2\u0ca6\5\u0096L\2\u0ca3")
        buf.write("\u0ca6\5\u0102\u0082\2\u0ca4\u0ca6\5\u0104\u0083\2\u0ca5")
        buf.write("\u0ca2\3\2\2\2\u0ca5\u0ca3\3\2\2\2\u0ca5\u0ca4\3\2\2\2")
        buf.write("\u0ca6\u0101\3\2\2\2\u0ca7\u0cab\5\u0116\u008c\2\u0ca8")
        buf.write("\u0caa\7\7\2\2\u0ca9\u0ca8\3\2\2\2\u0caa\u0cad\3\2\2\2")
        buf.write("\u0cab\u0ca9\3\2\2\2\u0cab\u0cac\3\2\2\2\u0cac\u0cae\3")
        buf.write("\2\2\2\u0cad\u0cab\3\2\2\2\u0cae\u0caf\5\u0096L\2\u0caf")
        buf.write("\u0103\3\2\2\2\u0cb0\u0cb4\5\u0118\u008d\2\u0cb1\u0cb3")
        buf.write("\7\7\2\2\u0cb2\u0cb1\3\2\2\2\u0cb3\u0cb6\3\2\2\2\u0cb4")
        buf.write("\u0cb2\3\2\2\2\u0cb4\u0cb5\3\2\2\2\u0cb5\u0cb7\3\2\2\2")
        buf.write("\u0cb6\u0cb4\3\2\2\2\u0cb7\u0cb8\5b\62\2\u0cb8\u0105\3")
        buf.write("\2\2\2\u0cb9\u0cbd\7\\\2\2\u0cba\u0cbc\7\7\2\2\u0cbb\u0cba")
        buf.write("\3\2\2\2\u0cbc\u0cbf\3\2\2\2\u0cbd\u0cbb\3\2\2\2\u0cbd")
        buf.write("\u0cbe\3\2\2\2\u0cbe\u0cc0\3\2\2\2\u0cbf\u0cbd\3\2\2\2")
        buf.write("\u0cc0\u0cdc\5\u0086D\2\u0cc1\u0cc3\7\7\2\2\u0cc2\u0cc1")
        buf.write("\3\2\2\2\u0cc3\u0cc6\3\2\2\2\u0cc4\u0cc2\3\2\2\2\u0cc4")
        buf.write("\u0cc5\3\2\2\2\u0cc5\u0cc7\3\2\2\2\u0cc6\u0cc4\3\2\2\2")
        buf.write("\u0cc7\u0cc9\5\u0108\u0085\2\u0cc8\u0cc4\3\2\2\2\u0cc9")
        buf.write("\u0cca\3\2\2\2\u0cca\u0cc8\3\2\2\2\u0cca\u0ccb\3\2\2\2")
        buf.write("\u0ccb\u0cd3\3\2\2\2\u0ccc\u0cce\7\7\2\2\u0ccd\u0ccc\3")
        buf.write("\2\2\2\u0cce\u0cd1\3\2\2\2\u0ccf\u0ccd\3\2\2\2\u0ccf\u0cd0")
        buf.write("\3\2\2\2\u0cd0\u0cd2\3\2\2\2\u0cd1\u0ccf\3\2\2\2\u0cd2")
        buf.write("\u0cd4\5\u010a\u0086\2\u0cd3\u0ccf\3\2\2\2\u0cd3\u0cd4")
        buf.write("\3\2\2\2\u0cd4\u0cdd\3\2\2\2\u0cd5\u0cd7\7\7\2\2\u0cd6")
        buf.write("\u0cd5\3\2\2\2\u0cd7\u0cda\3\2\2\2\u0cd8\u0cd6\3\2\2\2")
        buf.write("\u0cd8\u0cd9\3\2\2\2\u0cd9\u0cdb\3\2\2\2\u0cda\u0cd8\3")
        buf.write("\2\2\2\u0cdb\u0cdd\5\u010a\u0086\2\u0cdc\u0cc8\3\2\2\2")
        buf.write("\u0cdc\u0cd8\3\2\2\2\u0cdd\u0107\3\2\2\2\u0cde\u0ce2\7")
        buf.write("]\2\2\u0cdf\u0ce1\7\7\2\2\u0ce0\u0cdf\3\2\2\2\u0ce1\u0ce4")
        buf.write("\3\2\2\2\u0ce2\u0ce0\3\2\2\2\u0ce2\u0ce3\3\2\2\2\u0ce3")
        buf.write("\u0ce5\3\2\2\2\u0ce4\u0ce2\3\2\2\2\u0ce5\u0ce9\7\13\2")
        buf.write("\2\u0ce6\u0ce8\5\u014c\u00a7\2\u0ce7\u0ce6\3\2\2\2\u0ce8")
        buf.write("\u0ceb\3\2\2\2\u0ce9\u0ce7\3\2\2\2\u0ce9\u0cea\3\2\2\2")
        buf.write("\u0cea\u0cec\3\2\2\2\u0ceb\u0ce9\3\2\2\2\u0cec\u0ced\5")
        buf.write("\u0156\u00ac\2\u0ced\u0cee\7\34\2\2\u0cee\u0cf6\5b\62")
        buf.write("\2\u0cef\u0cf1\7\7\2\2\u0cf0\u0cef\3\2\2\2\u0cf1\u0cf4")
        buf.write("\3\2\2\2\u0cf2\u0cf0\3\2\2\2\u0cf2\u0cf3\3\2\2\2\u0cf3")
        buf.write("\u0cf5\3\2\2\2\u0cf4\u0cf2\3\2\2\2\u0cf5\u0cf7\7\n\2\2")
        buf.write("\u0cf6\u0cf2\3\2\2\2\u0cf6\u0cf7\3\2\2\2\u0cf7\u0cf8\3")
        buf.write("\2\2\2\u0cf8\u0cfc\7\f\2\2\u0cf9\u0cfb\7\7\2\2\u0cfa\u0cf9")
        buf.write("\3\2\2\2\u0cfb\u0cfe\3\2\2\2\u0cfc\u0cfa\3\2\2\2\u0cfc")
        buf.write("\u0cfd\3\2\2\2\u0cfd\u0cff\3\2\2\2\u0cfe\u0cfc\3\2\2\2")
        buf.write("\u0cff\u0d00\5\u0086D\2\u0d00\u0109\3\2\2\2\u0d01\u0d05")
        buf.write("\7^\2\2\u0d02\u0d04\7\7\2\2\u0d03\u0d02\3\2\2\2\u0d04")
        buf.write("\u0d07\3\2\2\2\u0d05\u0d03\3\2\2\2\u0d05\u0d06\3\2\2\2")
        buf.write("\u0d06\u0d08\3\2\2\2\u0d07\u0d05\3\2\2\2\u0d08\u0d09\5")
        buf.write("\u0086D\2\u0d09\u010b\3\2\2\2\u0d0a\u0d0e\7b\2\2\u0d0b")
        buf.write("\u0d0d\7\7\2\2\u0d0c\u0d0b\3\2\2\2\u0d0d\u0d10\3\2\2\2")
        buf.write("\u0d0e\u0d0c\3\2\2\2\u0d0e\u0d0f\3\2\2\2\u0d0f\u0d11\3")
        buf.write("\2\2\2\u0d10\u0d0e\3\2\2\2\u0d11\u0d1b\5\u0096L\2\u0d12")
        buf.write("\u0d14\t\13\2\2\u0d13\u0d15\5\u0096L\2\u0d14\u0d13\3\2")
        buf.write("\2\2\u0d14\u0d15\3\2\2\2\u0d15\u0d1b\3\2\2\2\u0d16\u0d1b")
        buf.write("\7d\2\2\u0d17\u0d1b\7;\2\2\u0d18\u0d1b\7e\2\2\u0d19\u0d1b")
        buf.write("\7<\2\2\u0d1a\u0d0a\3\2\2\2\u0d1a\u0d12\3\2\2\2\u0d1a")
        buf.write("\u0d16\3\2\2\2\u0d1a\u0d17\3\2\2\2\u0d1a\u0d18\3\2\2\2")
        buf.write("\u0d1a\u0d19\3\2\2\2\u0d1b\u010d\3\2\2\2\u0d1c\u0d1e\5")
        buf.write("z>\2\u0d1d\u0d1c\3\2\2\2\u0d1d\u0d1e\3\2\2\2\u0d1e\u0d22")
        buf.write("\3\2\2\2\u0d1f\u0d21\7\7\2\2\u0d20\u0d1f\3\2\2\2\u0d21")
        buf.write("\u0d24\3\2\2\2\u0d22\u0d20\3\2\2\2\u0d22\u0d23\3\2\2\2")
        buf.write("\u0d23\u0d25\3\2\2\2\u0d24\u0d22\3\2\2\2\u0d25\u0d29\7")
        buf.write("\'\2\2\u0d26\u0d28\7\7\2\2\u0d27\u0d26\3\2\2\2\u0d28\u0d2b")
        buf.write("\3\2\2\2\u0d29\u0d27\3\2\2\2\u0d29\u0d2a\3\2\2\2\u0d2a")
        buf.write("\u0d2e\3\2\2\2\u0d2b\u0d29\3\2\2\2\u0d2c\u0d2f\5\u0156")
        buf.write("\u00ac\2\u0d2d\u0d2f\7J\2\2\u0d2e\u0d2c\3\2\2\2\u0d2e")
        buf.write("\u0d2d\3\2\2\2\u0d2f\u010f\3\2\2\2\u0d30\u0d31\t\f\2\2")
        buf.write("\u0d31\u0111\3\2\2\2\u0d32\u0d33\t\r\2\2\u0d33\u0113\3")
        buf.write("\2\2\2\u0d34\u0d35\t\16\2\2\u0d35\u0115\3\2\2\2\u0d36")
        buf.write("\u0d37\t\17\2\2\u0d37\u0117\3\2\2\2\u0d38\u0d39\t\20\2")
        buf.write("\2\u0d39\u0119\3\2\2\2\u0d3a\u0d3b\t\21\2\2\u0d3b\u011b")
        buf.write("\3\2\2\2\u0d3c\u0d3d\t\22\2\2\u0d3d\u011d\3\2\2\2\u0d3e")
        buf.write("\u0d3f\t\23\2\2\u0d3f\u011f\3\2\2\2\u0d40\u0d46\7\26\2")
        buf.write("\2\u0d41\u0d46\7\27\2\2\u0d42\u0d46\7\25\2\2\u0d43\u0d46")
        buf.write("\7\24\2\2\u0d44\u0d46\5\u0124\u0093\2\u0d45\u0d40\3\2")
        buf.write("\2\2\u0d45\u0d41\3\2\2\2\u0d45\u0d42\3\2\2\2\u0d45\u0d43")
        buf.write("\3\2\2\2\u0d45\u0d44\3\2\2\2\u0d46\u0121\3\2\2\2\u0d47")
        buf.write("\u0d4c\7\26\2\2\u0d48\u0d4c\7\27\2\2\u0d49\u0d4a\7\33")
        buf.write("\2\2\u0d4a\u0d4c\5\u0124\u0093\2\u0d4b\u0d47\3\2\2\2\u0d4b")
        buf.write("\u0d48\3\2\2\2\u0d4b\u0d49\3\2\2\2\u0d4c\u0123\3\2\2\2")
        buf.write("\u0d4d\u0d4e\t\24\2\2\u0d4e\u0125\3\2\2\2\u0d4f\u0d53")
        buf.write("\7\t\2\2\u0d50\u0d53\5\u0128\u0095\2\u0d51\u0d53\7\'\2")
        buf.write("\2\u0d52\u0d4f\3\2\2\2\u0d52\u0d50\3\2\2\2\u0d52\u0d51")
        buf.write("\3\2\2\2\u0d53\u0127\3\2\2\2\u0d54\u0d55\7/\2\2\u0d55")
        buf.write("\u0d56\7\t\2\2\u0d56\u0129\3\2\2\2\u0d57\u0d5a\5\u014c")
        buf.write("\u00a7\2\u0d58\u0d5a\5\u012e\u0098\2\u0d59\u0d57\3\2\2")
        buf.write("\2\u0d59\u0d58\3\2\2\2\u0d5a\u0d5b\3\2\2\2\u0d5b\u0d59")
        buf.write("\3\2\2\2\u0d5b\u0d5c\3\2\2\2\u0d5c\u012b\3\2\2\2\u0d5d")
        buf.write("\u0d60\5\u014c\u00a7\2\u0d5e\u0d60\5\u0146\u00a4\2\u0d5f")
        buf.write("\u0d5d\3\2\2\2\u0d5f\u0d5e\3\2\2\2\u0d60\u0d61\3\2\2\2")
        buf.write("\u0d61\u0d5f\3\2\2\2\u0d61\u0d62\3\2\2\2\u0d62\u012d\3")
        buf.write("\2\2\2\u0d63\u0d6c\5\u0134\u009b\2\u0d64\u0d6c\5\u0136")
        buf.write("\u009c\2\u0d65\u0d6c\5\u0138\u009d\2\u0d66\u0d6c\5\u0140")
        buf.write("\u00a1\2\u0d67\u0d6c\5\u0142\u00a2\2\u0d68\u0d6c\5\u0144")
        buf.write("\u00a3\2\u0d69\u0d6c\5\u0146\u00a4\2\u0d6a\u0d6c\5\u014a")
        buf.write("\u00a6\2\u0d6b\u0d63\3\2\2\2\u0d6b\u0d64\3\2\2\2\u0d6b")
        buf.write("\u0d65\3\2\2\2\u0d6b\u0d66\3\2\2\2\u0d6b\u0d67\3\2\2\2")
        buf.write("\u0d6b\u0d68\3\2\2\2\u0d6b\u0d69\3\2\2\2\u0d6b\u0d6a\3")
        buf.write("\2\2\2\u0d6c\u0d70\3\2\2\2\u0d6d\u0d6f\7\7\2\2\u0d6e\u0d6d")
        buf.write("\3\2\2\2\u0d6f\u0d72\3\2\2\2\u0d70\u0d6e\3\2\2\2\u0d70")
        buf.write("\u0d71\3\2\2\2\u0d71\u012f\3\2\2\2\u0d72\u0d70\3\2\2\2")
        buf.write("\u0d73\u0d75\5\u0132\u009a\2\u0d74\u0d73\3\2\2\2\u0d75")
        buf.write("\u0d76\3\2\2\2\u0d76\u0d74\3\2\2\2\u0d76\u0d77\3\2\2\2")
        buf.write("\u0d77\u0131\3\2\2\2\u0d78\u0d81\5\u014c\u00a7\2\u0d79")
        buf.write("\u0d7d\7{\2\2\u0d7a\u0d7c\7\7\2\2\u0d7b\u0d7a\3\2\2\2")
        buf.write("\u0d7c\u0d7f\3\2\2\2\u0d7d\u0d7b\3\2\2\2\u0d7d\u0d7e\3")
        buf.write("\2\2\2\u0d7e\u0d81\3\2\2\2\u0d7f\u0d7d\3\2\2\2\u0d80\u0d78")
        buf.write("\3\2\2\2\u0d80\u0d79\3\2\2\2\u0d81\u0133\3\2\2\2\u0d82")
        buf.write("\u0d83\t\25\2\2\u0d83\u0135\3\2\2\2\u0d84\u0d85\t\26\2")
        buf.write("\2\u0d85\u0137\3\2\2\2\u0d86\u0d87\t\27\2\2\u0d87\u0139")
        buf.write("\3\2\2\2\u0d88\u0d89\t\30\2\2\u0d89\u013b\3\2\2\2\u0d8a")
        buf.write("\u0d8c\5\u013e\u00a0\2\u0d8b\u0d8a\3\2\2\2\u0d8c\u0d8d")
        buf.write("\3\2\2\2\u0d8d\u0d8b\3\2\2\2\u0d8d\u0d8e\3\2\2\2\u0d8e")
        buf.write("\u013d\3\2\2\2\u0d8f\u0d93\5\u0148\u00a5\2\u0d90\u0d92")
        buf.write("\7\7\2\2\u0d91\u0d90\3\2\2\2\u0d92\u0d95\3\2\2\2\u0d93")
        buf.write("\u0d91\3\2\2\2\u0d93\u0d94\3\2\2\2\u0d94\u0d9f\3\2\2\2")
        buf.write("\u0d95\u0d93\3\2\2\2\u0d96\u0d9a\5\u013a\u009e\2\u0d97")
        buf.write("\u0d99\7\7\2\2\u0d98\u0d97\3\2\2\2\u0d99\u0d9c\3\2\2\2")
        buf.write("\u0d9a\u0d98\3\2\2\2\u0d9a\u0d9b\3\2\2\2\u0d9b\u0d9f\3")
        buf.write("\2\2\2\u0d9c\u0d9a\3\2\2\2\u0d9d\u0d9f\5\u014c\u00a7\2")
        buf.write("\u0d9e\u0d8f\3\2\2\2\u0d9e\u0d96\3\2\2\2\u0d9e\u0d9d\3")
        buf.write("\2\2\2\u0d9f\u013f\3\2\2\2\u0da0\u0da1\t\31\2\2\u0da1")
        buf.write("\u0141\3\2\2\2\u0da2\u0da3\7\u0080\2\2\u0da3\u0143\3\2")
        buf.write("\2\2\u0da4\u0da5\t\32\2\2\u0da5\u0145\3\2\2\2\u0da6\u0da7")
        buf.write("\t\33\2\2\u0da7\u0147\3\2\2\2\u0da8\u0da9\7\u0085\2\2")
        buf.write("\u0da9\u0149\3\2\2\2\u0daa\u0dab\t\34\2\2\u0dab\u014b")
        buf.write("\3\2\2\2\u0dac\u0daf\5\u014e\u00a8\2\u0dad\u0daf\5\u0150")
        buf.write("\u00a9\2\u0dae\u0dac\3\2\2\2\u0dae\u0dad\3\2\2\2\u0daf")
        buf.write("\u0db3\3\2\2\2\u0db0\u0db2\7\7\2\2\u0db1\u0db0\3\2\2\2")
        buf.write("\u0db2\u0db5\3\2\2\2\u0db3\u0db1\3\2\2\2\u0db3\u0db4\3")
        buf.write("\2\2\2\u0db4\u014d\3\2\2\2\u0db5\u0db3\3\2\2\2\u0db6\u0dba")
        buf.write("\5\u0152\u00aa\2\u0db7\u0db9\7\7\2\2\u0db8\u0db7\3\2\2")
        buf.write("\2\u0db9\u0dbc\3\2\2\2\u0dba\u0db8\3\2\2\2\u0dba\u0dbb")
        buf.write("\3\2\2\2\u0dbb\u0dbd\3\2\2\2\u0dbc\u0dba\3\2\2\2\u0dbd")
        buf.write("\u0dbe\5\u0154\u00ab\2\u0dbe\u0dc2\3\2\2\2\u0dbf\u0dc0")
        buf.write("\t\2\2\2\u0dc0\u0dc2\5\u0154\u00ab\2\u0dc1\u0db6\3\2\2")
        buf.write("\2\u0dc1\u0dbf\3\2\2\2\u0dc2\u014f\3\2\2\2\u0dc3\u0dc7")
        buf.write("\5\u0152\u00aa\2\u0dc4\u0dc6\7\7\2\2\u0dc5\u0dc4\3\2\2")
        buf.write("\2\u0dc6\u0dc9\3\2\2\2\u0dc7\u0dc5\3\2\2\2\u0dc7\u0dc8")
        buf.write("\3\2\2\2\u0dc8\u0dca\3\2\2\2\u0dc9\u0dc7\3\2\2\2\u0dca")
        buf.write("\u0dcc\7\r\2\2\u0dcb\u0dcd\5\u0154\u00ab\2\u0dcc\u0dcb")
        buf.write("\3\2\2\2\u0dcd\u0dce\3\2\2\2\u0dce\u0dcc\3\2\2\2\u0dce")
        buf.write("\u0dcf\3\2\2\2\u0dcf\u0dd0\3\2\2\2\u0dd0\u0dd1\7\16\2")
        buf.write("\2\u0dd1\u0ddc\3\2\2\2\u0dd2\u0dd3\t\2\2\2\u0dd3\u0dd5")
        buf.write("\7\r\2\2\u0dd4\u0dd6\5\u0154\u00ab\2\u0dd5\u0dd4\3\2\2")
        buf.write("\2\u0dd6\u0dd7\3\2\2\2\u0dd7\u0dd5\3\2\2\2\u0dd7\u0dd8")
        buf.write("\3\2\2\2\u0dd8\u0dd9\3\2\2\2\u0dd9\u0dda\7\16\2\2\u0dda")
        buf.write("\u0ddc\3\2\2\2\u0ddb\u0dc3\3\2\2\2\u0ddb\u0dd2\3\2\2\2")
        buf.write("\u0ddc\u0151\3\2\2\2\u0ddd\u0dde\t\2\2\2\u0dde\u0de2\t")
        buf.write("\35\2\2\u0ddf\u0de1\7\7\2\2\u0de0\u0ddf\3\2\2\2\u0de1")
        buf.write("\u0de4\3\2\2\2\u0de2\u0de0\3\2\2\2\u0de2\u0de3\3\2\2\2")
        buf.write("\u0de3\u0de5\3\2\2\2\u0de4\u0de2\3\2\2\2\u0de5\u0de6\7")
        buf.write("\34\2\2\u0de6\u0153\3\2\2\2\u0de7\u0dea\5&\24\2\u0de8")
        buf.write("\u0dea\5j\66\2\u0de9\u0de7\3\2\2\2\u0de9\u0de8\3\2\2\2")
        buf.write("\u0dea\u0155\3\2\2\2\u0deb\u0dec\t\36\2\2\u0dec\u0157")
        buf.write("\3\2\2\2\u0ded\u0df8\5\u0156\u00ac\2\u0dee\u0df0\7\7\2")
        buf.write("\2\u0def\u0dee\3\2\2\2\u0df0\u0df3\3\2\2\2\u0df1\u0def")
        buf.write("\3\2\2\2\u0df1\u0df2\3\2\2\2\u0df2\u0df4\3\2\2\2\u0df3")
        buf.write("\u0df1\3\2\2\2\u0df4\u0df5\7\t\2\2\u0df5\u0df7\5\u0156")
        buf.write("\u00ac\2\u0df6\u0df1\3\2\2\2\u0df7\u0dfa\3\2\2\2\u0df8")
        buf.write("\u0df6\3\2\2\2\u0df8\u0df9\3\2\2\2\u0df9\u0159\3\2\2\2")
        buf.write("\u0dfa\u0df8\3\2\2\2\u0228\u015b\u0160\u0166\u016e\u0174")
        buf.write("\u0179\u017f\u0189\u0192\u0199\u01a0\u01a7\u01ac\u01b1")
        buf.write("\u01b7\u01b9\u01be\u01c6\u01c9\u01d0\u01d3\u01d9\u01e0")
        buf.write("\u01e4\u01e9\u01f0\u01fa\u01fd\u0204\u0207\u020a\u020f")
        buf.write("\u0216\u021a\u021f\u0223\u0228\u022f\u0233\u0238\u023c")
        buf.write("\u0241\u0248\u024c\u024f\u0255\u0258\u0260\u0267\u0270")
        buf.write("\u0277\u027e\u0284\u028a\u028e\u0290\u0295\u029b\u029e")
        buf.write("\u02a3\u02ab\u02b2\u02b9\u02bd\u02c3\u02ca\u02d0\u02d7")
        buf.write("\u02df\u02e5\u02ec\u02f1\u02f8\u0301\u0308\u030f\u0315")
        buf.write("\u031b\u031f\u0324\u032a\u032f\u0336\u033d\u0341\u0347")
        buf.write("\u034e\u0355\u035b\u0361\u0368\u036f\u0376\u037a\u0381")
        buf.write("\u0387\u038d\u0393\u039a\u039e\u03a3\u03aa\u03ae\u03b3")
        buf.write("\u03b7\u03bd\u03c4\u03cb\u03d1\u03d7\u03db\u03dd\u03e2")
        buf.write("\u03e8\u03ee\u03f5\u03f9\u03fc\u0402\u0406\u040b\u0412")
        buf.write("\u0417\u041c\u0423\u042a\u0431\u0435\u043a\u043e\u0443")
        buf.write("\u0447\u044e\u0452\u0457\u045d\u0464\u046b\u046f\u0475")
        buf.write("\u047c\u0483\u0489\u048f\u0493\u0498\u049e\u04a4\u04a8")
        buf.write("\u04ad\u04b4\u04b9\u04be\u04c3\u04c8\u04cc\u04d1\u04d8")
        buf.write("\u04dd\u04df\u04e4\u04e7\u04ec\u04f0\u04f5\u04f9\u04fc")
        buf.write("\u04ff\u0504\u0508\u050b\u050d\u0513\u0519\u051d\u0523")
        buf.write("\u052a\u0531\u0538\u053c\u0541\u0545\u0548\u054c\u0552")
        buf.write("\u0559\u0560\u0564\u0569\u0570\u0577\u057b\u0580\u0585")
        buf.write("\u058b\u0592\u0599\u059f\u05a5\u05a9\u05ab\u05b0\u05b6")
        buf.write("\u05bc\u05c3\u05c7\u05cd\u05d4\u05da\u05e0\u05e7\u05ee")
        buf.write("\u05f2\u05f7\u05fb\u05fe\u0604\u060b\u0612\u0616\u061b")
        buf.write("\u061f\u0625\u062d\u0631\u0637\u063b\u0640\u0647\u064b")
        buf.write("\u0650\u0659\u0660\u0666\u066c\u0670\u0676\u0679\u067f")
        buf.write("\u0683\u0688\u068c\u068f\u0695\u0699\u069d\u06a2\u06a8")
        buf.write("\u06b0\u06b7\u06bd\u06c4\u06c8\u06cb\u06cf\u06d4\u06da")
        buf.write("\u06de\u06e4\u06eb\u06ee\u06f4\u06fb\u0704\u0709\u070e")
        buf.write("\u0715\u071a\u071e\u0724\u0728\u072d\u0736\u073d\u0743")
        buf.write("\u0748\u074e\u0755\u075e\u0765\u076a\u0772\u0775\u0778")
        buf.write("\u077c\u077e\u0785\u078c\u0791\u0797\u079e\u07a6\u07ac")
        buf.write("\u07b3\u07b8\u07c0\u07c4\u07ca\u07d3\u07dc\u07e5\u07ea")
        buf.write("\u07f0\u07f4\u07f9\u0800\u080c\u0816\u081b\u0821\u0825")
        buf.write("\u082a\u082d\u0835\u083c\u0842\u0849\u0850\u0856\u085e")
        buf.write("\u0865\u086d\u0872\u0879\u0882\u0887\u0889\u0890\u0897")
        buf.write("\u089e\u08a9\u08b0\u08b8\u08be\u08c6\u08cd\u08d5\u08dc")
        buf.write("\u08e3\u08ea\u08ef\u08f5\u08fc\u0903\u090a\u090f\u0913")
        buf.write("\u0919\u0924\u0927\u092e\u0930\u0937\u093e\u0944\u094b")
        buf.write("\u0952\u0958\u095f\u0967\u096d\u0974\u097b\u0981\u0987")
        buf.write("\u098b\u0990\u0998\u099f\u09a5\u09a8\u09ab\u09af\u09b2")
        buf.write("\u09b7\u09bb\u09c0\u09c9\u09d0\u09d7\u09dd\u09e3\u09e7")
        buf.write("\u09ec\u09f5\u09fd\u0a04\u0a0b\u0a11\u0a17\u0a1b\u0a20")
        buf.write("\u0a25\u0a28\u0a2d\u0a34\u0a3b\u0a3e\u0a41\u0a46\u0a59")
        buf.write("\u0a5f\u0a66\u0a6f\u0a76\u0a7d\u0a83\u0a89\u0a8d\u0a92")
        buf.write("\u0a9b\u0a9f\u0aa5\u0aaa\u0aac\u0ab5\u0ab7\u0ac8\u0acf")
        buf.write("\u0ad8\u0adf\u0ae8\u0aec\u0af1\u0af8\u0aff\u0b04\u0b0a")
        buf.write("\u0b11\u0b17\u0b1d\u0b21\u0b28\u0b2f\u0b33\u0b35\u0b3b")
        buf.write("\u0b42\u0b47\u0b4c\u0b53\u0b5a\u0b5e\u0b63\u0b67\u0b6c")
        buf.write("\u0b70\u0b74\u0b7a\u0b81\u0b88\u0b91\u0b95\u0b9e\u0ba5")
        buf.write("\u0baa\u0bae\u0bb1\u0bb7\u0bbe\u0bc5\u0bcc\u0bd1\u0bd7")
        buf.write("\u0bde\u0be5\u0bec\u0bf0\u0bf5\u0bf9\u0bfe\u0c05\u0c0a")
        buf.write("\u0c0c\u0c12\u0c18\u0c1f\u0c26\u0c2d\u0c30\u0c39\u0c3d")
        buf.write("\u0c42\u0c49\u0c50\u0c55\u0c5b\u0c64\u0c6b\u0c71\u0c77")
        buf.write("\u0c7b\u0c80\u0c87\u0c8c\u0c92\u0c99\u0c9e\u0ca0\u0ca5")
        buf.write("\u0cab\u0cb4\u0cbd\u0cc4\u0cca\u0ccf\u0cd3\u0cd8\u0cdc")
        buf.write("\u0ce2\u0ce9\u0cf2\u0cf6\u0cfc\u0d05\u0d0e\u0d14\u0d1a")
        buf.write("\u0d1d\u0d22\u0d29\u0d2e\u0d45\u0d4b\u0d52\u0d59\u0d5b")
        buf.write("\u0d5f\u0d61\u0d6b\u0d70\u0d76\u0d7d\u0d80\u0d8d\u0d93")
        buf.write("\u0d9a\u0d9e\u0dae\u0db3\u0dba\u0dc1\u0dc7\u0dce\u0dd7")
        buf.write("\u0ddb\u0de2\u0de9\u0df1\u0df8")
        return buf.getvalue()


class KotlinParser ( Parser ):

    grammarFileName = "KotlinParser.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'...'", "'.'", "','", "'('", 
                     "')'", "'['", "']'", "'{'", "'}'", "'*'", "'%'", "'/'", 
                     "'+'", "'-'", "'++'", "'--'", "'&&'", "'||'", "<INVALID>", 
                     "'!'", "':'", "';'", "'='", "'+='", "'-='", "'*='", 
                     "'/='", "'%='", "'->'", "'=>'", "'..'", "'::'", "';;'", 
                     "'#'", "'@'", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "'?'", "'<'", "'>'", "'<='", "'>='", "'!='", 
                     "'!=='", "'as?'", "'=='", "'==='", "'''", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'file'", "'field'", "'property'", "'get'", "'set'", 
                     "'receiver'", "'param'", "'setparam'", "'delegate'", 
                     "'package'", "'import'", "'class'", "'interface'", 
                     "'fun'", "'object'", "'val'", "'var'", "'typealias'", 
                     "'constructor'", "'by'", "'companion'", "'init'", "'this'", 
                     "'super'", "'typeof'", "'where'", "'if'", "'else'", 
                     "'when'", "'try'", "'catch'", "'finally'", "'for'", 
                     "'do'", "'while'", "'throw'", "'return'", "'continue'", 
                     "'break'", "'as'", "'is'", "'in'", "<INVALID>", "<INVALID>", 
                     "'out'", "'dynamic'", "'public'", "'private'", "'protected'", 
                     "'internal'", "'enum'", "'sealed'", "'annotation'", 
                     "'data'", "'inner'", "'tailrec'", "'operator'", "'inline'", 
                     "'infix'", "'external'", "'suspend'", "'override'", 
                     "'abstract'", "'final'", "'open'", "'const'", "'lateinit'", 
                     "'vararg'", "'noinline'", "'crossinline'", "'reified'", 
                     "'expect'", "'actual'", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'null'", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "'\"\"\"'" ]

    symbolicNames = [ "<INVALID>", "ShebangLine", "DelimitedComment", "LineComment", 
                      "WS", "NL", "RESERVED", "DOT", "COMMA", "LPAREN", 
                      "RPAREN", "LSQUARE", "RSQUARE", "LCURL", "RCURL", 
                      "MULT", "MOD", "DIV", "ADD", "SUB", "INCR", "DECR", 
                      "CONJ", "DISJ", "EXCL_WS", "EXCL_NO_WS", "COLON", 
                      "SEMICOLON", "ASSIGNMENT", "ADD_ASSIGNMENT", "SUB_ASSIGNMENT", 
                      "MULT_ASSIGNMENT", "DIV_ASSIGNMENT", "MOD_ASSIGNMENT", 
                      "ARROW", "DOUBLE_ARROW", "RANGE", "COLONCOLON", "DOUBLE_SEMICOLON", 
                      "HASH", "AT_NO_WS", "AT_POST_WS", "AT_PRE_WS", "AT_BOTH_WS", 
                      "QUEST_WS", "QUEST_NO_WS", "LANGLE", "RANGLE", "LE", 
                      "GE", "EXCL_EQ", "EXCL_EQEQ", "AS_SAFE", "EQEQ", "EQEQEQ", 
                      "SINGLE_QUOTE", "RETURN_AT", "CONTINUE_AT", "BREAK_AT", 
                      "THIS_AT", "SUPER_AT", "FILE", "FIELD", "PROPERTY", 
                      "GET", "SET", "RECEIVER", "PARAM", "SETPARAM", "DELEGATE", 
                      "PACKAGE", "IMPORT", "CLASS", "INTERFACE", "FUN", 
                      "OBJECT", "VAL", "VAR", "TYPE_ALIAS", "CONSTRUCTOR", 
                      "BY", "COMPANION", "INIT", "THIS", "SUPER", "TYPEOF", 
                      "WHERE", "IF", "ELSE", "WHEN", "TRY", "CATCH", "FINALLY", 
                      "FOR", "DO", "WHILE", "THROW", "RETURN", "CONTINUE", 
                      "BREAK", "AS", "IS", "IN", "NOT_IS", "NOT_IN", "OUT", 
                      "DYNAMIC", "PUBLIC", "PRIVATE", "PROTECTED", "INTERNAL", 
                      "ENUM", "SEALED", "ANNOTATION", "DATA", "INNER", "TAILREC", 
                      "OPERATOR", "INLINE", "INFIX", "EXTERNAL", "SUSPEND", 
                      "OVERRIDE", "ABSTRACT", "FINAL", "OPEN", "CONST", 
                      "LATEINIT", "VARARG", "NOINLINE", "CROSSINLINE", "REIFIED", 
                      "EXPECT", "ACTUAL", "RealLiteral", "FloatLiteral", 
                      "DoubleLiteral", "IntegerLiteral", "HexLiteral", "BinLiteral", 
                      "UnsignedLiteral", "LongLiteral", "BooleanLiteral", 
                      "NullLiteral", "CharacterLiteral", "Identifier", "IdentifierOrSoftKey", 
                      "FieldIdentifier", "QUOTE_OPEN", "TRIPLE_QUOTE_OPEN", 
                      "UNICODE_CLASS_LL", "UNICODE_CLASS_LM", "UNICODE_CLASS_LO", 
                      "UNICODE_CLASS_LT", "UNICODE_CLASS_LU", "UNICODE_CLASS_ND", 
                      "UNICODE_CLASS_NL", "QUOTE_CLOSE", "LineStrRef", "LineStrText", 
                      "LineStrEscapedChar", "LineStrExprStart", "TRIPLE_QUOTE_CLOSE", 
                      "MultiLineStringQuote", "MultiLineStrRef", "MultiLineStrText", 
                      "MultiLineStrExprStart", "Inside_Comment", "Inside_WS", 
                      "Inside_NL", "ErrorCharacter" ]

    RULE_kotlinFile = 0
    RULE_script = 1
    RULE_shebangLine = 2
    RULE_fileAnnotation = 3
    RULE_packageHeader = 4
    RULE_importList = 5
    RULE_importHeader = 6
    RULE_importAlias = 7
    RULE_topLevelObject = 8
    RULE_typeAlias = 9
    RULE_declaration = 10
    RULE_classDeclaration = 11
    RULE_primaryConstructor = 12
    RULE_classBody = 13
    RULE_classParameters = 14
    RULE_classParameter = 15
    RULE_delegationSpecifiers = 16
    RULE_delegationSpecifier = 17
    RULE_constructorInvocation = 18
    RULE_annotatedDelegationSpecifier = 19
    RULE_explicitDelegation = 20
    RULE_typeParameters = 21
    RULE_typeParameter = 22
    RULE_typeConstraints = 23
    RULE_typeConstraint = 24
    RULE_classMemberDeclarations = 25
    RULE_classMemberDeclaration = 26
    RULE_anonymousInitializer = 27
    RULE_companionObject = 28
    RULE_functionValueParameters = 29
    RULE_functionValueParameter = 30
    RULE_functionDeclaration = 31
    RULE_functionBody = 32
    RULE_variableDeclaration = 33
    RULE_multiVariableDeclaration = 34
    RULE_propertyDeclaration = 35
    RULE_propertyDelegate = 36
    RULE_getter = 37
    RULE_setter = 38
    RULE_parametersWithOptionalType = 39
    RULE_parameterWithOptionalType = 40
    RULE_parameter = 41
    RULE_objectDeclaration = 42
    RULE_secondaryConstructor = 43
    RULE_constructorDelegationCall = 44
    RULE_enumClassBody = 45
    RULE_enumEntries = 46
    RULE_enumEntry = 47
    RULE_type = 48
    RULE_typeReference = 49
    RULE_nullableType = 50
    RULE_quest = 51
    RULE_userType = 52
    RULE_simpleUserType = 53
    RULE_typeProjection = 54
    RULE_typeProjectionModifiers = 55
    RULE_typeProjectionModifier = 56
    RULE_functionType = 57
    RULE_functionTypeParameters = 58
    RULE_parenthesizedType = 59
    RULE_receiverType = 60
    RULE_parenthesizedUserType = 61
    RULE_statements = 62
    RULE_statement = 63
    RULE_label = 64
    RULE_controlStructureBody = 65
    RULE_block = 66
    RULE_loopStatement = 67
    RULE_forStatement = 68
    RULE_whileStatement = 69
    RULE_doWhileStatement = 70
    RULE_assignment = 71
    RULE_semi = 72
    RULE_semis = 73
    RULE_expression = 74
    RULE_disjunction = 75
    RULE_conjunction = 76
    RULE_equality = 77
    RULE_comparison = 78
    RULE_infixOperation = 79
    RULE_elvisExpression = 80
    RULE_elvis = 81
    RULE_infixFunctionCall = 82
    RULE_rangeExpression = 83
    RULE_additiveExpression = 84
    RULE_multiplicativeExpression = 85
    RULE_asExpression = 86
    RULE_comparisonWithLiteralRightSide = 87
    RULE_prefixUnaryExpression = 88
    RULE_unaryPrefix = 89
    RULE_postfixUnaryExpression = 90
    RULE_postfixUnarySuffix = 91
    RULE_directlyAssignableExpression = 92
    RULE_parenthesizedDirectlyAssignableExpression = 93
    RULE_assignableExpression = 94
    RULE_parenthesizedAssignableExpression = 95
    RULE_assignableSuffix = 96
    RULE_indexingSuffix = 97
    RULE_navigationSuffix = 98
    RULE_callSuffix = 99
    RULE_annotatedLambda = 100
    RULE_typeArguments = 101
    RULE_valueArguments = 102
    RULE_valueArgument = 103
    RULE_primaryExpression = 104
    RULE_parenthesizedExpression = 105
    RULE_collectionLiteral = 106
    RULE_literalConstant = 107
    RULE_stringLiteral = 108
    RULE_lineStringLiteral = 109
    RULE_multiLineStringLiteral = 110
    RULE_lineStringContent = 111
    RULE_lineStringExpression = 112
    RULE_multiLineStringContent = 113
    RULE_multiLineStringExpression = 114
    RULE_lambdaLiteral = 115
    RULE_lambdaParameters = 116
    RULE_lambdaParameter = 117
    RULE_anonymousFunction = 118
    RULE_functionLiteral = 119
    RULE_objectLiteral = 120
    RULE_thisExpression = 121
    RULE_superExpression = 122
    RULE_ifExpression = 123
    RULE_whenSubject = 124
    RULE_whenExpression = 125
    RULE_whenEntry = 126
    RULE_whenCondition = 127
    RULE_rangeTest = 128
    RULE_typeTest = 129
    RULE_tryExpression = 130
    RULE_catchBlock = 131
    RULE_finallyBlock = 132
    RULE_jumpExpression = 133
    RULE_callableReference = 134
    RULE_assignmentAndOperator = 135
    RULE_equalityOperator = 136
    RULE_comparisonOperator = 137
    RULE_inOperator = 138
    RULE_isOperator = 139
    RULE_additiveOperator = 140
    RULE_multiplicativeOperator = 141
    RULE_asOperator = 142
    RULE_prefixUnaryOperator = 143
    RULE_postfixUnaryOperator = 144
    RULE_excl = 145
    RULE_memberAccessOperator = 146
    RULE_safeNav = 147
    RULE_modifiers = 148
    RULE_parameterModifiers = 149
    RULE_modifier = 150
    RULE_typeModifiers = 151
    RULE_typeModifier = 152
    RULE_classModifier = 153
    RULE_memberModifier = 154
    RULE_visibilityModifier = 155
    RULE_varianceModifier = 156
    RULE_typeParameterModifiers = 157
    RULE_typeParameterModifier = 158
    RULE_functionModifier = 159
    RULE_propertyModifier = 160
    RULE_inheritanceModifier = 161
    RULE_parameterModifier = 162
    RULE_reificationModifier = 163
    RULE_platformModifier = 164
    RULE_annotation = 165
    RULE_singleAnnotation = 166
    RULE_multiAnnotation = 167
    RULE_annotationUseSiteTarget = 168
    RULE_unescapedAnnotation = 169
    RULE_simpleIdentifier = 170
    RULE_identifier = 171

    ruleNames =  [ "kotlinFile", "script", "shebangLine", "fileAnnotation", 
                   "packageHeader", "importList", "importHeader", "importAlias", 
                   "topLevelObject", "typeAlias", "declaration", "classDeclaration", 
                   "primaryConstructor", "classBody", "classParameters", 
                   "classParameter", "delegationSpecifiers", "delegationSpecifier", 
                   "constructorInvocation", "annotatedDelegationSpecifier", 
                   "explicitDelegation", "typeParameters", "typeParameter", 
                   "typeConstraints", "typeConstraint", "classMemberDeclarations", 
                   "classMemberDeclaration", "anonymousInitializer", "companionObject", 
                   "functionValueParameters", "functionValueParameter", 
                   "functionDeclaration", "functionBody", "variableDeclaration", 
                   "multiVariableDeclaration", "propertyDeclaration", "propertyDelegate", 
                   "getter", "setter", "parametersWithOptionalType", "parameterWithOptionalType", 
                   "parameter", "objectDeclaration", "secondaryConstructor", 
                   "constructorDelegationCall", "enumClassBody", "enumEntries", 
                   "enumEntry", "type", "typeReference", "nullableType", 
                   "quest", "userType", "simpleUserType", "typeProjection", 
                   "typeProjectionModifiers", "typeProjectionModifier", 
                   "functionType", "functionTypeParameters", "parenthesizedType", 
                   "receiverType", "parenthesizedUserType", "statements", 
                   "statement", "label", "controlStructureBody", "block", 
                   "loopStatement", "forStatement", "whileStatement", "doWhileStatement", 
                   "assignment", "semi", "semis", "expression", "disjunction", 
                   "conjunction", "equality", "comparison", "infixOperation", 
                   "elvisExpression", "elvis", "infixFunctionCall", "rangeExpression", 
                   "additiveExpression", "multiplicativeExpression", "asExpression", 
                   "comparisonWithLiteralRightSide", "prefixUnaryExpression", 
                   "unaryPrefix", "postfixUnaryExpression", "postfixUnarySuffix", 
                   "directlyAssignableExpression", "parenthesizedDirectlyAssignableExpression", 
                   "assignableExpression", "parenthesizedAssignableExpression", 
                   "assignableSuffix", "indexingSuffix", "navigationSuffix", 
                   "callSuffix", "annotatedLambda", "typeArguments", "valueArguments", 
                   "valueArgument", "primaryExpression", "parenthesizedExpression", 
                   "collectionLiteral", "literalConstant", "stringLiteral", 
                   "lineStringLiteral", "multiLineStringLiteral", "lineStringContent", 
                   "lineStringExpression", "multiLineStringContent", "multiLineStringExpression", 
                   "lambdaLiteral", "lambdaParameters", "lambdaParameter", 
                   "anonymousFunction", "functionLiteral", "objectLiteral", 
                   "thisExpression", "superExpression", "ifExpression", 
                   "whenSubject", "whenExpression", "whenEntry", "whenCondition", 
                   "rangeTest", "typeTest", "tryExpression", "catchBlock", 
                   "finallyBlock", "jumpExpression", "callableReference", 
                   "assignmentAndOperator", "equalityOperator", "comparisonOperator", 
                   "inOperator", "isOperator", "additiveOperator", "multiplicativeOperator", 
                   "asOperator", "prefixUnaryOperator", "postfixUnaryOperator", 
                   "excl", "memberAccessOperator", "safeNav", "modifiers", 
                   "parameterModifiers", "modifier", "typeModifiers", "typeModifier", 
                   "classModifier", "memberModifier", "visibilityModifier", 
                   "varianceModifier", "typeParameterModifiers", "typeParameterModifier", 
                   "functionModifier", "propertyModifier", "inheritanceModifier", 
                   "parameterModifier", "reificationModifier", "platformModifier", 
                   "annotation", "singleAnnotation", "multiAnnotation", 
                   "annotationUseSiteTarget", "unescapedAnnotation", "simpleIdentifier", 
                   "identifier" ]

    EOF = Token.EOF
    ShebangLine=1
    DelimitedComment=2
    LineComment=3
    WS=4
    NL=5
    RESERVED=6
    DOT=7
    COMMA=8
    LPAREN=9
    RPAREN=10
    LSQUARE=11
    RSQUARE=12
    LCURL=13
    RCURL=14
    MULT=15
    MOD=16
    DIV=17
    ADD=18
    SUB=19
    INCR=20
    DECR=21
    CONJ=22
    DISJ=23
    EXCL_WS=24
    EXCL_NO_WS=25
    COLON=26
    SEMICOLON=27
    ASSIGNMENT=28
    ADD_ASSIGNMENT=29
    SUB_ASSIGNMENT=30
    MULT_ASSIGNMENT=31
    DIV_ASSIGNMENT=32
    MOD_ASSIGNMENT=33
    ARROW=34
    DOUBLE_ARROW=35
    RANGE=36
    COLONCOLON=37
    DOUBLE_SEMICOLON=38
    HASH=39
    AT_NO_WS=40
    AT_POST_WS=41
    AT_PRE_WS=42
    AT_BOTH_WS=43
    QUEST_WS=44
    QUEST_NO_WS=45
    LANGLE=46
    RANGLE=47
    LE=48
    GE=49
    EXCL_EQ=50
    EXCL_EQEQ=51
    AS_SAFE=52
    EQEQ=53
    EQEQEQ=54
    SINGLE_QUOTE=55
    RETURN_AT=56
    CONTINUE_AT=57
    BREAK_AT=58
    THIS_AT=59
    SUPER_AT=60
    FILE=61
    FIELD=62
    PROPERTY=63
    GET=64
    SET=65
    RECEIVER=66
    PARAM=67
    SETPARAM=68
    DELEGATE=69
    PACKAGE=70
    IMPORT=71
    CLASS=72
    INTERFACE=73
    FUN=74
    OBJECT=75
    VAL=76
    VAR=77
    TYPE_ALIAS=78
    CONSTRUCTOR=79
    BY=80
    COMPANION=81
    INIT=82
    THIS=83
    SUPER=84
    TYPEOF=85
    WHERE=86
    IF=87
    ELSE=88
    WHEN=89
    TRY=90
    CATCH=91
    FINALLY=92
    FOR=93
    DO=94
    WHILE=95
    THROW=96
    RETURN=97
    CONTINUE=98
    BREAK=99
    AS=100
    IS=101
    IN=102
    NOT_IS=103
    NOT_IN=104
    OUT=105
    DYNAMIC=106
    PUBLIC=107
    PRIVATE=108
    PROTECTED=109
    INTERNAL=110
    ENUM=111
    SEALED=112
    ANNOTATION=113
    DATA=114
    INNER=115
    TAILREC=116
    OPERATOR=117
    INLINE=118
    INFIX=119
    EXTERNAL=120
    SUSPEND=121
    OVERRIDE=122
    ABSTRACT=123
    FINAL=124
    OPEN=125
    CONST=126
    LATEINIT=127
    VARARG=128
    NOINLINE=129
    CROSSINLINE=130
    REIFIED=131
    EXPECT=132
    ACTUAL=133
    RealLiteral=134
    FloatLiteral=135
    DoubleLiteral=136
    IntegerLiteral=137
    HexLiteral=138
    BinLiteral=139
    UnsignedLiteral=140
    LongLiteral=141
    BooleanLiteral=142
    NullLiteral=143
    CharacterLiteral=144
    Identifier=145
    IdentifierOrSoftKey=146
    FieldIdentifier=147
    QUOTE_OPEN=148
    TRIPLE_QUOTE_OPEN=149
    UNICODE_CLASS_LL=150
    UNICODE_CLASS_LM=151
    UNICODE_CLASS_LO=152
    UNICODE_CLASS_LT=153
    UNICODE_CLASS_LU=154
    UNICODE_CLASS_ND=155
    UNICODE_CLASS_NL=156
    QUOTE_CLOSE=157
    LineStrRef=158
    LineStrText=159
    LineStrEscapedChar=160
    LineStrExprStart=161
    TRIPLE_QUOTE_CLOSE=162
    MultiLineStringQuote=163
    MultiLineStrRef=164
    MultiLineStrText=165
    MultiLineStrExprStart=166
    Inside_Comment=167
    Inside_WS=168
    Inside_NL=169
    ErrorCharacter=170

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class KotlinFileContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def packageHeader(self):
            return self.getTypedRuleContext(KotlinParser.PackageHeaderContext,0)


        def importList(self):
            return self.getTypedRuleContext(KotlinParser.ImportListContext,0)


        def EOF(self):
            return self.getToken(KotlinParser.EOF, 0)

        def shebangLine(self):
            return self.getTypedRuleContext(KotlinParser.ShebangLineContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def fileAnnotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.FileAnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.FileAnnotationContext,i)


        def topLevelObject(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TopLevelObjectContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TopLevelObjectContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_kotlinFile

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterKotlinFile" ):
                listener.enterKotlinFile(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitKotlinFile" ):
                listener.exitKotlinFile(self)




    def kotlinFile(self):

        localctx = KotlinParser.KotlinFileContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_kotlinFile)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 345
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.ShebangLine:
                self.state = 344
                self.shebangLine()


            self.state = 350
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 347
                self.match(KotlinParser.NL)
                self.state = 352
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 356
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 353
                    self.fileAnnotation() 
                self.state = 358
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

            self.state = 359
            self.packageHeader()
            self.state = 360
            self.importList()
            self.state = 364
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 72)) & ~0x3f) == 0 and ((1 << (_la - 72)) & ((1 << (KotlinParser.CLASS - 72)) | (1 << (KotlinParser.INTERFACE - 72)) | (1 << (KotlinParser.FUN - 72)) | (1 << (KotlinParser.OBJECT - 72)) | (1 << (KotlinParser.VAL - 72)) | (1 << (KotlinParser.VAR - 72)) | (1 << (KotlinParser.TYPE_ALIAS - 72)) | (1 << (KotlinParser.PUBLIC - 72)) | (1 << (KotlinParser.PRIVATE - 72)) | (1 << (KotlinParser.PROTECTED - 72)) | (1 << (KotlinParser.INTERNAL - 72)) | (1 << (KotlinParser.ENUM - 72)) | (1 << (KotlinParser.SEALED - 72)) | (1 << (KotlinParser.ANNOTATION - 72)) | (1 << (KotlinParser.DATA - 72)) | (1 << (KotlinParser.INNER - 72)) | (1 << (KotlinParser.TAILREC - 72)) | (1 << (KotlinParser.OPERATOR - 72)) | (1 << (KotlinParser.INLINE - 72)) | (1 << (KotlinParser.INFIX - 72)) | (1 << (KotlinParser.EXTERNAL - 72)) | (1 << (KotlinParser.SUSPEND - 72)) | (1 << (KotlinParser.OVERRIDE - 72)) | (1 << (KotlinParser.ABSTRACT - 72)) | (1 << (KotlinParser.FINAL - 72)) | (1 << (KotlinParser.OPEN - 72)) | (1 << (KotlinParser.CONST - 72)) | (1 << (KotlinParser.LATEINIT - 72)) | (1 << (KotlinParser.VARARG - 72)) | (1 << (KotlinParser.NOINLINE - 72)) | (1 << (KotlinParser.CROSSINLINE - 72)) | (1 << (KotlinParser.EXPECT - 72)) | (1 << (KotlinParser.ACTUAL - 72)))) != 0):
                self.state = 361
                self.topLevelObject()
                self.state = 366
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 367
            self.match(KotlinParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ScriptContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def packageHeader(self):
            return self.getTypedRuleContext(KotlinParser.PackageHeaderContext,0)


        def importList(self):
            return self.getTypedRuleContext(KotlinParser.ImportListContext,0)


        def EOF(self):
            return self.getToken(KotlinParser.EOF, 0)

        def shebangLine(self):
            return self.getTypedRuleContext(KotlinParser.ShebangLineContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def fileAnnotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.FileAnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.FileAnnotationContext,i)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.StatementContext)
            else:
                return self.getTypedRuleContext(KotlinParser.StatementContext,i)


        def semi(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.SemiContext)
            else:
                return self.getTypedRuleContext(KotlinParser.SemiContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_script

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScript" ):
                listener.enterScript(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScript" ):
                listener.exitScript(self)




    def script(self):

        localctx = KotlinParser.ScriptContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_script)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 370
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.ShebangLine:
                self.state = 369
                self.shebangLine()


            self.state = 375
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 372
                    self.match(KotlinParser.NL) 
                self.state = 377
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

            self.state = 381
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 378
                    self.fileAnnotation() 
                self.state = 383
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

            self.state = 384
            self.packageHeader()
            self.state = 385
            self.importList()
            self.state = 391
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << KotlinParser.NL) | (1 << KotlinParser.LPAREN) | (1 << KotlinParser.LSQUARE) | (1 << KotlinParser.LCURL) | (1 << KotlinParser.ADD) | (1 << KotlinParser.SUB) | (1 << KotlinParser.INCR) | (1 << KotlinParser.DECR) | (1 << KotlinParser.EXCL_WS) | (1 << KotlinParser.EXCL_NO_WS) | (1 << KotlinParser.COLONCOLON) | (1 << KotlinParser.AT_NO_WS) | (1 << KotlinParser.AT_PRE_WS) | (1 << KotlinParser.RETURN_AT) | (1 << KotlinParser.CONTINUE_AT) | (1 << KotlinParser.BREAK_AT) | (1 << KotlinParser.THIS_AT) | (1 << KotlinParser.SUPER_AT) | (1 << KotlinParser.FILE) | (1 << KotlinParser.FIELD) | (1 << KotlinParser.PROPERTY))) != 0) or ((((_la - 64)) & ~0x3f) == 0 and ((1 << (_la - 64)) & ((1 << (KotlinParser.GET - 64)) | (1 << (KotlinParser.SET - 64)) | (1 << (KotlinParser.RECEIVER - 64)) | (1 << (KotlinParser.PARAM - 64)) | (1 << (KotlinParser.SETPARAM - 64)) | (1 << (KotlinParser.DELEGATE - 64)) | (1 << (KotlinParser.IMPORT - 64)) | (1 << (KotlinParser.CLASS - 64)) | (1 << (KotlinParser.INTERFACE - 64)) | (1 << (KotlinParser.FUN - 64)) | (1 << (KotlinParser.OBJECT - 64)) | (1 << (KotlinParser.VAL - 64)) | (1 << (KotlinParser.VAR - 64)) | (1 << (KotlinParser.TYPE_ALIAS - 64)) | (1 << (KotlinParser.CONSTRUCTOR - 64)) | (1 << (KotlinParser.BY - 64)) | (1 << (KotlinParser.COMPANION - 64)) | (1 << (KotlinParser.INIT - 64)) | (1 << (KotlinParser.THIS - 64)) | (1 << (KotlinParser.SUPER - 64)) | (1 << (KotlinParser.WHERE - 64)) | (1 << (KotlinParser.IF - 64)) | (1 << (KotlinParser.WHEN - 64)) | (1 << (KotlinParser.TRY - 64)) | (1 << (KotlinParser.CATCH - 64)) | (1 << (KotlinParser.FINALLY - 64)) | (1 << (KotlinParser.FOR - 64)) | (1 << (KotlinParser.DO - 64)) | (1 << (KotlinParser.WHILE - 64)) | (1 << (KotlinParser.THROW - 64)) | (1 << (KotlinParser.RETURN - 64)) | (1 << (KotlinParser.CONTINUE - 64)) | (1 << (KotlinParser.BREAK - 64)) | (1 << (KotlinParser.OUT - 64)) | (1 << (KotlinParser.DYNAMIC - 64)) | (1 << (KotlinParser.PUBLIC - 64)) | (1 << (KotlinParser.PRIVATE - 64)) | (1 << (KotlinParser.PROTECTED - 64)) | (1 << (KotlinParser.INTERNAL - 64)) | (1 << (KotlinParser.ENUM - 64)) | (1 << (KotlinParser.SEALED - 64)) | (1 << (KotlinParser.ANNOTATION - 64)) | (1 << (KotlinParser.DATA - 64)) | (1 << (KotlinParser.INNER - 64)) | (1 << (KotlinParser.TAILREC - 64)) | (1 << (KotlinParser.OPERATOR - 64)) | (1 << (KotlinParser.INLINE - 64)) | (1 << (KotlinParser.INFIX - 64)) | (1 << (KotlinParser.EXTERNAL - 64)) | (1 << (KotlinParser.SUSPEND - 64)) | (1 << (KotlinParser.OVERRIDE - 64)) | (1 << (KotlinParser.ABSTRACT - 64)) | (1 << (KotlinParser.FINAL - 64)) | (1 << (KotlinParser.OPEN - 64)) | (1 << (KotlinParser.CONST - 64)) | (1 << (KotlinParser.LATEINIT - 64)))) != 0) or ((((_la - 128)) & ~0x3f) == 0 and ((1 << (_la - 128)) & ((1 << (KotlinParser.VARARG - 128)) | (1 << (KotlinParser.NOINLINE - 128)) | (1 << (KotlinParser.CROSSINLINE - 128)) | (1 << (KotlinParser.REIFIED - 128)) | (1 << (KotlinParser.EXPECT - 128)) | (1 << (KotlinParser.ACTUAL - 128)) | (1 << (KotlinParser.RealLiteral - 128)) | (1 << (KotlinParser.IntegerLiteral - 128)) | (1 << (KotlinParser.HexLiteral - 128)) | (1 << (KotlinParser.BinLiteral - 128)) | (1 << (KotlinParser.UnsignedLiteral - 128)) | (1 << (KotlinParser.LongLiteral - 128)) | (1 << (KotlinParser.BooleanLiteral - 128)) | (1 << (KotlinParser.NullLiteral - 128)) | (1 << (KotlinParser.CharacterLiteral - 128)) | (1 << (KotlinParser.Identifier - 128)) | (1 << (KotlinParser.QUOTE_OPEN - 128)) | (1 << (KotlinParser.TRIPLE_QUOTE_OPEN - 128)))) != 0):
                self.state = 386
                self.statement()
                self.state = 387
                self.semi()
                self.state = 393
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 394
            self.match(KotlinParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ShebangLineContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ShebangLine(self):
            return self.getToken(KotlinParser.ShebangLine, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_shebangLine

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterShebangLine" ):
                listener.enterShebangLine(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitShebangLine" ):
                listener.exitShebangLine(self)




    def shebangLine(self):

        localctx = KotlinParser.ShebangLineContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_shebangLine)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 396
            self.match(KotlinParser.ShebangLine)
            self.state = 398 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 397
                    self.match(KotlinParser.NL)

                else:
                    raise NoViableAltException(self)
                self.state = 400 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FileAnnotationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FILE(self):
            return self.getToken(KotlinParser.FILE, 0)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def AT_NO_WS(self):
            return self.getToken(KotlinParser.AT_NO_WS, 0)

        def AT_PRE_WS(self):
            return self.getToken(KotlinParser.AT_PRE_WS, 0)

        def LSQUARE(self):
            return self.getToken(KotlinParser.LSQUARE, 0)

        def RSQUARE(self):
            return self.getToken(KotlinParser.RSQUARE, 0)

        def unescapedAnnotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.UnescapedAnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.UnescapedAnnotationContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_fileAnnotation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFileAnnotation" ):
                listener.enterFileAnnotation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFileAnnotation" ):
                listener.exitFileAnnotation(self)




    def fileAnnotation(self):

        localctx = KotlinParser.FileAnnotationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_fileAnnotation)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 402
            _la = self._input.LA(1)
            if not(_la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 403
            self.match(KotlinParser.FILE)
            self.state = 407
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 404
                self.match(KotlinParser.NL)
                self.state = 409
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 410
            self.match(KotlinParser.COLON)
            self.state = 414
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 411
                self.match(KotlinParser.NL)
                self.state = 416
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 426
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.LSQUARE]:
                self.state = 417
                self.match(KotlinParser.LSQUARE)
                self.state = 419 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 418
                    self.unescapedAnnotation()
                    self.state = 421 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (((((_la - 61)) & ~0x3f) == 0 and ((1 << (_la - 61)) & ((1 << (KotlinParser.FILE - 61)) | (1 << (KotlinParser.FIELD - 61)) | (1 << (KotlinParser.PROPERTY - 61)) | (1 << (KotlinParser.GET - 61)) | (1 << (KotlinParser.SET - 61)) | (1 << (KotlinParser.RECEIVER - 61)) | (1 << (KotlinParser.PARAM - 61)) | (1 << (KotlinParser.SETPARAM - 61)) | (1 << (KotlinParser.DELEGATE - 61)) | (1 << (KotlinParser.IMPORT - 61)) | (1 << (KotlinParser.CONSTRUCTOR - 61)) | (1 << (KotlinParser.BY - 61)) | (1 << (KotlinParser.COMPANION - 61)) | (1 << (KotlinParser.INIT - 61)) | (1 << (KotlinParser.WHERE - 61)) | (1 << (KotlinParser.CATCH - 61)) | (1 << (KotlinParser.FINALLY - 61)) | (1 << (KotlinParser.OUT - 61)) | (1 << (KotlinParser.DYNAMIC - 61)) | (1 << (KotlinParser.PUBLIC - 61)) | (1 << (KotlinParser.PRIVATE - 61)) | (1 << (KotlinParser.PROTECTED - 61)) | (1 << (KotlinParser.INTERNAL - 61)) | (1 << (KotlinParser.ENUM - 61)) | (1 << (KotlinParser.SEALED - 61)) | (1 << (KotlinParser.ANNOTATION - 61)) | (1 << (KotlinParser.DATA - 61)) | (1 << (KotlinParser.INNER - 61)) | (1 << (KotlinParser.TAILREC - 61)) | (1 << (KotlinParser.OPERATOR - 61)) | (1 << (KotlinParser.INLINE - 61)) | (1 << (KotlinParser.INFIX - 61)) | (1 << (KotlinParser.EXTERNAL - 61)) | (1 << (KotlinParser.SUSPEND - 61)) | (1 << (KotlinParser.OVERRIDE - 61)) | (1 << (KotlinParser.ABSTRACT - 61)) | (1 << (KotlinParser.FINAL - 61)))) != 0) or ((((_la - 125)) & ~0x3f) == 0 and ((1 << (_la - 125)) & ((1 << (KotlinParser.OPEN - 125)) | (1 << (KotlinParser.CONST - 125)) | (1 << (KotlinParser.LATEINIT - 125)) | (1 << (KotlinParser.VARARG - 125)) | (1 << (KotlinParser.NOINLINE - 125)) | (1 << (KotlinParser.CROSSINLINE - 125)) | (1 << (KotlinParser.REIFIED - 125)) | (1 << (KotlinParser.EXPECT - 125)) | (1 << (KotlinParser.ACTUAL - 125)) | (1 << (KotlinParser.Identifier - 125)))) != 0)):
                        break

                self.state = 423
                self.match(KotlinParser.RSQUARE)
                pass
            elif token in [KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.state = 425
                self.unescapedAnnotation()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 431
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,13,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 428
                    self.match(KotlinParser.NL) 
                self.state = 433
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,13,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PackageHeaderContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PACKAGE(self):
            return self.getToken(KotlinParser.PACKAGE, 0)

        def identifier(self):
            return self.getTypedRuleContext(KotlinParser.IdentifierContext,0)


        def semi(self):
            return self.getTypedRuleContext(KotlinParser.SemiContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_packageHeader

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPackageHeader" ):
                listener.enterPackageHeader(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPackageHeader" ):
                listener.exitPackageHeader(self)




    def packageHeader(self):

        localctx = KotlinParser.PackageHeaderContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_packageHeader)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 439
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.PACKAGE:
                self.state = 434
                self.match(KotlinParser.PACKAGE)
                self.state = 435
                self.identifier()
                self.state = 437
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
                if la_ == 1:
                    self.state = 436
                    self.semi()




        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ImportListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def importHeader(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ImportHeaderContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ImportHeaderContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_importList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImportList" ):
                listener.enterImportList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImportList" ):
                listener.exitImportList(self)




    def importList(self):

        localctx = KotlinParser.ImportListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_importList)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 444
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 441
                    self.importHeader() 
                self.state = 446
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ImportHeaderContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IMPORT(self):
            return self.getToken(KotlinParser.IMPORT, 0)

        def identifier(self):
            return self.getTypedRuleContext(KotlinParser.IdentifierContext,0)


        def DOT(self):
            return self.getToken(KotlinParser.DOT, 0)

        def MULT(self):
            return self.getToken(KotlinParser.MULT, 0)

        def importAlias(self):
            return self.getTypedRuleContext(KotlinParser.ImportAliasContext,0)


        def semi(self):
            return self.getTypedRuleContext(KotlinParser.SemiContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_importHeader

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImportHeader" ):
                listener.enterImportHeader(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImportHeader" ):
                listener.exitImportHeader(self)




    def importHeader(self):

        localctx = KotlinParser.ImportHeaderContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_importHeader)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 447
            self.match(KotlinParser.IMPORT)
            self.state = 448
            self.identifier()
            self.state = 452
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.DOT]:
                self.state = 449
                self.match(KotlinParser.DOT)
                self.state = 450
                self.match(KotlinParser.MULT)
                pass
            elif token in [KotlinParser.AS]:
                self.state = 451
                self.importAlias()
                pass
            elif token in [KotlinParser.EOF, KotlinParser.NL, KotlinParser.LPAREN, KotlinParser.LSQUARE, KotlinParser.LCURL, KotlinParser.ADD, KotlinParser.SUB, KotlinParser.INCR, KotlinParser.DECR, KotlinParser.EXCL_WS, KotlinParser.EXCL_NO_WS, KotlinParser.SEMICOLON, KotlinParser.COLONCOLON, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.RETURN_AT, KotlinParser.CONTINUE_AT, KotlinParser.BREAK_AT, KotlinParser.THIS_AT, KotlinParser.SUPER_AT, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CLASS, KotlinParser.INTERFACE, KotlinParser.FUN, KotlinParser.OBJECT, KotlinParser.VAL, KotlinParser.VAR, KotlinParser.TYPE_ALIAS, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.THIS, KotlinParser.SUPER, KotlinParser.WHERE, KotlinParser.IF, KotlinParser.WHEN, KotlinParser.TRY, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.FOR, KotlinParser.DO, KotlinParser.WHILE, KotlinParser.THROW, KotlinParser.RETURN, KotlinParser.CONTINUE, KotlinParser.BREAK, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.RealLiteral, KotlinParser.IntegerLiteral, KotlinParser.HexLiteral, KotlinParser.BinLiteral, KotlinParser.UnsignedLiteral, KotlinParser.LongLiteral, KotlinParser.BooleanLiteral, KotlinParser.NullLiteral, KotlinParser.CharacterLiteral, KotlinParser.Identifier, KotlinParser.QUOTE_OPEN, KotlinParser.TRIPLE_QUOTE_OPEN]:
                pass
            else:
                pass
            self.state = 455
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.state = 454
                self.semi()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ImportAliasContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def AS(self):
            return self.getToken(KotlinParser.AS, 0)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_importAlias

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImportAlias" ):
                listener.enterImportAlias(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImportAlias" ):
                listener.exitImportAlias(self)




    def importAlias(self):

        localctx = KotlinParser.ImportAliasContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_importAlias)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 457
            self.match(KotlinParser.AS)
            self.state = 458
            self.simpleIdentifier()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TopLevelObjectContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def declaration(self):
            return self.getTypedRuleContext(KotlinParser.DeclarationContext,0)


        def semis(self):
            return self.getTypedRuleContext(KotlinParser.SemisContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_topLevelObject

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTopLevelObject" ):
                listener.enterTopLevelObject(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTopLevelObject" ):
                listener.exitTopLevelObject(self)




    def topLevelObject(self):

        localctx = KotlinParser.TopLevelObjectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_topLevelObject)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 460
            self.declaration()
            self.state = 462
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.state = 461
                self.semis()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeAliasContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TYPE_ALIAS(self):
            return self.getToken(KotlinParser.TYPE_ALIAS, 0)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def ASSIGNMENT(self):
            return self.getToken(KotlinParser.ASSIGNMENT, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def typeParameters(self):
            return self.getTypedRuleContext(KotlinParser.TypeParametersContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_typeAlias

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeAlias" ):
                listener.enterTypeAlias(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeAlias" ):
                listener.exitTypeAlias(self)




    def typeAlias(self):

        localctx = KotlinParser.TypeAliasContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_typeAlias)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 465
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                self.state = 464
                self.modifiers()


            self.state = 467
            self.match(KotlinParser.TYPE_ALIAS)
            self.state = 471
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 468
                self.match(KotlinParser.NL)
                self.state = 473
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 474
            self.simpleIdentifier()
            self.state = 482
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,23,self._ctx)
            if la_ == 1:
                self.state = 478
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 475
                    self.match(KotlinParser.NL)
                    self.state = 480
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 481
                self.typeParameters()


            self.state = 487
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 484
                self.match(KotlinParser.NL)
                self.state = 489
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 490
            self.match(KotlinParser.ASSIGNMENT)
            self.state = 494
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 491
                self.match(KotlinParser.NL)
                self.state = 496
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 497
            self.type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def classDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.ClassDeclarationContext,0)


        def objectDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.ObjectDeclarationContext,0)


        def functionDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.FunctionDeclarationContext,0)


        def propertyDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.PropertyDeclarationContext,0)


        def typeAlias(self):
            return self.getTypedRuleContext(KotlinParser.TypeAliasContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_declaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDeclaration" ):
                listener.enterDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDeclaration" ):
                listener.exitDeclaration(self)




    def declaration(self):

        localctx = KotlinParser.DeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_declaration)
        try:
            self.state = 504
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 499
                self.classDeclaration()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 500
                self.objectDeclaration()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 501
                self.functionDeclaration()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 502
                self.propertyDeclaration()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 503
                self.typeAlias()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassDeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def CLASS(self):
            return self.getToken(KotlinParser.CLASS, 0)

        def INTERFACE(self):
            return self.getToken(KotlinParser.INTERFACE, 0)

        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def typeParameters(self):
            return self.getTypedRuleContext(KotlinParser.TypeParametersContext,0)


        def primaryConstructor(self):
            return self.getTypedRuleContext(KotlinParser.PrimaryConstructorContext,0)


        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def delegationSpecifiers(self):
            return self.getTypedRuleContext(KotlinParser.DelegationSpecifiersContext,0)


        def typeConstraints(self):
            return self.getTypedRuleContext(KotlinParser.TypeConstraintsContext,0)


        def classBody(self):
            return self.getTypedRuleContext(KotlinParser.ClassBodyContext,0)


        def enumClassBody(self):
            return self.getTypedRuleContext(KotlinParser.EnumClassBodyContext,0)


        def FUN(self):
            return self.getToken(KotlinParser.FUN, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_classDeclaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassDeclaration" ):
                listener.enterClassDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassDeclaration" ):
                listener.exitClassDeclaration(self)




    def classDeclaration(self):

        localctx = KotlinParser.ClassDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_classDeclaration)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 507
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                self.state = 506
                self.modifiers()


            self.state = 520
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.CLASS]:
                self.state = 509
                self.match(KotlinParser.CLASS)
                pass
            elif token in [KotlinParser.INTERFACE, KotlinParser.FUN]:
                self.state = 517
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.FUN:
                    self.state = 510
                    self.match(KotlinParser.FUN)
                    self.state = 514
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 511
                        self.match(KotlinParser.NL)
                        self.state = 516
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)



                self.state = 519
                self.match(KotlinParser.INTERFACE)
                pass
            else:
                raise NoViableAltException(self)

            self.state = 525
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 522
                self.match(KotlinParser.NL)
                self.state = 527
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 528
            self.simpleIdentifier()
            self.state = 536
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,33,self._ctx)
            if la_ == 1:
                self.state = 532
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 529
                    self.match(KotlinParser.NL)
                    self.state = 534
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 535
                self.typeParameters()


            self.state = 545
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,35,self._ctx)
            if la_ == 1:
                self.state = 541
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 538
                    self.match(KotlinParser.NL)
                    self.state = 543
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 544
                self.primaryConstructor()


            self.state = 561
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,38,self._ctx)
            if la_ == 1:
                self.state = 550
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 547
                    self.match(KotlinParser.NL)
                    self.state = 552
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 553
                self.match(KotlinParser.COLON)
                self.state = 557
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,37,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 554
                        self.match(KotlinParser.NL) 
                    self.state = 559
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,37,self._ctx)

                self.state = 560
                self.delegationSpecifiers()


            self.state = 570
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,40,self._ctx)
            if la_ == 1:
                self.state = 566
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 563
                    self.match(KotlinParser.NL)
                    self.state = 568
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 569
                self.typeConstraints()


            self.state = 586
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,43,self._ctx)
            if la_ == 1:
                self.state = 575
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 572
                    self.match(KotlinParser.NL)
                    self.state = 577
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 578
                self.classBody()

            elif la_ == 2:
                self.state = 582
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 579
                    self.match(KotlinParser.NL)
                    self.state = 584
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 585
                self.enumClassBody()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrimaryConstructorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def classParameters(self):
            return self.getTypedRuleContext(KotlinParser.ClassParametersContext,0)


        def CONSTRUCTOR(self):
            return self.getToken(KotlinParser.CONSTRUCTOR, 0)

        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_primaryConstructor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimaryConstructor" ):
                listener.enterPrimaryConstructor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimaryConstructor" ):
                listener.exitPrimaryConstructor(self)




    def primaryConstructor(self):

        localctx = KotlinParser.PrimaryConstructorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_primaryConstructor)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 598
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 79)) & ~0x3f) == 0 and ((1 << (_la - 79)) & ((1 << (KotlinParser.CONSTRUCTOR - 79)) | (1 << (KotlinParser.PUBLIC - 79)) | (1 << (KotlinParser.PRIVATE - 79)) | (1 << (KotlinParser.PROTECTED - 79)) | (1 << (KotlinParser.INTERNAL - 79)) | (1 << (KotlinParser.ENUM - 79)) | (1 << (KotlinParser.SEALED - 79)) | (1 << (KotlinParser.ANNOTATION - 79)) | (1 << (KotlinParser.DATA - 79)) | (1 << (KotlinParser.INNER - 79)) | (1 << (KotlinParser.TAILREC - 79)) | (1 << (KotlinParser.OPERATOR - 79)) | (1 << (KotlinParser.INLINE - 79)) | (1 << (KotlinParser.INFIX - 79)) | (1 << (KotlinParser.EXTERNAL - 79)) | (1 << (KotlinParser.SUSPEND - 79)) | (1 << (KotlinParser.OVERRIDE - 79)) | (1 << (KotlinParser.ABSTRACT - 79)) | (1 << (KotlinParser.FINAL - 79)) | (1 << (KotlinParser.OPEN - 79)) | (1 << (KotlinParser.CONST - 79)) | (1 << (KotlinParser.LATEINIT - 79)) | (1 << (KotlinParser.VARARG - 79)) | (1 << (KotlinParser.NOINLINE - 79)) | (1 << (KotlinParser.CROSSINLINE - 79)) | (1 << (KotlinParser.EXPECT - 79)) | (1 << (KotlinParser.ACTUAL - 79)))) != 0):
                self.state = 589
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                    self.state = 588
                    self.modifiers()


                self.state = 591
                self.match(KotlinParser.CONSTRUCTOR)
                self.state = 595
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 592
                    self.match(KotlinParser.NL)
                    self.state = 597
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 600
            self.classParameters()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassBodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LCURL(self):
            return self.getToken(KotlinParser.LCURL, 0)

        def classMemberDeclarations(self):
            return self.getTypedRuleContext(KotlinParser.ClassMemberDeclarationsContext,0)


        def RCURL(self):
            return self.getToken(KotlinParser.RCURL, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_classBody

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassBody" ):
                listener.enterClassBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassBody" ):
                listener.exitClassBody(self)




    def classBody(self):

        localctx = KotlinParser.ClassBodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_classBody)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 602
            self.match(KotlinParser.LCURL)
            self.state = 606
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,47,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 603
                    self.match(KotlinParser.NL) 
                self.state = 608
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,47,self._ctx)

            self.state = 609
            self.classMemberDeclarations()
            self.state = 613
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 610
                self.match(KotlinParser.NL)
                self.state = 615
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 616
            self.match(KotlinParser.RCURL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassParametersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def classParameter(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ClassParameterContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ClassParameterContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_classParameters

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassParameters" ):
                listener.enterClassParameters(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassParameters" ):
                listener.exitClassParameters(self)




    def classParameters(self):

        localctx = KotlinParser.ClassParametersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_classParameters)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 618
            self.match(KotlinParser.LPAREN)
            self.state = 622
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,49,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 619
                    self.match(KotlinParser.NL) 
                self.state = 624
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,49,self._ctx)

            self.state = 654
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,55,self._ctx)
            if la_ == 1:
                self.state = 625
                self.classParameter()
                self.state = 642
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,52,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 629
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 626
                            self.match(KotlinParser.NL)
                            self.state = 631
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 632
                        self.match(KotlinParser.COMMA)
                        self.state = 636
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,51,self._ctx)
                        while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                            if _alt==1:
                                self.state = 633
                                self.match(KotlinParser.NL) 
                            self.state = 638
                            self._errHandler.sync(self)
                            _alt = self._interp.adaptivePredict(self._input,51,self._ctx)

                        self.state = 639
                        self.classParameter() 
                    self.state = 644
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,52,self._ctx)

                self.state = 652
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,54,self._ctx)
                if la_ == 1:
                    self.state = 648
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 645
                        self.match(KotlinParser.NL)
                        self.state = 650
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 651
                    self.match(KotlinParser.COMMA)




            self.state = 659
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 656
                self.match(KotlinParser.NL)
                self.state = 661
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 662
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassParameterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def ASSIGNMENT(self):
            return self.getToken(KotlinParser.ASSIGNMENT, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def VAL(self):
            return self.getToken(KotlinParser.VAL, 0)

        def VAR(self):
            return self.getToken(KotlinParser.VAR, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_classParameter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassParameter" ):
                listener.enterClassParameter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassParameter" ):
                listener.exitClassParameter(self)




    def classParameter(self):

        localctx = KotlinParser.ClassParameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_classParameter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 665
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,57,self._ctx)
            if la_ == 1:
                self.state = 664
                self.modifiers()


            self.state = 668
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.VAL or _la==KotlinParser.VAR:
                self.state = 667
                _la = self._input.LA(1)
                if not(_la==KotlinParser.VAL or _la==KotlinParser.VAR):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()


            self.state = 673
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 670
                self.match(KotlinParser.NL)
                self.state = 675
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 676
            self.simpleIdentifier()
            self.state = 677
            self.match(KotlinParser.COLON)
            self.state = 681
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 678
                self.match(KotlinParser.NL)
                self.state = 683
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 684
            self.type()
            self.state = 699
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,63,self._ctx)
            if la_ == 1:
                self.state = 688
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 685
                    self.match(KotlinParser.NL)
                    self.state = 690
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 691
                self.match(KotlinParser.ASSIGNMENT)
                self.state = 695
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,62,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 692
                        self.match(KotlinParser.NL) 
                    self.state = 697
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,62,self._ctx)

                self.state = 698
                self.expression()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DelegationSpecifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotatedDelegationSpecifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotatedDelegationSpecifierContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotatedDelegationSpecifierContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_delegationSpecifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDelegationSpecifiers" ):
                listener.enterDelegationSpecifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDelegationSpecifiers" ):
                listener.exitDelegationSpecifiers(self)




    def delegationSpecifiers(self):

        localctx = KotlinParser.DelegationSpecifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_delegationSpecifiers)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 701
            self.annotatedDelegationSpecifier()
            self.state = 718
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,66,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 705
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 702
                        self.match(KotlinParser.NL)
                        self.state = 707
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 708
                    self.match(KotlinParser.COMMA)
                    self.state = 712
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,65,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 709
                            self.match(KotlinParser.NL) 
                        self.state = 714
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,65,self._ctx)

                    self.state = 715
                    self.annotatedDelegationSpecifier() 
                self.state = 720
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,66,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DelegationSpecifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def constructorInvocation(self):
            return self.getTypedRuleContext(KotlinParser.ConstructorInvocationContext,0)


        def explicitDelegation(self):
            return self.getTypedRuleContext(KotlinParser.ExplicitDelegationContext,0)


        def userType(self):
            return self.getTypedRuleContext(KotlinParser.UserTypeContext,0)


        def functionType(self):
            return self.getTypedRuleContext(KotlinParser.FunctionTypeContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_delegationSpecifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDelegationSpecifier" ):
                listener.enterDelegationSpecifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDelegationSpecifier" ):
                listener.exitDelegationSpecifier(self)




    def delegationSpecifier(self):

        localctx = KotlinParser.DelegationSpecifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_delegationSpecifier)
        try:
            self.state = 725
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,67,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 721
                self.constructorInvocation()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 722
                self.explicitDelegation()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 723
                self.userType()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 724
                self.functionType()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConstructorInvocationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def userType(self):
            return self.getTypedRuleContext(KotlinParser.UserTypeContext,0)


        def valueArguments(self):
            return self.getTypedRuleContext(KotlinParser.ValueArgumentsContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_constructorInvocation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConstructorInvocation" ):
                listener.enterConstructorInvocation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConstructorInvocation" ):
                listener.exitConstructorInvocation(self)




    def constructorInvocation(self):

        localctx = KotlinParser.ConstructorInvocationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_constructorInvocation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 727
            self.userType()
            self.state = 728
            self.valueArguments()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnnotatedDelegationSpecifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def delegationSpecifier(self):
            return self.getTypedRuleContext(KotlinParser.DelegationSpecifierContext,0)


        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_annotatedDelegationSpecifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnnotatedDelegationSpecifier" ):
                listener.enterAnnotatedDelegationSpecifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnnotatedDelegationSpecifier" ):
                listener.exitAnnotatedDelegationSpecifier(self)




    def annotatedDelegationSpecifier(self):

        localctx = KotlinParser.AnnotatedDelegationSpecifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_annotatedDelegationSpecifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 733
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,68,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 730
                    self.annotation() 
                self.state = 735
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,68,self._ctx)

            self.state = 739
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 736
                self.match(KotlinParser.NL)
                self.state = 741
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 742
            self.delegationSpecifier()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExplicitDelegationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BY(self):
            return self.getToken(KotlinParser.BY, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def userType(self):
            return self.getTypedRuleContext(KotlinParser.UserTypeContext,0)


        def functionType(self):
            return self.getTypedRuleContext(KotlinParser.FunctionTypeContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_explicitDelegation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExplicitDelegation" ):
                listener.enterExplicitDelegation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExplicitDelegation" ):
                listener.exitExplicitDelegation(self)




    def explicitDelegation(self):

        localctx = KotlinParser.ExplicitDelegationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_explicitDelegation)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 746
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,70,self._ctx)
            if la_ == 1:
                self.state = 744
                self.userType()
                pass

            elif la_ == 2:
                self.state = 745
                self.functionType()
                pass


            self.state = 751
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 748
                self.match(KotlinParser.NL)
                self.state = 753
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 754
            self.match(KotlinParser.BY)
            self.state = 758
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,72,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 755
                    self.match(KotlinParser.NL) 
                self.state = 760
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,72,self._ctx)

            self.state = 761
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeParametersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LANGLE(self):
            return self.getToken(KotlinParser.LANGLE, 0)

        def typeParameter(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeParameterContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeParameterContext,i)


        def RANGLE(self):
            return self.getToken(KotlinParser.RANGLE, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_typeParameters

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeParameters" ):
                listener.enterTypeParameters(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeParameters" ):
                listener.exitTypeParameters(self)




    def typeParameters(self):

        localctx = KotlinParser.TypeParametersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_typeParameters)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 763
            self.match(KotlinParser.LANGLE)
            self.state = 767
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,73,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 764
                    self.match(KotlinParser.NL) 
                self.state = 769
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,73,self._ctx)

            self.state = 770
            self.typeParameter()
            self.state = 787
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,76,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 774
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 771
                        self.match(KotlinParser.NL)
                        self.state = 776
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 777
                    self.match(KotlinParser.COMMA)
                    self.state = 781
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,75,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 778
                            self.match(KotlinParser.NL) 
                        self.state = 783
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,75,self._ctx)

                    self.state = 784
                    self.typeParameter() 
                self.state = 789
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,76,self._ctx)

            self.state = 797
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,78,self._ctx)
            if la_ == 1:
                self.state = 793
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 790
                    self.match(KotlinParser.NL)
                    self.state = 795
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 796
                self.match(KotlinParser.COMMA)


            self.state = 802
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 799
                self.match(KotlinParser.NL)
                self.state = 804
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 805
            self.match(KotlinParser.RANGLE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeParameterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def typeParameterModifiers(self):
            return self.getTypedRuleContext(KotlinParser.TypeParameterModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_typeParameter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeParameter" ):
                listener.enterTypeParameter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeParameter" ):
                listener.exitTypeParameter(self)




    def typeParameter(self):

        localctx = KotlinParser.TypeParameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_typeParameter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 808
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,80,self._ctx)
            if la_ == 1:
                self.state = 807
                self.typeParameterModifiers()


            self.state = 813
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 810
                self.match(KotlinParser.NL)
                self.state = 815
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 816
            self.simpleIdentifier()
            self.state = 831
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,84,self._ctx)
            if la_ == 1:
                self.state = 820
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 817
                    self.match(KotlinParser.NL)
                    self.state = 822
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 823
                self.match(KotlinParser.COLON)
                self.state = 827
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 824
                    self.match(KotlinParser.NL)
                    self.state = 829
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 830
                self.type()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeConstraintsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WHERE(self):
            return self.getToken(KotlinParser.WHERE, 0)

        def typeConstraint(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeConstraintContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeConstraintContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_typeConstraints

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeConstraints" ):
                listener.enterTypeConstraints(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeConstraints" ):
                listener.exitTypeConstraints(self)




    def typeConstraints(self):

        localctx = KotlinParser.TypeConstraintsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_typeConstraints)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 833
            self.match(KotlinParser.WHERE)
            self.state = 837
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 834
                self.match(KotlinParser.NL)
                self.state = 839
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 840
            self.typeConstraint()
            self.state = 857
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,88,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 844
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 841
                        self.match(KotlinParser.NL)
                        self.state = 846
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 847
                    self.match(KotlinParser.COMMA)
                    self.state = 851
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 848
                        self.match(KotlinParser.NL)
                        self.state = 853
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 854
                    self.typeConstraint() 
                self.state = 859
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,88,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeConstraintContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_typeConstraint

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeConstraint" ):
                listener.enterTypeConstraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeConstraint" ):
                listener.exitTypeConstraint(self)




    def typeConstraint(self):

        localctx = KotlinParser.TypeConstraintContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_typeConstraint)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 863
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS:
                self.state = 860
                self.annotation()
                self.state = 865
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 866
            self.simpleIdentifier()
            self.state = 870
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 867
                self.match(KotlinParser.NL)
                self.state = 872
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 873
            self.match(KotlinParser.COLON)
            self.state = 877
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 874
                self.match(KotlinParser.NL)
                self.state = 879
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 880
            self.type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassMemberDeclarationsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def classMemberDeclaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ClassMemberDeclarationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ClassMemberDeclarationContext,i)


        def semis(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.SemisContext)
            else:
                return self.getTypedRuleContext(KotlinParser.SemisContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_classMemberDeclarations

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassMemberDeclarations" ):
                listener.enterClassMemberDeclarations(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassMemberDeclarations" ):
                listener.exitClassMemberDeclarations(self)




    def classMemberDeclarations(self):

        localctx = KotlinParser.ClassMemberDeclarationsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_classMemberDeclarations)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 888
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 72)) & ~0x3f) == 0 and ((1 << (_la - 72)) & ((1 << (KotlinParser.CLASS - 72)) | (1 << (KotlinParser.INTERFACE - 72)) | (1 << (KotlinParser.FUN - 72)) | (1 << (KotlinParser.OBJECT - 72)) | (1 << (KotlinParser.VAL - 72)) | (1 << (KotlinParser.VAR - 72)) | (1 << (KotlinParser.TYPE_ALIAS - 72)) | (1 << (KotlinParser.CONSTRUCTOR - 72)) | (1 << (KotlinParser.COMPANION - 72)) | (1 << (KotlinParser.INIT - 72)) | (1 << (KotlinParser.PUBLIC - 72)) | (1 << (KotlinParser.PRIVATE - 72)) | (1 << (KotlinParser.PROTECTED - 72)) | (1 << (KotlinParser.INTERNAL - 72)) | (1 << (KotlinParser.ENUM - 72)) | (1 << (KotlinParser.SEALED - 72)) | (1 << (KotlinParser.ANNOTATION - 72)) | (1 << (KotlinParser.DATA - 72)) | (1 << (KotlinParser.INNER - 72)) | (1 << (KotlinParser.TAILREC - 72)) | (1 << (KotlinParser.OPERATOR - 72)) | (1 << (KotlinParser.INLINE - 72)) | (1 << (KotlinParser.INFIX - 72)) | (1 << (KotlinParser.EXTERNAL - 72)) | (1 << (KotlinParser.SUSPEND - 72)) | (1 << (KotlinParser.OVERRIDE - 72)) | (1 << (KotlinParser.ABSTRACT - 72)) | (1 << (KotlinParser.FINAL - 72)) | (1 << (KotlinParser.OPEN - 72)) | (1 << (KotlinParser.CONST - 72)) | (1 << (KotlinParser.LATEINIT - 72)) | (1 << (KotlinParser.VARARG - 72)) | (1 << (KotlinParser.NOINLINE - 72)) | (1 << (KotlinParser.CROSSINLINE - 72)) | (1 << (KotlinParser.EXPECT - 72)) | (1 << (KotlinParser.ACTUAL - 72)))) != 0):
                self.state = 882
                self.classMemberDeclaration()
                self.state = 884
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,92,self._ctx)
                if la_ == 1:
                    self.state = 883
                    self.semis()


                self.state = 890
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassMemberDeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def declaration(self):
            return self.getTypedRuleContext(KotlinParser.DeclarationContext,0)


        def companionObject(self):
            return self.getTypedRuleContext(KotlinParser.CompanionObjectContext,0)


        def anonymousInitializer(self):
            return self.getTypedRuleContext(KotlinParser.AnonymousInitializerContext,0)


        def secondaryConstructor(self):
            return self.getTypedRuleContext(KotlinParser.SecondaryConstructorContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_classMemberDeclaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassMemberDeclaration" ):
                listener.enterClassMemberDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassMemberDeclaration" ):
                listener.exitClassMemberDeclaration(self)




    def classMemberDeclaration(self):

        localctx = KotlinParser.ClassMemberDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_classMemberDeclaration)
        try:
            self.state = 895
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,94,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 891
                self.declaration()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 892
                self.companionObject()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 893
                self.anonymousInitializer()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 894
                self.secondaryConstructor()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnonymousInitializerContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INIT(self):
            return self.getToken(KotlinParser.INIT, 0)

        def block(self):
            return self.getTypedRuleContext(KotlinParser.BlockContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_anonymousInitializer

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnonymousInitializer" ):
                listener.enterAnonymousInitializer(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnonymousInitializer" ):
                listener.exitAnonymousInitializer(self)




    def anonymousInitializer(self):

        localctx = KotlinParser.AnonymousInitializerContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_anonymousInitializer)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 897
            self.match(KotlinParser.INIT)
            self.state = 901
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 898
                self.match(KotlinParser.NL)
                self.state = 903
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 904
            self.block()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CompanionObjectContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def COMPANION(self):
            return self.getToken(KotlinParser.COMPANION, 0)

        def OBJECT(self):
            return self.getToken(KotlinParser.OBJECT, 0)

        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def delegationSpecifiers(self):
            return self.getTypedRuleContext(KotlinParser.DelegationSpecifiersContext,0)


        def classBody(self):
            return self.getTypedRuleContext(KotlinParser.ClassBodyContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_companionObject

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCompanionObject" ):
                listener.enterCompanionObject(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCompanionObject" ):
                listener.exitCompanionObject(self)




    def companionObject(self):

        localctx = KotlinParser.CompanionObjectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_companionObject)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 907
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                self.state = 906
                self.modifiers()


            self.state = 909
            self.match(KotlinParser.COMPANION)
            self.state = 913
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 910
                self.match(KotlinParser.NL)
                self.state = 915
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 916
            self.match(KotlinParser.OBJECT)
            self.state = 924
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,99,self._ctx)
            if la_ == 1:
                self.state = 920
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 917
                    self.match(KotlinParser.NL)
                    self.state = 922
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 923
                self.simpleIdentifier()


            self.state = 940
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,102,self._ctx)
            if la_ == 1:
                self.state = 929
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 926
                    self.match(KotlinParser.NL)
                    self.state = 931
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 932
                self.match(KotlinParser.COLON)
                self.state = 936
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,101,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 933
                        self.match(KotlinParser.NL) 
                    self.state = 938
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,101,self._ctx)

                self.state = 939
                self.delegationSpecifiers()


            self.state = 949
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,104,self._ctx)
            if la_ == 1:
                self.state = 945
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 942
                    self.match(KotlinParser.NL)
                    self.state = 947
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 948
                self.classBody()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionValueParametersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def functionValueParameter(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.FunctionValueParameterContext)
            else:
                return self.getTypedRuleContext(KotlinParser.FunctionValueParameterContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_functionValueParameters

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionValueParameters" ):
                listener.enterFunctionValueParameters(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionValueParameters" ):
                listener.exitFunctionValueParameters(self)




    def functionValueParameters(self):

        localctx = KotlinParser.FunctionValueParametersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_functionValueParameters)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 951
            self.match(KotlinParser.LPAREN)
            self.state = 955
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,105,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 952
                    self.match(KotlinParser.NL) 
                self.state = 957
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,105,self._ctx)

            self.state = 987
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 40)) & ~0x3f) == 0 and ((1 << (_la - 40)) & ((1 << (KotlinParser.AT_NO_WS - 40)) | (1 << (KotlinParser.AT_PRE_WS - 40)) | (1 << (KotlinParser.FILE - 40)) | (1 << (KotlinParser.FIELD - 40)) | (1 << (KotlinParser.PROPERTY - 40)) | (1 << (KotlinParser.GET - 40)) | (1 << (KotlinParser.SET - 40)) | (1 << (KotlinParser.RECEIVER - 40)) | (1 << (KotlinParser.PARAM - 40)) | (1 << (KotlinParser.SETPARAM - 40)) | (1 << (KotlinParser.DELEGATE - 40)) | (1 << (KotlinParser.IMPORT - 40)) | (1 << (KotlinParser.CONSTRUCTOR - 40)) | (1 << (KotlinParser.BY - 40)) | (1 << (KotlinParser.COMPANION - 40)) | (1 << (KotlinParser.INIT - 40)) | (1 << (KotlinParser.WHERE - 40)) | (1 << (KotlinParser.CATCH - 40)) | (1 << (KotlinParser.FINALLY - 40)))) != 0) or ((((_la - 105)) & ~0x3f) == 0 and ((1 << (_la - 105)) & ((1 << (KotlinParser.OUT - 105)) | (1 << (KotlinParser.DYNAMIC - 105)) | (1 << (KotlinParser.PUBLIC - 105)) | (1 << (KotlinParser.PRIVATE - 105)) | (1 << (KotlinParser.PROTECTED - 105)) | (1 << (KotlinParser.INTERNAL - 105)) | (1 << (KotlinParser.ENUM - 105)) | (1 << (KotlinParser.SEALED - 105)) | (1 << (KotlinParser.ANNOTATION - 105)) | (1 << (KotlinParser.DATA - 105)) | (1 << (KotlinParser.INNER - 105)) | (1 << (KotlinParser.TAILREC - 105)) | (1 << (KotlinParser.OPERATOR - 105)) | (1 << (KotlinParser.INLINE - 105)) | (1 << (KotlinParser.INFIX - 105)) | (1 << (KotlinParser.EXTERNAL - 105)) | (1 << (KotlinParser.SUSPEND - 105)) | (1 << (KotlinParser.OVERRIDE - 105)) | (1 << (KotlinParser.ABSTRACT - 105)) | (1 << (KotlinParser.FINAL - 105)) | (1 << (KotlinParser.OPEN - 105)) | (1 << (KotlinParser.CONST - 105)) | (1 << (KotlinParser.LATEINIT - 105)) | (1 << (KotlinParser.VARARG - 105)) | (1 << (KotlinParser.NOINLINE - 105)) | (1 << (KotlinParser.CROSSINLINE - 105)) | (1 << (KotlinParser.REIFIED - 105)) | (1 << (KotlinParser.EXPECT - 105)) | (1 << (KotlinParser.ACTUAL - 105)) | (1 << (KotlinParser.Identifier - 105)))) != 0):
                self.state = 958
                self.functionValueParameter()
                self.state = 975
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,108,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 962
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 959
                            self.match(KotlinParser.NL)
                            self.state = 964
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 965
                        self.match(KotlinParser.COMMA)
                        self.state = 969
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 966
                            self.match(KotlinParser.NL)
                            self.state = 971
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 972
                        self.functionValueParameter() 
                    self.state = 977
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,108,self._ctx)

                self.state = 985
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,110,self._ctx)
                if la_ == 1:
                    self.state = 981
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 978
                        self.match(KotlinParser.NL)
                        self.state = 983
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 984
                    self.match(KotlinParser.COMMA)




            self.state = 992
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 989
                self.match(KotlinParser.NL)
                self.state = 994
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 995
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionValueParameterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parameter(self):
            return self.getTypedRuleContext(KotlinParser.ParameterContext,0)


        def parameterModifiers(self):
            return self.getTypedRuleContext(KotlinParser.ParameterModifiersContext,0)


        def ASSIGNMENT(self):
            return self.getToken(KotlinParser.ASSIGNMENT, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_functionValueParameter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionValueParameter" ):
                listener.enterFunctionValueParameter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionValueParameter" ):
                listener.exitFunctionValueParameter(self)




    def functionValueParameter(self):

        localctx = KotlinParser.FunctionValueParameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_functionValueParameter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 998
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,113,self._ctx)
            if la_ == 1:
                self.state = 997
                self.parameterModifiers()


            self.state = 1000
            self.parameter()
            self.state = 1015
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,116,self._ctx)
            if la_ == 1:
                self.state = 1004
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1001
                    self.match(KotlinParser.NL)
                    self.state = 1006
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1007
                self.match(KotlinParser.ASSIGNMENT)
                self.state = 1011
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,115,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 1008
                        self.match(KotlinParser.NL) 
                    self.state = 1013
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,115,self._ctx)

                self.state = 1014
                self.expression()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionDeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FUN(self):
            return self.getToken(KotlinParser.FUN, 0)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def functionValueParameters(self):
            return self.getTypedRuleContext(KotlinParser.FunctionValueParametersContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def typeParameters(self):
            return self.getTypedRuleContext(KotlinParser.TypeParametersContext,0)


        def receiverType(self):
            return self.getTypedRuleContext(KotlinParser.ReceiverTypeContext,0)


        def DOT(self):
            return self.getToken(KotlinParser.DOT, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def typeConstraints(self):
            return self.getTypedRuleContext(KotlinParser.TypeConstraintsContext,0)


        def functionBody(self):
            return self.getTypedRuleContext(KotlinParser.FunctionBodyContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_functionDeclaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionDeclaration" ):
                listener.enterFunctionDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionDeclaration" ):
                listener.exitFunctionDeclaration(self)




    def functionDeclaration(self):

        localctx = KotlinParser.FunctionDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_functionDeclaration)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1018
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                self.state = 1017
                self.modifiers()


            self.state = 1020
            self.match(KotlinParser.FUN)
            self.state = 1028
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,119,self._ctx)
            if la_ == 1:
                self.state = 1024
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1021
                    self.match(KotlinParser.NL)
                    self.state = 1026
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1027
                self.typeParameters()


            self.state = 1045
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,122,self._ctx)
            if la_ == 1:
                self.state = 1033
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1030
                    self.match(KotlinParser.NL)
                    self.state = 1035
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1036
                self.receiverType()
                self.state = 1040
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1037
                    self.match(KotlinParser.NL)
                    self.state = 1042
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1043
                self.match(KotlinParser.DOT)


            self.state = 1050
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1047
                self.match(KotlinParser.NL)
                self.state = 1052
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1053
            self.simpleIdentifier()
            self.state = 1057
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1054
                self.match(KotlinParser.NL)
                self.state = 1059
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1060
            self.functionValueParameters()
            self.state = 1075
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,127,self._ctx)
            if la_ == 1:
                self.state = 1064
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1061
                    self.match(KotlinParser.NL)
                    self.state = 1066
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1067
                self.match(KotlinParser.COLON)
                self.state = 1071
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1068
                    self.match(KotlinParser.NL)
                    self.state = 1073
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1074
                self.type()


            self.state = 1084
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,129,self._ctx)
            if la_ == 1:
                self.state = 1080
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1077
                    self.match(KotlinParser.NL)
                    self.state = 1082
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1083
                self.typeConstraints()


            self.state = 1093
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,131,self._ctx)
            if la_ == 1:
                self.state = 1089
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1086
                    self.match(KotlinParser.NL)
                    self.state = 1091
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1092
                self.functionBody()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionBodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def block(self):
            return self.getTypedRuleContext(KotlinParser.BlockContext,0)


        def ASSIGNMENT(self):
            return self.getToken(KotlinParser.ASSIGNMENT, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_functionBody

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionBody" ):
                listener.enterFunctionBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionBody" ):
                listener.exitFunctionBody(self)




    def functionBody(self):

        localctx = KotlinParser.FunctionBodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_functionBody)
        try:
            self.state = 1104
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.LCURL]:
                self.enterOuterAlt(localctx, 1)
                self.state = 1095
                self.block()
                pass
            elif token in [KotlinParser.ASSIGNMENT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 1096
                self.match(KotlinParser.ASSIGNMENT)
                self.state = 1100
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,132,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 1097
                        self.match(KotlinParser.NL) 
                    self.state = 1102
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,132,self._ctx)

                self.state = 1103
                self.expression()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VariableDeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_variableDeclaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVariableDeclaration" ):
                listener.enterVariableDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVariableDeclaration" ):
                listener.exitVariableDeclaration(self)




    def variableDeclaration(self):

        localctx = KotlinParser.VariableDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_variableDeclaration)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1109
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS:
                self.state = 1106
                self.annotation()
                self.state = 1111
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1115
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1112
                self.match(KotlinParser.NL)
                self.state = 1117
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1118
            self.simpleIdentifier()
            self.state = 1133
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,138,self._ctx)
            if la_ == 1:
                self.state = 1122
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1119
                    self.match(KotlinParser.NL)
                    self.state = 1124
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1125
                self.match(KotlinParser.COLON)
                self.state = 1129
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1126
                    self.match(KotlinParser.NL)
                    self.state = 1131
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1132
                self.type()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultiVariableDeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def variableDeclaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.VariableDeclarationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.VariableDeclarationContext,i)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_multiVariableDeclaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultiVariableDeclaration" ):
                listener.enterMultiVariableDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultiVariableDeclaration" ):
                listener.exitMultiVariableDeclaration(self)




    def multiVariableDeclaration(self):

        localctx = KotlinParser.MultiVariableDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_multiVariableDeclaration)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1135
            self.match(KotlinParser.LPAREN)
            self.state = 1139
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,139,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1136
                    self.match(KotlinParser.NL) 
                self.state = 1141
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,139,self._ctx)

            self.state = 1142
            self.variableDeclaration()
            self.state = 1159
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,142,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1146
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1143
                        self.match(KotlinParser.NL)
                        self.state = 1148
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1149
                    self.match(KotlinParser.COMMA)
                    self.state = 1153
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,141,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 1150
                            self.match(KotlinParser.NL) 
                        self.state = 1155
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,141,self._ctx)

                    self.state = 1156
                    self.variableDeclaration() 
                self.state = 1161
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,142,self._ctx)

            self.state = 1169
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,144,self._ctx)
            if la_ == 1:
                self.state = 1165
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1162
                    self.match(KotlinParser.NL)
                    self.state = 1167
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1168
                self.match(KotlinParser.COMMA)


            self.state = 1174
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1171
                self.match(KotlinParser.NL)
                self.state = 1176
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1177
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PropertyDeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAL(self):
            return self.getToken(KotlinParser.VAL, 0)

        def VAR(self):
            return self.getToken(KotlinParser.VAR, 0)

        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def typeParameters(self):
            return self.getTypedRuleContext(KotlinParser.TypeParametersContext,0)


        def receiverType(self):
            return self.getTypedRuleContext(KotlinParser.ReceiverTypeContext,0)


        def DOT(self):
            return self.getToken(KotlinParser.DOT, 0)

        def typeConstraints(self):
            return self.getTypedRuleContext(KotlinParser.TypeConstraintsContext,0)


        def SEMICOLON(self):
            return self.getToken(KotlinParser.SEMICOLON, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def multiVariableDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.MultiVariableDeclarationContext,0)


        def variableDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.VariableDeclarationContext,0)


        def ASSIGNMENT(self):
            return self.getToken(KotlinParser.ASSIGNMENT, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def propertyDelegate(self):
            return self.getTypedRuleContext(KotlinParser.PropertyDelegateContext,0)


        def getter(self):
            return self.getTypedRuleContext(KotlinParser.GetterContext,0)


        def setter(self):
            return self.getTypedRuleContext(KotlinParser.SetterContext,0)


        def semi(self):
            return self.getTypedRuleContext(KotlinParser.SemiContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_propertyDeclaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPropertyDeclaration" ):
                listener.enterPropertyDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPropertyDeclaration" ):
                listener.exitPropertyDeclaration(self)




    def propertyDeclaration(self):

        localctx = KotlinParser.PropertyDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_propertyDeclaration)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1180
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                self.state = 1179
                self.modifiers()


            self.state = 1182
            _la = self._input.LA(1)
            if not(_la==KotlinParser.VAL or _la==KotlinParser.VAR):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 1190
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,148,self._ctx)
            if la_ == 1:
                self.state = 1186
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1183
                    self.match(KotlinParser.NL)
                    self.state = 1188
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1189
                self.typeParameters()


            self.state = 1207
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,151,self._ctx)
            if la_ == 1:
                self.state = 1195
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1192
                    self.match(KotlinParser.NL)
                    self.state = 1197
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1198
                self.receiverType()
                self.state = 1202
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1199
                    self.match(KotlinParser.NL)
                    self.state = 1204
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1205
                self.match(KotlinParser.DOT)


            self.state = 1212
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,152,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1209
                    self.match(KotlinParser.NL) 
                self.state = 1214
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,152,self._ctx)

            self.state = 1217
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.LPAREN]:
                self.state = 1215
                self.multiVariableDeclaration()
                pass
            elif token in [KotlinParser.NL, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.state = 1216
                self.variableDeclaration()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 1226
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,155,self._ctx)
            if la_ == 1:
                self.state = 1222
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1219
                    self.match(KotlinParser.NL)
                    self.state = 1224
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1225
                self.typeConstraints()


            self.state = 1245
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,159,self._ctx)
            if la_ == 1:
                self.state = 1231
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1228
                    self.match(KotlinParser.NL)
                    self.state = 1233
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1243
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [KotlinParser.ASSIGNMENT]:
                    self.state = 1234
                    self.match(KotlinParser.ASSIGNMENT)
                    self.state = 1238
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,157,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 1235
                            self.match(KotlinParser.NL) 
                        self.state = 1240
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,157,self._ctx)

                    self.state = 1241
                    self.expression()
                    pass
                elif token in [KotlinParser.BY]:
                    self.state = 1242
                    self.propertyDelegate()
                    pass
                else:
                    raise NoViableAltException(self)



            self.state = 1253
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,161,self._ctx)
            if la_ == 1:
                self.state = 1248 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 1247
                    self.match(KotlinParser.NL)
                    self.state = 1250 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==KotlinParser.NL):
                        break

                self.state = 1252
                self.match(KotlinParser.SEMICOLON)


            self.state = 1258
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,162,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1255
                    self.match(KotlinParser.NL) 
                self.state = 1260
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,162,self._ctx)

            self.state = 1291
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,171,self._ctx)
            if la_ == 1:
                self.state = 1262
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,163,self._ctx)
                if la_ == 1:
                    self.state = 1261
                    self.getter()


                self.state = 1274
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,166,self._ctx)
                if la_ == 1:
                    self.state = 1267
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,164,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 1264
                            self.match(KotlinParser.NL) 
                        self.state = 1269
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,164,self._ctx)

                    self.state = 1271
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if ((((_la - -1)) & ~0x3f) == 0 and ((1 << (_la - -1)) & ((1 << (KotlinParser.EOF - -1)) | (1 << (KotlinParser.NL - -1)) | (1 << (KotlinParser.SEMICOLON - -1)))) != 0):
                        self.state = 1270
                        self.semi()


                    self.state = 1273
                    self.setter()


                pass

            elif la_ == 2:
                self.state = 1277
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,167,self._ctx)
                if la_ == 1:
                    self.state = 1276
                    self.setter()


                self.state = 1289
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,170,self._ctx)
                if la_ == 1:
                    self.state = 1282
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,168,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 1279
                            self.match(KotlinParser.NL) 
                        self.state = 1284
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,168,self._ctx)

                    self.state = 1286
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if ((((_la - -1)) & ~0x3f) == 0 and ((1 << (_la - -1)) & ((1 << (KotlinParser.EOF - -1)) | (1 << (KotlinParser.NL - -1)) | (1 << (KotlinParser.SEMICOLON - -1)))) != 0):
                        self.state = 1285
                        self.semi()


                    self.state = 1288
                    self.getter()


                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PropertyDelegateContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BY(self):
            return self.getToken(KotlinParser.BY, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_propertyDelegate

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPropertyDelegate" ):
                listener.enterPropertyDelegate(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPropertyDelegate" ):
                listener.exitPropertyDelegate(self)




    def propertyDelegate(self):

        localctx = KotlinParser.PropertyDelegateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_propertyDelegate)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1293
            self.match(KotlinParser.BY)
            self.state = 1297
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,172,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1294
                    self.match(KotlinParser.NL) 
                self.state = 1299
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,172,self._ctx)

            self.state = 1300
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class GetterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def GET(self):
            return self.getToken(KotlinParser.GET, 0)

        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def functionBody(self):
            return self.getTypedRuleContext(KotlinParser.FunctionBodyContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_getter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGetter" ):
                listener.enterGetter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGetter" ):
                listener.exitGetter(self)




    def getter(self):

        localctx = KotlinParser.GetterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_getter)
        self._la = 0 # Token type
        try:
            self.state = 1347
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,181,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 1303
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                    self.state = 1302
                    self.modifiers()


                self.state = 1305
                self.match(KotlinParser.GET)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 1307
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                    self.state = 1306
                    self.modifiers()


                self.state = 1309
                self.match(KotlinParser.GET)
                self.state = 1313
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1310
                    self.match(KotlinParser.NL)
                    self.state = 1315
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1316
                self.match(KotlinParser.LPAREN)
                self.state = 1320
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1317
                    self.match(KotlinParser.NL)
                    self.state = 1322
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1323
                self.match(KotlinParser.RPAREN)
                self.state = 1338
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,179,self._ctx)
                if la_ == 1:
                    self.state = 1327
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1324
                        self.match(KotlinParser.NL)
                        self.state = 1329
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1330
                    self.match(KotlinParser.COLON)
                    self.state = 1334
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1331
                        self.match(KotlinParser.NL)
                        self.state = 1336
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1337
                    self.type()


                self.state = 1343
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1340
                    self.match(KotlinParser.NL)
                    self.state = 1345
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1346
                self.functionBody()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SetterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SET(self):
            return self.getToken(KotlinParser.SET, 0)

        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def parameterWithOptionalType(self):
            return self.getTypedRuleContext(KotlinParser.ParameterWithOptionalTypeContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def functionBody(self):
            return self.getTypedRuleContext(KotlinParser.FunctionBodyContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COMMA(self):
            return self.getToken(KotlinParser.COMMA, 0)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_setter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSetter" ):
                listener.enterSetter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSetter" ):
                listener.exitSetter(self)




    def setter(self):

        localctx = KotlinParser.SetterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_setter)
        self._la = 0 # Token type
        try:
            self.state = 1411
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,193,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 1350
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                    self.state = 1349
                    self.modifiers()


                self.state = 1352
                self.match(KotlinParser.SET)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 1354
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                    self.state = 1353
                    self.modifiers()


                self.state = 1356
                self.match(KotlinParser.SET)
                self.state = 1360
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1357
                    self.match(KotlinParser.NL)
                    self.state = 1362
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1363
                self.match(KotlinParser.LPAREN)
                self.state = 1367
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1364
                    self.match(KotlinParser.NL)
                    self.state = 1369
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1370
                self.parameterWithOptionalType()
                self.state = 1378
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,187,self._ctx)
                if la_ == 1:
                    self.state = 1374
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1371
                        self.match(KotlinParser.NL)
                        self.state = 1376
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1377
                    self.match(KotlinParser.COMMA)


                self.state = 1383
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1380
                    self.match(KotlinParser.NL)
                    self.state = 1385
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1386
                self.match(KotlinParser.RPAREN)
                self.state = 1401
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,191,self._ctx)
                if la_ == 1:
                    self.state = 1390
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1387
                        self.match(KotlinParser.NL)
                        self.state = 1392
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1393
                    self.match(KotlinParser.COLON)
                    self.state = 1397
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1394
                        self.match(KotlinParser.NL)
                        self.state = 1399
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1400
                    self.type()


                self.state = 1406
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1403
                    self.match(KotlinParser.NL)
                    self.state = 1408
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1409
                self.functionBody()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParametersWithOptionalTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def parameterWithOptionalType(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ParameterWithOptionalTypeContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ParameterWithOptionalTypeContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_parametersWithOptionalType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParametersWithOptionalType" ):
                listener.enterParametersWithOptionalType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParametersWithOptionalType" ):
                listener.exitParametersWithOptionalType(self)




    def parametersWithOptionalType(self):

        localctx = KotlinParser.ParametersWithOptionalTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_parametersWithOptionalType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1413
            self.match(KotlinParser.LPAREN)
            self.state = 1417
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,194,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1414
                    self.match(KotlinParser.NL) 
                self.state = 1419
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,194,self._ctx)

            self.state = 1449
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 40)) & ~0x3f) == 0 and ((1 << (_la - 40)) & ((1 << (KotlinParser.AT_NO_WS - 40)) | (1 << (KotlinParser.AT_PRE_WS - 40)) | (1 << (KotlinParser.FILE - 40)) | (1 << (KotlinParser.FIELD - 40)) | (1 << (KotlinParser.PROPERTY - 40)) | (1 << (KotlinParser.GET - 40)) | (1 << (KotlinParser.SET - 40)) | (1 << (KotlinParser.RECEIVER - 40)) | (1 << (KotlinParser.PARAM - 40)) | (1 << (KotlinParser.SETPARAM - 40)) | (1 << (KotlinParser.DELEGATE - 40)) | (1 << (KotlinParser.IMPORT - 40)) | (1 << (KotlinParser.CONSTRUCTOR - 40)) | (1 << (KotlinParser.BY - 40)) | (1 << (KotlinParser.COMPANION - 40)) | (1 << (KotlinParser.INIT - 40)) | (1 << (KotlinParser.WHERE - 40)) | (1 << (KotlinParser.CATCH - 40)) | (1 << (KotlinParser.FINALLY - 40)))) != 0) or ((((_la - 105)) & ~0x3f) == 0 and ((1 << (_la - 105)) & ((1 << (KotlinParser.OUT - 105)) | (1 << (KotlinParser.DYNAMIC - 105)) | (1 << (KotlinParser.PUBLIC - 105)) | (1 << (KotlinParser.PRIVATE - 105)) | (1 << (KotlinParser.PROTECTED - 105)) | (1 << (KotlinParser.INTERNAL - 105)) | (1 << (KotlinParser.ENUM - 105)) | (1 << (KotlinParser.SEALED - 105)) | (1 << (KotlinParser.ANNOTATION - 105)) | (1 << (KotlinParser.DATA - 105)) | (1 << (KotlinParser.INNER - 105)) | (1 << (KotlinParser.TAILREC - 105)) | (1 << (KotlinParser.OPERATOR - 105)) | (1 << (KotlinParser.INLINE - 105)) | (1 << (KotlinParser.INFIX - 105)) | (1 << (KotlinParser.EXTERNAL - 105)) | (1 << (KotlinParser.SUSPEND - 105)) | (1 << (KotlinParser.OVERRIDE - 105)) | (1 << (KotlinParser.ABSTRACT - 105)) | (1 << (KotlinParser.FINAL - 105)) | (1 << (KotlinParser.OPEN - 105)) | (1 << (KotlinParser.CONST - 105)) | (1 << (KotlinParser.LATEINIT - 105)) | (1 << (KotlinParser.VARARG - 105)) | (1 << (KotlinParser.NOINLINE - 105)) | (1 << (KotlinParser.CROSSINLINE - 105)) | (1 << (KotlinParser.REIFIED - 105)) | (1 << (KotlinParser.EXPECT - 105)) | (1 << (KotlinParser.ACTUAL - 105)) | (1 << (KotlinParser.Identifier - 105)))) != 0):
                self.state = 1420
                self.parameterWithOptionalType()
                self.state = 1437
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,197,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 1424
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 1421
                            self.match(KotlinParser.NL)
                            self.state = 1426
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 1427
                        self.match(KotlinParser.COMMA)
                        self.state = 1431
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 1428
                            self.match(KotlinParser.NL)
                            self.state = 1433
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 1434
                        self.parameterWithOptionalType() 
                    self.state = 1439
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,197,self._ctx)

                self.state = 1447
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,199,self._ctx)
                if la_ == 1:
                    self.state = 1443
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1440
                        self.match(KotlinParser.NL)
                        self.state = 1445
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1446
                    self.match(KotlinParser.COMMA)




            self.state = 1454
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1451
                self.match(KotlinParser.NL)
                self.state = 1456
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1457
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterWithOptionalTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def parameterModifiers(self):
            return self.getTypedRuleContext(KotlinParser.ParameterModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_parameterWithOptionalType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameterWithOptionalType" ):
                listener.enterParameterWithOptionalType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameterWithOptionalType" ):
                listener.exitParameterWithOptionalType(self)




    def parameterWithOptionalType(self):

        localctx = KotlinParser.ParameterWithOptionalTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_parameterWithOptionalType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1460
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,202,self._ctx)
            if la_ == 1:
                self.state = 1459
                self.parameterModifiers()


            self.state = 1462
            self.simpleIdentifier()
            self.state = 1466
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,203,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1463
                    self.match(KotlinParser.NL) 
                self.state = 1468
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,203,self._ctx)

            self.state = 1477
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.COLON:
                self.state = 1469
                self.match(KotlinParser.COLON)
                self.state = 1473
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1470
                    self.match(KotlinParser.NL)
                    self.state = 1475
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1476
                self.type()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_parameter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameter" ):
                listener.enterParameter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameter" ):
                listener.exitParameter(self)




    def parameter(self):

        localctx = KotlinParser.ParameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_parameter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1479
            self.simpleIdentifier()
            self.state = 1483
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1480
                self.match(KotlinParser.NL)
                self.state = 1485
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1486
            self.match(KotlinParser.COLON)
            self.state = 1490
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1487
                self.match(KotlinParser.NL)
                self.state = 1492
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1493
            self.type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ObjectDeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OBJECT(self):
            return self.getToken(KotlinParser.OBJECT, 0)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def delegationSpecifiers(self):
            return self.getTypedRuleContext(KotlinParser.DelegationSpecifiersContext,0)


        def classBody(self):
            return self.getTypedRuleContext(KotlinParser.ClassBodyContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_objectDeclaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObjectDeclaration" ):
                listener.enterObjectDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObjectDeclaration" ):
                listener.exitObjectDeclaration(self)




    def objectDeclaration(self):

        localctx = KotlinParser.ObjectDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_objectDeclaration)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1496
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                self.state = 1495
                self.modifiers()


            self.state = 1498
            self.match(KotlinParser.OBJECT)
            self.state = 1502
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1499
                self.match(KotlinParser.NL)
                self.state = 1504
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1505
            self.simpleIdentifier()
            self.state = 1520
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,212,self._ctx)
            if la_ == 1:
                self.state = 1509
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1506
                    self.match(KotlinParser.NL)
                    self.state = 1511
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1512
                self.match(KotlinParser.COLON)
                self.state = 1516
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,211,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 1513
                        self.match(KotlinParser.NL) 
                    self.state = 1518
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,211,self._ctx)

                self.state = 1519
                self.delegationSpecifiers()


            self.state = 1529
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,214,self._ctx)
            if la_ == 1:
                self.state = 1525
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1522
                    self.match(KotlinParser.NL)
                    self.state = 1527
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1528
                self.classBody()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SecondaryConstructorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONSTRUCTOR(self):
            return self.getToken(KotlinParser.CONSTRUCTOR, 0)

        def functionValueParameters(self):
            return self.getTypedRuleContext(KotlinParser.FunctionValueParametersContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def constructorDelegationCall(self):
            return self.getTypedRuleContext(KotlinParser.ConstructorDelegationCallContext,0)


        def block(self):
            return self.getTypedRuleContext(KotlinParser.BlockContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_secondaryConstructor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSecondaryConstructor" ):
                listener.enterSecondaryConstructor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSecondaryConstructor" ):
                listener.exitSecondaryConstructor(self)




    def secondaryConstructor(self):

        localctx = KotlinParser.SecondaryConstructorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 86, self.RULE_secondaryConstructor)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1532
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS or ((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)) | (1 << (KotlinParser.ENUM - 107)) | (1 << (KotlinParser.SEALED - 107)) | (1 << (KotlinParser.ANNOTATION - 107)) | (1 << (KotlinParser.DATA - 107)) | (1 << (KotlinParser.INNER - 107)) | (1 << (KotlinParser.TAILREC - 107)) | (1 << (KotlinParser.OPERATOR - 107)) | (1 << (KotlinParser.INLINE - 107)) | (1 << (KotlinParser.INFIX - 107)) | (1 << (KotlinParser.EXTERNAL - 107)) | (1 << (KotlinParser.SUSPEND - 107)) | (1 << (KotlinParser.OVERRIDE - 107)) | (1 << (KotlinParser.ABSTRACT - 107)) | (1 << (KotlinParser.FINAL - 107)) | (1 << (KotlinParser.OPEN - 107)) | (1 << (KotlinParser.CONST - 107)) | (1 << (KotlinParser.LATEINIT - 107)) | (1 << (KotlinParser.VARARG - 107)) | (1 << (KotlinParser.NOINLINE - 107)) | (1 << (KotlinParser.CROSSINLINE - 107)) | (1 << (KotlinParser.EXPECT - 107)) | (1 << (KotlinParser.ACTUAL - 107)))) != 0):
                self.state = 1531
                self.modifiers()


            self.state = 1534
            self.match(KotlinParser.CONSTRUCTOR)
            self.state = 1538
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1535
                self.match(KotlinParser.NL)
                self.state = 1540
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1541
            self.functionValueParameters()
            self.state = 1556
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,219,self._ctx)
            if la_ == 1:
                self.state = 1545
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1542
                    self.match(KotlinParser.NL)
                    self.state = 1547
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1548
                self.match(KotlinParser.COLON)
                self.state = 1552
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1549
                    self.match(KotlinParser.NL)
                    self.state = 1554
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1555
                self.constructorDelegationCall()


            self.state = 1561
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,220,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1558
                    self.match(KotlinParser.NL) 
                self.state = 1563
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,220,self._ctx)

            self.state = 1565
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.LCURL:
                self.state = 1564
                self.block()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConstructorDelegationCallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def THIS(self):
            return self.getToken(KotlinParser.THIS, 0)

        def valueArguments(self):
            return self.getTypedRuleContext(KotlinParser.ValueArgumentsContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def SUPER(self):
            return self.getToken(KotlinParser.SUPER, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_constructorDelegationCall

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConstructorDelegationCall" ):
                listener.enterConstructorDelegationCall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConstructorDelegationCall" ):
                listener.exitConstructorDelegationCall(self)




    def constructorDelegationCall(self):

        localctx = KotlinParser.ConstructorDelegationCallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 88, self.RULE_constructorDelegationCall)
        self._la = 0 # Token type
        try:
            self.state = 1583
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.THIS]:
                self.enterOuterAlt(localctx, 1)
                self.state = 1567
                self.match(KotlinParser.THIS)
                self.state = 1571
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1568
                    self.match(KotlinParser.NL)
                    self.state = 1573
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1574
                self.valueArguments()
                pass
            elif token in [KotlinParser.SUPER]:
                self.enterOuterAlt(localctx, 2)
                self.state = 1575
                self.match(KotlinParser.SUPER)
                self.state = 1579
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1576
                    self.match(KotlinParser.NL)
                    self.state = 1581
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1582
                self.valueArguments()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EnumClassBodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LCURL(self):
            return self.getToken(KotlinParser.LCURL, 0)

        def RCURL(self):
            return self.getToken(KotlinParser.RCURL, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def enumEntries(self):
            return self.getTypedRuleContext(KotlinParser.EnumEntriesContext,0)


        def SEMICOLON(self):
            return self.getToken(KotlinParser.SEMICOLON, 0)

        def classMemberDeclarations(self):
            return self.getTypedRuleContext(KotlinParser.ClassMemberDeclarationsContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_enumClassBody

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnumClassBody" ):
                listener.enterEnumClassBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnumClassBody" ):
                listener.exitEnumClassBody(self)




    def enumClassBody(self):

        localctx = KotlinParser.EnumClassBodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 90, self.RULE_enumClassBody)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1585
            self.match(KotlinParser.LCURL)
            self.state = 1589
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,225,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1586
                    self.match(KotlinParser.NL) 
                self.state = 1591
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,225,self._ctx)

            self.state = 1593
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 40)) & ~0x3f) == 0 and ((1 << (_la - 40)) & ((1 << (KotlinParser.AT_NO_WS - 40)) | (1 << (KotlinParser.AT_PRE_WS - 40)) | (1 << (KotlinParser.FILE - 40)) | (1 << (KotlinParser.FIELD - 40)) | (1 << (KotlinParser.PROPERTY - 40)) | (1 << (KotlinParser.GET - 40)) | (1 << (KotlinParser.SET - 40)) | (1 << (KotlinParser.RECEIVER - 40)) | (1 << (KotlinParser.PARAM - 40)) | (1 << (KotlinParser.SETPARAM - 40)) | (1 << (KotlinParser.DELEGATE - 40)) | (1 << (KotlinParser.IMPORT - 40)) | (1 << (KotlinParser.CONSTRUCTOR - 40)) | (1 << (KotlinParser.BY - 40)) | (1 << (KotlinParser.COMPANION - 40)) | (1 << (KotlinParser.INIT - 40)) | (1 << (KotlinParser.WHERE - 40)) | (1 << (KotlinParser.CATCH - 40)) | (1 << (KotlinParser.FINALLY - 40)))) != 0) or ((((_la - 105)) & ~0x3f) == 0 and ((1 << (_la - 105)) & ((1 << (KotlinParser.OUT - 105)) | (1 << (KotlinParser.DYNAMIC - 105)) | (1 << (KotlinParser.PUBLIC - 105)) | (1 << (KotlinParser.PRIVATE - 105)) | (1 << (KotlinParser.PROTECTED - 105)) | (1 << (KotlinParser.INTERNAL - 105)) | (1 << (KotlinParser.ENUM - 105)) | (1 << (KotlinParser.SEALED - 105)) | (1 << (KotlinParser.ANNOTATION - 105)) | (1 << (KotlinParser.DATA - 105)) | (1 << (KotlinParser.INNER - 105)) | (1 << (KotlinParser.TAILREC - 105)) | (1 << (KotlinParser.OPERATOR - 105)) | (1 << (KotlinParser.INLINE - 105)) | (1 << (KotlinParser.INFIX - 105)) | (1 << (KotlinParser.EXTERNAL - 105)) | (1 << (KotlinParser.SUSPEND - 105)) | (1 << (KotlinParser.OVERRIDE - 105)) | (1 << (KotlinParser.ABSTRACT - 105)) | (1 << (KotlinParser.FINAL - 105)) | (1 << (KotlinParser.OPEN - 105)) | (1 << (KotlinParser.CONST - 105)) | (1 << (KotlinParser.LATEINIT - 105)) | (1 << (KotlinParser.VARARG - 105)) | (1 << (KotlinParser.NOINLINE - 105)) | (1 << (KotlinParser.CROSSINLINE - 105)) | (1 << (KotlinParser.REIFIED - 105)) | (1 << (KotlinParser.EXPECT - 105)) | (1 << (KotlinParser.ACTUAL - 105)) | (1 << (KotlinParser.Identifier - 105)))) != 0):
                self.state = 1592
                self.enumEntries()


            self.state = 1609
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,229,self._ctx)
            if la_ == 1:
                self.state = 1598
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1595
                    self.match(KotlinParser.NL)
                    self.state = 1600
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1601
                self.match(KotlinParser.SEMICOLON)
                self.state = 1605
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,228,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 1602
                        self.match(KotlinParser.NL) 
                    self.state = 1607
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,228,self._ctx)

                self.state = 1608
                self.classMemberDeclarations()


            self.state = 1614
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1611
                self.match(KotlinParser.NL)
                self.state = 1616
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1617
            self.match(KotlinParser.RCURL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EnumEntriesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def enumEntry(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.EnumEntryContext)
            else:
                return self.getTypedRuleContext(KotlinParser.EnumEntryContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_enumEntries

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnumEntries" ):
                listener.enterEnumEntries(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnumEntries" ):
                listener.exitEnumEntries(self)




    def enumEntries(self):

        localctx = KotlinParser.EnumEntriesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 92, self.RULE_enumEntries)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1619
            self.enumEntry()
            self.state = 1636
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,233,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1623
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1620
                        self.match(KotlinParser.NL)
                        self.state = 1625
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1626
                    self.match(KotlinParser.COMMA)
                    self.state = 1630
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1627
                        self.match(KotlinParser.NL)
                        self.state = 1632
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1633
                    self.enumEntry() 
                self.state = 1638
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,233,self._ctx)

            self.state = 1642
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,234,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1639
                    self.match(KotlinParser.NL) 
                self.state = 1644
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,234,self._ctx)

            self.state = 1646
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.COMMA:
                self.state = 1645
                self.match(KotlinParser.COMMA)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EnumEntryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def modifiers(self):
            return self.getTypedRuleContext(KotlinParser.ModifiersContext,0)


        def valueArguments(self):
            return self.getTypedRuleContext(KotlinParser.ValueArgumentsContext,0)


        def classBody(self):
            return self.getTypedRuleContext(KotlinParser.ClassBodyContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_enumEntry

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnumEntry" ):
                listener.enterEnumEntry(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnumEntry" ):
                listener.exitEnumEntry(self)




    def enumEntry(self):

        localctx = KotlinParser.EnumEntryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 94, self.RULE_enumEntry)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1655
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,237,self._ctx)
            if la_ == 1:
                self.state = 1648
                self.modifiers()
                self.state = 1652
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1649
                    self.match(KotlinParser.NL)
                    self.state = 1654
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 1657
            self.simpleIdentifier()
            self.state = 1665
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,239,self._ctx)
            if la_ == 1:
                self.state = 1661
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1658
                    self.match(KotlinParser.NL)
                    self.state = 1663
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1664
                self.valueArguments()


            self.state = 1674
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,241,self._ctx)
            if la_ == 1:
                self.state = 1670
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1667
                    self.match(KotlinParser.NL)
                    self.state = 1672
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1673
                self.classBody()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parenthesizedType(self):
            return self.getTypedRuleContext(KotlinParser.ParenthesizedTypeContext,0)


        def nullableType(self):
            return self.getTypedRuleContext(KotlinParser.NullableTypeContext,0)


        def typeReference(self):
            return self.getTypedRuleContext(KotlinParser.TypeReferenceContext,0)


        def functionType(self):
            return self.getTypedRuleContext(KotlinParser.FunctionTypeContext,0)


        def typeModifiers(self):
            return self.getTypedRuleContext(KotlinParser.TypeModifiersContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType" ):
                listener.enterType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType" ):
                listener.exitType(self)




    def type(self):

        localctx = KotlinParser.TypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 96, self.RULE_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1677
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,242,self._ctx)
            if la_ == 1:
                self.state = 1676
                self.typeModifiers()


            self.state = 1683
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,243,self._ctx)
            if la_ == 1:
                self.state = 1679
                self.parenthesizedType()
                pass

            elif la_ == 2:
                self.state = 1680
                self.nullableType()
                pass

            elif la_ == 3:
                self.state = 1681
                self.typeReference()
                pass

            elif la_ == 4:
                self.state = 1682
                self.functionType()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeReferenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def userType(self):
            return self.getTypedRuleContext(KotlinParser.UserTypeContext,0)


        def DYNAMIC(self):
            return self.getToken(KotlinParser.DYNAMIC, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_typeReference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeReference" ):
                listener.enterTypeReference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeReference" ):
                listener.exitTypeReference(self)




    def typeReference(self):

        localctx = KotlinParser.TypeReferenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 98, self.RULE_typeReference)
        try:
            self.state = 1687
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,244,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 1685
                self.userType()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 1686
                self.match(KotlinParser.DYNAMIC)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NullableTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typeReference(self):
            return self.getTypedRuleContext(KotlinParser.TypeReferenceContext,0)


        def parenthesizedType(self):
            return self.getTypedRuleContext(KotlinParser.ParenthesizedTypeContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def quest(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.QuestContext)
            else:
                return self.getTypedRuleContext(KotlinParser.QuestContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_nullableType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNullableType" ):
                listener.enterNullableType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNullableType" ):
                listener.exitNullableType(self)




    def nullableType(self):

        localctx = KotlinParser.NullableTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 100, self.RULE_nullableType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1691
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.state = 1689
                self.typeReference()
                pass
            elif token in [KotlinParser.LPAREN]:
                self.state = 1690
                self.parenthesizedType()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 1696
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1693
                self.match(KotlinParser.NL)
                self.state = 1698
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1700 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 1699
                    self.quest()

                else:
                    raise NoViableAltException(self)
                self.state = 1702 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,247,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class QuestContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def QUEST_NO_WS(self):
            return self.getToken(KotlinParser.QUEST_NO_WS, 0)

        def QUEST_WS(self):
            return self.getToken(KotlinParser.QUEST_WS, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_quest

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuest" ):
                listener.enterQuest(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuest" ):
                listener.exitQuest(self)




    def quest(self):

        localctx = KotlinParser.QuestContext(self, self._ctx, self.state)
        self.enterRule(localctx, 102, self.RULE_quest)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1704
            _la = self._input.LA(1)
            if not(_la==KotlinParser.QUEST_WS or _la==KotlinParser.QUEST_NO_WS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UserTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleUserType(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.SimpleUserTypeContext)
            else:
                return self.getTypedRuleContext(KotlinParser.SimpleUserTypeContext,i)


        def DOT(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.DOT)
            else:
                return self.getToken(KotlinParser.DOT, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_userType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUserType" ):
                listener.enterUserType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUserType" ):
                listener.exitUserType(self)




    def userType(self):

        localctx = KotlinParser.UserTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 104, self.RULE_userType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1706
            self.simpleUserType()
            self.state = 1723
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,250,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1710
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1707
                        self.match(KotlinParser.NL)
                        self.state = 1712
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1713
                    self.match(KotlinParser.DOT)
                    self.state = 1717
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1714
                        self.match(KotlinParser.NL)
                        self.state = 1719
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1720
                    self.simpleUserType() 
                self.state = 1725
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,250,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SimpleUserTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def typeArguments(self):
            return self.getTypedRuleContext(KotlinParser.TypeArgumentsContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_simpleUserType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSimpleUserType" ):
                listener.enterSimpleUserType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSimpleUserType" ):
                listener.exitSimpleUserType(self)




    def simpleUserType(self):

        localctx = KotlinParser.SimpleUserTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 106, self.RULE_simpleUserType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1726
            self.simpleIdentifier()
            self.state = 1734
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,252,self._ctx)
            if la_ == 1:
                self.state = 1730
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1727
                    self.match(KotlinParser.NL)
                    self.state = 1732
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1733
                self.typeArguments()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeProjectionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def typeProjectionModifiers(self):
            return self.getTypedRuleContext(KotlinParser.TypeProjectionModifiersContext,0)


        def MULT(self):
            return self.getToken(KotlinParser.MULT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_typeProjection

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeProjection" ):
                listener.enterTypeProjection(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeProjection" ):
                listener.exitTypeProjection(self)




    def typeProjection(self):

        localctx = KotlinParser.TypeProjectionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 108, self.RULE_typeProjection)
        try:
            self.state = 1741
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.LPAREN, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.IN, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.enterOuterAlt(localctx, 1)
                self.state = 1737
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,253,self._ctx)
                if la_ == 1:
                    self.state = 1736
                    self.typeProjectionModifiers()


                self.state = 1739
                self.type()
                pass
            elif token in [KotlinParser.MULT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 1740
                self.match(KotlinParser.MULT)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeProjectionModifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typeProjectionModifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeProjectionModifierContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeProjectionModifierContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_typeProjectionModifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeProjectionModifiers" ):
                listener.enterTypeProjectionModifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeProjectionModifiers" ):
                listener.exitTypeProjectionModifiers(self)




    def typeProjectionModifiers(self):

        localctx = KotlinParser.TypeProjectionModifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 110, self.RULE_typeProjectionModifiers)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1744 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 1743
                    self.typeProjectionModifier()

                else:
                    raise NoViableAltException(self)
                self.state = 1746 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,255,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeProjectionModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varianceModifier(self):
            return self.getTypedRuleContext(KotlinParser.VarianceModifierContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def annotation(self):
            return self.getTypedRuleContext(KotlinParser.AnnotationContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_typeProjectionModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeProjectionModifier" ):
                listener.enterTypeProjectionModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeProjectionModifier" ):
                listener.exitTypeProjectionModifier(self)




    def typeProjectionModifier(self):

        localctx = KotlinParser.TypeProjectionModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 112, self.RULE_typeProjectionModifier)
        self._la = 0 # Token type
        try:
            self.state = 1756
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.IN, KotlinParser.OUT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 1748
                self.varianceModifier()
                self.state = 1752
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1749
                    self.match(KotlinParser.NL)
                    self.state = 1754
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS]:
                self.enterOuterAlt(localctx, 2)
                self.state = 1755
                self.annotation()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def functionTypeParameters(self):
            return self.getTypedRuleContext(KotlinParser.FunctionTypeParametersContext,0)


        def ARROW(self):
            return self.getToken(KotlinParser.ARROW, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def receiverType(self):
            return self.getTypedRuleContext(KotlinParser.ReceiverTypeContext,0)


        def DOT(self):
            return self.getToken(KotlinParser.DOT, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_functionType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionType" ):
                listener.enterFunctionType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionType" ):
                listener.exitFunctionType(self)




    def functionType(self):

        localctx = KotlinParser.FunctionTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 114, self.RULE_functionType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1772
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,260,self._ctx)
            if la_ == 1:
                self.state = 1758
                self.receiverType()
                self.state = 1762
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1759
                    self.match(KotlinParser.NL)
                    self.state = 1764
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1765
                self.match(KotlinParser.DOT)
                self.state = 1769
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1766
                    self.match(KotlinParser.NL)
                    self.state = 1771
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 1774
            self.functionTypeParameters()
            self.state = 1778
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1775
                self.match(KotlinParser.NL)
                self.state = 1780
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1781
            self.match(KotlinParser.ARROW)
            self.state = 1785
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1782
                self.match(KotlinParser.NL)
                self.state = 1787
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1788
            self.type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionTypeParametersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def parameter(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ParameterContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ParameterContext,i)


        def type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_functionTypeParameters

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionTypeParameters" ):
                listener.enterFunctionTypeParameters(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionTypeParameters" ):
                listener.exitFunctionTypeParameters(self)




    def functionTypeParameters(self):

        localctx = KotlinParser.FunctionTypeParametersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 116, self.RULE_functionTypeParameters)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1790
            self.match(KotlinParser.LPAREN)
            self.state = 1794
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,263,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1791
                    self.match(KotlinParser.NL) 
                self.state = 1796
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,263,self._ctx)

            self.state = 1799
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,264,self._ctx)
            if la_ == 1:
                self.state = 1797
                self.parameter()

            elif la_ == 2:
                self.state = 1798
                self.type()


            self.state = 1820
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,268,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1804
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1801
                        self.match(KotlinParser.NL)
                        self.state = 1806
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1807
                    self.match(KotlinParser.COMMA)
                    self.state = 1811
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 1808
                        self.match(KotlinParser.NL)
                        self.state = 1813
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 1816
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,267,self._ctx)
                    if la_ == 1:
                        self.state = 1814
                        self.parameter()
                        pass

                    elif la_ == 2:
                        self.state = 1815
                        self.type()
                        pass

             
                self.state = 1822
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,268,self._ctx)

            self.state = 1830
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,270,self._ctx)
            if la_ == 1:
                self.state = 1826
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1823
                    self.match(KotlinParser.NL)
                    self.state = 1828
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1829
                self.match(KotlinParser.COMMA)


            self.state = 1835
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1832
                self.match(KotlinParser.NL)
                self.state = 1837
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1838
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParenthesizedTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_parenthesizedType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParenthesizedType" ):
                listener.enterParenthesizedType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParenthesizedType" ):
                listener.exitParenthesizedType(self)




    def parenthesizedType(self):

        localctx = KotlinParser.ParenthesizedTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 118, self.RULE_parenthesizedType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1840
            self.match(KotlinParser.LPAREN)
            self.state = 1844
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1841
                self.match(KotlinParser.NL)
                self.state = 1846
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1847
            self.type()
            self.state = 1851
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1848
                self.match(KotlinParser.NL)
                self.state = 1853
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1854
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReceiverTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parenthesizedType(self):
            return self.getTypedRuleContext(KotlinParser.ParenthesizedTypeContext,0)


        def nullableType(self):
            return self.getTypedRuleContext(KotlinParser.NullableTypeContext,0)


        def typeReference(self):
            return self.getTypedRuleContext(KotlinParser.TypeReferenceContext,0)


        def typeModifiers(self):
            return self.getTypedRuleContext(KotlinParser.TypeModifiersContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_receiverType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReceiverType" ):
                listener.enterReceiverType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReceiverType" ):
                listener.exitReceiverType(self)




    def receiverType(self):

        localctx = KotlinParser.ReceiverTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 120, self.RULE_receiverType)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1857
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,274,self._ctx)
            if la_ == 1:
                self.state = 1856
                self.typeModifiers()


            self.state = 1862
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,275,self._ctx)
            if la_ == 1:
                self.state = 1859
                self.parenthesizedType()
                pass

            elif la_ == 2:
                self.state = 1860
                self.nullableType()
                pass

            elif la_ == 3:
                self.state = 1861
                self.typeReference()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParenthesizedUserTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def userType(self):
            return self.getTypedRuleContext(KotlinParser.UserTypeContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def parenthesizedUserType(self):
            return self.getTypedRuleContext(KotlinParser.ParenthesizedUserTypeContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_parenthesizedUserType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParenthesizedUserType" ):
                listener.enterParenthesizedUserType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParenthesizedUserType" ):
                listener.exitParenthesizedUserType(self)




    def parenthesizedUserType(self):

        localctx = KotlinParser.ParenthesizedUserTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 122, self.RULE_parenthesizedUserType)
        self._la = 0 # Token type
        try:
            self.state = 1896
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,280,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 1864
                self.match(KotlinParser.LPAREN)
                self.state = 1868
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1865
                    self.match(KotlinParser.NL)
                    self.state = 1870
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1871
                self.userType()
                self.state = 1875
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1872
                    self.match(KotlinParser.NL)
                    self.state = 1877
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1878
                self.match(KotlinParser.RPAREN)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 1880
                self.match(KotlinParser.LPAREN)
                self.state = 1884
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1881
                    self.match(KotlinParser.NL)
                    self.state = 1886
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1887
                self.parenthesizedUserType()
                self.state = 1891
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1888
                    self.match(KotlinParser.NL)
                    self.state = 1893
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1894
                self.match(KotlinParser.RPAREN)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.StatementContext)
            else:
                return self.getTypedRuleContext(KotlinParser.StatementContext,i)


        def semis(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.SemisContext)
            else:
                return self.getTypedRuleContext(KotlinParser.SemisContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_statements

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatements" ):
                listener.enterStatements(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatements" ):
                listener.exitStatements(self)




    def statements(self):

        localctx = KotlinParser.StatementsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 124, self.RULE_statements)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1907
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,282,self._ctx)
            if la_ == 1:
                self.state = 1898
                self.statement()
                self.state = 1904
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,281,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 1899
                        self.semis()
                        self.state = 1900
                        self.statement() 
                    self.state = 1906
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,281,self._ctx)



            self.state = 1910
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,283,self._ctx)
            if la_ == 1:
                self.state = 1909
                self.semis()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def declaration(self):
            return self.getTypedRuleContext(KotlinParser.DeclarationContext,0)


        def assignment(self):
            return self.getTypedRuleContext(KotlinParser.AssignmentContext,0)


        def loopStatement(self):
            return self.getTypedRuleContext(KotlinParser.LoopStatementContext,0)


        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def label(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.LabelContext)
            else:
                return self.getTypedRuleContext(KotlinParser.LabelContext,i)


        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = KotlinParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 126, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1916
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,285,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1914
                    self._errHandler.sync(self)
                    token = self._input.LA(1)
                    if token in [KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                        self.state = 1912
                        self.label()
                        pass
                    elif token in [KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS]:
                        self.state = 1913
                        self.annotation()
                        pass
                    else:
                        raise NoViableAltException(self)
             
                self.state = 1918
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,285,self._ctx)

            self.state = 1923
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,286,self._ctx)
            if la_ == 1:
                self.state = 1919
                self.declaration()
                pass

            elif la_ == 2:
                self.state = 1920
                self.assignment()
                pass

            elif la_ == 3:
                self.state = 1921
                self.loopStatement()
                pass

            elif la_ == 4:
                self.state = 1922
                self.expression()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LabelContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def AT_NO_WS(self):
            return self.getToken(KotlinParser.AT_NO_WS, 0)

        def AT_POST_WS(self):
            return self.getToken(KotlinParser.AT_POST_WS, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_label

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLabel" ):
                listener.enterLabel(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLabel" ):
                listener.exitLabel(self)




    def label(self):

        localctx = KotlinParser.LabelContext(self, self._ctx, self.state)
        self.enterRule(localctx, 128, self.RULE_label)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1925
            self.simpleIdentifier()
            self.state = 1926
            _la = self._input.LA(1)
            if not(_la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_POST_WS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 1930
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,287,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1927
                    self.match(KotlinParser.NL) 
                self.state = 1932
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,287,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ControlStructureBodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def block(self):
            return self.getTypedRuleContext(KotlinParser.BlockContext,0)


        def statement(self):
            return self.getTypedRuleContext(KotlinParser.StatementContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_controlStructureBody

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterControlStructureBody" ):
                listener.enterControlStructureBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitControlStructureBody" ):
                listener.exitControlStructureBody(self)




    def controlStructureBody(self):

        localctx = KotlinParser.ControlStructureBodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 130, self.RULE_controlStructureBody)
        try:
            self.state = 1935
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,288,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 1933
                self.block()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 1934
                self.statement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BlockContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LCURL(self):
            return self.getToken(KotlinParser.LCURL, 0)

        def statements(self):
            return self.getTypedRuleContext(KotlinParser.StatementsContext,0)


        def RCURL(self):
            return self.getToken(KotlinParser.RCURL, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_block

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBlock" ):
                listener.enterBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBlock" ):
                listener.exitBlock(self)




    def block(self):

        localctx = KotlinParser.BlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 132, self.RULE_block)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1937
            self.match(KotlinParser.LCURL)
            self.state = 1941
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,289,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1938
                    self.match(KotlinParser.NL) 
                self.state = 1943
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,289,self._ctx)

            self.state = 1944
            self.statements()
            self.state = 1948
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1945
                self.match(KotlinParser.NL)
                self.state = 1950
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1951
            self.match(KotlinParser.RCURL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LoopStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def forStatement(self):
            return self.getTypedRuleContext(KotlinParser.ForStatementContext,0)


        def whileStatement(self):
            return self.getTypedRuleContext(KotlinParser.WhileStatementContext,0)


        def doWhileStatement(self):
            return self.getTypedRuleContext(KotlinParser.DoWhileStatementContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_loopStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLoopStatement" ):
                listener.enterLoopStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLoopStatement" ):
                listener.exitLoopStatement(self)




    def loopStatement(self):

        localctx = KotlinParser.LoopStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 134, self.RULE_loopStatement)
        try:
            self.state = 1956
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.FOR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 1953
                self.forStatement()
                pass
            elif token in [KotlinParser.WHILE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 1954
                self.whileStatement()
                pass
            elif token in [KotlinParser.DO]:
                self.enterOuterAlt(localctx, 3)
                self.state = 1955
                self.doWhileStatement()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ForStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(KotlinParser.FOR, 0)

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def IN(self):
            return self.getToken(KotlinParser.IN, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def variableDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.VariableDeclarationContext,0)


        def multiVariableDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.MultiVariableDeclarationContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def controlStructureBody(self):
            return self.getTypedRuleContext(KotlinParser.ControlStructureBodyContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_forStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterForStatement" ):
                listener.enterForStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitForStatement" ):
                listener.exitForStatement(self)




    def forStatement(self):

        localctx = KotlinParser.ForStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 136, self.RULE_forStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 1958
            self.match(KotlinParser.FOR)
            self.state = 1962
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 1959
                self.match(KotlinParser.NL)
                self.state = 1964
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 1965
            self.match(KotlinParser.LPAREN)
            self.state = 1969
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,293,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1966
                    self.annotation() 
                self.state = 1971
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,293,self._ctx)

            self.state = 1974
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.NL, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.state = 1972
                self.variableDeclaration()
                pass
            elif token in [KotlinParser.LPAREN]:
                self.state = 1973
                self.multiVariableDeclaration()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 1976
            self.match(KotlinParser.IN)
            self.state = 1977
            self.expression()
            self.state = 1978
            self.match(KotlinParser.RPAREN)
            self.state = 1982
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,295,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 1979
                    self.match(KotlinParser.NL) 
                self.state = 1984
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,295,self._ctx)

            self.state = 1986
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,296,self._ctx)
            if la_ == 1:
                self.state = 1985
                self.controlStructureBody()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhileStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WHILE(self):
            return self.getToken(KotlinParser.WHILE, 0)

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def controlStructureBody(self):
            return self.getTypedRuleContext(KotlinParser.ControlStructureBodyContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def SEMICOLON(self):
            return self.getToken(KotlinParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_whileStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhileStatement" ):
                listener.enterWhileStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhileStatement" ):
                listener.exitWhileStatement(self)




    def whileStatement(self):

        localctx = KotlinParser.WhileStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 138, self.RULE_whileStatement)
        self._la = 0 # Token type
        try:
            self.state = 2024
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,301,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 1988
                self.match(KotlinParser.WHILE)
                self.state = 1992
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 1989
                    self.match(KotlinParser.NL)
                    self.state = 1994
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 1995
                self.match(KotlinParser.LPAREN)
                self.state = 1996
                self.expression()
                self.state = 1997
                self.match(KotlinParser.RPAREN)
                self.state = 2001
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,298,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 1998
                        self.match(KotlinParser.NL) 
                    self.state = 2003
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,298,self._ctx)

                self.state = 2004
                self.controlStructureBody()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2006
                self.match(KotlinParser.WHILE)
                self.state = 2010
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2007
                    self.match(KotlinParser.NL)
                    self.state = 2012
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2013
                self.match(KotlinParser.LPAREN)
                self.state = 2014
                self.expression()
                self.state = 2015
                self.match(KotlinParser.RPAREN)
                self.state = 2019
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2016
                    self.match(KotlinParser.NL)
                    self.state = 2021
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2022
                self.match(KotlinParser.SEMICOLON)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DoWhileStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DO(self):
            return self.getToken(KotlinParser.DO, 0)

        def WHILE(self):
            return self.getToken(KotlinParser.WHILE, 0)

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def controlStructureBody(self):
            return self.getTypedRuleContext(KotlinParser.ControlStructureBodyContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_doWhileStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDoWhileStatement" ):
                listener.enterDoWhileStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDoWhileStatement" ):
                listener.exitDoWhileStatement(self)




    def doWhileStatement(self):

        localctx = KotlinParser.DoWhileStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 140, self.RULE_doWhileStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2026
            self.match(KotlinParser.DO)
            self.state = 2030
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,302,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2027
                    self.match(KotlinParser.NL) 
                self.state = 2032
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,302,self._ctx)

            self.state = 2034
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,303,self._ctx)
            if la_ == 1:
                self.state = 2033
                self.controlStructureBody()


            self.state = 2039
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2036
                self.match(KotlinParser.NL)
                self.state = 2041
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2042
            self.match(KotlinParser.WHILE)
            self.state = 2046
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2043
                self.match(KotlinParser.NL)
                self.state = 2048
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2049
            self.match(KotlinParser.LPAREN)
            self.state = 2050
            self.expression()
            self.state = 2051
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignmentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def directlyAssignableExpression(self):
            return self.getTypedRuleContext(KotlinParser.DirectlyAssignableExpressionContext,0)


        def ASSIGNMENT(self):
            return self.getToken(KotlinParser.ASSIGNMENT, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def assignableExpression(self):
            return self.getTypedRuleContext(KotlinParser.AssignableExpressionContext,0)


        def assignmentAndOperator(self):
            return self.getTypedRuleContext(KotlinParser.AssignmentAndOperatorContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_assignment

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignment" ):
                listener.enterAssignment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignment" ):
                listener.exitAssignment(self)




    def assignment(self):

        localctx = KotlinParser.AssignmentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 142, self.RULE_assignment)
        try:
            self.state = 2073
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,308,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2053
                self.directlyAssignableExpression()
                self.state = 2054
                self.match(KotlinParser.ASSIGNMENT)
                self.state = 2058
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,306,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2055
                        self.match(KotlinParser.NL) 
                    self.state = 2060
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,306,self._ctx)

                self.state = 2061
                self.expression()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2063
                self.assignableExpression()
                self.state = 2064
                self.assignmentAndOperator()
                self.state = 2068
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,307,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2065
                        self.match(KotlinParser.NL) 
                    self.state = 2070
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,307,self._ctx)

                self.state = 2071
                self.expression()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SemiContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SEMICOLON(self):
            return self.getToken(KotlinParser.SEMICOLON, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def EOF(self):
            return self.getToken(KotlinParser.EOF, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_semi

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSemi" ):
                listener.enterSemi(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSemi" ):
                listener.exitSemi(self)




    def semi(self):

        localctx = KotlinParser.SemiContext(self, self._ctx, self.state)
        self.enterRule(localctx, 144, self.RULE_semi)
        self._la = 0 # Token type
        try:
            self.state = 2083
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.NL, KotlinParser.SEMICOLON]:
                self.enterOuterAlt(localctx, 1)
                self.state = 2075
                _la = self._input.LA(1)
                if not(_la==KotlinParser.NL or _la==KotlinParser.SEMICOLON):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 2079
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,309,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2076
                        self.match(KotlinParser.NL) 
                    self.state = 2081
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,309,self._ctx)

                pass
            elif token in [KotlinParser.EOF]:
                self.enterOuterAlt(localctx, 2)
                self.state = 2082
                self.match(KotlinParser.EOF)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SemisContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SEMICOLON(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.SEMICOLON)
            else:
                return self.getToken(KotlinParser.SEMICOLON, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def EOF(self):
            return self.getToken(KotlinParser.EOF, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_semis

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSemis" ):
                listener.enterSemis(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSemis" ):
                listener.exitSemis(self)




    def semis(self):

        localctx = KotlinParser.SemisContext(self, self._ctx, self.state)
        self.enterRule(localctx, 146, self.RULE_semis)
        self._la = 0 # Token type
        try:
            self.state = 2091
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.NL, KotlinParser.SEMICOLON]:
                self.enterOuterAlt(localctx, 1)
                self.state = 2086 
                self._errHandler.sync(self)
                _alt = 1
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1:
                        self.state = 2085
                        _la = self._input.LA(1)
                        if not(_la==KotlinParser.NL or _la==KotlinParser.SEMICOLON):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()

                    else:
                        raise NoViableAltException(self)
                    self.state = 2088 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,311,self._ctx)

                pass
            elif token in [KotlinParser.EOF]:
                self.enterOuterAlt(localctx, 2)
                self.state = 2090
                self.match(KotlinParser.EOF)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def disjunction(self):
            return self.getTypedRuleContext(KotlinParser.DisjunctionContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)




    def expression(self):

        localctx = KotlinParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 148, self.RULE_expression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2093
            self.disjunction()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DisjunctionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def conjunction(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ConjunctionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ConjunctionContext,i)


        def DISJ(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.DISJ)
            else:
                return self.getToken(KotlinParser.DISJ, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_disjunction

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDisjunction" ):
                listener.enterDisjunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDisjunction" ):
                listener.exitDisjunction(self)




    def disjunction(self):

        localctx = KotlinParser.DisjunctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 150, self.RULE_disjunction)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2095
            self.conjunction()
            self.state = 2112
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,315,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2099
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2096
                        self.match(KotlinParser.NL)
                        self.state = 2101
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2102
                    self.match(KotlinParser.DISJ)
                    self.state = 2106
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,314,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2103
                            self.match(KotlinParser.NL) 
                        self.state = 2108
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,314,self._ctx)

                    self.state = 2109
                    self.conjunction() 
                self.state = 2114
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,315,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConjunctionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equality(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.EqualityContext)
            else:
                return self.getTypedRuleContext(KotlinParser.EqualityContext,i)


        def CONJ(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.CONJ)
            else:
                return self.getToken(KotlinParser.CONJ, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_conjunction

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConjunction" ):
                listener.enterConjunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConjunction" ):
                listener.exitConjunction(self)




    def conjunction(self):

        localctx = KotlinParser.ConjunctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 152, self.RULE_conjunction)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2115
            self.equality()
            self.state = 2132
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,318,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2119
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2116
                        self.match(KotlinParser.NL)
                        self.state = 2121
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2122
                    self.match(KotlinParser.CONJ)
                    self.state = 2126
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,317,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2123
                            self.match(KotlinParser.NL) 
                        self.state = 2128
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,317,self._ctx)

                    self.state = 2129
                    self.equality() 
                self.state = 2134
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,318,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EqualityContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def comparison(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ComparisonContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ComparisonContext,i)


        def equalityOperator(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.EqualityOperatorContext)
            else:
                return self.getTypedRuleContext(KotlinParser.EqualityOperatorContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_equality

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEquality" ):
                listener.enterEquality(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEquality" ):
                listener.exitEquality(self)




    def equality(self):

        localctx = KotlinParser.EqualityContext(self, self._ctx, self.state)
        self.enterRule(localctx, 154, self.RULE_equality)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2135
            self.comparison()
            self.state = 2147
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,320,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2136
                    self.equalityOperator()
                    self.state = 2140
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,319,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2137
                            self.match(KotlinParser.NL) 
                        self.state = 2142
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,319,self._ctx)

                    self.state = 2143
                    self.comparison() 
                self.state = 2149
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,320,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ComparisonContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def infixOperation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.InfixOperationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.InfixOperationContext,i)


        def comparisonOperator(self):
            return self.getTypedRuleContext(KotlinParser.ComparisonOperatorContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_comparison

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComparison" ):
                listener.enterComparison(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComparison" ):
                listener.exitComparison(self)




    def comparison(self):

        localctx = KotlinParser.ComparisonContext(self, self._ctx, self.state)
        self.enterRule(localctx, 156, self.RULE_comparison)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2150
            self.infixOperation()
            self.state = 2160
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,322,self._ctx)
            if la_ == 1:
                self.state = 2151
                self.comparisonOperator()
                self.state = 2155
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,321,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2152
                        self.match(KotlinParser.NL) 
                    self.state = 2157
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,321,self._ctx)

                self.state = 2158
                self.infixOperation()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InfixOperationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def elvisExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ElvisExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ElvisExpressionContext,i)


        def inOperator(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.InOperatorContext)
            else:
                return self.getTypedRuleContext(KotlinParser.InOperatorContext,i)


        def isOperator(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.IsOperatorContext)
            else:
                return self.getTypedRuleContext(KotlinParser.IsOperatorContext,i)


        def type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_infixOperation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfixOperation" ):
                listener.enterInfixOperation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfixOperation" ):
                listener.exitInfixOperation(self)




    def infixOperation(self):

        localctx = KotlinParser.InfixOperationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 158, self.RULE_infixOperation)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2162
            self.elvisExpression()
            self.state = 2183
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,326,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2181
                    self._errHandler.sync(self)
                    token = self._input.LA(1)
                    if token in [KotlinParser.IN, KotlinParser.NOT_IN]:
                        self.state = 2163
                        self.inOperator()
                        self.state = 2167
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,323,self._ctx)
                        while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                            if _alt==1:
                                self.state = 2164
                                self.match(KotlinParser.NL) 
                            self.state = 2169
                            self._errHandler.sync(self)
                            _alt = self._interp.adaptivePredict(self._input,323,self._ctx)

                        self.state = 2170
                        self.elvisExpression()
                        pass
                    elif token in [KotlinParser.IS, KotlinParser.NOT_IS]:
                        self.state = 2172
                        self.isOperator()
                        self.state = 2176
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 2173
                            self.match(KotlinParser.NL)
                            self.state = 2178
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 2179
                        self.type()
                        pass
                    else:
                        raise NoViableAltException(self)
             
                self.state = 2185
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,326,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ElvisExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def infixFunctionCall(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.InfixFunctionCallContext)
            else:
                return self.getTypedRuleContext(KotlinParser.InfixFunctionCallContext,i)


        def elvis(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ElvisContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ElvisContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_elvisExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterElvisExpression" ):
                listener.enterElvisExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitElvisExpression" ):
                listener.exitElvisExpression(self)




    def elvisExpression(self):

        localctx = KotlinParser.ElvisExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 160, self.RULE_elvisExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2186
            self.infixFunctionCall()
            self.state = 2204
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,329,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2190
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2187
                        self.match(KotlinParser.NL)
                        self.state = 2192
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2193
                    self.elvis()
                    self.state = 2197
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,328,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2194
                            self.match(KotlinParser.NL) 
                        self.state = 2199
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,328,self._ctx)

                    self.state = 2200
                    self.infixFunctionCall() 
                self.state = 2206
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,329,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ElvisContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def QUEST_NO_WS(self):
            return self.getToken(KotlinParser.QUEST_NO_WS, 0)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_elvis

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterElvis" ):
                listener.enterElvis(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitElvis" ):
                listener.exitElvis(self)




    def elvis(self):

        localctx = KotlinParser.ElvisContext(self, self._ctx, self.state)
        self.enterRule(localctx, 162, self.RULE_elvis)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2207
            self.match(KotlinParser.QUEST_NO_WS)
            self.state = 2208
            self.match(KotlinParser.COLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InfixFunctionCallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def rangeExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.RangeExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.RangeExpressionContext,i)


        def simpleIdentifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.SimpleIdentifierContext)
            else:
                return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_infixFunctionCall

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfixFunctionCall" ):
                listener.enterInfixFunctionCall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfixFunctionCall" ):
                listener.exitInfixFunctionCall(self)




    def infixFunctionCall(self):

        localctx = KotlinParser.InfixFunctionCallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 164, self.RULE_infixFunctionCall)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2210
            self.rangeExpression()
            self.state = 2222
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,331,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2211
                    self.simpleIdentifier()
                    self.state = 2215
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,330,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2212
                            self.match(KotlinParser.NL) 
                        self.state = 2217
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,330,self._ctx)

                    self.state = 2218
                    self.rangeExpression() 
                self.state = 2224
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,331,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RangeExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def additiveExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AdditiveExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AdditiveExpressionContext,i)


        def RANGE(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.RANGE)
            else:
                return self.getToken(KotlinParser.RANGE, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_rangeExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRangeExpression" ):
                listener.enterRangeExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRangeExpression" ):
                listener.exitRangeExpression(self)




    def rangeExpression(self):

        localctx = KotlinParser.RangeExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 166, self.RULE_rangeExpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2225
            self.additiveExpression()
            self.state = 2236
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,333,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2226
                    self.match(KotlinParser.RANGE)
                    self.state = 2230
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,332,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2227
                            self.match(KotlinParser.NL) 
                        self.state = 2232
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,332,self._ctx)

                    self.state = 2233
                    self.additiveExpression() 
                self.state = 2238
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,333,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AdditiveExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def multiplicativeExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.MultiplicativeExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.MultiplicativeExpressionContext,i)


        def additiveOperator(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AdditiveOperatorContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AdditiveOperatorContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_additiveExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAdditiveExpression" ):
                listener.enterAdditiveExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAdditiveExpression" ):
                listener.exitAdditiveExpression(self)




    def additiveExpression(self):

        localctx = KotlinParser.AdditiveExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 168, self.RULE_additiveExpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2239
            self.multiplicativeExpression()
            self.state = 2251
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,335,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2240
                    self.additiveOperator()
                    self.state = 2244
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,334,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2241
                            self.match(KotlinParser.NL) 
                        self.state = 2246
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,334,self._ctx)

                    self.state = 2247
                    self.multiplicativeExpression() 
                self.state = 2253
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,335,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultiplicativeExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def asExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AsExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AsExpressionContext,i)


        def multiplicativeOperator(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.MultiplicativeOperatorContext)
            else:
                return self.getTypedRuleContext(KotlinParser.MultiplicativeOperatorContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_multiplicativeExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultiplicativeExpression" ):
                listener.enterMultiplicativeExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultiplicativeExpression" ):
                listener.exitMultiplicativeExpression(self)




    def multiplicativeExpression(self):

        localctx = KotlinParser.MultiplicativeExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 170, self.RULE_multiplicativeExpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2254
            self.asExpression()
            self.state = 2266
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,337,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2255
                    self.multiplicativeOperator()
                    self.state = 2259
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,336,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2256
                            self.match(KotlinParser.NL) 
                        self.state = 2261
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,336,self._ctx)

                    self.state = 2262
                    self.asExpression() 
                self.state = 2268
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,337,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AsExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def comparisonWithLiteralRightSide(self):
            return self.getTypedRuleContext(KotlinParser.ComparisonWithLiteralRightSideContext,0)


        def asOperator(self):
            return self.getTypedRuleContext(KotlinParser.AsOperatorContext,0)


        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_asExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAsExpression" ):
                listener.enterAsExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAsExpression" ):
                listener.exitAsExpression(self)




    def asExpression(self):

        localctx = KotlinParser.AsExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 172, self.RULE_asExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2269
            self.comparisonWithLiteralRightSide()
            self.state = 2285
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,340,self._ctx)
            if la_ == 1:
                self.state = 2273
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2270
                    self.match(KotlinParser.NL)
                    self.state = 2275
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2276
                self.asOperator()
                self.state = 2280
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2277
                    self.match(KotlinParser.NL)
                    self.state = 2282
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2283
                self.type()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ComparisonWithLiteralRightSideContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def prefixUnaryExpression(self):
            return self.getTypedRuleContext(KotlinParser.PrefixUnaryExpressionContext,0)


        def LANGLE(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.LANGLE)
            else:
                return self.getToken(KotlinParser.LANGLE, i)

        def literalConstant(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.LiteralConstantContext)
            else:
                return self.getTypedRuleContext(KotlinParser.LiteralConstantContext,i)


        def RANGLE(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.RANGLE)
            else:
                return self.getToken(KotlinParser.RANGLE, i)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ExpressionContext,i)


        def parenthesizedExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ParenthesizedExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ParenthesizedExpressionContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_comparisonWithLiteralRightSide

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComparisonWithLiteralRightSide" ):
                listener.enterComparisonWithLiteralRightSide(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComparisonWithLiteralRightSide" ):
                listener.exitComparisonWithLiteralRightSide(self)




    def comparisonWithLiteralRightSide(self):

        localctx = KotlinParser.ComparisonWithLiteralRightSideContext(self, self._ctx, self.state)
        self.enterRule(localctx, 174, self.RULE_comparisonWithLiteralRightSide)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2287
            self.prefixUnaryExpression()
            self.state = 2321
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,346,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2291
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2288
                        self.match(KotlinParser.NL)
                        self.state = 2293
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2294
                    self.match(KotlinParser.LANGLE)
                    self.state = 2298
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2295
                        self.match(KotlinParser.NL)
                        self.state = 2300
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2301
                    self.literalConstant()
                    self.state = 2305
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2302
                        self.match(KotlinParser.NL)
                        self.state = 2307
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2308
                    self.match(KotlinParser.RANGLE)
                    self.state = 2312
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,344,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2309
                            self.match(KotlinParser.NL) 
                        self.state = 2314
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,344,self._ctx)

                    self.state = 2317
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,345,self._ctx)
                    if la_ == 1:
                        self.state = 2315
                        self.expression()
                        pass

                    elif la_ == 2:
                        self.state = 2316
                        self.parenthesizedExpression()
                        pass

             
                self.state = 2323
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,346,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrefixUnaryExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def postfixUnaryExpression(self):
            return self.getTypedRuleContext(KotlinParser.PostfixUnaryExpressionContext,0)


        def unaryPrefix(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.UnaryPrefixContext)
            else:
                return self.getTypedRuleContext(KotlinParser.UnaryPrefixContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_prefixUnaryExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrefixUnaryExpression" ):
                listener.enterPrefixUnaryExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrefixUnaryExpression" ):
                listener.exitPrefixUnaryExpression(self)




    def prefixUnaryExpression(self):

        localctx = KotlinParser.PrefixUnaryExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 176, self.RULE_prefixUnaryExpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2327
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,347,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2324
                    self.unaryPrefix() 
                self.state = 2329
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,347,self._ctx)

            self.state = 2330
            self.postfixUnaryExpression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnaryPrefixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotation(self):
            return self.getTypedRuleContext(KotlinParser.AnnotationContext,0)


        def label(self):
            return self.getTypedRuleContext(KotlinParser.LabelContext,0)


        def prefixUnaryOperator(self):
            return self.getTypedRuleContext(KotlinParser.PrefixUnaryOperatorContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_unaryPrefix

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnaryPrefix" ):
                listener.enterUnaryPrefix(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnaryPrefix" ):
                listener.exitUnaryPrefix(self)




    def unaryPrefix(self):

        localctx = KotlinParser.UnaryPrefixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 178, self.RULE_unaryPrefix)
        try:
            self.state = 2341
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS]:
                self.enterOuterAlt(localctx, 1)
                self.state = 2332
                self.annotation()
                pass
            elif token in [KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.enterOuterAlt(localctx, 2)
                self.state = 2333
                self.label()
                pass
            elif token in [KotlinParser.ADD, KotlinParser.SUB, KotlinParser.INCR, KotlinParser.DECR, KotlinParser.EXCL_WS, KotlinParser.EXCL_NO_WS]:
                self.enterOuterAlt(localctx, 3)
                self.state = 2334
                self.prefixUnaryOperator()
                self.state = 2338
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,348,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2335
                        self.match(KotlinParser.NL) 
                    self.state = 2340
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,348,self._ctx)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PostfixUnaryExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primaryExpression(self):
            return self.getTypedRuleContext(KotlinParser.PrimaryExpressionContext,0)


        def postfixUnarySuffix(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.PostfixUnarySuffixContext)
            else:
                return self.getTypedRuleContext(KotlinParser.PostfixUnarySuffixContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_postfixUnaryExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPostfixUnaryExpression" ):
                listener.enterPostfixUnaryExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPostfixUnaryExpression" ):
                listener.exitPostfixUnaryExpression(self)




    def postfixUnaryExpression(self):

        localctx = KotlinParser.PostfixUnaryExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 180, self.RULE_postfixUnaryExpression)
        try:
            self.state = 2350
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,351,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2343
                self.primaryExpression()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2344
                self.primaryExpression()
                self.state = 2346 
                self._errHandler.sync(self)
                _alt = 1
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1:
                        self.state = 2345
                        self.postfixUnarySuffix()

                    else:
                        raise NoViableAltException(self)
                    self.state = 2348 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,350,self._ctx)

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PostfixUnarySuffixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def postfixUnaryOperator(self):
            return self.getTypedRuleContext(KotlinParser.PostfixUnaryOperatorContext,0)


        def typeArguments(self):
            return self.getTypedRuleContext(KotlinParser.TypeArgumentsContext,0)


        def callSuffix(self):
            return self.getTypedRuleContext(KotlinParser.CallSuffixContext,0)


        def indexingSuffix(self):
            return self.getTypedRuleContext(KotlinParser.IndexingSuffixContext,0)


        def navigationSuffix(self):
            return self.getTypedRuleContext(KotlinParser.NavigationSuffixContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_postfixUnarySuffix

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPostfixUnarySuffix" ):
                listener.enterPostfixUnarySuffix(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPostfixUnarySuffix" ):
                listener.exitPostfixUnarySuffix(self)




    def postfixUnarySuffix(self):

        localctx = KotlinParser.PostfixUnarySuffixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 182, self.RULE_postfixUnarySuffix)
        try:
            self.state = 2357
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,352,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2352
                self.postfixUnaryOperator()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2353
                self.typeArguments()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 2354
                self.callSuffix()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 2355
                self.indexingSuffix()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 2356
                self.navigationSuffix()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DirectlyAssignableExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def postfixUnaryExpression(self):
            return self.getTypedRuleContext(KotlinParser.PostfixUnaryExpressionContext,0)


        def assignableSuffix(self):
            return self.getTypedRuleContext(KotlinParser.AssignableSuffixContext,0)


        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def parenthesizedDirectlyAssignableExpression(self):
            return self.getTypedRuleContext(KotlinParser.ParenthesizedDirectlyAssignableExpressionContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_directlyAssignableExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDirectlyAssignableExpression" ):
                listener.enterDirectlyAssignableExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDirectlyAssignableExpression" ):
                listener.exitDirectlyAssignableExpression(self)




    def directlyAssignableExpression(self):

        localctx = KotlinParser.DirectlyAssignableExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 184, self.RULE_directlyAssignableExpression)
        try:
            self.state = 2364
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,353,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2359
                self.postfixUnaryExpression()
                self.state = 2360
                self.assignableSuffix()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2362
                self.simpleIdentifier()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 2363
                self.parenthesizedDirectlyAssignableExpression()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParenthesizedDirectlyAssignableExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def directlyAssignableExpression(self):
            return self.getTypedRuleContext(KotlinParser.DirectlyAssignableExpressionContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_parenthesizedDirectlyAssignableExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParenthesizedDirectlyAssignableExpression" ):
                listener.enterParenthesizedDirectlyAssignableExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParenthesizedDirectlyAssignableExpression" ):
                listener.exitParenthesizedDirectlyAssignableExpression(self)




    def parenthesizedDirectlyAssignableExpression(self):

        localctx = KotlinParser.ParenthesizedDirectlyAssignableExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 186, self.RULE_parenthesizedDirectlyAssignableExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2366
            self.match(KotlinParser.LPAREN)
            self.state = 2370
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,354,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2367
                    self.match(KotlinParser.NL) 
                self.state = 2372
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,354,self._ctx)

            self.state = 2373
            self.directlyAssignableExpression()
            self.state = 2377
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2374
                self.match(KotlinParser.NL)
                self.state = 2379
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2380
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignableExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def prefixUnaryExpression(self):
            return self.getTypedRuleContext(KotlinParser.PrefixUnaryExpressionContext,0)


        def parenthesizedAssignableExpression(self):
            return self.getTypedRuleContext(KotlinParser.ParenthesizedAssignableExpressionContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_assignableExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignableExpression" ):
                listener.enterAssignableExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignableExpression" ):
                listener.exitAssignableExpression(self)




    def assignableExpression(self):

        localctx = KotlinParser.AssignableExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 188, self.RULE_assignableExpression)
        try:
            self.state = 2384
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,356,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2382
                self.prefixUnaryExpression()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2383
                self.parenthesizedAssignableExpression()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParenthesizedAssignableExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def assignableExpression(self):
            return self.getTypedRuleContext(KotlinParser.AssignableExpressionContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_parenthesizedAssignableExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParenthesizedAssignableExpression" ):
                listener.enterParenthesizedAssignableExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParenthesizedAssignableExpression" ):
                listener.exitParenthesizedAssignableExpression(self)




    def parenthesizedAssignableExpression(self):

        localctx = KotlinParser.ParenthesizedAssignableExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 190, self.RULE_parenthesizedAssignableExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2386
            self.match(KotlinParser.LPAREN)
            self.state = 2390
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,357,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2387
                    self.match(KotlinParser.NL) 
                self.state = 2392
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,357,self._ctx)

            self.state = 2393
            self.assignableExpression()
            self.state = 2397
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2394
                self.match(KotlinParser.NL)
                self.state = 2399
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2400
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignableSuffixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typeArguments(self):
            return self.getTypedRuleContext(KotlinParser.TypeArgumentsContext,0)


        def indexingSuffix(self):
            return self.getTypedRuleContext(KotlinParser.IndexingSuffixContext,0)


        def navigationSuffix(self):
            return self.getTypedRuleContext(KotlinParser.NavigationSuffixContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_assignableSuffix

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignableSuffix" ):
                listener.enterAssignableSuffix(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignableSuffix" ):
                listener.exitAssignableSuffix(self)




    def assignableSuffix(self):

        localctx = KotlinParser.AssignableSuffixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 192, self.RULE_assignableSuffix)
        try:
            self.state = 2405
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.LANGLE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 2402
                self.typeArguments()
                pass
            elif token in [KotlinParser.LSQUARE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 2403
                self.indexingSuffix()
                pass
            elif token in [KotlinParser.NL, KotlinParser.DOT, KotlinParser.COLONCOLON, KotlinParser.QUEST_NO_WS]:
                self.enterOuterAlt(localctx, 3)
                self.state = 2404
                self.navigationSuffix()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IndexingSuffixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LSQUARE(self):
            return self.getToken(KotlinParser.LSQUARE, 0)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ExpressionContext,i)


        def RSQUARE(self):
            return self.getToken(KotlinParser.RSQUARE, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_indexingSuffix

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIndexingSuffix" ):
                listener.enterIndexingSuffix(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIndexingSuffix" ):
                listener.exitIndexingSuffix(self)




    def indexingSuffix(self):

        localctx = KotlinParser.IndexingSuffixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 194, self.RULE_indexingSuffix)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2407
            self.match(KotlinParser.LSQUARE)
            self.state = 2411
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,360,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2408
                    self.match(KotlinParser.NL) 
                self.state = 2413
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,360,self._ctx)

            self.state = 2414
            self.expression()
            self.state = 2431
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,363,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2418
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2415
                        self.match(KotlinParser.NL)
                        self.state = 2420
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2421
                    self.match(KotlinParser.COMMA)
                    self.state = 2425
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,362,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2422
                            self.match(KotlinParser.NL) 
                        self.state = 2427
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,362,self._ctx)

                    self.state = 2428
                    self.expression() 
                self.state = 2433
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,363,self._ctx)

            self.state = 2441
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,365,self._ctx)
            if la_ == 1:
                self.state = 2437
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2434
                    self.match(KotlinParser.NL)
                    self.state = 2439
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2440
                self.match(KotlinParser.COMMA)


            self.state = 2446
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2443
                self.match(KotlinParser.NL)
                self.state = 2448
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2449
            self.match(KotlinParser.RSQUARE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NavigationSuffixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def memberAccessOperator(self):
            return self.getTypedRuleContext(KotlinParser.MemberAccessOperatorContext,0)


        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def parenthesizedExpression(self):
            return self.getTypedRuleContext(KotlinParser.ParenthesizedExpressionContext,0)


        def CLASS(self):
            return self.getToken(KotlinParser.CLASS, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_navigationSuffix

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNavigationSuffix" ):
                listener.enterNavigationSuffix(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNavigationSuffix" ):
                listener.exitNavigationSuffix(self)




    def navigationSuffix(self):

        localctx = KotlinParser.NavigationSuffixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 196, self.RULE_navigationSuffix)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2454
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2451
                self.match(KotlinParser.NL)
                self.state = 2456
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2457
            self.memberAccessOperator()
            self.state = 2461
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2458
                self.match(KotlinParser.NL)
                self.state = 2463
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2467
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.state = 2464
                self.simpleIdentifier()
                pass
            elif token in [KotlinParser.LPAREN]:
                self.state = 2465
                self.parenthesizedExpression()
                pass
            elif token in [KotlinParser.CLASS]:
                self.state = 2466
                self.match(KotlinParser.CLASS)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CallSuffixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotatedLambda(self):
            return self.getTypedRuleContext(KotlinParser.AnnotatedLambdaContext,0)


        def typeArguments(self):
            return self.getTypedRuleContext(KotlinParser.TypeArgumentsContext,0)


        def valueArguments(self):
            return self.getTypedRuleContext(KotlinParser.ValueArgumentsContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_callSuffix

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCallSuffix" ):
                listener.enterCallSuffix(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCallSuffix" ):
                listener.exitCallSuffix(self)




    def callSuffix(self):

        localctx = KotlinParser.CallSuffixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 198, self.RULE_callSuffix)
        self._la = 0 # Token type
        try:
            self.state = 2480
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,373,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2470
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.LANGLE:
                    self.state = 2469
                    self.typeArguments()


                self.state = 2473
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.LPAREN:
                    self.state = 2472
                    self.valueArguments()


                self.state = 2475
                self.annotatedLambda()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2477
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.LANGLE:
                    self.state = 2476
                    self.typeArguments()


                self.state = 2479
                self.valueArguments()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnnotatedLambdaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def lambdaLiteral(self):
            return self.getTypedRuleContext(KotlinParser.LambdaLiteralContext,0)


        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def label(self):
            return self.getTypedRuleContext(KotlinParser.LabelContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_annotatedLambda

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnnotatedLambda" ):
                listener.enterAnnotatedLambda(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnnotatedLambda" ):
                listener.exitAnnotatedLambda(self)




    def annotatedLambda(self):

        localctx = KotlinParser.AnnotatedLambdaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 200, self.RULE_annotatedLambda)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2485
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS:
                self.state = 2482
                self.annotation()
                self.state = 2487
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2489
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if ((((_la - 61)) & ~0x3f) == 0 and ((1 << (_la - 61)) & ((1 << (KotlinParser.FILE - 61)) | (1 << (KotlinParser.FIELD - 61)) | (1 << (KotlinParser.PROPERTY - 61)) | (1 << (KotlinParser.GET - 61)) | (1 << (KotlinParser.SET - 61)) | (1 << (KotlinParser.RECEIVER - 61)) | (1 << (KotlinParser.PARAM - 61)) | (1 << (KotlinParser.SETPARAM - 61)) | (1 << (KotlinParser.DELEGATE - 61)) | (1 << (KotlinParser.IMPORT - 61)) | (1 << (KotlinParser.CONSTRUCTOR - 61)) | (1 << (KotlinParser.BY - 61)) | (1 << (KotlinParser.COMPANION - 61)) | (1 << (KotlinParser.INIT - 61)) | (1 << (KotlinParser.WHERE - 61)) | (1 << (KotlinParser.CATCH - 61)) | (1 << (KotlinParser.FINALLY - 61)) | (1 << (KotlinParser.OUT - 61)) | (1 << (KotlinParser.DYNAMIC - 61)) | (1 << (KotlinParser.PUBLIC - 61)) | (1 << (KotlinParser.PRIVATE - 61)) | (1 << (KotlinParser.PROTECTED - 61)) | (1 << (KotlinParser.INTERNAL - 61)) | (1 << (KotlinParser.ENUM - 61)) | (1 << (KotlinParser.SEALED - 61)) | (1 << (KotlinParser.ANNOTATION - 61)) | (1 << (KotlinParser.DATA - 61)) | (1 << (KotlinParser.INNER - 61)) | (1 << (KotlinParser.TAILREC - 61)) | (1 << (KotlinParser.OPERATOR - 61)) | (1 << (KotlinParser.INLINE - 61)) | (1 << (KotlinParser.INFIX - 61)) | (1 << (KotlinParser.EXTERNAL - 61)) | (1 << (KotlinParser.SUSPEND - 61)) | (1 << (KotlinParser.OVERRIDE - 61)) | (1 << (KotlinParser.ABSTRACT - 61)) | (1 << (KotlinParser.FINAL - 61)))) != 0) or ((((_la - 125)) & ~0x3f) == 0 and ((1 << (_la - 125)) & ((1 << (KotlinParser.OPEN - 125)) | (1 << (KotlinParser.CONST - 125)) | (1 << (KotlinParser.LATEINIT - 125)) | (1 << (KotlinParser.VARARG - 125)) | (1 << (KotlinParser.NOINLINE - 125)) | (1 << (KotlinParser.CROSSINLINE - 125)) | (1 << (KotlinParser.REIFIED - 125)) | (1 << (KotlinParser.EXPECT - 125)) | (1 << (KotlinParser.ACTUAL - 125)) | (1 << (KotlinParser.Identifier - 125)))) != 0):
                self.state = 2488
                self.label()


            self.state = 2494
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2491
                self.match(KotlinParser.NL)
                self.state = 2496
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2497
            self.lambdaLiteral()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeArgumentsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LANGLE(self):
            return self.getToken(KotlinParser.LANGLE, 0)

        def typeProjection(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeProjectionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeProjectionContext,i)


        def RANGLE(self):
            return self.getToken(KotlinParser.RANGLE, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_typeArguments

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeArguments" ):
                listener.enterTypeArguments(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeArguments" ):
                listener.exitTypeArguments(self)




    def typeArguments(self):

        localctx = KotlinParser.TypeArgumentsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 202, self.RULE_typeArguments)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2499
            self.match(KotlinParser.LANGLE)
            self.state = 2503
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2500
                self.match(KotlinParser.NL)
                self.state = 2505
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2506
            self.typeProjection()
            self.state = 2523
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,380,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2510
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2507
                        self.match(KotlinParser.NL)
                        self.state = 2512
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2513
                    self.match(KotlinParser.COMMA)
                    self.state = 2517
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2514
                        self.match(KotlinParser.NL)
                        self.state = 2519
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2520
                    self.typeProjection() 
                self.state = 2525
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,380,self._ctx)

            self.state = 2533
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,382,self._ctx)
            if la_ == 1:
                self.state = 2529
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2526
                    self.match(KotlinParser.NL)
                    self.state = 2531
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2532
                self.match(KotlinParser.COMMA)


            self.state = 2538
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2535
                self.match(KotlinParser.NL)
                self.state = 2540
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2541
            self.match(KotlinParser.RANGLE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ValueArgumentsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def valueArgument(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ValueArgumentContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ValueArgumentContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_valueArguments

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterValueArguments" ):
                listener.enterValueArguments(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitValueArguments" ):
                listener.exitValueArguments(self)




    def valueArguments(self):

        localctx = KotlinParser.ValueArgumentsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 204, self.RULE_valueArguments)
        self._la = 0 # Token type
        try:
            self.state = 2595
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,392,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2543
                self.match(KotlinParser.LPAREN)
                self.state = 2547
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2544
                    self.match(KotlinParser.NL)
                    self.state = 2549
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2550
                self.match(KotlinParser.RPAREN)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2551
                self.match(KotlinParser.LPAREN)
                self.state = 2555
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,385,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2552
                        self.match(KotlinParser.NL) 
                    self.state = 2557
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,385,self._ctx)

                self.state = 2558
                self.valueArgument()
                self.state = 2575
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,388,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2562
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 2559
                            self.match(KotlinParser.NL)
                            self.state = 2564
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 2565
                        self.match(KotlinParser.COMMA)
                        self.state = 2569
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,387,self._ctx)
                        while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                            if _alt==1:
                                self.state = 2566
                                self.match(KotlinParser.NL) 
                            self.state = 2571
                            self._errHandler.sync(self)
                            _alt = self._interp.adaptivePredict(self._input,387,self._ctx)

                        self.state = 2572
                        self.valueArgument() 
                    self.state = 2577
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,388,self._ctx)

                self.state = 2585
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,390,self._ctx)
                if la_ == 1:
                    self.state = 2581
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2578
                        self.match(KotlinParser.NL)
                        self.state = 2583
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2584
                    self.match(KotlinParser.COMMA)


                self.state = 2590
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2587
                    self.match(KotlinParser.NL)
                    self.state = 2592
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2593
                self.match(KotlinParser.RPAREN)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ValueArgumentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def annotation(self):
            return self.getTypedRuleContext(KotlinParser.AnnotationContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def ASSIGNMENT(self):
            return self.getToken(KotlinParser.ASSIGNMENT, 0)

        def MULT(self):
            return self.getToken(KotlinParser.MULT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_valueArgument

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterValueArgument" ):
                listener.enterValueArgument(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitValueArgument" ):
                listener.exitValueArgument(self)




    def valueArgument(self):

        localctx = KotlinParser.ValueArgumentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 206, self.RULE_valueArgument)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2598
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,393,self._ctx)
            if la_ == 1:
                self.state = 2597
                self.annotation()


            self.state = 2603
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,394,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2600
                    self.match(KotlinParser.NL) 
                self.state = 2605
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,394,self._ctx)

            self.state = 2620
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,397,self._ctx)
            if la_ == 1:
                self.state = 2606
                self.simpleIdentifier()
                self.state = 2610
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2607
                    self.match(KotlinParser.NL)
                    self.state = 2612
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2613
                self.match(KotlinParser.ASSIGNMENT)
                self.state = 2617
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,396,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2614
                        self.match(KotlinParser.NL) 
                    self.state = 2619
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,396,self._ctx)



            self.state = 2623
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.MULT:
                self.state = 2622
                self.match(KotlinParser.MULT)


            self.state = 2628
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,399,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2625
                    self.match(KotlinParser.NL) 
                self.state = 2630
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,399,self._ctx)

            self.state = 2631
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrimaryExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parenthesizedExpression(self):
            return self.getTypedRuleContext(KotlinParser.ParenthesizedExpressionContext,0)


        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def literalConstant(self):
            return self.getTypedRuleContext(KotlinParser.LiteralConstantContext,0)


        def stringLiteral(self):
            return self.getTypedRuleContext(KotlinParser.StringLiteralContext,0)


        def callableReference(self):
            return self.getTypedRuleContext(KotlinParser.CallableReferenceContext,0)


        def functionLiteral(self):
            return self.getTypedRuleContext(KotlinParser.FunctionLiteralContext,0)


        def objectLiteral(self):
            return self.getTypedRuleContext(KotlinParser.ObjectLiteralContext,0)


        def collectionLiteral(self):
            return self.getTypedRuleContext(KotlinParser.CollectionLiteralContext,0)


        def thisExpression(self):
            return self.getTypedRuleContext(KotlinParser.ThisExpressionContext,0)


        def superExpression(self):
            return self.getTypedRuleContext(KotlinParser.SuperExpressionContext,0)


        def ifExpression(self):
            return self.getTypedRuleContext(KotlinParser.IfExpressionContext,0)


        def whenExpression(self):
            return self.getTypedRuleContext(KotlinParser.WhenExpressionContext,0)


        def tryExpression(self):
            return self.getTypedRuleContext(KotlinParser.TryExpressionContext,0)


        def jumpExpression(self):
            return self.getTypedRuleContext(KotlinParser.JumpExpressionContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_primaryExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimaryExpression" ):
                listener.enterPrimaryExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimaryExpression" ):
                listener.exitPrimaryExpression(self)




    def primaryExpression(self):

        localctx = KotlinParser.PrimaryExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 208, self.RULE_primaryExpression)
        try:
            self.state = 2647
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,400,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2633
                self.parenthesizedExpression()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2634
                self.simpleIdentifier()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 2635
                self.literalConstant()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 2636
                self.stringLiteral()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 2637
                self.callableReference()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 2638
                self.functionLiteral()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 2639
                self.objectLiteral()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 2640
                self.collectionLiteral()
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 2641
                self.thisExpression()
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 2642
                self.superExpression()
                pass

            elif la_ == 11:
                self.enterOuterAlt(localctx, 11)
                self.state = 2643
                self.ifExpression()
                pass

            elif la_ == 12:
                self.enterOuterAlt(localctx, 12)
                self.state = 2644
                self.whenExpression()
                pass

            elif la_ == 13:
                self.enterOuterAlt(localctx, 13)
                self.state = 2645
                self.tryExpression()
                pass

            elif la_ == 14:
                self.enterOuterAlt(localctx, 14)
                self.state = 2646
                self.jumpExpression()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParenthesizedExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_parenthesizedExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParenthesizedExpression" ):
                listener.enterParenthesizedExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParenthesizedExpression" ):
                listener.exitParenthesizedExpression(self)




    def parenthesizedExpression(self):

        localctx = KotlinParser.ParenthesizedExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 210, self.RULE_parenthesizedExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2649
            self.match(KotlinParser.LPAREN)
            self.state = 2653
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,401,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2650
                    self.match(KotlinParser.NL) 
                self.state = 2655
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,401,self._ctx)

            self.state = 2656
            self.expression()
            self.state = 2660
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2657
                self.match(KotlinParser.NL)
                self.state = 2662
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2663
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CollectionLiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LSQUARE(self):
            return self.getToken(KotlinParser.LSQUARE, 0)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ExpressionContext,i)


        def RSQUARE(self):
            return self.getToken(KotlinParser.RSQUARE, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_collectionLiteral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCollectionLiteral" ):
                listener.enterCollectionLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCollectionLiteral" ):
                listener.exitCollectionLiteral(self)




    def collectionLiteral(self):

        localctx = KotlinParser.CollectionLiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 212, self.RULE_collectionLiteral)
        self._la = 0 # Token type
        try:
            self.state = 2717
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,411,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2665
                self.match(KotlinParser.LSQUARE)
                self.state = 2669
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,403,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2666
                        self.match(KotlinParser.NL) 
                    self.state = 2671
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,403,self._ctx)

                self.state = 2672
                self.expression()
                self.state = 2689
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,406,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2676
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 2673
                            self.match(KotlinParser.NL)
                            self.state = 2678
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 2679
                        self.match(KotlinParser.COMMA)
                        self.state = 2683
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,405,self._ctx)
                        while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                            if _alt==1:
                                self.state = 2680
                                self.match(KotlinParser.NL) 
                            self.state = 2685
                            self._errHandler.sync(self)
                            _alt = self._interp.adaptivePredict(self._input,405,self._ctx)

                        self.state = 2686
                        self.expression() 
                    self.state = 2691
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,406,self._ctx)

                self.state = 2699
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,408,self._ctx)
                if la_ == 1:
                    self.state = 2695
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2692
                        self.match(KotlinParser.NL)
                        self.state = 2697
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2698
                    self.match(KotlinParser.COMMA)


                self.state = 2704
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2701
                    self.match(KotlinParser.NL)
                    self.state = 2706
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2707
                self.match(KotlinParser.RSQUARE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2709
                self.match(KotlinParser.LSQUARE)
                self.state = 2713
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2710
                    self.match(KotlinParser.NL)
                    self.state = 2715
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2716
                self.match(KotlinParser.RSQUARE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LiteralConstantContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BooleanLiteral(self):
            return self.getToken(KotlinParser.BooleanLiteral, 0)

        def IntegerLiteral(self):
            return self.getToken(KotlinParser.IntegerLiteral, 0)

        def HexLiteral(self):
            return self.getToken(KotlinParser.HexLiteral, 0)

        def BinLiteral(self):
            return self.getToken(KotlinParser.BinLiteral, 0)

        def CharacterLiteral(self):
            return self.getToken(KotlinParser.CharacterLiteral, 0)

        def RealLiteral(self):
            return self.getToken(KotlinParser.RealLiteral, 0)

        def NullLiteral(self):
            return self.getToken(KotlinParser.NullLiteral, 0)

        def LongLiteral(self):
            return self.getToken(KotlinParser.LongLiteral, 0)

        def UnsignedLiteral(self):
            return self.getToken(KotlinParser.UnsignedLiteral, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_literalConstant

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLiteralConstant" ):
                listener.enterLiteralConstant(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLiteralConstant" ):
                listener.exitLiteralConstant(self)




    def literalConstant(self):

        localctx = KotlinParser.LiteralConstantContext(self, self._ctx, self.state)
        self.enterRule(localctx, 214, self.RULE_literalConstant)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2719
            _la = self._input.LA(1)
            if not(((((_la - 134)) & ~0x3f) == 0 and ((1 << (_la - 134)) & ((1 << (KotlinParser.RealLiteral - 134)) | (1 << (KotlinParser.IntegerLiteral - 134)) | (1 << (KotlinParser.HexLiteral - 134)) | (1 << (KotlinParser.BinLiteral - 134)) | (1 << (KotlinParser.UnsignedLiteral - 134)) | (1 << (KotlinParser.LongLiteral - 134)) | (1 << (KotlinParser.BooleanLiteral - 134)) | (1 << (KotlinParser.NullLiteral - 134)) | (1 << (KotlinParser.CharacterLiteral - 134)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StringLiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def lineStringLiteral(self):
            return self.getTypedRuleContext(KotlinParser.LineStringLiteralContext,0)


        def multiLineStringLiteral(self):
            return self.getTypedRuleContext(KotlinParser.MultiLineStringLiteralContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_stringLiteral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStringLiteral" ):
                listener.enterStringLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStringLiteral" ):
                listener.exitStringLiteral(self)




    def stringLiteral(self):

        localctx = KotlinParser.StringLiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 216, self.RULE_stringLiteral)
        try:
            self.state = 2723
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.QUOTE_OPEN]:
                self.enterOuterAlt(localctx, 1)
                self.state = 2721
                self.lineStringLiteral()
                pass
            elif token in [KotlinParser.TRIPLE_QUOTE_OPEN]:
                self.enterOuterAlt(localctx, 2)
                self.state = 2722
                self.multiLineStringLiteral()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LineStringLiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def QUOTE_OPEN(self):
            return self.getToken(KotlinParser.QUOTE_OPEN, 0)

        def QUOTE_CLOSE(self):
            return self.getToken(KotlinParser.QUOTE_CLOSE, 0)

        def lineStringContent(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.LineStringContentContext)
            else:
                return self.getTypedRuleContext(KotlinParser.LineStringContentContext,i)


        def lineStringExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.LineStringExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.LineStringExpressionContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_lineStringLiteral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLineStringLiteral" ):
                listener.enterLineStringLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLineStringLiteral" ):
                listener.exitLineStringLiteral(self)




    def lineStringLiteral(self):

        localctx = KotlinParser.LineStringLiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 218, self.RULE_lineStringLiteral)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2725
            self.match(KotlinParser.QUOTE_OPEN)
            self.state = 2730
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 158)) & ~0x3f) == 0 and ((1 << (_la - 158)) & ((1 << (KotlinParser.LineStrRef - 158)) | (1 << (KotlinParser.LineStrText - 158)) | (1 << (KotlinParser.LineStrEscapedChar - 158)) | (1 << (KotlinParser.LineStrExprStart - 158)))) != 0):
                self.state = 2728
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [KotlinParser.LineStrRef, KotlinParser.LineStrText, KotlinParser.LineStrEscapedChar]:
                    self.state = 2726
                    self.lineStringContent()
                    pass
                elif token in [KotlinParser.LineStrExprStart]:
                    self.state = 2727
                    self.lineStringExpression()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 2732
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2733
            self.match(KotlinParser.QUOTE_CLOSE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultiLineStringLiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TRIPLE_QUOTE_OPEN(self):
            return self.getToken(KotlinParser.TRIPLE_QUOTE_OPEN, 0)

        def TRIPLE_QUOTE_CLOSE(self):
            return self.getToken(KotlinParser.TRIPLE_QUOTE_CLOSE, 0)

        def multiLineStringContent(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.MultiLineStringContentContext)
            else:
                return self.getTypedRuleContext(KotlinParser.MultiLineStringContentContext,i)


        def multiLineStringExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.MultiLineStringExpressionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.MultiLineStringExpressionContext,i)


        def MultiLineStringQuote(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.MultiLineStringQuote)
            else:
                return self.getToken(KotlinParser.MultiLineStringQuote, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_multiLineStringLiteral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultiLineStringLiteral" ):
                listener.enterMultiLineStringLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultiLineStringLiteral" ):
                listener.exitMultiLineStringLiteral(self)




    def multiLineStringLiteral(self):

        localctx = KotlinParser.MultiLineStringLiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 220, self.RULE_multiLineStringLiteral)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2735
            self.match(KotlinParser.TRIPLE_QUOTE_OPEN)
            self.state = 2741
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 163)) & ~0x3f) == 0 and ((1 << (_la - 163)) & ((1 << (KotlinParser.MultiLineStringQuote - 163)) | (1 << (KotlinParser.MultiLineStrRef - 163)) | (1 << (KotlinParser.MultiLineStrText - 163)) | (1 << (KotlinParser.MultiLineStrExprStart - 163)))) != 0):
                self.state = 2739
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,415,self._ctx)
                if la_ == 1:
                    self.state = 2736
                    self.multiLineStringContent()
                    pass

                elif la_ == 2:
                    self.state = 2737
                    self.multiLineStringExpression()
                    pass

                elif la_ == 3:
                    self.state = 2738
                    self.match(KotlinParser.MultiLineStringQuote)
                    pass


                self.state = 2743
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2744
            self.match(KotlinParser.TRIPLE_QUOTE_CLOSE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LineStringContentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LineStrText(self):
            return self.getToken(KotlinParser.LineStrText, 0)

        def LineStrEscapedChar(self):
            return self.getToken(KotlinParser.LineStrEscapedChar, 0)

        def LineStrRef(self):
            return self.getToken(KotlinParser.LineStrRef, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_lineStringContent

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLineStringContent" ):
                listener.enterLineStringContent(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLineStringContent" ):
                listener.exitLineStringContent(self)




    def lineStringContent(self):

        localctx = KotlinParser.LineStringContentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 222, self.RULE_lineStringContent)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2746
            _la = self._input.LA(1)
            if not(((((_la - 158)) & ~0x3f) == 0 and ((1 << (_la - 158)) & ((1 << (KotlinParser.LineStrRef - 158)) | (1 << (KotlinParser.LineStrText - 158)) | (1 << (KotlinParser.LineStrEscapedChar - 158)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LineStringExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LineStrExprStart(self):
            return self.getToken(KotlinParser.LineStrExprStart, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def RCURL(self):
            return self.getToken(KotlinParser.RCURL, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_lineStringExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLineStringExpression" ):
                listener.enterLineStringExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLineStringExpression" ):
                listener.exitLineStringExpression(self)




    def lineStringExpression(self):

        localctx = KotlinParser.LineStringExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 224, self.RULE_lineStringExpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2748
            self.match(KotlinParser.LineStrExprStart)
            self.state = 2749
            self.expression()
            self.state = 2750
            self.match(KotlinParser.RCURL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultiLineStringContentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MultiLineStrText(self):
            return self.getToken(KotlinParser.MultiLineStrText, 0)

        def MultiLineStringQuote(self):
            return self.getToken(KotlinParser.MultiLineStringQuote, 0)

        def MultiLineStrRef(self):
            return self.getToken(KotlinParser.MultiLineStrRef, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_multiLineStringContent

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultiLineStringContent" ):
                listener.enterMultiLineStringContent(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultiLineStringContent" ):
                listener.exitMultiLineStringContent(self)




    def multiLineStringContent(self):

        localctx = KotlinParser.MultiLineStringContentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 226, self.RULE_multiLineStringContent)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2752
            _la = self._input.LA(1)
            if not(((((_la - 163)) & ~0x3f) == 0 and ((1 << (_la - 163)) & ((1 << (KotlinParser.MultiLineStringQuote - 163)) | (1 << (KotlinParser.MultiLineStrRef - 163)) | (1 << (KotlinParser.MultiLineStrText - 163)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultiLineStringExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MultiLineStrExprStart(self):
            return self.getToken(KotlinParser.MultiLineStrExprStart, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def RCURL(self):
            return self.getToken(KotlinParser.RCURL, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_multiLineStringExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultiLineStringExpression" ):
                listener.enterMultiLineStringExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultiLineStringExpression" ):
                listener.exitMultiLineStringExpression(self)




    def multiLineStringExpression(self):

        localctx = KotlinParser.MultiLineStringExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 228, self.RULE_multiLineStringExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2754
            self.match(KotlinParser.MultiLineStrExprStart)
            self.state = 2758
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,417,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2755
                    self.match(KotlinParser.NL) 
                self.state = 2760
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,417,self._ctx)

            self.state = 2761
            self.expression()
            self.state = 2765
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2762
                self.match(KotlinParser.NL)
                self.state = 2767
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2768
            self.match(KotlinParser.RCURL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LambdaLiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LCURL(self):
            return self.getToken(KotlinParser.LCURL, 0)

        def statements(self):
            return self.getTypedRuleContext(KotlinParser.StatementsContext,0)


        def RCURL(self):
            return self.getToken(KotlinParser.RCURL, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def ARROW(self):
            return self.getToken(KotlinParser.ARROW, 0)

        def lambdaParameters(self):
            return self.getTypedRuleContext(KotlinParser.LambdaParametersContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_lambdaLiteral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLambdaLiteral" ):
                listener.enterLambdaLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLambdaLiteral" ):
                listener.exitLambdaLiteral(self)




    def lambdaLiteral(self):

        localctx = KotlinParser.LambdaLiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 230, self.RULE_lambdaLiteral)
        self._la = 0 # Token type
        try:
            self.state = 2818
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,426,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2770
                self.match(KotlinParser.LCURL)
                self.state = 2774
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,419,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2771
                        self.match(KotlinParser.NL) 
                    self.state = 2776
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,419,self._ctx)

                self.state = 2777
                self.statements()
                self.state = 2781
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2778
                    self.match(KotlinParser.NL)
                    self.state = 2783
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2784
                self.match(KotlinParser.RCURL)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2786
                self.match(KotlinParser.LCURL)
                self.state = 2790
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,421,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2787
                        self.match(KotlinParser.NL) 
                    self.state = 2792
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,421,self._ctx)

                self.state = 2794
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,422,self._ctx)
                if la_ == 1:
                    self.state = 2793
                    self.lambdaParameters()


                self.state = 2799
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2796
                    self.match(KotlinParser.NL)
                    self.state = 2801
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2802
                self.match(KotlinParser.ARROW)
                self.state = 2806
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,424,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2803
                        self.match(KotlinParser.NL) 
                    self.state = 2808
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,424,self._ctx)

                self.state = 2809
                self.statements()
                self.state = 2813
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2810
                    self.match(KotlinParser.NL)
                    self.state = 2815
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2816
                self.match(KotlinParser.RCURL)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LambdaParametersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def lambdaParameter(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.LambdaParameterContext)
            else:
                return self.getTypedRuleContext(KotlinParser.LambdaParameterContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_lambdaParameters

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLambdaParameters" ):
                listener.enterLambdaParameters(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLambdaParameters" ):
                listener.exitLambdaParameters(self)




    def lambdaParameters(self):

        localctx = KotlinParser.LambdaParametersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 232, self.RULE_lambdaParameters)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2820
            self.lambdaParameter()
            self.state = 2837
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,429,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 2824
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2821
                        self.match(KotlinParser.NL)
                        self.state = 2826
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2827
                    self.match(KotlinParser.COMMA)
                    self.state = 2831
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,428,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 2828
                            self.match(KotlinParser.NL) 
                        self.state = 2833
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,428,self._ctx)

                    self.state = 2834
                    self.lambdaParameter() 
                self.state = 2839
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,429,self._ctx)

            self.state = 2847
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,431,self._ctx)
            if la_ == 1:
                self.state = 2843
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2840
                    self.match(KotlinParser.NL)
                    self.state = 2845
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2846
                self.match(KotlinParser.COMMA)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LambdaParameterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def variableDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.VariableDeclarationContext,0)


        def multiVariableDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.MultiVariableDeclarationContext,0)


        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_lambdaParameter

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLambdaParameter" ):
                listener.enterLambdaParameter(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLambdaParameter" ):
                listener.exitLambdaParameter(self)




    def lambdaParameter(self):

        localctx = KotlinParser.LambdaParameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 234, self.RULE_lambdaParameter)
        self._la = 0 # Token type
        try:
            self.state = 2867
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.NL, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.enterOuterAlt(localctx, 1)
                self.state = 2849
                self.variableDeclaration()
                pass
            elif token in [KotlinParser.LPAREN]:
                self.enterOuterAlt(localctx, 2)
                self.state = 2850
                self.multiVariableDeclaration()
                self.state = 2865
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,434,self._ctx)
                if la_ == 1:
                    self.state = 2854
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2851
                        self.match(KotlinParser.NL)
                        self.state = 2856
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2857
                    self.match(KotlinParser.COLON)
                    self.state = 2861
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2858
                        self.match(KotlinParser.NL)
                        self.state = 2863
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2864
                    self.type()


                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnonymousFunctionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FUN(self):
            return self.getToken(KotlinParser.FUN, 0)

        def parametersWithOptionalType(self):
            return self.getTypedRuleContext(KotlinParser.ParametersWithOptionalTypeContext,0)


        def type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeContext,i)


        def DOT(self):
            return self.getToken(KotlinParser.DOT, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def typeConstraints(self):
            return self.getTypedRuleContext(KotlinParser.TypeConstraintsContext,0)


        def functionBody(self):
            return self.getTypedRuleContext(KotlinParser.FunctionBodyContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_anonymousFunction

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnonymousFunction" ):
                listener.enterAnonymousFunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnonymousFunction" ):
                listener.exitAnonymousFunction(self)




    def anonymousFunction(self):

        localctx = KotlinParser.AnonymousFunctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 236, self.RULE_anonymousFunction)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2869
            self.match(KotlinParser.FUN)
            self.state = 2885
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,438,self._ctx)
            if la_ == 1:
                self.state = 2873
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2870
                    self.match(KotlinParser.NL)
                    self.state = 2875
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2876
                self.type()
                self.state = 2880
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2877
                    self.match(KotlinParser.NL)
                    self.state = 2882
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2883
                self.match(KotlinParser.DOT)


            self.state = 2890
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 2887
                self.match(KotlinParser.NL)
                self.state = 2892
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 2893
            self.parametersWithOptionalType()
            self.state = 2908
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,442,self._ctx)
            if la_ == 1:
                self.state = 2897
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2894
                    self.match(KotlinParser.NL)
                    self.state = 2899
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2900
                self.match(KotlinParser.COLON)
                self.state = 2904
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2901
                    self.match(KotlinParser.NL)
                    self.state = 2906
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2907
                self.type()


            self.state = 2917
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,444,self._ctx)
            if la_ == 1:
                self.state = 2913
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2910
                    self.match(KotlinParser.NL)
                    self.state = 2915
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2916
                self.typeConstraints()


            self.state = 2926
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,446,self._ctx)
            if la_ == 1:
                self.state = 2922
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2919
                    self.match(KotlinParser.NL)
                    self.state = 2924
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2925
                self.functionBody()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionLiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def lambdaLiteral(self):
            return self.getTypedRuleContext(KotlinParser.LambdaLiteralContext,0)


        def anonymousFunction(self):
            return self.getTypedRuleContext(KotlinParser.AnonymousFunctionContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_functionLiteral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionLiteral" ):
                listener.enterFunctionLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionLiteral" ):
                listener.exitFunctionLiteral(self)




    def functionLiteral(self):

        localctx = KotlinParser.FunctionLiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 238, self.RULE_functionLiteral)
        try:
            self.state = 2930
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.LCURL]:
                self.enterOuterAlt(localctx, 1)
                self.state = 2928
                self.lambdaLiteral()
                pass
            elif token in [KotlinParser.FUN]:
                self.enterOuterAlt(localctx, 2)
                self.state = 2929
                self.anonymousFunction()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ObjectLiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OBJECT(self):
            return self.getToken(KotlinParser.OBJECT, 0)

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def delegationSpecifiers(self):
            return self.getTypedRuleContext(KotlinParser.DelegationSpecifiersContext,0)


        def classBody(self):
            return self.getTypedRuleContext(KotlinParser.ClassBodyContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_objectLiteral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObjectLiteral" ):
                listener.enterObjectLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObjectLiteral" ):
                listener.exitObjectLiteral(self)




    def objectLiteral(self):

        localctx = KotlinParser.ObjectLiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 240, self.RULE_objectLiteral)
        self._la = 0 # Token type
        try:
            self.state = 2963
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,452,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2932
                self.match(KotlinParser.OBJECT)
                self.state = 2936
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2933
                    self.match(KotlinParser.NL)
                    self.state = 2938
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2939
                self.match(KotlinParser.COLON)
                self.state = 2943
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,449,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 2940
                        self.match(KotlinParser.NL) 
                    self.state = 2945
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,449,self._ctx)

                self.state = 2946
                self.delegationSpecifiers()
                self.state = 2950
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2947
                    self.match(KotlinParser.NL)
                    self.state = 2952
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2953
                self.classBody()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 2955
                self.match(KotlinParser.OBJECT)
                self.state = 2959
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2956
                    self.match(KotlinParser.NL)
                    self.state = 2961
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 2962
                self.classBody()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ThisExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def THIS(self):
            return self.getToken(KotlinParser.THIS, 0)

        def THIS_AT(self):
            return self.getToken(KotlinParser.THIS_AT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_thisExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterThisExpression" ):
                listener.enterThisExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitThisExpression" ):
                listener.exitThisExpression(self)




    def thisExpression(self):

        localctx = KotlinParser.ThisExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 242, self.RULE_thisExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 2965
            _la = self._input.LA(1)
            if not(_la==KotlinParser.THIS_AT or _la==KotlinParser.THIS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SuperExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SUPER(self):
            return self.getToken(KotlinParser.SUPER, 0)

        def LANGLE(self):
            return self.getToken(KotlinParser.LANGLE, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def RANGLE(self):
            return self.getToken(KotlinParser.RANGLE, 0)

        def AT_NO_WS(self):
            return self.getToken(KotlinParser.AT_NO_WS, 0)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def SUPER_AT(self):
            return self.getToken(KotlinParser.SUPER_AT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_superExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSuperExpression" ):
                listener.enterSuperExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSuperExpression" ):
                listener.exitSuperExpression(self)




    def superExpression(self):

        localctx = KotlinParser.SuperExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 244, self.RULE_superExpression)
        self._la = 0 # Token type
        try:
            self.state = 2991
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.SUPER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 2967
                self.match(KotlinParser.SUPER)
                self.state = 2984
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,455,self._ctx)
                if la_ == 1:
                    self.state = 2968
                    self.match(KotlinParser.LANGLE)
                    self.state = 2972
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2969
                        self.match(KotlinParser.NL)
                        self.state = 2974
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2975
                    self.type()
                    self.state = 2979
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 2976
                        self.match(KotlinParser.NL)
                        self.state = 2981
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 2982
                    self.match(KotlinParser.RANGLE)


                self.state = 2988
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,456,self._ctx)
                if la_ == 1:
                    self.state = 2986
                    self.match(KotlinParser.AT_NO_WS)
                    self.state = 2987
                    self.simpleIdentifier()


                pass
            elif token in [KotlinParser.SUPER_AT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 2990
                self.match(KotlinParser.SUPER_AT)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(KotlinParser.IF, 0)

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def controlStructureBody(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ControlStructureBodyContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ControlStructureBodyContext,i)


        def SEMICOLON(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.SEMICOLON)
            else:
                return self.getToken(KotlinParser.SEMICOLON, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def ELSE(self):
            return self.getToken(KotlinParser.ELSE, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_ifExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIfExpression" ):
                listener.enterIfExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIfExpression" ):
                listener.exitIfExpression(self)




    def ifExpression(self):

        localctx = KotlinParser.IfExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 246, self.RULE_ifExpression)
        self._la = 0 # Token type
        try:
            self.state = 3082
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,473,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2993
                self.match(KotlinParser.IF)
                self.state = 2997
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 2994
                    self.match(KotlinParser.NL)
                    self.state = 2999
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3000
                self.match(KotlinParser.LPAREN)
                self.state = 3004
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,459,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3001
                        self.match(KotlinParser.NL) 
                    self.state = 3006
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,459,self._ctx)

                self.state = 3007
                self.expression()
                self.state = 3011
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3008
                    self.match(KotlinParser.NL)
                    self.state = 3013
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3014
                self.match(KotlinParser.RPAREN)
                self.state = 3018
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,461,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3015
                        self.match(KotlinParser.NL) 
                    self.state = 3020
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,461,self._ctx)

                self.state = 3023
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [KotlinParser.NL, KotlinParser.LPAREN, KotlinParser.LSQUARE, KotlinParser.LCURL, KotlinParser.ADD, KotlinParser.SUB, KotlinParser.INCR, KotlinParser.DECR, KotlinParser.EXCL_WS, KotlinParser.EXCL_NO_WS, KotlinParser.COLONCOLON, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.RETURN_AT, KotlinParser.CONTINUE_AT, KotlinParser.BREAK_AT, KotlinParser.THIS_AT, KotlinParser.SUPER_AT, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CLASS, KotlinParser.INTERFACE, KotlinParser.FUN, KotlinParser.OBJECT, KotlinParser.VAL, KotlinParser.VAR, KotlinParser.TYPE_ALIAS, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.THIS, KotlinParser.SUPER, KotlinParser.WHERE, KotlinParser.IF, KotlinParser.WHEN, KotlinParser.TRY, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.FOR, KotlinParser.DO, KotlinParser.WHILE, KotlinParser.THROW, KotlinParser.RETURN, KotlinParser.CONTINUE, KotlinParser.BREAK, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.RealLiteral, KotlinParser.IntegerLiteral, KotlinParser.HexLiteral, KotlinParser.BinLiteral, KotlinParser.UnsignedLiteral, KotlinParser.LongLiteral, KotlinParser.BooleanLiteral, KotlinParser.NullLiteral, KotlinParser.CharacterLiteral, KotlinParser.Identifier, KotlinParser.QUOTE_OPEN, KotlinParser.TRIPLE_QUOTE_OPEN]:
                    self.state = 3021
                    self.controlStructureBody()
                    pass
                elif token in [KotlinParser.SEMICOLON]:
                    self.state = 3022
                    self.match(KotlinParser.SEMICOLON)
                    pass
                else:
                    raise NoViableAltException(self)

                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 3025
                self.match(KotlinParser.IF)
                self.state = 3029
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3026
                    self.match(KotlinParser.NL)
                    self.state = 3031
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3032
                self.match(KotlinParser.LPAREN)
                self.state = 3036
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,464,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3033
                        self.match(KotlinParser.NL) 
                    self.state = 3038
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,464,self._ctx)

                self.state = 3039
                self.expression()
                self.state = 3043
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3040
                    self.match(KotlinParser.NL)
                    self.state = 3045
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3046
                self.match(KotlinParser.RPAREN)
                self.state = 3050
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,466,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3047
                        self.match(KotlinParser.NL) 
                    self.state = 3052
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,466,self._ctx)

                self.state = 3054
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,467,self._ctx)
                if la_ == 1:
                    self.state = 3053
                    self.controlStructureBody()


                self.state = 3059
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,468,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3056
                        self.match(KotlinParser.NL) 
                    self.state = 3061
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,468,self._ctx)

                self.state = 3063
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==KotlinParser.SEMICOLON:
                    self.state = 3062
                    self.match(KotlinParser.SEMICOLON)


                self.state = 3068
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3065
                    self.match(KotlinParser.NL)
                    self.state = 3070
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3071
                self.match(KotlinParser.ELSE)
                self.state = 3075
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,471,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3072
                        self.match(KotlinParser.NL) 
                    self.state = 3077
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,471,self._ctx)

                self.state = 3080
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [KotlinParser.NL, KotlinParser.LPAREN, KotlinParser.LSQUARE, KotlinParser.LCURL, KotlinParser.ADD, KotlinParser.SUB, KotlinParser.INCR, KotlinParser.DECR, KotlinParser.EXCL_WS, KotlinParser.EXCL_NO_WS, KotlinParser.COLONCOLON, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.RETURN_AT, KotlinParser.CONTINUE_AT, KotlinParser.BREAK_AT, KotlinParser.THIS_AT, KotlinParser.SUPER_AT, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CLASS, KotlinParser.INTERFACE, KotlinParser.FUN, KotlinParser.OBJECT, KotlinParser.VAL, KotlinParser.VAR, KotlinParser.TYPE_ALIAS, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.THIS, KotlinParser.SUPER, KotlinParser.WHERE, KotlinParser.IF, KotlinParser.WHEN, KotlinParser.TRY, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.FOR, KotlinParser.DO, KotlinParser.WHILE, KotlinParser.THROW, KotlinParser.RETURN, KotlinParser.CONTINUE, KotlinParser.BREAK, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.RealLiteral, KotlinParser.IntegerLiteral, KotlinParser.HexLiteral, KotlinParser.BinLiteral, KotlinParser.UnsignedLiteral, KotlinParser.LongLiteral, KotlinParser.BooleanLiteral, KotlinParser.NullLiteral, KotlinParser.CharacterLiteral, KotlinParser.Identifier, KotlinParser.QUOTE_OPEN, KotlinParser.TRIPLE_QUOTE_OPEN]:
                    self.state = 3078
                    self.controlStructureBody()
                    pass
                elif token in [KotlinParser.SEMICOLON]:
                    self.state = 3079
                    self.match(KotlinParser.SEMICOLON)
                    pass
                else:
                    raise NoViableAltException(self)

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhenSubjectContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def VAL(self):
            return self.getToken(KotlinParser.VAL, 0)

        def variableDeclaration(self):
            return self.getTypedRuleContext(KotlinParser.VariableDeclarationContext,0)


        def ASSIGNMENT(self):
            return self.getToken(KotlinParser.ASSIGNMENT, 0)

        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_whenSubject

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhenSubject" ):
                listener.enterWhenSubject(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhenSubject" ):
                listener.exitWhenSubject(self)




    def whenSubject(self):

        localctx = KotlinParser.WhenSubjectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 248, self.RULE_whenSubject)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3084
            self.match(KotlinParser.LPAREN)
            self.state = 3118
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,479,self._ctx)
            if la_ == 1:
                self.state = 3088
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS:
                    self.state = 3085
                    self.annotation()
                    self.state = 3090
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3094
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3091
                    self.match(KotlinParser.NL)
                    self.state = 3096
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3097
                self.match(KotlinParser.VAL)
                self.state = 3101
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,476,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3098
                        self.match(KotlinParser.NL) 
                    self.state = 3103
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,476,self._ctx)

                self.state = 3104
                self.variableDeclaration()
                self.state = 3108
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3105
                    self.match(KotlinParser.NL)
                    self.state = 3110
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3111
                self.match(KotlinParser.ASSIGNMENT)
                self.state = 3115
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,478,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3112
                        self.match(KotlinParser.NL) 
                    self.state = 3117
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,478,self._ctx)



            self.state = 3120
            self.expression()
            self.state = 3121
            self.match(KotlinParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhenExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WHEN(self):
            return self.getToken(KotlinParser.WHEN, 0)

        def LCURL(self):
            return self.getToken(KotlinParser.LCURL, 0)

        def RCURL(self):
            return self.getToken(KotlinParser.RCURL, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def whenSubject(self):
            return self.getTypedRuleContext(KotlinParser.WhenSubjectContext,0)


        def whenEntry(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.WhenEntryContext)
            else:
                return self.getTypedRuleContext(KotlinParser.WhenEntryContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_whenExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhenExpression" ):
                listener.enterWhenExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhenExpression" ):
                listener.exitWhenExpression(self)




    def whenExpression(self):

        localctx = KotlinParser.WhenExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 250, self.RULE_whenExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3123
            self.match(KotlinParser.WHEN)
            self.state = 3127
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,480,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 3124
                    self.match(KotlinParser.NL) 
                self.state = 3129
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,480,self._ctx)

            self.state = 3131
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.LPAREN:
                self.state = 3130
                self.whenSubject()


            self.state = 3136
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3133
                self.match(KotlinParser.NL)
                self.state = 3138
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3139
            self.match(KotlinParser.LCURL)
            self.state = 3143
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,483,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 3140
                    self.match(KotlinParser.NL) 
                self.state = 3145
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,483,self._ctx)

            self.state = 3155
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,485,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 3146
                    self.whenEntry()
                    self.state = 3150
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,484,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 3147
                            self.match(KotlinParser.NL) 
                        self.state = 3152
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,484,self._ctx)
             
                self.state = 3157
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,485,self._ctx)

            self.state = 3161
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3158
                self.match(KotlinParser.NL)
                self.state = 3163
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3164
            self.match(KotlinParser.RCURL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhenEntryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def whenCondition(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.WhenConditionContext)
            else:
                return self.getTypedRuleContext(KotlinParser.WhenConditionContext,i)


        def ARROW(self):
            return self.getToken(KotlinParser.ARROW, 0)

        def controlStructureBody(self):
            return self.getTypedRuleContext(KotlinParser.ControlStructureBodyContext,0)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.COMMA)
            else:
                return self.getToken(KotlinParser.COMMA, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def semi(self):
            return self.getTypedRuleContext(KotlinParser.SemiContext,0)


        def ELSE(self):
            return self.getToken(KotlinParser.ELSE, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_whenEntry

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhenEntry" ):
                listener.enterWhenEntry(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhenEntry" ):
                listener.exitWhenEntry(self)




    def whenEntry(self):

        localctx = KotlinParser.WhenEntryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 252, self.RULE_whenEntry)
        self._la = 0 # Token type
        try:
            self.state = 3230
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.NL, KotlinParser.LPAREN, KotlinParser.LSQUARE, KotlinParser.LCURL, KotlinParser.ADD, KotlinParser.SUB, KotlinParser.INCR, KotlinParser.DECR, KotlinParser.EXCL_WS, KotlinParser.EXCL_NO_WS, KotlinParser.COLONCOLON, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.RETURN_AT, KotlinParser.CONTINUE_AT, KotlinParser.BREAK_AT, KotlinParser.THIS_AT, KotlinParser.SUPER_AT, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.FUN, KotlinParser.OBJECT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.THIS, KotlinParser.SUPER, KotlinParser.WHERE, KotlinParser.IF, KotlinParser.WHEN, KotlinParser.TRY, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.THROW, KotlinParser.RETURN, KotlinParser.CONTINUE, KotlinParser.BREAK, KotlinParser.IS, KotlinParser.IN, KotlinParser.NOT_IS, KotlinParser.NOT_IN, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.RealLiteral, KotlinParser.IntegerLiteral, KotlinParser.HexLiteral, KotlinParser.BinLiteral, KotlinParser.UnsignedLiteral, KotlinParser.LongLiteral, KotlinParser.BooleanLiteral, KotlinParser.NullLiteral, KotlinParser.CharacterLiteral, KotlinParser.Identifier, KotlinParser.QUOTE_OPEN, KotlinParser.TRIPLE_QUOTE_OPEN]:
                self.enterOuterAlt(localctx, 1)
                self.state = 3166
                self.whenCondition()
                self.state = 3183
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,489,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3170
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 3167
                            self.match(KotlinParser.NL)
                            self.state = 3172
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 3173
                        self.match(KotlinParser.COMMA)
                        self.state = 3177
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,488,self._ctx)
                        while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                            if _alt==1:
                                self.state = 3174
                                self.match(KotlinParser.NL) 
                            self.state = 3179
                            self._errHandler.sync(self)
                            _alt = self._interp.adaptivePredict(self._input,488,self._ctx)

                        self.state = 3180
                        self.whenCondition() 
                    self.state = 3185
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,489,self._ctx)

                self.state = 3193
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,491,self._ctx)
                if la_ == 1:
                    self.state = 3189
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 3186
                        self.match(KotlinParser.NL)
                        self.state = 3191
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 3192
                    self.match(KotlinParser.COMMA)


                self.state = 3198
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3195
                    self.match(KotlinParser.NL)
                    self.state = 3200
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3201
                self.match(KotlinParser.ARROW)
                self.state = 3205
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,493,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3202
                        self.match(KotlinParser.NL) 
                    self.state = 3207
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,493,self._ctx)

                self.state = 3208
                self.controlStructureBody()
                self.state = 3210
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,494,self._ctx)
                if la_ == 1:
                    self.state = 3209
                    self.semi()


                pass
            elif token in [KotlinParser.ELSE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 3212
                self.match(KotlinParser.ELSE)
                self.state = 3216
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3213
                    self.match(KotlinParser.NL)
                    self.state = 3218
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3219
                self.match(KotlinParser.ARROW)
                self.state = 3223
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,496,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3220
                        self.match(KotlinParser.NL) 
                    self.state = 3225
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,496,self._ctx)

                self.state = 3226
                self.controlStructureBody()
                self.state = 3228
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,497,self._ctx)
                if la_ == 1:
                    self.state = 3227
                    self.semi()


                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhenConditionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def rangeTest(self):
            return self.getTypedRuleContext(KotlinParser.RangeTestContext,0)


        def typeTest(self):
            return self.getTypedRuleContext(KotlinParser.TypeTestContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_whenCondition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhenCondition" ):
                listener.enterWhenCondition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhenCondition" ):
                listener.exitWhenCondition(self)




    def whenCondition(self):

        localctx = KotlinParser.WhenConditionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 254, self.RULE_whenCondition)
        try:
            self.state = 3235
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.NL, KotlinParser.LPAREN, KotlinParser.LSQUARE, KotlinParser.LCURL, KotlinParser.ADD, KotlinParser.SUB, KotlinParser.INCR, KotlinParser.DECR, KotlinParser.EXCL_WS, KotlinParser.EXCL_NO_WS, KotlinParser.COLONCOLON, KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS, KotlinParser.RETURN_AT, KotlinParser.CONTINUE_AT, KotlinParser.BREAK_AT, KotlinParser.THIS_AT, KotlinParser.SUPER_AT, KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.FUN, KotlinParser.OBJECT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.THIS, KotlinParser.SUPER, KotlinParser.WHERE, KotlinParser.IF, KotlinParser.WHEN, KotlinParser.TRY, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.THROW, KotlinParser.RETURN, KotlinParser.CONTINUE, KotlinParser.BREAK, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.RealLiteral, KotlinParser.IntegerLiteral, KotlinParser.HexLiteral, KotlinParser.BinLiteral, KotlinParser.UnsignedLiteral, KotlinParser.LongLiteral, KotlinParser.BooleanLiteral, KotlinParser.NullLiteral, KotlinParser.CharacterLiteral, KotlinParser.Identifier, KotlinParser.QUOTE_OPEN, KotlinParser.TRIPLE_QUOTE_OPEN]:
                self.enterOuterAlt(localctx, 1)
                self.state = 3232
                self.expression()
                pass
            elif token in [KotlinParser.IN, KotlinParser.NOT_IN]:
                self.enterOuterAlt(localctx, 2)
                self.state = 3233
                self.rangeTest()
                pass
            elif token in [KotlinParser.IS, KotlinParser.NOT_IS]:
                self.enterOuterAlt(localctx, 3)
                self.state = 3234
                self.typeTest()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RangeTestContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def inOperator(self):
            return self.getTypedRuleContext(KotlinParser.InOperatorContext,0)


        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_rangeTest

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRangeTest" ):
                listener.enterRangeTest(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRangeTest" ):
                listener.exitRangeTest(self)




    def rangeTest(self):

        localctx = KotlinParser.RangeTestContext(self, self._ctx, self.state)
        self.enterRule(localctx, 256, self.RULE_rangeTest)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3237
            self.inOperator()
            self.state = 3241
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,500,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 3238
                    self.match(KotlinParser.NL) 
                self.state = 3243
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,500,self._ctx)

            self.state = 3244
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeTestContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def isOperator(self):
            return self.getTypedRuleContext(KotlinParser.IsOperatorContext,0)


        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_typeTest

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeTest" ):
                listener.enterTypeTest(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeTest" ):
                listener.exitTypeTest(self)




    def typeTest(self):

        localctx = KotlinParser.TypeTestContext(self, self._ctx, self.state)
        self.enterRule(localctx, 258, self.RULE_typeTest)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3246
            self.isOperator()
            self.state = 3250
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3247
                self.match(KotlinParser.NL)
                self.state = 3252
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3253
            self.type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TryExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TRY(self):
            return self.getToken(KotlinParser.TRY, 0)

        def block(self):
            return self.getTypedRuleContext(KotlinParser.BlockContext,0)


        def finallyBlock(self):
            return self.getTypedRuleContext(KotlinParser.FinallyBlockContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def catchBlock(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.CatchBlockContext)
            else:
                return self.getTypedRuleContext(KotlinParser.CatchBlockContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_tryExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTryExpression" ):
                listener.enterTryExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTryExpression" ):
                listener.exitTryExpression(self)




    def tryExpression(self):

        localctx = KotlinParser.TryExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 260, self.RULE_tryExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3255
            self.match(KotlinParser.TRY)
            self.state = 3259
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3256
                self.match(KotlinParser.NL)
                self.state = 3261
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3262
            self.block()
            self.state = 3290
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,508,self._ctx)
            if la_ == 1:
                self.state = 3270 
                self._errHandler.sync(self)
                _alt = 1
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1:
                        self.state = 3266
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==KotlinParser.NL:
                            self.state = 3263
                            self.match(KotlinParser.NL)
                            self.state = 3268
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 3269
                        self.catchBlock()

                    else:
                        raise NoViableAltException(self)
                    self.state = 3272 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,504,self._ctx)

                self.state = 3281
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,506,self._ctx)
                if la_ == 1:
                    self.state = 3277
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 3274
                        self.match(KotlinParser.NL)
                        self.state = 3279
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 3280
                    self.finallyBlock()


                pass

            elif la_ == 2:
                self.state = 3286
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3283
                    self.match(KotlinParser.NL)
                    self.state = 3288
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3289
                self.finallyBlock()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CatchBlockContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CATCH(self):
            return self.getToken(KotlinParser.CATCH, 0)

        def LPAREN(self):
            return self.getToken(KotlinParser.LPAREN, 0)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def type(self):
            return self.getTypedRuleContext(KotlinParser.TypeContext,0)


        def RPAREN(self):
            return self.getToken(KotlinParser.RPAREN, 0)

        def block(self):
            return self.getTypedRuleContext(KotlinParser.BlockContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def COMMA(self):
            return self.getToken(KotlinParser.COMMA, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_catchBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCatchBlock" ):
                listener.enterCatchBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCatchBlock" ):
                listener.exitCatchBlock(self)




    def catchBlock(self):

        localctx = KotlinParser.CatchBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 262, self.RULE_catchBlock)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3292
            self.match(KotlinParser.CATCH)
            self.state = 3296
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3293
                self.match(KotlinParser.NL)
                self.state = 3298
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3299
            self.match(KotlinParser.LPAREN)
            self.state = 3303
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS:
                self.state = 3300
                self.annotation()
                self.state = 3305
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3306
            self.simpleIdentifier()
            self.state = 3307
            self.match(KotlinParser.COLON)
            self.state = 3308
            self.type()
            self.state = 3316
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==KotlinParser.NL or _la==KotlinParser.COMMA:
                self.state = 3312
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3309
                    self.match(KotlinParser.NL)
                    self.state = 3314
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3315
                self.match(KotlinParser.COMMA)


            self.state = 3318
            self.match(KotlinParser.RPAREN)
            self.state = 3322
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3319
                self.match(KotlinParser.NL)
                self.state = 3324
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3325
            self.block()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FinallyBlockContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FINALLY(self):
            return self.getToken(KotlinParser.FINALLY, 0)

        def block(self):
            return self.getTypedRuleContext(KotlinParser.BlockContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_finallyBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFinallyBlock" ):
                listener.enterFinallyBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFinallyBlock" ):
                listener.exitFinallyBlock(self)




    def finallyBlock(self):

        localctx = KotlinParser.FinallyBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 264, self.RULE_finallyBlock)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3327
            self.match(KotlinParser.FINALLY)
            self.state = 3331
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3328
                self.match(KotlinParser.NL)
                self.state = 3333
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3334
            self.block()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class JumpExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def THROW(self):
            return self.getToken(KotlinParser.THROW, 0)

        def expression(self):
            return self.getTypedRuleContext(KotlinParser.ExpressionContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def RETURN(self):
            return self.getToken(KotlinParser.RETURN, 0)

        def RETURN_AT(self):
            return self.getToken(KotlinParser.RETURN_AT, 0)

        def CONTINUE(self):
            return self.getToken(KotlinParser.CONTINUE, 0)

        def CONTINUE_AT(self):
            return self.getToken(KotlinParser.CONTINUE_AT, 0)

        def BREAK(self):
            return self.getToken(KotlinParser.BREAK, 0)

        def BREAK_AT(self):
            return self.getToken(KotlinParser.BREAK_AT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_jumpExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterJumpExpression" ):
                listener.enterJumpExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitJumpExpression" ):
                listener.exitJumpExpression(self)




    def jumpExpression(self):

        localctx = KotlinParser.JumpExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 266, self.RULE_jumpExpression)
        self._la = 0 # Token type
        try:
            self.state = 3352
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.THROW]:
                self.enterOuterAlt(localctx, 1)
                self.state = 3336
                self.match(KotlinParser.THROW)
                self.state = 3340
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,515,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3337
                        self.match(KotlinParser.NL) 
                    self.state = 3342
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,515,self._ctx)

                self.state = 3343
                self.expression()
                pass
            elif token in [KotlinParser.RETURN_AT, KotlinParser.RETURN]:
                self.enterOuterAlt(localctx, 2)
                self.state = 3344
                _la = self._input.LA(1)
                if not(_la==KotlinParser.RETURN_AT or _la==KotlinParser.RETURN):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 3346
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,516,self._ctx)
                if la_ == 1:
                    self.state = 3345
                    self.expression()


                pass
            elif token in [KotlinParser.CONTINUE]:
                self.enterOuterAlt(localctx, 3)
                self.state = 3348
                self.match(KotlinParser.CONTINUE)
                pass
            elif token in [KotlinParser.CONTINUE_AT]:
                self.enterOuterAlt(localctx, 4)
                self.state = 3349
                self.match(KotlinParser.CONTINUE_AT)
                pass
            elif token in [KotlinParser.BREAK]:
                self.enterOuterAlt(localctx, 5)
                self.state = 3350
                self.match(KotlinParser.BREAK)
                pass
            elif token in [KotlinParser.BREAK_AT]:
                self.enterOuterAlt(localctx, 6)
                self.state = 3351
                self.match(KotlinParser.BREAK_AT)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CallableReferenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def COLONCOLON(self):
            return self.getToken(KotlinParser.COLONCOLON, 0)

        def simpleIdentifier(self):
            return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,0)


        def CLASS(self):
            return self.getToken(KotlinParser.CLASS, 0)

        def receiverType(self):
            return self.getTypedRuleContext(KotlinParser.ReceiverTypeContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_callableReference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCallableReference" ):
                listener.enterCallableReference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCallableReference" ):
                listener.exitCallableReference(self)




    def callableReference(self):

        localctx = KotlinParser.CallableReferenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 268, self.RULE_callableReference)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3355
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << KotlinParser.LPAREN) | (1 << KotlinParser.AT_NO_WS) | (1 << KotlinParser.AT_PRE_WS) | (1 << KotlinParser.FILE) | (1 << KotlinParser.FIELD) | (1 << KotlinParser.PROPERTY))) != 0) or ((((_la - 64)) & ~0x3f) == 0 and ((1 << (_la - 64)) & ((1 << (KotlinParser.GET - 64)) | (1 << (KotlinParser.SET - 64)) | (1 << (KotlinParser.RECEIVER - 64)) | (1 << (KotlinParser.PARAM - 64)) | (1 << (KotlinParser.SETPARAM - 64)) | (1 << (KotlinParser.DELEGATE - 64)) | (1 << (KotlinParser.IMPORT - 64)) | (1 << (KotlinParser.CONSTRUCTOR - 64)) | (1 << (KotlinParser.BY - 64)) | (1 << (KotlinParser.COMPANION - 64)) | (1 << (KotlinParser.INIT - 64)) | (1 << (KotlinParser.WHERE - 64)) | (1 << (KotlinParser.CATCH - 64)) | (1 << (KotlinParser.FINALLY - 64)) | (1 << (KotlinParser.OUT - 64)) | (1 << (KotlinParser.DYNAMIC - 64)) | (1 << (KotlinParser.PUBLIC - 64)) | (1 << (KotlinParser.PRIVATE - 64)) | (1 << (KotlinParser.PROTECTED - 64)) | (1 << (KotlinParser.INTERNAL - 64)) | (1 << (KotlinParser.ENUM - 64)) | (1 << (KotlinParser.SEALED - 64)) | (1 << (KotlinParser.ANNOTATION - 64)) | (1 << (KotlinParser.DATA - 64)) | (1 << (KotlinParser.INNER - 64)) | (1 << (KotlinParser.TAILREC - 64)) | (1 << (KotlinParser.OPERATOR - 64)) | (1 << (KotlinParser.INLINE - 64)) | (1 << (KotlinParser.INFIX - 64)) | (1 << (KotlinParser.EXTERNAL - 64)) | (1 << (KotlinParser.SUSPEND - 64)) | (1 << (KotlinParser.OVERRIDE - 64)) | (1 << (KotlinParser.ABSTRACT - 64)) | (1 << (KotlinParser.FINAL - 64)) | (1 << (KotlinParser.OPEN - 64)) | (1 << (KotlinParser.CONST - 64)) | (1 << (KotlinParser.LATEINIT - 64)))) != 0) or ((((_la - 128)) & ~0x3f) == 0 and ((1 << (_la - 128)) & ((1 << (KotlinParser.VARARG - 128)) | (1 << (KotlinParser.NOINLINE - 128)) | (1 << (KotlinParser.CROSSINLINE - 128)) | (1 << (KotlinParser.REIFIED - 128)) | (1 << (KotlinParser.EXPECT - 128)) | (1 << (KotlinParser.ACTUAL - 128)) | (1 << (KotlinParser.Identifier - 128)))) != 0):
                self.state = 3354
                self.receiverType()


            self.state = 3360
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3357
                self.match(KotlinParser.NL)
                self.state = 3362
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3363
            self.match(KotlinParser.COLONCOLON)
            self.state = 3367
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3364
                self.match(KotlinParser.NL)
                self.state = 3369
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3372
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.FILE, KotlinParser.FIELD, KotlinParser.PROPERTY, KotlinParser.GET, KotlinParser.SET, KotlinParser.RECEIVER, KotlinParser.PARAM, KotlinParser.SETPARAM, KotlinParser.DELEGATE, KotlinParser.IMPORT, KotlinParser.CONSTRUCTOR, KotlinParser.BY, KotlinParser.COMPANION, KotlinParser.INIT, KotlinParser.WHERE, KotlinParser.CATCH, KotlinParser.FINALLY, KotlinParser.OUT, KotlinParser.DYNAMIC, KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.REIFIED, KotlinParser.EXPECT, KotlinParser.ACTUAL, KotlinParser.Identifier]:
                self.state = 3370
                self.simpleIdentifier()
                pass
            elif token in [KotlinParser.CLASS]:
                self.state = 3371
                self.match(KotlinParser.CLASS)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignmentAndOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ADD_ASSIGNMENT(self):
            return self.getToken(KotlinParser.ADD_ASSIGNMENT, 0)

        def SUB_ASSIGNMENT(self):
            return self.getToken(KotlinParser.SUB_ASSIGNMENT, 0)

        def MULT_ASSIGNMENT(self):
            return self.getToken(KotlinParser.MULT_ASSIGNMENT, 0)

        def DIV_ASSIGNMENT(self):
            return self.getToken(KotlinParser.DIV_ASSIGNMENT, 0)

        def MOD_ASSIGNMENT(self):
            return self.getToken(KotlinParser.MOD_ASSIGNMENT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_assignmentAndOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignmentAndOperator" ):
                listener.enterAssignmentAndOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignmentAndOperator" ):
                listener.exitAssignmentAndOperator(self)




    def assignmentAndOperator(self):

        localctx = KotlinParser.AssignmentAndOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 270, self.RULE_assignmentAndOperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3374
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << KotlinParser.ADD_ASSIGNMENT) | (1 << KotlinParser.SUB_ASSIGNMENT) | (1 << KotlinParser.MULT_ASSIGNMENT) | (1 << KotlinParser.DIV_ASSIGNMENT) | (1 << KotlinParser.MOD_ASSIGNMENT))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EqualityOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EXCL_EQ(self):
            return self.getToken(KotlinParser.EXCL_EQ, 0)

        def EXCL_EQEQ(self):
            return self.getToken(KotlinParser.EXCL_EQEQ, 0)

        def EQEQ(self):
            return self.getToken(KotlinParser.EQEQ, 0)

        def EQEQEQ(self):
            return self.getToken(KotlinParser.EQEQEQ, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_equalityOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEqualityOperator" ):
                listener.enterEqualityOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEqualityOperator" ):
                listener.exitEqualityOperator(self)




    def equalityOperator(self):

        localctx = KotlinParser.EqualityOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 272, self.RULE_equalityOperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3376
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << KotlinParser.EXCL_EQ) | (1 << KotlinParser.EXCL_EQEQ) | (1 << KotlinParser.EQEQ) | (1 << KotlinParser.EQEQEQ))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ComparisonOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LANGLE(self):
            return self.getToken(KotlinParser.LANGLE, 0)

        def RANGLE(self):
            return self.getToken(KotlinParser.RANGLE, 0)

        def LE(self):
            return self.getToken(KotlinParser.LE, 0)

        def GE(self):
            return self.getToken(KotlinParser.GE, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_comparisonOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComparisonOperator" ):
                listener.enterComparisonOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComparisonOperator" ):
                listener.exitComparisonOperator(self)




    def comparisonOperator(self):

        localctx = KotlinParser.ComparisonOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 274, self.RULE_comparisonOperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3378
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << KotlinParser.LANGLE) | (1 << KotlinParser.RANGLE) | (1 << KotlinParser.LE) | (1 << KotlinParser.GE))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IN(self):
            return self.getToken(KotlinParser.IN, 0)

        def NOT_IN(self):
            return self.getToken(KotlinParser.NOT_IN, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_inOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInOperator" ):
                listener.enterInOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInOperator" ):
                listener.exitInOperator(self)




    def inOperator(self):

        localctx = KotlinParser.InOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 276, self.RULE_inOperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3380
            _la = self._input.LA(1)
            if not(_la==KotlinParser.IN or _la==KotlinParser.NOT_IN):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IsOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IS(self):
            return self.getToken(KotlinParser.IS, 0)

        def NOT_IS(self):
            return self.getToken(KotlinParser.NOT_IS, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_isOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIsOperator" ):
                listener.enterIsOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIsOperator" ):
                listener.exitIsOperator(self)




    def isOperator(self):

        localctx = KotlinParser.IsOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 278, self.RULE_isOperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3382
            _la = self._input.LA(1)
            if not(_la==KotlinParser.IS or _la==KotlinParser.NOT_IS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AdditiveOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ADD(self):
            return self.getToken(KotlinParser.ADD, 0)

        def SUB(self):
            return self.getToken(KotlinParser.SUB, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_additiveOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAdditiveOperator" ):
                listener.enterAdditiveOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAdditiveOperator" ):
                listener.exitAdditiveOperator(self)




    def additiveOperator(self):

        localctx = KotlinParser.AdditiveOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 280, self.RULE_additiveOperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3384
            _la = self._input.LA(1)
            if not(_la==KotlinParser.ADD or _la==KotlinParser.SUB):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultiplicativeOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MULT(self):
            return self.getToken(KotlinParser.MULT, 0)

        def DIV(self):
            return self.getToken(KotlinParser.DIV, 0)

        def MOD(self):
            return self.getToken(KotlinParser.MOD, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_multiplicativeOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultiplicativeOperator" ):
                listener.enterMultiplicativeOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultiplicativeOperator" ):
                listener.exitMultiplicativeOperator(self)




    def multiplicativeOperator(self):

        localctx = KotlinParser.MultiplicativeOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 282, self.RULE_multiplicativeOperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3386
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << KotlinParser.MULT) | (1 << KotlinParser.MOD) | (1 << KotlinParser.DIV))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AsOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def AS(self):
            return self.getToken(KotlinParser.AS, 0)

        def AS_SAFE(self):
            return self.getToken(KotlinParser.AS_SAFE, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_asOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAsOperator" ):
                listener.enterAsOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAsOperator" ):
                listener.exitAsOperator(self)




    def asOperator(self):

        localctx = KotlinParser.AsOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 284, self.RULE_asOperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3388
            _la = self._input.LA(1)
            if not(_la==KotlinParser.AS_SAFE or _la==KotlinParser.AS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrefixUnaryOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INCR(self):
            return self.getToken(KotlinParser.INCR, 0)

        def DECR(self):
            return self.getToken(KotlinParser.DECR, 0)

        def SUB(self):
            return self.getToken(KotlinParser.SUB, 0)

        def ADD(self):
            return self.getToken(KotlinParser.ADD, 0)

        def excl(self):
            return self.getTypedRuleContext(KotlinParser.ExclContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_prefixUnaryOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrefixUnaryOperator" ):
                listener.enterPrefixUnaryOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrefixUnaryOperator" ):
                listener.exitPrefixUnaryOperator(self)




    def prefixUnaryOperator(self):

        localctx = KotlinParser.PrefixUnaryOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 286, self.RULE_prefixUnaryOperator)
        try:
            self.state = 3395
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.INCR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 3390
                self.match(KotlinParser.INCR)
                pass
            elif token in [KotlinParser.DECR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 3391
                self.match(KotlinParser.DECR)
                pass
            elif token in [KotlinParser.SUB]:
                self.enterOuterAlt(localctx, 3)
                self.state = 3392
                self.match(KotlinParser.SUB)
                pass
            elif token in [KotlinParser.ADD]:
                self.enterOuterAlt(localctx, 4)
                self.state = 3393
                self.match(KotlinParser.ADD)
                pass
            elif token in [KotlinParser.EXCL_WS, KotlinParser.EXCL_NO_WS]:
                self.enterOuterAlt(localctx, 5)
                self.state = 3394
                self.excl()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PostfixUnaryOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INCR(self):
            return self.getToken(KotlinParser.INCR, 0)

        def DECR(self):
            return self.getToken(KotlinParser.DECR, 0)

        def EXCL_NO_WS(self):
            return self.getToken(KotlinParser.EXCL_NO_WS, 0)

        def excl(self):
            return self.getTypedRuleContext(KotlinParser.ExclContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_postfixUnaryOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPostfixUnaryOperator" ):
                listener.enterPostfixUnaryOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPostfixUnaryOperator" ):
                listener.exitPostfixUnaryOperator(self)




    def postfixUnaryOperator(self):

        localctx = KotlinParser.PostfixUnaryOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 288, self.RULE_postfixUnaryOperator)
        try:
            self.state = 3401
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.INCR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 3397
                self.match(KotlinParser.INCR)
                pass
            elif token in [KotlinParser.DECR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 3398
                self.match(KotlinParser.DECR)
                pass
            elif token in [KotlinParser.EXCL_NO_WS]:
                self.enterOuterAlt(localctx, 3)
                self.state = 3399
                self.match(KotlinParser.EXCL_NO_WS)
                self.state = 3400
                self.excl()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EXCL_NO_WS(self):
            return self.getToken(KotlinParser.EXCL_NO_WS, 0)

        def EXCL_WS(self):
            return self.getToken(KotlinParser.EXCL_WS, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_excl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExcl" ):
                listener.enterExcl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExcl" ):
                listener.exitExcl(self)




    def excl(self):

        localctx = KotlinParser.ExclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 290, self.RULE_excl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3403
            _la = self._input.LA(1)
            if not(_la==KotlinParser.EXCL_WS or _la==KotlinParser.EXCL_NO_WS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MemberAccessOperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DOT(self):
            return self.getToken(KotlinParser.DOT, 0)

        def safeNav(self):
            return self.getTypedRuleContext(KotlinParser.SafeNavContext,0)


        def COLONCOLON(self):
            return self.getToken(KotlinParser.COLONCOLON, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_memberAccessOperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMemberAccessOperator" ):
                listener.enterMemberAccessOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMemberAccessOperator" ):
                listener.exitMemberAccessOperator(self)




    def memberAccessOperator(self):

        localctx = KotlinParser.MemberAccessOperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 292, self.RULE_memberAccessOperator)
        try:
            self.state = 3408
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.DOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 3405
                self.match(KotlinParser.DOT)
                pass
            elif token in [KotlinParser.QUEST_NO_WS]:
                self.enterOuterAlt(localctx, 2)
                self.state = 3406
                self.safeNav()
                pass
            elif token in [KotlinParser.COLONCOLON]:
                self.enterOuterAlt(localctx, 3)
                self.state = 3407
                self.match(KotlinParser.COLONCOLON)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SafeNavContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def QUEST_NO_WS(self):
            return self.getToken(KotlinParser.QUEST_NO_WS, 0)

        def DOT(self):
            return self.getToken(KotlinParser.DOT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_safeNav

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSafeNav" ):
                listener.enterSafeNav(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSafeNav" ):
                listener.exitSafeNav(self)




    def safeNav(self):

        localctx = KotlinParser.SafeNavContext(self, self._ctx, self.state)
        self.enterRule(localctx, 294, self.RULE_safeNav)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3410
            self.match(KotlinParser.QUEST_NO_WS)
            self.state = 3411
            self.match(KotlinParser.DOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ModifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def modifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ModifierContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ModifierContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_modifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterModifiers" ):
                listener.enterModifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitModifiers" ):
                listener.exitModifiers(self)




    def modifiers(self):

        localctx = KotlinParser.ModifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 296, self.RULE_modifiers)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3415 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 3415
                    self._errHandler.sync(self)
                    token = self._input.LA(1)
                    if token in [KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS]:
                        self.state = 3413
                        self.annotation()
                        pass
                    elif token in [KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL, KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER, KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND, KotlinParser.OVERRIDE, KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN, KotlinParser.CONST, KotlinParser.LATEINIT, KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE, KotlinParser.EXPECT, KotlinParser.ACTUAL]:
                        self.state = 3414
                        self.modifier()
                        pass
                    else:
                        raise NoViableAltException(self)


                else:
                    raise NoViableAltException(self)
                self.state = 3417 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,526,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterModifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.AnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.AnnotationContext,i)


        def parameterModifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.ParameterModifierContext)
            else:
                return self.getTypedRuleContext(KotlinParser.ParameterModifierContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_parameterModifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameterModifiers" ):
                listener.enterParameterModifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameterModifiers" ):
                listener.exitParameterModifiers(self)




    def parameterModifiers(self):

        localctx = KotlinParser.ParameterModifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 298, self.RULE_parameterModifiers)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3421 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 3421
                    self._errHandler.sync(self)
                    token = self._input.LA(1)
                    if token in [KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS]:
                        self.state = 3419
                        self.annotation()
                        pass
                    elif token in [KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE]:
                        self.state = 3420
                        self.parameterModifier()
                        pass
                    else:
                        raise NoViableAltException(self)


                else:
                    raise NoViableAltException(self)
                self.state = 3423 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,528,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def classModifier(self):
            return self.getTypedRuleContext(KotlinParser.ClassModifierContext,0)


        def memberModifier(self):
            return self.getTypedRuleContext(KotlinParser.MemberModifierContext,0)


        def visibilityModifier(self):
            return self.getTypedRuleContext(KotlinParser.VisibilityModifierContext,0)


        def functionModifier(self):
            return self.getTypedRuleContext(KotlinParser.FunctionModifierContext,0)


        def propertyModifier(self):
            return self.getTypedRuleContext(KotlinParser.PropertyModifierContext,0)


        def inheritanceModifier(self):
            return self.getTypedRuleContext(KotlinParser.InheritanceModifierContext,0)


        def parameterModifier(self):
            return self.getTypedRuleContext(KotlinParser.ParameterModifierContext,0)


        def platformModifier(self):
            return self.getTypedRuleContext(KotlinParser.PlatformModifierContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_modifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterModifier" ):
                listener.enterModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitModifier" ):
                listener.exitModifier(self)




    def modifier(self):

        localctx = KotlinParser.ModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 300, self.RULE_modifier)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3433
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.ENUM, KotlinParser.SEALED, KotlinParser.ANNOTATION, KotlinParser.DATA, KotlinParser.INNER]:
                self.state = 3425
                self.classModifier()
                pass
            elif token in [KotlinParser.OVERRIDE, KotlinParser.LATEINIT]:
                self.state = 3426
                self.memberModifier()
                pass
            elif token in [KotlinParser.PUBLIC, KotlinParser.PRIVATE, KotlinParser.PROTECTED, KotlinParser.INTERNAL]:
                self.state = 3427
                self.visibilityModifier()
                pass
            elif token in [KotlinParser.TAILREC, KotlinParser.OPERATOR, KotlinParser.INLINE, KotlinParser.INFIX, KotlinParser.EXTERNAL, KotlinParser.SUSPEND]:
                self.state = 3428
                self.functionModifier()
                pass
            elif token in [KotlinParser.CONST]:
                self.state = 3429
                self.propertyModifier()
                pass
            elif token in [KotlinParser.ABSTRACT, KotlinParser.FINAL, KotlinParser.OPEN]:
                self.state = 3430
                self.inheritanceModifier()
                pass
            elif token in [KotlinParser.VARARG, KotlinParser.NOINLINE, KotlinParser.CROSSINLINE]:
                self.state = 3431
                self.parameterModifier()
                pass
            elif token in [KotlinParser.EXPECT, KotlinParser.ACTUAL]:
                self.state = 3432
                self.platformModifier()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 3438
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,530,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 3435
                    self.match(KotlinParser.NL) 
                self.state = 3440
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,530,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeModifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typeModifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeModifierContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeModifierContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_typeModifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeModifiers" ):
                listener.enterTypeModifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeModifiers" ):
                listener.exitTypeModifiers(self)




    def typeModifiers(self):

        localctx = KotlinParser.TypeModifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 302, self.RULE_typeModifiers)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3442 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 3441
                    self.typeModifier()

                else:
                    raise NoViableAltException(self)
                self.state = 3444 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,531,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotation(self):
            return self.getTypedRuleContext(KotlinParser.AnnotationContext,0)


        def SUSPEND(self):
            return self.getToken(KotlinParser.SUSPEND, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_typeModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeModifier" ):
                listener.enterTypeModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeModifier" ):
                listener.exitTypeModifier(self)




    def typeModifier(self):

        localctx = KotlinParser.TypeModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 304, self.RULE_typeModifier)
        self._la = 0 # Token type
        try:
            self.state = 3454
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS]:
                self.enterOuterAlt(localctx, 1)
                self.state = 3446
                self.annotation()
                pass
            elif token in [KotlinParser.SUSPEND]:
                self.enterOuterAlt(localctx, 2)
                self.state = 3447
                self.match(KotlinParser.SUSPEND)
                self.state = 3451
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3448
                    self.match(KotlinParser.NL)
                    self.state = 3453
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClassModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ENUM(self):
            return self.getToken(KotlinParser.ENUM, 0)

        def SEALED(self):
            return self.getToken(KotlinParser.SEALED, 0)

        def ANNOTATION(self):
            return self.getToken(KotlinParser.ANNOTATION, 0)

        def DATA(self):
            return self.getToken(KotlinParser.DATA, 0)

        def INNER(self):
            return self.getToken(KotlinParser.INNER, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_classModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassModifier" ):
                listener.enterClassModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassModifier" ):
                listener.exitClassModifier(self)




    def classModifier(self):

        localctx = KotlinParser.ClassModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 306, self.RULE_classModifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3456
            _la = self._input.LA(1)
            if not(((((_la - 111)) & ~0x3f) == 0 and ((1 << (_la - 111)) & ((1 << (KotlinParser.ENUM - 111)) | (1 << (KotlinParser.SEALED - 111)) | (1 << (KotlinParser.ANNOTATION - 111)) | (1 << (KotlinParser.DATA - 111)) | (1 << (KotlinParser.INNER - 111)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MemberModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OVERRIDE(self):
            return self.getToken(KotlinParser.OVERRIDE, 0)

        def LATEINIT(self):
            return self.getToken(KotlinParser.LATEINIT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_memberModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMemberModifier" ):
                listener.enterMemberModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMemberModifier" ):
                listener.exitMemberModifier(self)




    def memberModifier(self):

        localctx = KotlinParser.MemberModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 308, self.RULE_memberModifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3458
            _la = self._input.LA(1)
            if not(_la==KotlinParser.OVERRIDE or _la==KotlinParser.LATEINIT):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VisibilityModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PUBLIC(self):
            return self.getToken(KotlinParser.PUBLIC, 0)

        def PRIVATE(self):
            return self.getToken(KotlinParser.PRIVATE, 0)

        def INTERNAL(self):
            return self.getToken(KotlinParser.INTERNAL, 0)

        def PROTECTED(self):
            return self.getToken(KotlinParser.PROTECTED, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_visibilityModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVisibilityModifier" ):
                listener.enterVisibilityModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVisibilityModifier" ):
                listener.exitVisibilityModifier(self)




    def visibilityModifier(self):

        localctx = KotlinParser.VisibilityModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 310, self.RULE_visibilityModifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3460
            _la = self._input.LA(1)
            if not(((((_la - 107)) & ~0x3f) == 0 and ((1 << (_la - 107)) & ((1 << (KotlinParser.PUBLIC - 107)) | (1 << (KotlinParser.PRIVATE - 107)) | (1 << (KotlinParser.PROTECTED - 107)) | (1 << (KotlinParser.INTERNAL - 107)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarianceModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IN(self):
            return self.getToken(KotlinParser.IN, 0)

        def OUT(self):
            return self.getToken(KotlinParser.OUT, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_varianceModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarianceModifier" ):
                listener.enterVarianceModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarianceModifier" ):
                listener.exitVarianceModifier(self)




    def varianceModifier(self):

        localctx = KotlinParser.VarianceModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 312, self.RULE_varianceModifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3462
            _la = self._input.LA(1)
            if not(_la==KotlinParser.IN or _la==KotlinParser.OUT):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeParameterModifiersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typeParameterModifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.TypeParameterModifierContext)
            else:
                return self.getTypedRuleContext(KotlinParser.TypeParameterModifierContext,i)


        def getRuleIndex(self):
            return KotlinParser.RULE_typeParameterModifiers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeParameterModifiers" ):
                listener.enterTypeParameterModifiers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeParameterModifiers" ):
                listener.exitTypeParameterModifiers(self)




    def typeParameterModifiers(self):

        localctx = KotlinParser.TypeParameterModifiersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 314, self.RULE_typeParameterModifiers)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3465 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 3464
                    self.typeParameterModifier()

                else:
                    raise NoViableAltException(self)
                self.state = 3467 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,534,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeParameterModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def reificationModifier(self):
            return self.getTypedRuleContext(KotlinParser.ReificationModifierContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def varianceModifier(self):
            return self.getTypedRuleContext(KotlinParser.VarianceModifierContext,0)


        def annotation(self):
            return self.getTypedRuleContext(KotlinParser.AnnotationContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_typeParameterModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeParameterModifier" ):
                listener.enterTypeParameterModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeParameterModifier" ):
                listener.exitTypeParameterModifier(self)




    def typeParameterModifier(self):

        localctx = KotlinParser.TypeParameterModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 316, self.RULE_typeParameterModifier)
        try:
            self.state = 3484
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [KotlinParser.REIFIED]:
                self.enterOuterAlt(localctx, 1)
                self.state = 3469
                self.reificationModifier()
                self.state = 3473
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,535,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3470
                        self.match(KotlinParser.NL) 
                    self.state = 3475
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,535,self._ctx)

                pass
            elif token in [KotlinParser.IN, KotlinParser.OUT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 3476
                self.varianceModifier()
                self.state = 3480
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,536,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 3477
                        self.match(KotlinParser.NL) 
                    self.state = 3482
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,536,self._ctx)

                pass
            elif token in [KotlinParser.AT_NO_WS, KotlinParser.AT_PRE_WS]:
                self.enterOuterAlt(localctx, 3)
                self.state = 3483
                self.annotation()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TAILREC(self):
            return self.getToken(KotlinParser.TAILREC, 0)

        def OPERATOR(self):
            return self.getToken(KotlinParser.OPERATOR, 0)

        def INFIX(self):
            return self.getToken(KotlinParser.INFIX, 0)

        def INLINE(self):
            return self.getToken(KotlinParser.INLINE, 0)

        def EXTERNAL(self):
            return self.getToken(KotlinParser.EXTERNAL, 0)

        def SUSPEND(self):
            return self.getToken(KotlinParser.SUSPEND, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_functionModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionModifier" ):
                listener.enterFunctionModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionModifier" ):
                listener.exitFunctionModifier(self)




    def functionModifier(self):

        localctx = KotlinParser.FunctionModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 318, self.RULE_functionModifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3486
            _la = self._input.LA(1)
            if not(((((_la - 116)) & ~0x3f) == 0 and ((1 << (_la - 116)) & ((1 << (KotlinParser.TAILREC - 116)) | (1 << (KotlinParser.OPERATOR - 116)) | (1 << (KotlinParser.INLINE - 116)) | (1 << (KotlinParser.INFIX - 116)) | (1 << (KotlinParser.EXTERNAL - 116)) | (1 << (KotlinParser.SUSPEND - 116)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PropertyModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONST(self):
            return self.getToken(KotlinParser.CONST, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_propertyModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPropertyModifier" ):
                listener.enterPropertyModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPropertyModifier" ):
                listener.exitPropertyModifier(self)




    def propertyModifier(self):

        localctx = KotlinParser.PropertyModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 320, self.RULE_propertyModifier)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3488
            self.match(KotlinParser.CONST)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InheritanceModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ABSTRACT(self):
            return self.getToken(KotlinParser.ABSTRACT, 0)

        def FINAL(self):
            return self.getToken(KotlinParser.FINAL, 0)

        def OPEN(self):
            return self.getToken(KotlinParser.OPEN, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_inheritanceModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInheritanceModifier" ):
                listener.enterInheritanceModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInheritanceModifier" ):
                listener.exitInheritanceModifier(self)




    def inheritanceModifier(self):

        localctx = KotlinParser.InheritanceModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 322, self.RULE_inheritanceModifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3490
            _la = self._input.LA(1)
            if not(((((_la - 123)) & ~0x3f) == 0 and ((1 << (_la - 123)) & ((1 << (KotlinParser.ABSTRACT - 123)) | (1 << (KotlinParser.FINAL - 123)) | (1 << (KotlinParser.OPEN - 123)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARARG(self):
            return self.getToken(KotlinParser.VARARG, 0)

        def NOINLINE(self):
            return self.getToken(KotlinParser.NOINLINE, 0)

        def CROSSINLINE(self):
            return self.getToken(KotlinParser.CROSSINLINE, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_parameterModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameterModifier" ):
                listener.enterParameterModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameterModifier" ):
                listener.exitParameterModifier(self)




    def parameterModifier(self):

        localctx = KotlinParser.ParameterModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 324, self.RULE_parameterModifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3492
            _la = self._input.LA(1)
            if not(((((_la - 128)) & ~0x3f) == 0 and ((1 << (_la - 128)) & ((1 << (KotlinParser.VARARG - 128)) | (1 << (KotlinParser.NOINLINE - 128)) | (1 << (KotlinParser.CROSSINLINE - 128)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReificationModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def REIFIED(self):
            return self.getToken(KotlinParser.REIFIED, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_reificationModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReificationModifier" ):
                listener.enterReificationModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReificationModifier" ):
                listener.exitReificationModifier(self)




    def reificationModifier(self):

        localctx = KotlinParser.ReificationModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 326, self.RULE_reificationModifier)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3494
            self.match(KotlinParser.REIFIED)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PlatformModifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EXPECT(self):
            return self.getToken(KotlinParser.EXPECT, 0)

        def ACTUAL(self):
            return self.getToken(KotlinParser.ACTUAL, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_platformModifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPlatformModifier" ):
                listener.enterPlatformModifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPlatformModifier" ):
                listener.exitPlatformModifier(self)




    def platformModifier(self):

        localctx = KotlinParser.PlatformModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 328, self.RULE_platformModifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3496
            _la = self._input.LA(1)
            if not(_la==KotlinParser.EXPECT or _la==KotlinParser.ACTUAL):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnnotationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def singleAnnotation(self):
            return self.getTypedRuleContext(KotlinParser.SingleAnnotationContext,0)


        def multiAnnotation(self):
            return self.getTypedRuleContext(KotlinParser.MultiAnnotationContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_annotation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnnotation" ):
                listener.enterAnnotation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnnotation" ):
                listener.exitAnnotation(self)




    def annotation(self):

        localctx = KotlinParser.AnnotationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 330, self.RULE_annotation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3500
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,538,self._ctx)
            if la_ == 1:
                self.state = 3498
                self.singleAnnotation()
                pass

            elif la_ == 2:
                self.state = 3499
                self.multiAnnotation()
                pass


            self.state = 3505
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,539,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 3502
                    self.match(KotlinParser.NL) 
                self.state = 3507
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,539,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SingleAnnotationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotationUseSiteTarget(self):
            return self.getTypedRuleContext(KotlinParser.AnnotationUseSiteTargetContext,0)


        def unescapedAnnotation(self):
            return self.getTypedRuleContext(KotlinParser.UnescapedAnnotationContext,0)


        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def AT_NO_WS(self):
            return self.getToken(KotlinParser.AT_NO_WS, 0)

        def AT_PRE_WS(self):
            return self.getToken(KotlinParser.AT_PRE_WS, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_singleAnnotation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSingleAnnotation" ):
                listener.enterSingleAnnotation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSingleAnnotation" ):
                listener.exitSingleAnnotation(self)




    def singleAnnotation(self):

        localctx = KotlinParser.SingleAnnotationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 332, self.RULE_singleAnnotation)
        self._la = 0 # Token type
        try:
            self.state = 3519
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,541,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 3508
                self.annotationUseSiteTarget()
                self.state = 3512
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3509
                    self.match(KotlinParser.NL)
                    self.state = 3514
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3515
                self.unescapedAnnotation()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 3517
                _la = self._input.LA(1)
                if not(_la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 3518
                self.unescapedAnnotation()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultiAnnotationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotationUseSiteTarget(self):
            return self.getTypedRuleContext(KotlinParser.AnnotationUseSiteTargetContext,0)


        def LSQUARE(self):
            return self.getToken(KotlinParser.LSQUARE, 0)

        def RSQUARE(self):
            return self.getToken(KotlinParser.RSQUARE, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def unescapedAnnotation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.UnescapedAnnotationContext)
            else:
                return self.getTypedRuleContext(KotlinParser.UnescapedAnnotationContext,i)


        def AT_NO_WS(self):
            return self.getToken(KotlinParser.AT_NO_WS, 0)

        def AT_PRE_WS(self):
            return self.getToken(KotlinParser.AT_PRE_WS, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_multiAnnotation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultiAnnotation" ):
                listener.enterMultiAnnotation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultiAnnotation" ):
                listener.exitMultiAnnotation(self)




    def multiAnnotation(self):

        localctx = KotlinParser.MultiAnnotationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 334, self.RULE_multiAnnotation)
        self._la = 0 # Token type
        try:
            self.state = 3545
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,545,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 3521
                self.annotationUseSiteTarget()
                self.state = 3525
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==KotlinParser.NL:
                    self.state = 3522
                    self.match(KotlinParser.NL)
                    self.state = 3527
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 3528
                self.match(KotlinParser.LSQUARE)
                self.state = 3530 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 3529
                    self.unescapedAnnotation()
                    self.state = 3532 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (((((_la - 61)) & ~0x3f) == 0 and ((1 << (_la - 61)) & ((1 << (KotlinParser.FILE - 61)) | (1 << (KotlinParser.FIELD - 61)) | (1 << (KotlinParser.PROPERTY - 61)) | (1 << (KotlinParser.GET - 61)) | (1 << (KotlinParser.SET - 61)) | (1 << (KotlinParser.RECEIVER - 61)) | (1 << (KotlinParser.PARAM - 61)) | (1 << (KotlinParser.SETPARAM - 61)) | (1 << (KotlinParser.DELEGATE - 61)) | (1 << (KotlinParser.IMPORT - 61)) | (1 << (KotlinParser.CONSTRUCTOR - 61)) | (1 << (KotlinParser.BY - 61)) | (1 << (KotlinParser.COMPANION - 61)) | (1 << (KotlinParser.INIT - 61)) | (1 << (KotlinParser.WHERE - 61)) | (1 << (KotlinParser.CATCH - 61)) | (1 << (KotlinParser.FINALLY - 61)) | (1 << (KotlinParser.OUT - 61)) | (1 << (KotlinParser.DYNAMIC - 61)) | (1 << (KotlinParser.PUBLIC - 61)) | (1 << (KotlinParser.PRIVATE - 61)) | (1 << (KotlinParser.PROTECTED - 61)) | (1 << (KotlinParser.INTERNAL - 61)) | (1 << (KotlinParser.ENUM - 61)) | (1 << (KotlinParser.SEALED - 61)) | (1 << (KotlinParser.ANNOTATION - 61)) | (1 << (KotlinParser.DATA - 61)) | (1 << (KotlinParser.INNER - 61)) | (1 << (KotlinParser.TAILREC - 61)) | (1 << (KotlinParser.OPERATOR - 61)) | (1 << (KotlinParser.INLINE - 61)) | (1 << (KotlinParser.INFIX - 61)) | (1 << (KotlinParser.EXTERNAL - 61)) | (1 << (KotlinParser.SUSPEND - 61)) | (1 << (KotlinParser.OVERRIDE - 61)) | (1 << (KotlinParser.ABSTRACT - 61)) | (1 << (KotlinParser.FINAL - 61)))) != 0) or ((((_la - 125)) & ~0x3f) == 0 and ((1 << (_la - 125)) & ((1 << (KotlinParser.OPEN - 125)) | (1 << (KotlinParser.CONST - 125)) | (1 << (KotlinParser.LATEINIT - 125)) | (1 << (KotlinParser.VARARG - 125)) | (1 << (KotlinParser.NOINLINE - 125)) | (1 << (KotlinParser.CROSSINLINE - 125)) | (1 << (KotlinParser.REIFIED - 125)) | (1 << (KotlinParser.EXPECT - 125)) | (1 << (KotlinParser.ACTUAL - 125)) | (1 << (KotlinParser.Identifier - 125)))) != 0)):
                        break

                self.state = 3534
                self.match(KotlinParser.RSQUARE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 3536
                _la = self._input.LA(1)
                if not(_la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 3537
                self.match(KotlinParser.LSQUARE)
                self.state = 3539 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 3538
                    self.unescapedAnnotation()
                    self.state = 3541 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (((((_la - 61)) & ~0x3f) == 0 and ((1 << (_la - 61)) & ((1 << (KotlinParser.FILE - 61)) | (1 << (KotlinParser.FIELD - 61)) | (1 << (KotlinParser.PROPERTY - 61)) | (1 << (KotlinParser.GET - 61)) | (1 << (KotlinParser.SET - 61)) | (1 << (KotlinParser.RECEIVER - 61)) | (1 << (KotlinParser.PARAM - 61)) | (1 << (KotlinParser.SETPARAM - 61)) | (1 << (KotlinParser.DELEGATE - 61)) | (1 << (KotlinParser.IMPORT - 61)) | (1 << (KotlinParser.CONSTRUCTOR - 61)) | (1 << (KotlinParser.BY - 61)) | (1 << (KotlinParser.COMPANION - 61)) | (1 << (KotlinParser.INIT - 61)) | (1 << (KotlinParser.WHERE - 61)) | (1 << (KotlinParser.CATCH - 61)) | (1 << (KotlinParser.FINALLY - 61)) | (1 << (KotlinParser.OUT - 61)) | (1 << (KotlinParser.DYNAMIC - 61)) | (1 << (KotlinParser.PUBLIC - 61)) | (1 << (KotlinParser.PRIVATE - 61)) | (1 << (KotlinParser.PROTECTED - 61)) | (1 << (KotlinParser.INTERNAL - 61)) | (1 << (KotlinParser.ENUM - 61)) | (1 << (KotlinParser.SEALED - 61)) | (1 << (KotlinParser.ANNOTATION - 61)) | (1 << (KotlinParser.DATA - 61)) | (1 << (KotlinParser.INNER - 61)) | (1 << (KotlinParser.TAILREC - 61)) | (1 << (KotlinParser.OPERATOR - 61)) | (1 << (KotlinParser.INLINE - 61)) | (1 << (KotlinParser.INFIX - 61)) | (1 << (KotlinParser.EXTERNAL - 61)) | (1 << (KotlinParser.SUSPEND - 61)) | (1 << (KotlinParser.OVERRIDE - 61)) | (1 << (KotlinParser.ABSTRACT - 61)) | (1 << (KotlinParser.FINAL - 61)))) != 0) or ((((_la - 125)) & ~0x3f) == 0 and ((1 << (_la - 125)) & ((1 << (KotlinParser.OPEN - 125)) | (1 << (KotlinParser.CONST - 125)) | (1 << (KotlinParser.LATEINIT - 125)) | (1 << (KotlinParser.VARARG - 125)) | (1 << (KotlinParser.NOINLINE - 125)) | (1 << (KotlinParser.CROSSINLINE - 125)) | (1 << (KotlinParser.REIFIED - 125)) | (1 << (KotlinParser.EXPECT - 125)) | (1 << (KotlinParser.ACTUAL - 125)) | (1 << (KotlinParser.Identifier - 125)))) != 0)):
                        break

                self.state = 3543
                self.match(KotlinParser.RSQUARE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnnotationUseSiteTargetContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def COLON(self):
            return self.getToken(KotlinParser.COLON, 0)

        def AT_NO_WS(self):
            return self.getToken(KotlinParser.AT_NO_WS, 0)

        def AT_PRE_WS(self):
            return self.getToken(KotlinParser.AT_PRE_WS, 0)

        def FIELD(self):
            return self.getToken(KotlinParser.FIELD, 0)

        def PROPERTY(self):
            return self.getToken(KotlinParser.PROPERTY, 0)

        def GET(self):
            return self.getToken(KotlinParser.GET, 0)

        def SET(self):
            return self.getToken(KotlinParser.SET, 0)

        def RECEIVER(self):
            return self.getToken(KotlinParser.RECEIVER, 0)

        def PARAM(self):
            return self.getToken(KotlinParser.PARAM, 0)

        def SETPARAM(self):
            return self.getToken(KotlinParser.SETPARAM, 0)

        def DELEGATE(self):
            return self.getToken(KotlinParser.DELEGATE, 0)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_annotationUseSiteTarget

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnnotationUseSiteTarget" ):
                listener.enterAnnotationUseSiteTarget(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnnotationUseSiteTarget" ):
                listener.exitAnnotationUseSiteTarget(self)




    def annotationUseSiteTarget(self):

        localctx = KotlinParser.AnnotationUseSiteTargetContext(self, self._ctx, self.state)
        self.enterRule(localctx, 336, self.RULE_annotationUseSiteTarget)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3547
            _la = self._input.LA(1)
            if not(_la==KotlinParser.AT_NO_WS or _la==KotlinParser.AT_PRE_WS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 3548
            _la = self._input.LA(1)
            if not(((((_la - 62)) & ~0x3f) == 0 and ((1 << (_la - 62)) & ((1 << (KotlinParser.FIELD - 62)) | (1 << (KotlinParser.PROPERTY - 62)) | (1 << (KotlinParser.GET - 62)) | (1 << (KotlinParser.SET - 62)) | (1 << (KotlinParser.RECEIVER - 62)) | (1 << (KotlinParser.PARAM - 62)) | (1 << (KotlinParser.SETPARAM - 62)) | (1 << (KotlinParser.DELEGATE - 62)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 3552
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==KotlinParser.NL:
                self.state = 3549
                self.match(KotlinParser.NL)
                self.state = 3554
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 3555
            self.match(KotlinParser.COLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnescapedAnnotationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def constructorInvocation(self):
            return self.getTypedRuleContext(KotlinParser.ConstructorInvocationContext,0)


        def userType(self):
            return self.getTypedRuleContext(KotlinParser.UserTypeContext,0)


        def getRuleIndex(self):
            return KotlinParser.RULE_unescapedAnnotation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnescapedAnnotation" ):
                listener.enterUnescapedAnnotation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnescapedAnnotation" ):
                listener.exitUnescapedAnnotation(self)




    def unescapedAnnotation(self):

        localctx = KotlinParser.UnescapedAnnotationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 338, self.RULE_unescapedAnnotation)
        try:
            self.state = 3559
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,547,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 3557
                self.constructorInvocation()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 3558
                self.userType()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SimpleIdentifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(KotlinParser.Identifier, 0)

        def ABSTRACT(self):
            return self.getToken(KotlinParser.ABSTRACT, 0)

        def ANNOTATION(self):
            return self.getToken(KotlinParser.ANNOTATION, 0)

        def BY(self):
            return self.getToken(KotlinParser.BY, 0)

        def CATCH(self):
            return self.getToken(KotlinParser.CATCH, 0)

        def COMPANION(self):
            return self.getToken(KotlinParser.COMPANION, 0)

        def CONSTRUCTOR(self):
            return self.getToken(KotlinParser.CONSTRUCTOR, 0)

        def CROSSINLINE(self):
            return self.getToken(KotlinParser.CROSSINLINE, 0)

        def DATA(self):
            return self.getToken(KotlinParser.DATA, 0)

        def DYNAMIC(self):
            return self.getToken(KotlinParser.DYNAMIC, 0)

        def ENUM(self):
            return self.getToken(KotlinParser.ENUM, 0)

        def EXTERNAL(self):
            return self.getToken(KotlinParser.EXTERNAL, 0)

        def FINAL(self):
            return self.getToken(KotlinParser.FINAL, 0)

        def FINALLY(self):
            return self.getToken(KotlinParser.FINALLY, 0)

        def GET(self):
            return self.getToken(KotlinParser.GET, 0)

        def IMPORT(self):
            return self.getToken(KotlinParser.IMPORT, 0)

        def INFIX(self):
            return self.getToken(KotlinParser.INFIX, 0)

        def INIT(self):
            return self.getToken(KotlinParser.INIT, 0)

        def INLINE(self):
            return self.getToken(KotlinParser.INLINE, 0)

        def INNER(self):
            return self.getToken(KotlinParser.INNER, 0)

        def INTERNAL(self):
            return self.getToken(KotlinParser.INTERNAL, 0)

        def LATEINIT(self):
            return self.getToken(KotlinParser.LATEINIT, 0)

        def NOINLINE(self):
            return self.getToken(KotlinParser.NOINLINE, 0)

        def OPEN(self):
            return self.getToken(KotlinParser.OPEN, 0)

        def OPERATOR(self):
            return self.getToken(KotlinParser.OPERATOR, 0)

        def OUT(self):
            return self.getToken(KotlinParser.OUT, 0)

        def OVERRIDE(self):
            return self.getToken(KotlinParser.OVERRIDE, 0)

        def PRIVATE(self):
            return self.getToken(KotlinParser.PRIVATE, 0)

        def PROTECTED(self):
            return self.getToken(KotlinParser.PROTECTED, 0)

        def PUBLIC(self):
            return self.getToken(KotlinParser.PUBLIC, 0)

        def REIFIED(self):
            return self.getToken(KotlinParser.REIFIED, 0)

        def SEALED(self):
            return self.getToken(KotlinParser.SEALED, 0)

        def TAILREC(self):
            return self.getToken(KotlinParser.TAILREC, 0)

        def SET(self):
            return self.getToken(KotlinParser.SET, 0)

        def VARARG(self):
            return self.getToken(KotlinParser.VARARG, 0)

        def WHERE(self):
            return self.getToken(KotlinParser.WHERE, 0)

        def FIELD(self):
            return self.getToken(KotlinParser.FIELD, 0)

        def PROPERTY(self):
            return self.getToken(KotlinParser.PROPERTY, 0)

        def RECEIVER(self):
            return self.getToken(KotlinParser.RECEIVER, 0)

        def PARAM(self):
            return self.getToken(KotlinParser.PARAM, 0)

        def SETPARAM(self):
            return self.getToken(KotlinParser.SETPARAM, 0)

        def DELEGATE(self):
            return self.getToken(KotlinParser.DELEGATE, 0)

        def FILE(self):
            return self.getToken(KotlinParser.FILE, 0)

        def EXPECT(self):
            return self.getToken(KotlinParser.EXPECT, 0)

        def ACTUAL(self):
            return self.getToken(KotlinParser.ACTUAL, 0)

        def CONST(self):
            return self.getToken(KotlinParser.CONST, 0)

        def SUSPEND(self):
            return self.getToken(KotlinParser.SUSPEND, 0)

        def getRuleIndex(self):
            return KotlinParser.RULE_simpleIdentifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSimpleIdentifier" ):
                listener.enterSimpleIdentifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSimpleIdentifier" ):
                listener.exitSimpleIdentifier(self)




    def simpleIdentifier(self):

        localctx = KotlinParser.SimpleIdentifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 340, self.RULE_simpleIdentifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3561
            _la = self._input.LA(1)
            if not(((((_la - 61)) & ~0x3f) == 0 and ((1 << (_la - 61)) & ((1 << (KotlinParser.FILE - 61)) | (1 << (KotlinParser.FIELD - 61)) | (1 << (KotlinParser.PROPERTY - 61)) | (1 << (KotlinParser.GET - 61)) | (1 << (KotlinParser.SET - 61)) | (1 << (KotlinParser.RECEIVER - 61)) | (1 << (KotlinParser.PARAM - 61)) | (1 << (KotlinParser.SETPARAM - 61)) | (1 << (KotlinParser.DELEGATE - 61)) | (1 << (KotlinParser.IMPORT - 61)) | (1 << (KotlinParser.CONSTRUCTOR - 61)) | (1 << (KotlinParser.BY - 61)) | (1 << (KotlinParser.COMPANION - 61)) | (1 << (KotlinParser.INIT - 61)) | (1 << (KotlinParser.WHERE - 61)) | (1 << (KotlinParser.CATCH - 61)) | (1 << (KotlinParser.FINALLY - 61)) | (1 << (KotlinParser.OUT - 61)) | (1 << (KotlinParser.DYNAMIC - 61)) | (1 << (KotlinParser.PUBLIC - 61)) | (1 << (KotlinParser.PRIVATE - 61)) | (1 << (KotlinParser.PROTECTED - 61)) | (1 << (KotlinParser.INTERNAL - 61)) | (1 << (KotlinParser.ENUM - 61)) | (1 << (KotlinParser.SEALED - 61)) | (1 << (KotlinParser.ANNOTATION - 61)) | (1 << (KotlinParser.DATA - 61)) | (1 << (KotlinParser.INNER - 61)) | (1 << (KotlinParser.TAILREC - 61)) | (1 << (KotlinParser.OPERATOR - 61)) | (1 << (KotlinParser.INLINE - 61)) | (1 << (KotlinParser.INFIX - 61)) | (1 << (KotlinParser.EXTERNAL - 61)) | (1 << (KotlinParser.SUSPEND - 61)) | (1 << (KotlinParser.OVERRIDE - 61)) | (1 << (KotlinParser.ABSTRACT - 61)) | (1 << (KotlinParser.FINAL - 61)))) != 0) or ((((_la - 125)) & ~0x3f) == 0 and ((1 << (_la - 125)) & ((1 << (KotlinParser.OPEN - 125)) | (1 << (KotlinParser.CONST - 125)) | (1 << (KotlinParser.LATEINIT - 125)) | (1 << (KotlinParser.VARARG - 125)) | (1 << (KotlinParser.NOINLINE - 125)) | (1 << (KotlinParser.CROSSINLINE - 125)) | (1 << (KotlinParser.REIFIED - 125)) | (1 << (KotlinParser.EXPECT - 125)) | (1 << (KotlinParser.ACTUAL - 125)) | (1 << (KotlinParser.Identifier - 125)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IdentifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def simpleIdentifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(KotlinParser.SimpleIdentifierContext)
            else:
                return self.getTypedRuleContext(KotlinParser.SimpleIdentifierContext,i)


        def DOT(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.DOT)
            else:
                return self.getToken(KotlinParser.DOT, i)

        def NL(self, i:int=None):
            if i is None:
                return self.getTokens(KotlinParser.NL)
            else:
                return self.getToken(KotlinParser.NL, i)

        def getRuleIndex(self):
            return KotlinParser.RULE_identifier

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIdentifier" ):
                listener.enterIdentifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIdentifier" ):
                listener.exitIdentifier(self)




    def identifier(self):

        localctx = KotlinParser.IdentifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 342, self.RULE_identifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 3563
            self.simpleIdentifier()
            self.state = 3574
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,549,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 3567
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==KotlinParser.NL:
                        self.state = 3564
                        self.match(KotlinParser.NL)
                        self.state = 3569
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)

                    self.state = 3570
                    self.match(KotlinParser.DOT)
                    self.state = 3571
                    self.simpleIdentifier() 
                self.state = 3576
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,549,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





