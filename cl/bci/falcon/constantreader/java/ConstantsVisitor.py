import re

from cl.bci.falcon.constantreader.java.Java8Parser import Java8Parser
from cl.bci.falcon.constantreader.java.Java8ParserListener import Java8ParserListener


class ConstantsVisitor(Java8ParserListener):

    def __init__(self, package_name):
        self.package_name = package_name
        self.__imports = []
        self.__constante = {}

    def enterSingleStaticImportDeclaration(self, ctx: Java8Parser.SingleStaticImportDeclarationContext):
        self.__imports.append('%s.%s' % (ctx.typeName().getText(), ctx.Identifier().getText()))

    def enterVariableDeclarator(self, ctx: Java8Parser.VariableDeclaratorContext):
        parent_classes = self.get_parent_classes(ctx)
        contante = ctx.variableDeclaratorId().getText()
        value = ctx.variableInitializer().getText()
        full_path = self.package_name + '.' + parent_classes + contante
        self.__constante[full_path] = value

    def get_parent_classes(self, ctx: Java8Parser.VariableDeclaratorContext):
        constante = ''
        parentCtx = ctx.parentCtx
        while parentCtx:
            parentCtx = parentCtx.parentCtx
            if isinstance(parentCtx, (Java8Parser.NormalClassDeclarationContext,
                                      Java8Parser.NormalInterfaceDeclarationContext)):
                constante = f'{parentCtx.Identifier().getText()}.{constante}'
        return constante

    def get_constants(self, network_module_constant):
        self.__constante.update(network_module_constant)
        for constant, value in self.__constante.items():
            self.__constante[constant] = self.resolve_constant_value(value)
        return self.__constante

    def resolve_constant_value(self, value):
        pattern = r'[A-Z]{2,}(?:_[A-Z]+)*'
        matches = re.findall(pattern, value)
        for match in matches:
            posible_matches = self.find_constantes(match)
            if posible_matches:
                value = re.sub(r'\b' + match + r'\b', posible_matches[0], value)
            else:
                import_matches = self.find_imports(match)
                if import_matches:
                    value = re.sub(r'\b' + match + r'\b', '>>>>{%s}<<<<' % import_matches[0], value)
        value = re.sub(r'"|\+|\s', '', value)
        return value

    def set_constante(self, constante):
        self.__constante = constante

    def find_constantes(self, match):
        result = []
        for key in self.__constante:
            if str(key).endswith('.%s' % match):
                result.append(self.__constante[key])
        return result

    def find_imports(self, match):
        result = []
        for item in self.__imports:
            if str(item).endswith('.%s' % match):
                result.append(item)
        return result

