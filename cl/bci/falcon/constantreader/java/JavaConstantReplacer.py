import re


class JavaConstantReplacer:

    def __init__(self, constante):
        self.constante = constante

    def replace(self, value):
        pattern = r'[A-Z]{2,}(?:_[A-Z]+)*'
        matches = re.findall(pattern, value)
        for match in matches:
            posible_matches = self.find_constantes(match)
            if posible_matches:
                value = re.sub(r'\b' + match + r'\b', posible_matches[0], value)
            else:
                import_matches = self.find_imports(match)
                if import_matches:
                    value = re.sub(r'\b' + match + r'\b', '>>>>{%s}<<<<' % import_matches[0], value)
        value = re.sub(r'"|\+|\s', '', value)
        return value

    def find_constantes(self, match):
        result = []
        for key in self.constante:
            if str(key).endswith('.%s' % match):
                result.append(self.constante[key])
        return result

    def find_imports(self, match):
        result = []
        for item in self.__imports:
            if str(item).endswith('.%s' % match):
                result.append(item)
        return result

