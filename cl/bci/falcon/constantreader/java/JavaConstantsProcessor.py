import sys

from antlr4 import *

from cl.bci.falcon.constantreader.AbstractConstantProcessor import AbstractConstantProcessor
from cl.bci.falcon.constantreader.java.ConstantsVisitor import ConstantsVisitor
from cl.bci.falcon.constantreader.java.Java8Lexer import Java8Lexer
from cl.bci.falcon.constantreader.java.Java8Parser import Java8Parser


class JavaConstantsProcessor(AbstractConstantProcessor):

    def __init__(self, network_module_constant):
        self.__network_module_constant = network_module_constant

    def process(self, path):
        input_stream = FileStream(path, 'utf-8')
        lexer = Java8Lexer(input_stream)
        stream = CommonTokenStream(lexer)
        parser = Java8Parser(stream)
        tree = parser.compilationUnit()
        package_name = tree.packageDeclaration().children[1].getText()

        visitor = ConstantsVisitor(package_name)
        walker = ParseTreeWalker()
        walker.walk(visitor, tree)
        return visitor.get_constants(self.__network_module_constant)


if __name__ == '__main__':
    file_path = sys.argv[1]
    JavaConstantsProcessor().process(file_path)
