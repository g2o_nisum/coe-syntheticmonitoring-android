import re

from cl.bci.falcon.AndroidInfo import AndroidInfo
from cl.bci.falcon.constantreader.ModuleConstantReader import ModuleConstantReader
from cl.bci.falcon.constantreader.NetworkModuleConstantsReader import NetworkModuleConstantsReader


class ApplicationConstantReader:

    def __init__(self, android_info: AndroidInfo):
        self.android_info = android_info
        self.constants = {}

    def read(self):
        print(f'>>>>> Cargando constantes de la app')
        route = self.android_info.folder_route
        network_module_constant = NetworkModuleConstantsReader(route).read
        self.constants = ModuleConstantReader(route, network_module_constant).read()
        self.constants.update(network_module_constant)

        # kt_constant_replacer = KtConstantReplacer(self.constants)
        # java_constant_replacer = JavaConstantReplacer(self.constants)
        #
        # for constant, value in self.constants.items():
        #     self.constants[constant] = kt_constant_replacer.replace(value)
        #     self.constants[constant] = java_constant_replacer.replace(value)

        print(f'<<<<< Finalizada carga de constantes de la app')
        self.check_unresolved_value()

        return self.constants

    def check_unresolved_value(self):
        for key, value in self.constants.items():
            if '>>>' in value:
                group = re.search(r'>>>>\{(.*?)\}<<<<', value, re.DOTALL).group(1)
                value = re.sub(r'>>>>\{(.*?)\}<<<<', self.constants[group], value)
                print(">>> entro a check_unresolved_value")
                self.constants[key] = value


if __name__ == '__main__':
    test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
    info = AndroidInfo(test_route)
    read = ApplicationConstantReader(info).read()
    [print(f'{key}={value}') for key, value in read.items()]
