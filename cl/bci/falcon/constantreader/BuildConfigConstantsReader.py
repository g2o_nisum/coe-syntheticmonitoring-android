import re

from cl.bci.falcon.utils.fileutils import get_file_content

RELEASE_FLAVOR_PATTERN = r"release\s{([^}]*)}"
BUILD_CONFIG_FIELD_PATTERN = r"buildConfigField (.*?)\n"


class BuildConfigConstantsReader:

    def __init__(self, route):
        self.route = route

    def read(self):
        build_type_path = '{}/tag/app/buildTypes.gradle'.format(self.route)
        message = get_file_content(build_type_path)

        release_flavor = re.compile(RELEASE_FLAVOR_PATTERN, re.MULTILINE)
        release_variables = release_flavor.search(message)

        result = {}
        if release_variables:
            group = release_variables.group()
            variables = re.findall(BUILD_CONFIG_FIELD_PATTERN, group)
            for variable in variables:
                split_variable = [self.clean(item) for item in variable.split(',')]
                key = split_variable[1]
                value = self.find_declared_value(split_variable[2], message)
                result[key] = value

        return result

    def find_declared_value(self, value, message):
        build_config_field_pattern = r"def\s" + value + r"\s{0,10}=\s{0,10}\"\\\"(.*?)\\\"\"\n"
        search = re.search(build_config_field_pattern, message, re.DOTALL)
        if search:
            value = search.groups()[0]
            return self.clean(value)
        return value

    def clean(self, item):
        dirty_characters = ['"', ' ', '\'', ' ', '\\']
        clean = item
        for dirty in dirty_characters:
            clean = clean.replace(dirty, '')
        return clean
