from cl.bci.falcon.AndroidInfo import AndroidInfo
from cl.bci.falcon.VersionDownloader import VersionDownloader


class MonitoringVersionDownloader:

    def __init__(self, route):
        self.route = route

    def download(self, module_list):
        android_info = self.prepare_android_info_from_module_list(module_list)
        VersionDownloader(android_info).download_folders()
        return android_info

    def prepare_android_info_from_module_list(self, module_list):
        android_info = AndroidInfo(self.route)
        repository_dict = android_info.repository
        repository_info_dict = android_info.repository_info

        for k, v in repository_dict.items():
            repository_dict[k] = False

        for module in module_list:
            artifact = module['artifact']
            if artifact in repository_dict:
                repository_dict[artifact] = True
                repository_info_dict[artifact].branch = module['version']
        return android_info
