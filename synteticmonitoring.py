import os
import shutil
import sys

from cl.bci.falcon.AndroidInfo import AndroidInfo
from cl.bci.falcon.ApplicationRestEndpointFinder import ApplicationRestEndpointFinder
from cl.bci.falcon.Downloader import Downloader
from cl.bci.falcon.MonitoringVersionDownloader import MonitoringVersionDownloader
from cl.bci.falcon.TagInfo import TagInfo
from cl.bci.falcon.VersionExtractor import VersionExtractor

try:
    route = sys.argv[1]
except IndexError:
    route = "/Users/nisum/Work/Bci/CoE/Script/repos"

try:
    tag_version = sys.argv[2]
except IndexError:
    tag_version = "master"

print("\nPreparing folder...\n")
if os.path.exists(route):
    shutil.rmtree(route)
os.mkdir(route)

print("\nDescargar la version release tag\n")
tag_info = TagInfo(route, tag_version)
Downloader(tag_info).download_folders()

print("\n*** Ver las versiones de los modulos que se liberaron ***\n")
module_list = VersionExtractor(tag_info).extract()
[print(module) for module in module_list]

print("\n*** Buscar las ramas de los modulos que tienen la version ***\n")
android_info = MonitoringVersionDownloader(route).download(module_list)

print("\n*** Extraer enpoint declarados ***\n")
test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
info = AndroidInfo(test_route)
ApplicationRestEndpointFinder(info).find()

print("\n*** Llamar al API de Dynatrace ***\n")
