from unittest import TestCase

from cl.bci.falcon.AndroidInfo import AndroidInfo
from cl.bci.falcon.apiparser.RestResourceParser import RestResourceParser


class TestRestResourceParser(TestCase):

    def setUp(self):
        self.test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        self.android_info = AndroidInfo(self.test_route)
        module_constants = []
        self.sut = RestResourceParser(self.test_route, module_constants)

    def test_read(self):
        api_files = self.sut.read()
        self.assertTrue(api_files)
        [print(api_file) for api_file in api_files]

    def test_find_module_name(self):
        # given
        expected = 'recargas'
        module_path = '/Users/nisum/Work/Bci/CoE/Script/repos/app-android-recargas/recargas/src/main/java/'

        # when
        module = self.sut.find_module_name(module_path)

        # then
        self.assertEqual(expected, module)

    def test_find_method_name(self):
        # given
        file_dict = {'line': 'MOCK_LOGIN',
                     'method_name': ''}
        # when
        result = self.sut.resource_is_mock(file_dict)

        # then
        self.assertFalse(result)

    def test_given_ClientePFMApi_when_read_then_method_name_should_not_be_empty(self):
        # given
        file_path = 'ClientePFMApi.java'
        route = self.test_route + '/app-android-pfm/pfm/src/main/java/cl/bci/app/pfm/data/api/'
        # when
        api_files = self.sut.process_file(route, 'pfm', file_path)

        # then
        [print(api_file['method_name']) for api_file in api_files]

    def test_given_TarjetaDebitoApi_when_read_then_method_name_should_not_be_empty(self):
        # given
        file_path = 'TarjetaDebitoApi.kt'
        route = self.test_route + '/app-android-tarjetacredito/tarjetacredito/src/main/java/cl/bci/app/tarjetacredito/data/api/'
        # when
        api_files = self.sut.process_file(route, 'tarjetacredito', file_path)

        # then
        [print(api_file) for api_file in api_files]

    def test_find_alcanciadigital(self):
        android_info = self.android_info
        application_constants = []
        module = 'alcanciadigital'
        alcanciadigital = android_info.repository_info[module]
        path = "%s/%s" % (android_info.folder_route, alcanciadigital.folder_name)
        sut = RestResourceParser(self.test_route, application_constants)
        result = sut.read()

        [print(item) for item in result]

    def test_find_autenticacion(self):
        android_info = self.android_info
        application_constants = []
        module = 'autenticacion'
        autenticacion = android_info.repository_info[module]
        path = "%s/%s" % (android_info.folder_route, autenticacion.folder_name)
        sut = RestResourceParser(self.test_route, application_constants)
        result = sut.read()

        [print(item) for item in result]
