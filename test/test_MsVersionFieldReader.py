from unittest import TestCase

from cl.bci.falcon.apiparser.MsVersionFieldReader import MsVersionFieldReader
from cl.bci.falcon.constantreader.NetworkModuleConstantsReader import NetworkModuleConstantsReader
from cl.bci.falcon.utils.fileutils import get_file_content


class TestMsVersionFieldReader(TestCase):

    def setUp(self):
        self.test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        constants = NetworkModuleConstantsReader(self.test_route).read
        self.sut = MsVersionFieldReader(self.test_route, constants)

    def test_read_network_module(self):
        result = self.sut.read()
        print(result)

    def test_given_descuentosdatasourceremote_when_parse_then_retorna_una_lista_con_los_contantes_y_los_metodos_donde_se_utilizan(
            self):
        # given
        root = self.test_route + "/app-android-descuentos/descuentos/src/main/java/cl/bci/app/descuentos/data" \
                                 "/datasource/remote"
        file = 'DescuentosDataSourceRemote.java'
        expected_constant = "cl.bci.app.descuentos.commons.Constants.API.MS_VERSION"

        # when
        result = self.sut.process_file(root, file)

        # then
        self.assertTrue(result)
        # contains = self.assertContainsExpectedConstan(expected_constant, result)
        # self.assertTrue(contains)

    def assertContainsExpectedConstan(self, expected_constant, result):
        contains = next((elem for elem in result if expected_constant == elem['constante']), '')
        return contains

    def test_given_detallecategoriamovimientosdatasourceremote_when_parse_then_retorna_una_lista_con_los_contantes_y_los_metodos_donde_se_utilizan(
            self):
        # given
        root = self.test_route + "/app-android-pfm/pfm/src/main/java/cl/bci/app/pfm/data/datasource/remote/"
        file = 'DetalleCategoriaMovimientosDataSourceRemote.java'
        expected_constant = "cl.bci.app.pfm.commons.utils.Constants.API.RESUMEN_GASTOS_VERSION"

        # when
        result = self.sut.process_file(root, file)

        # then
        self.assertTrue(result)
        # self.assertContainsExpectedConstan(expected_constant, result)

    def test_get_method_which_use_field(self):
        # given
        root = self.test_route + "/app-android-descuentos/descuentos/src/main/java/cl/bci/app/descuentos/data" \
                                 "/datasource/remote"
        file = 'DescuentosDataSourceRemote.java'
        file_path = '{}/{}'.format(root, file)
        content = get_file_content(file_path)
        field = ''

        # when
        result = self.sut.get_method_which_use_field(content, field)

        # then
        self.assertTrue(result)

    def test_get_method_which_use_field_alcancia(self):
        # given
        root = self.test_route + '/app-android-alcanciadigital/alcanciadigital/src/main/java/' \
                                 'cl/bci/app/alcanciadigital/data/datasource/remote'
        file = 'MetasDataSourceRemote.java'
        file_path = '{}/{}'.format(root, file)
        content = get_file_content(file_path)
        field = 'msVersion'

        # when
        result = self.sut.get_method_which_use_field(content, field)

        # then
        self.assertTrue(result)

    def test_given_IrisDatasource_when_process_file_then_should_not_return_no_encontrado(self):
        # given
        root = self.test_route + '/app-android-pfm/pfm/src/main/java/cl/bci/app/pfm/data/datasource/remote/'
        file = 'ConfiguracionesIrisDataSourceRemote.kt'
        file_path = '{}/{}'.format(root, file)

        # when
        result = self.sut.process_file(root, file)

        # then
        self.assertTrue(result)

    def test_get_method_which_use_field_alcancia(self):
        # given
        root = self.test_route + '/app-android-alcanciadigital'
        constants = NetworkModuleConstantsReader(self.test_route).read

        # when
        result = MsVersionFieldReader(root, constants).read()
        [print(item) for item in result]

    def test_read_tarjetacredito(self):
        # given
        root = self.test_route + '/app-android-tarjetacredito'
        constants = NetworkModuleConstantsReader(self.test_route).read

        # when
        result = MsVersionFieldReader(root, constants).read()
        [print(item) for item in result]

        # then
        self.assertTrue(result)



