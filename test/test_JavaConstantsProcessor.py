from unittest import TestCase

from cl.bci.falcon.constantreader.java.JavaConstantsProcessor import JavaConstantsProcessor


class TestJavaConstantsProcessor(TestCase):

    def test_process(self):
        file_path = '/Users/nisum/Work/Bci/CoE/Script/repos/app-android-notificaciones/notificaciones/src/main/java' \
                    '/cl/bci/app/notificaciones/commons/util/ConstantsNotificaciones.java'
        constant = JavaConstantsProcessor().process(file_path)
        print(constant)

    def test_process(self):
        file_path = '/Users/nisum/Work/Bci/CoE/Script/repos/app-android-consumo/consumo/src/main/java/cl/bci/app' \
                    '/consumo/commons/utils/Constants.java'

        constant = JavaConstantsProcessor().process(file_path)
        [print(item) for item in constant.items()]
