from unittest import TestCase

from cl.bci.falcon.utils.StaticImportResolver import StaticImportResolver
from cl.bci.falcon.utils.fileutils import get_file_content


class TestStaticImportResolver(TestCase):


    def test_given_MS_VERSION_content_from_chat_when_resolve_then_should_resolve_complete_constant(self):
        file_path = '/Users/nisum/Work/Bci/CoE/Script/repos/app-android-chat/chat/src/main/java/cl/bci/app/chat/data/datasource/remote/DataSourceRemote.kt'
        content = get_file_content(file_path)
        constant = 'MS_VERSION'

        sut = StaticImportResolver(content)
        result = sut.resolve(constant)

        self.assertTrue(result.startswith('cl.bci'))

    def test_given_MS_VERSION_content_from_chat_when_resolve_then_should_resolve_complete_constant(self):
        file_path = '/Users/nisum/Work/Bci/CoE/Script/repos/app-android-tarjetacredito/tarjetacredito/src/main/java' \
                    '/cl/bci/app/tarjetacredito/data/api/TarjetaDebitoApi.kt'
        content = get_file_content(file_path)
        constant = 'Constants.API.PATH_DEBITO'

        sut = StaticImportResolver(content)
        result = sut.resolve(constant)

        self.assertTrue(result.startswith('cl.bci'))

    def test_given_CARTERA_DINAMICA_(self):
        file_path = '/Users/nisum/Work/Bci/CoE/Script/repos/app-android-inversiones/inversiones/src/main/java/cl/bci' \
                    '/app/inversiones/data/api/ResumenInversionesApi.kt'
        content = get_file_content(file_path)
        constant = 'CARTERA_DINAMICAS'

        sut = StaticImportResolver(content)
        result = sut.resolve(constant)

        self.assertTrue(result.startswith('cl.bci'))

