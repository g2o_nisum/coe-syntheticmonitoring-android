from unittest import TestCase

from cl.bci.falcon.TagInfo import TagInfo
from cl.bci.falcon.VersionExtractor import VersionExtractor


class TestVersionExtractor(TestCase):

    def test_extract(self):
        route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        tag_info = TagInfo(route, '4.39.0')
        module_list = VersionExtractor(tag_info).extract()
        resultCrmFeed = self.find_module_in_list(module_list, 'crm-feed')
        resultEntrust = self.find_module_in_list(module_list, 'entrust')

        self.assertTrue(resultCrmFeed)
        self.assertTrue(resultEntrust)

    def test_find_main_applications_value(self):
        route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        tag_info = TagInfo(route, '4.39.0')
        module_list = VersionExtractor(tag_info).extract()
        print(module_list)

    def find_module_in_list(self, module_list, modulo):
        return any(d['artifact'] == modulo for d in module_list)


