from unittest import TestCase

from cl.bci.falcon.MonitoringVersionDownloader import MonitoringVersionDownloader
from cl.bci.falcon.TagInfo import TagInfo


class TestMonitoringVersionDownloader(TestCase):

    def test_prepare_android_info_from_module_list(self):
        # given
        route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        tag_info = TagInfo(route)
        sut = MonitoringVersionDownloader(tag_info)

        expected_artifact = 'tarjetavirtual'
        module_list = [{'group': 'cl.bci.app.android.operacionesyejecucion.tarjetas', 'artifact': expected_artifact,
                        'version': '1.4.1'}]

        # when
        android_info = sut.prepare_android_info_from_module_list(module_list)

        # then
        self.assertTrue(android_info.repository[expected_artifact])