from unittest import TestCase

from cl.bci.falcon.constantreader.java.ConstantsVisitor import ConstantsVisitor


class TestConstantsVisitor(TestCase):

    def test_resolve_constant_value_cuenta(self):
        constants = {
            'ConstantsCuentas.API.AUTENCACION_EXP_PATH': '"ms-autenticacion-personasmobile-exp"',
            'ConstantsCuentas.API.AUTENTICACION_VERSION': '"version"'
        }
        value = '"{"+AUTENTICACION_VERSION+"}"'
        ConstantsVisitor('test').resolve_constant_value(value, constants)

    # ../app-android-consumo/consumo/src/main/java/cl/bci/app/consumo/commons/utils/Constants.java
    def test_resolve_constant_value_consumo(self):
        constants = {
            'Constants.API.PATH': '"ms-creditoconsumo-personasmobile-exp"',
            'Constants.API.CONSUMO_BASE_URL': 'PATH + PATH_VERSION + "/consumo/"'
        }
        value = 'PATH + PATH_VERSION + "/consumo/"'

        visitor = ConstantsVisitor('test')
        visitor.set_constante(constants)
        result = visitor.resolve_constant_value(value)
        print(result)
        self.assertTrue(result)