from unittest import TestCase

from cl.bci.falcon.VersionDownloader import VersionDownloader


class TestVersionDownloader(TestCase):
    def test_check_branch(self):
        sut = VersionDownloader(None)
        path = "/Users/nisum/Work/Bci/CoE/Script/repos/tag"
        sut.check_branch(path, "5.0.1")

    def test_version_match(self):
        version_number = '5.0.1'
        hyphen_version_number = version_number.replace('.', '-')
        v_version = "v{}".format(hyphen_version_number)
        tag_name = "v4-32-1-inv"
        result = tag_name.endswith(version_number) or tag_name.endswith(hyphen_version_number) or (
                    v_version in tag_name)
        print(result)

    def test_given_consumo_repo_when_check_branch_then_should_return(self):
        sut = VersionDownloader(None)
        path = "/Users/nisum/Work/Bci/CoE/Script/repos/app-android-consumo"
        sut.check_branch(path, '1.6.5')

    def test_given_consumo_cuentas_when_check_branch_then_should_return(self):
        sut = VersionDownloader(None)
        path = "/Users/nisum/Work/Bci/CoE/Script/repos/app-android-cuentas"
        sut.check_branch(path, '1.9.3')
