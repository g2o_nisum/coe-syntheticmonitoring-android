from unittest import TestCase

from cl.bci.falcon.constantreader.BuildConfigConstantsReader import BuildConfigConstantsReader
from cl.bci.falcon.utils.fileutils import get_file_content


class TestBuildConfigConstantsReader(TestCase):

    def setUp(self):
        self.test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        self.sut = BuildConfigConstantsReader(self.test_route)

    def test_read(self):
        result = self.sut.read()
        self.assertIsNotNone(result)

    def test_find_declared_value(self):
        build_type_path = '{}/tag/app/buildTypes.gradle'.format(self.test_route)
        message = get_file_content(build_type_path)
        result = self.sut.find_value_declared_in_build_config('ms_onoff', message)
        print(result)
