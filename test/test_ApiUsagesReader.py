from unittest import TestCase

from cl.bci.falcon.apiparser.ApiUsagesReader import ApiUsagesReader
from cl.bci.falcon.apiparser.RestResourceParser import ApiDTO, RestResourceDTO


class TestApiUsagesReader(TestCase):

    def test_tarjeta_credito(self):
        # GIVEN ms_version field is provided in the same datasourceremote
        url = 'sample/{version}/test'
        version = '/v1.0.0'
        expectedUrl = url.replace('{version}', version)
        get_beneficios_resource = RestResourceDTO(tipo='POST', method_name='getBeneficios', url=url, line='')
        beneficios_api = ApiDTO(api='BeneficiosApi', file_path='sample/path')
        beneficios_api.resource_list = [get_beneficios_resource]
        rest_resource = [beneficios_api]

        root = self.get_root()
        file = 'BeneficiosDataSourceRemote.java'

        ms_version_field = [{'filename': file, 'constant': version, 'field': 'msVersion',
                             'field_name_getter': 'getMsVersion'},
                            {'filename': 'DataSourceRemote.java', 'constant': '/v1.1', 'field': 'msVersionSimulacion',
                             'field_name_getter': 'getMsVersionSimulacion'},
                            {'filename': 'DataSourceRemote.java', 'constant': '/v1.0', 'field': 'msVersionDebito',
                             'field_name_getter': 'getMsVersionDebito'}]
        # WHEN process_file
        api_dto_list = ApiUsagesReader('', rest_resource, ms_version_field).process_file(root, file)

        # THEN should be '/v1.0.0'
        url = next(iter([item.url for item in api_dto_list[0].resource_list if item.method_name == 'getBeneficios']),
                   None)
        self.assertEqual(expectedUrl, url)

    def test_single_datasource_with_ms_version_fields(self):
        # GIVEN single datasource in modules with msversion field
        url = 'sampleUrl{version}beneficios'
        version = '/v1.0.0'
        expectedUrl = url.replace('{version}', version)

        get_beneficios_resource = RestResourceDTO(tipo='POST', url=url, line='',
                                                  method_name='getBeneficios')
        beneficios_api = ApiDTO(api='BeneficiosApi', file_path='')
        beneficios_api.resource_list = [get_beneficios_resource]
        rest_resource = [beneficios_api]

        ms_version_field = [{'filename': 'DataSourceRemote.java', 'constant': version, 'field': 'msVersion',
                             'field_name_getter': 'getMsVersion'}]

        root = self.get_root()
        file = 'FeatureToggleDataSource.java'

        # WHEN call for api usages
        api_dto_list = ApiUsagesReader('', rest_resource, ms_version_field).process_file(root, file)

        # THEN should return all url that needs ms_version wi
        url = next(iter([item.url for item in api_dto_list[0].resource_list if item.method_name == 'getBeneficios']),
                   None)
        self.assertEqual(expectedUrl, url)

    def get_root(self):
        workspace = '/Users/nisum/Work/Bci/CoE/Script/repos'
        module_path = 'app-android-tarjetacredito/tarjetacredito'
        package_path = 'src/main/java/cl/bci/app/tarjetacredito/data/datasource/remote'
        root = '%s/%s/%s' % (workspace, module_path, package_path)
        return root
