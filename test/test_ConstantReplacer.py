from unittest import TestCase

from cl.bci.falcon.utils.KtConstantReplacer import KtConstantReplacer


class TestConstantReplacer(TestCase):

    def test_replace(self):
        constante = {'API.VERSION': 'develop', 'API.COMPUESTO': '${VERSION}'}

        replace = KtConstantReplacer(constante).replace('${VERSION}')
