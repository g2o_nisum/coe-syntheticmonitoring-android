from unittest import TestCase

from cl.bci.falcon.constantreader.NetworkModuleConstantsReader import NetworkModuleConstantsReader


class TestNetworkModuleConstantsReader(TestCase):

    def test_NetworkModuleConstantsReader(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos/"
        sut = NetworkModuleConstantsReader(test_route)
        read = sut.read
        [print(f'{key}={value}') for key, value in read.items()]
