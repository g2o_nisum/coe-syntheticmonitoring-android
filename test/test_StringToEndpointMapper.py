from unittest import TestCase

from cl.bci.falcon.apiparser.RestResourceParser import StringToEndpointMapper


class TestStringToEndpointMapper(TestCase):

    def test_map(self):
        expected = {'tipo': 'POST', 'url': 'metas/ms_metas/ms_version/monto'}
        content = "import cl.bci.app.Constant.PATH_METAS;\nimport cl.bci.app.Constant.MS_MIS_METAS;\n" \
                  "import cl.bci.app.Constant.PATH_VERSION;\nimport cl.bci.app.Constant.MONTO_AHORRO;\n"

        module_constants = {
            'cl.bci.app.Constant.PATH_METAS': 'metas/',
            'cl.bci.app.Constant.MS_MIS_METAS': 'ms_metas/',
            'cl.bci.app.Constant.PATH_VERSION': 'ms_version/',
            'cl.bci.app.Constant.MONTO_AHORRO': 'monto'
        }

        sample = '@POST(PATH_METAS + MS_MIS_METAS + PATH_VERSION + MONTO_AHORRO)'
        sut = StringToEndpointMapper(content, module_constants)
        result = sut.map(sample)

        self.assertEqual(expected, result)
