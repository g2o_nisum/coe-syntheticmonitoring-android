from unittest import TestCase

from cl.bci.falcon.AndroidInfo import AndroidInfo
from cl.bci.falcon.ApplicationRestEndpointFinder import ApplicationRestEndpointFinder


class TestApplicationRestEndpointFinder(TestCase):

    def setUp(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        android_info = AndroidInfo(test_route)
        self.sut = ApplicationRestEndpointFinder(android_info)

    def test_read_network_module(self):
        result = self.sut.find()

    def test_process_module_inversiones(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        info = AndroidInfo(test_route)
        key = 'inversiones'
        repository = info.repository_info[key]
        result = self.sut.process_module(key, repository)

    def test_process_module_chat(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        info = AndroidInfo(test_route)
        key = 'chat'
        repository = info.repository_info[key]
        result = self.sut.process_module(key, repository)

    def test_process_module_notificaciones(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        info = AndroidInfo(test_route)
        key = 'notificaciones'
        repository = info.repository_info[key]
        result = self.sut.process_module(key, repository)
        print(result)

    def test_process_module_commons(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        info = AndroidInfo(test_route)
        key = 'commons'

        repository = info.repository_info[key]
        result = self.sut.process_module(key, repository)
        print(result)

    def test_process_module_tarjetacredito(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos"
        info = AndroidInfo(test_route)
        key = 'tarjetacredito'

        repository = info.repository_info[key]
        result = self.sut.process_module(key, repository)
        print(result)





