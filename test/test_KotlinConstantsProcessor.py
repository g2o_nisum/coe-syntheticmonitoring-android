from unittest import TestCase

from cl.bci.falcon.constantreader.kt.KotlinConstantsProcessor import KotlinConstantsProcessor


class TestKotlinConstantsProcessor(TestCase):

    def test_process_in_chat(self):
        file_path = '/Users/nisum/Work/Bci/CoE/Script/repos/app-android-chat/chat/src/main/java/cl/bci/app/chat' \
                    '/commons/utils/Constants.kt'

        process = KotlinConstantsProcessor().process(file_path)
        [print('%s : %s'%(item, process[item])) for item in process]
