from unittest import TestCase

from cl.bci.falcon.constantreader.ModuleConstantReader import ModuleConstantReader


class TestModuleConstantReader(TestCase):

    def setUp(self):
        self.test_route = "/Users/nisum/Work/Bci/CoE/Script/repos/app-android-descuentos"
        self.sut = ModuleConstantReader(self.test_route, network_module_constant)

    def test_read_module_constant(self):
        result = self.sut.read()
        print(result)

    def test_read_network_module(self):
        content = """
            class Constants
            {
                class API
                {
                    companion object
                    {
                        const val MS_VERSION = "version_ms_chat"
                        const val MS_PP_VERSION_CHAT= "version_ms_pp_chat"
                        const val CHAT_CLIENTE = "chatCliente"
                        const val VERSION = "develop"
                        const val PATH = "ventas-y-servicios/canal-transversal/ms-chatatencionclientes-neg/"
                        const val PATH_VERSION = "{$VERSION}"
                        const val PATH_PLAN_PILOTO = "ventas-y-servicios/gestion-cliente/ms-planpilotoclientes-util/"
                    }
            
                    class CHAT
                    {
                        companion object
                        {
                            const val LOGIN = "/login"
                            const val VALIDAR_PILOTO = "/ValidarPiloto"
                        }
                    }
                }
            }
        """
        # tree = self.sut.find_constant_list(content)
        self.assertTrue(content)

    def test_resolve_full_constant_path(self):
        content = """
               class Constants
               {
                   class API
                   {
                       companion object
                       {
                           const val MS_VERSION = "version_ms_chat"
                           const val MS_PP_VERSION_CHAT= "version_ms_pp_chat"
                           const val CHAT_CLIENTE = "chatCliente"
                           const val VERSION = "develop"
                           const val PATH = "ventas-y-servicios/canal-transversal/ms-chatatencionclientes-neg/"
                           const val PATH_VERSION = "{$VERSION}"
                           const val PATH_PLAN_PILOTO = "ventas-y-servicios/gestion-cliente/ms-planpilotoclientes-util/"
                       }

                       class CHAT
                       {
                           companion object
                           {
                               const val LOGIN = "/login"
                               const val VALIDAR_PILOTO = "/ValidarPiloto"
                           }
                       }
                   }
               }
           """
        # tree = self.sut.find_constant_list(content)
        self.assertTrue(content)

    def test_chat(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos/app-android-chat"
        sut = ModuleConstantReader(test_route, network_module_constant)
        read = sut.read()
        print(read)

    def test_notificaciones(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos/app-android-notificaciones"
        sut = ModuleConstantReader(test_route, network_module_constant)
        read = sut.read()
        print(read)

    def test_pagos(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos/app-android-pagos"
        sut = ModuleConstantReader(test_route, network_module_constant)
        read = sut.read()
        print(read)

    def test_full(self):
        test_route = "/Users/nisum/Work/Bci/CoE/Script/repos/"
        sut = ModuleConstantReader(test_route, network_module_constant)
        read = sut.read()
        [print(f'{key}={value}') for key, value in read.items()]
